# Description

*En tant que [nom du personnage]( https://gitlab.libriciel.fr/i-parapheur/components/ip-web/wikis/personae#installateur)* ...

*Je veux* ...

*Afin de* ...

# Règles métiers

 * Règle 1
 * Règle 2
 * ...

# Tests d'acceptance

*Jeu de données*

**Nom du scénario 1**

*Étant donné que* ...

*Lorsque* ...

*Alors* ...

**Nom du scénario 2**

*Étant donné que* ...

*Lorsque* ...

*Alors* ...

# Maquettes

* [Nom du fichier](https://le_lien_en_question.fr)

-----------------
# Liens

## Tâche parente : #1
## Sous-tâches : #1, #2
## Bloqué par : #1, #2
## Bloque : #1, #2
## Lié à : #1
## Autres références

/label ~"User Stories"
/milestone %backlog
