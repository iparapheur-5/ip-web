# Contributing


### Keycloak conf

Check in http://localhost/auth :  
- `Client` → `ipcore-web` → `Direct Access Grants Enabled` : `✓`


### Starting project locally
 
Debug mode, without cross-platform check :
```bash
$ ng serve --host 0.0.0.0 --allowed-hosts iparapheur.dom.local
```

Or closer to the final build :
```bash
$ ng serve --prod --optimization=false --host 0.0.0.0 --allowed-hosts iparapheur.dom.local
```

Standalone dev mode, with mocked backend :
```bash
$ ng serve --configuration=debug
```


### Code style

The `.idea` folder has been committed. It should bring the appropriate code-style.  
Obviously, every modified file should be auto-formatted before any git push.  
No hook would reject a mis-formatted file for now... But if everything goes bananas, we may set it up.

If you are not an IntelliJ user, a `.editorconfig` file is also available, that should be recognized by most code editors,
 and contains ALL the rules used to enforce the code style of this project.
