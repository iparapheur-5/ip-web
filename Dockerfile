#
# iparapheur Web
# Copyright (C) 2019-2023 Libriciel SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

FROM hubdocker.libriciel.fr/nginx:1.24.0-bullseye

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ip-web/ /usr/share/nginx/html/
COPY src/scripts/ /usr/share/scripts

RUN rm /etc/nginx/conf.d/default.conf
COPY docker/nginx/* /etc/nginx/conf.d/

CMD sh /usr/share/scripts/setenv.sh  /usr/share/nginx/html/assets && nginx -g 'daemon off;'
