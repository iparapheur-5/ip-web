/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, LOCALE_ID, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { AboutComponent } from './components/settings/about/about.component';
import { DeskListComponent } from './components/main/desk-list/desk-list.component';
import { TaskListComponent } from './components/main/tasks/task-list/task-list.component';
import { initializer, getApplicationUrl } from './app-init';
import { CommonModule, registerLocaleData, DatePipe } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import * as dayjs from 'dayjs';
import * as fr from 'dayjs/locale/fr';
import * as isBetween from 'dayjs/plugin/isBetween';
import { FolderMainLayoutComponent } from './components/main/folder-view/folder-main-layout/folder-main-layout.component';
import { ActionListComponent } from './components/main/folder-view/action-list/action-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelectModule } from '@ng-select/ng-select';
import { WorkflowEditorComponent } from './components/admin/admin-sub-panels/workflow-editor/workflow-editor-main-layout/workflow-editor.component';
import { StepEditorComponent } from './components/admin/admin-sub-panels/workflow-editor/step-editor/step-editor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminTenantUserListComponent } from './components/admin/admin-sub-panels/tenant-users/admin-tenant-user-list/admin-tenant-user-list.component';
import { WorkflowListComponent } from './components/admin/admin-sub-panels/workflow-list/workflow-list.component';
import { AdminDeskListComponent } from './components/admin/admin-sub-panels/desk/admin-desk-list/admin-desk-list.component';
import { TimesPipe } from './utils/times.pipe';
import { AdminDeskPopupComponent } from './components/admin/admin-sub-panels/desk/admin-desk-popup/admin-desk-popup.component';
import { SimpleActionPopupComponent } from './components/main/folder-view/action-popups/simple-action-popup/simple-action-popup.component';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { AnnotationPopupComponent } from './components/main/folder-view/annotation-popup/annotation-popup.component';
import { ImportWorkflowPopupComponent } from './components/admin/admin-sub-panels/import-workflow-popup/import-workflow-popup.component';
import { LsWorkflowsModule } from '@libriciel/ls-composants/workflows';
import { ChunkPipe } from './utils/chunk.pipe';
import { AdminFolderListComponent } from './components/admin/admin-sub-panels/admin-folder-list/admin-folder-list.component';
import { AdminMainLayoutComponent } from './components/admin/admin-main-layout/admin-main-layout.component';
import { HeaderComponent } from './components/header/header.component';
import { DocumentListComponent } from './components/main/folder-view/document-list/document-list.component';
import { WorkflowInstanceSnapshotComponent } from './components/main/folder-view/workflow-instance-snapshot/workflow-instance-snapshot.component';
import { TypologyListComponent } from './components/admin/admin-sub-panels/typology/typology-list/typology-list.component';
import { TypologyTypePopupComponent } from './components/admin/admin-sub-panels/typology/typology-type-popup/typology-type-popup.component';
import { TypologySubtypePopupComponent } from './components/admin/admin-sub-panels/typology/typology-subtype-popup/typology-subtype-popup.component';
import { DraftFieldsComponent } from './components/main/folder-view/draft-fields/draft-fields.component';
import { SafePipe } from './utils/safe.pipe';
import { AdminTenantListComponent } from './components/admin/admin-sub-panels/tenants/admin-tenant-list/admin-tenant-list.component';
import { AdminTenantPopupComponent } from './components/admin/admin-sub-panels/tenants/admin-tenant-popup/admin-tenant-popup.component';
import { AdminUserEditPopupComponent } from './components/admin/admin-sub-panels/user-popup/admin-user-edit-popup/admin-user-edit-popup.component';
import { AdminTenantUserInfosComponent } from './components/admin/admin-sub-panels/user-popup/subpanels/admin-user-infos/admin-tenant-user-infos.component';
import { UserEditPasswordComponent } from './components/main/users/user-edit-page-subpanels/user-edit-password/user-edit-password.component';
import { UserEditNotificationsComponent } from './components/main/users/user-edit-page-subpanels/user-edit-notifications/user-edit-notifications.component';
import { EditSignatureComponent } from './components/main/users/user-edit-page-subpanels/user-edit-signature/edit-signature.component';
import { AdminUserPrivilegesComponent } from './components/admin/admin-sub-panels/user-popup/subpanels/admin-user-privileges/admin-user-privileges.component';
import { AdminTenantUserDesksComponent } from './components/admin/admin-sub-panels/user-popup/subpanels/admin-user-desks/admin-tenant-user-desks.component';
import { ToastrModule } from 'ngx-toastr';
import { SplitIconNamePipe } from './utils/split-icon-name.pipe';
import { GlobalErrorPopupComponent } from './components/header/global-error-popup/global-error-popup.component';
import { TrashBinListComponent } from './components/main/trash-bin-list/trash-bin-list.component';
import { ProfilePageComponent } from './components/main/users/profile-page/profile-page.component';
import { AdvancedAdminMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-main-layout/advanced-admin-main-layout.component';
import { SealCertificatesListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/seal-certificates/seal-certificates-list/seal-certificates-list.component';
import { MetadataListComponent } from './components/main/folder-view/metadata-list/metadata-list.component';
import { AdminMetadataListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/metadata/admin-metadata-list/admin-metadata-list.component';
import { AdvancedConfigMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/advanced-config-main-layout/advanced-config-main-layout.component';
import { ConnectorsMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-main-layout/connectors-main-layout.component';
import { ExternalSignatureConnectorsComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-sub-panels/external-signature/external-signature-connectors/external-signature-connectors.component';
import { EditMetadataPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/metadata/edit-metadata-popup/edit-metadata-popup.component';
import { AddSealCertificatePopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/seal-certificates/add-seal-certificate-popup/add-seal-certificate-popup.component';
import { SubtypeGeneralComponent } from './components/admin/admin-sub-panels/typology/typology-subtype-popup-subpanels/subtype-general/subtype-general.component';
import { SubtypePermissionsComponent } from './components/admin/admin-sub-panels/typology/typology-subtype-popup-subpanels/subtype-permissions/subtype-permissions.component';
import { SubtypeVisibilityComponent } from './components/admin/admin-sub-panels/typology/typology-subtype-popup-subpanels/subtype-visibility/subtype-visibility.component';
import { SubtypeWorkflowComponent } from './components/admin/admin-sub-panels/typology/typology-subtype-popup-subpanels/subtype-workflow/subtype-workflow.component';
import { SubtypeMetadataComponent } from './components/admin/admin-sub-panels/typology/typology-subtype-popup-subpanels/subtype-metadata/subtype-metadata.component';
import { SubtypeLayersComponent } from './components/admin/admin-sub-panels/typology/typology-subtype-popup-subpanels/subtype-layers/subtype-layers.component';
import { TypeGeneralComponent } from './components/admin/admin-sub-panels/typology/typology-type-popup-subpanels/type-general/type-general.component';
import { TypeProtocolComponent } from './components/admin/admin-sub-panels/typology/typology-type-popup-subpanels/type-protocol/type-protocol.component';
import { TypeSignatureStampComponent } from './components/admin/admin-sub-panels/typology/typology-type-popup-subpanels/type-signature-stamp/type-signature-stamp.component';
import { AdminTenantUserCreatePopupComponent } from './components/admin/admin-sub-panels/tenant-users/admin-tenant-user-create-popup/admin-tenant-user-create-popup.component';
import { AdminTenantUserMainCreationInfosComponent } from './components/admin/admin-sub-panels/user-popup/subpanels/admin-user-main-creation-infos/admin-tenant-user-main-creation-infos.component';
import { UserPasswordDoubledInputComponent } from './components/main/users/user-edit-page-subpanels/user-password-doubled-input/user-password-doubled-input.component';
import { UserFavoriteDesksComponent } from './components/main/users/user-edit-page-subpanels/user-favorite-desks/user-favorite-desks.component';
import { CreateFolderPageComponent } from './components/main/folder-creation/create-folder-page/create-folder-page.component';
import { GeneralDeskFormComponent } from './components/admin/admin-sub-panels/desk/forms/general/general-desk-form.component';
import { OwnersDeskFormComponent } from './components/admin/admin-sub-panels/desk/forms/owners/owners-desk-form.component';
import { AddExternalSignatureConnectorPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-sub-panels/external-signature/add-external-signature-connector/add-external-signature-connector-popup.component';
import { SecureMailConnectorsComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-sub-panels/secure-mail/secure-mail-connectors/secure-mail-connectors.component';
import { AddSecureMailConnectorComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-sub-panels/secure-mail/add-secure-mail-connector/add-secure-mail-connector.component';
import { ExternalStatePipe } from './utils/external-signature-state.pipe';
import { MetadataInputComponent } from './shared/components/metadata-input/metadata-input.component';
import { MonacoEditorComponent } from './shared/components/monaco-editor/monaco-editor.component';
import { ChainPopupComponent } from './components/main/folder-view/action-popups/chain-popup/chain-popup.component';
import { StatsComponent } from './components/main/stats/stats.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PtToMmPipe } from './utils/pt-to-mm.pipe';
import { SigningFormatNamePipe } from './utils/signing-format-name.pipe';
import { SignatureProtocolNamePipe } from './utils/signature-protocol-name.pipe';
import { IpngFormComponent } from './components/main/ipng/ipng-form/ipng-form.component';
import { IpngMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-main-layout/ipng-main-layout.component';
import { IpngHealthStateComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/ipng-health-state/ipng-health-state.component';
import { IpngTypologyComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/typology/ipng-typology/ipng-typology.component';
import { TemplatesListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/templates/templates-list/templates-list.component';
import { TemplatesEditPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/templates/templates-edit-popup/templates-edit-popup.component';
import { TemplateTypeNamePipe } from './utils/template-type-name.pipe';
import { SendByMailPopupComponent } from './components/main/folder-view/action-popups/mail/send-by-mail-popup/send-by-mail-popup.component';
import { GlobalHttpInterceptorService } from './services/interceptors/global-http-interceptor.service';
import { ServerDataMainLayoutComponent } from './components/admin/admin-sub-panels/server-data/server-data-main-layout/server-data-main-layout.component';
import { JobGraphDisplayComponent } from './components/admin/admin-sub-panels/server-data/job-graph-display/job-graph-display.component';
import { JobGraphListComponent } from './components/admin/admin-sub-panels/server-data/job-graph-list/job-graph-list.component';
import { EditSealCertificatePopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/seal-certificates/edit-seal-certificate-popup/edit-seal-certificate-popup.component';
import { PrintPopupComponent } from './components/main/folder-view/action-popups/print-popup/print-popup.component';
import { LayersListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/layers/layers-list/layers-list.component';
import { EditLayerPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/layers/edit-layer-popup/edit-layer-popup.component';
import { EditLayerStampListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/layers/edit-layer-popup-sub-panels/edit-layer-stamp-list/edit-layer-stamp-list.component';
import { EditLayerGeneralComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/layers/edit-layer-popup-sub-panels/edit-layer-general/edit-layer-general.component';
import { PdfExampleComponent } from './shared/components/pdf-example/pdf-example.component';
import { StampTypeIconPipe } from './utils/stamp-type-icon.pipe';
import { IpngDeskboxNamePipe } from './utils/ipng-deskbox-name.pipe';
import { IpngEntityNamePipe } from './utils/ipng-entity-name.pipe';
import { MultipleActionListComponent } from './components/main/tasks/multiple-action-list/multiple-action-list.component';
import { AdminTenantDeletePopupComponent } from './components/admin/admin-sub-panels/tenants/admin-tenant-delete-popup/admin-tenant-delete-popup.component';
import { UserTenantsEditComponent } from './components/admin/admin-sub-panels/user-popup/subpanels/admin-user-tenants/user-tenants-edit.component';
import { AllUsersDeletePopupComponent } from './components/admin/admin-sub-panels/all-users/all-users-delete-popup/all-users-delete-popup.component';
import { DragAndDropDirective } from './directives/drag-and-drop.directive';
import { SubtypeLayerAssociationNamePipe } from './utils/subtype-layer-association-name.pipe';
import { BooleanNamePipe } from './utils/boolean-name.pipe';
import { AdminAllUsersComponent } from './components/admin/admin-sub-panels/all-users/all-users-list/admin-all-users.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { AbsenceManagementComponent } from './components/admin/admin-sub-panels/absences/absence-management/absence-management.component';
import { AbsenceListComponent } from './components/admin/admin-sub-panels/absences/absence-list/absence-list.component';
import { AbsencePopupComponent } from './components/admin/admin-sub-panels/absences/absence-popup/absence-popup.component';
import { ExternalSignaturePopupComponent } from './components/main/folder-view/action-popups/external-signature-popup/external-signature-popup.component';
import { AuthorisationsComponent } from './components/admin/admin-sub-panels/desk/forms/authorisations/authorisations.component';
import { IpngDeskboxesComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/deskboxes/ipng-deskboxes/ipng-deskboxes.component';
import { IpngMetadataComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/metadata/ipng-metadata/ipng-metadata.component';
import { AddDeskboxPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/deskboxes/add-deskbox-popup/add-deskbox-popup.component';
import { LinkDeskboxPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/deskboxes/link-deskbox-popup/link-deskbox-popup.component';
import { IpngTypologyNamePipe } from './utils/ipng-typology-name.pipe';
import { AddTypologyLinkPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/typology/add-typology-link-popup/add-typology-link-popup.component';
import { RemoveTypologyLinkPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/typology/remove-typology-link-popup/remove-typology-link-popup.component';
import { AddMetadataLinkPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/metadata/add-metadata-link-popup/add-metadata-link-popup.component';
import { RemoveMetadataLinkPopupComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/metadata/remove-metadata-link-popup/remove-metadata-link-popup.component';
import { IpngMetadataNamePipe } from './utils/ipng-metadata-name.pipe';
import { IpngDeskboxDescriptionPipe } from './utils/ipng-deskbox-description.pipe';
import { AssociatesComponent } from './components/admin/admin-sub-panels/desk/forms/associates/associates.component';
import { DeskMetadataComponent } from './components/admin/admin-sub-panels/desk/forms/desk-metadata/desk-metadata.component';
import { TargetDeskActionPopupComponent } from './components/main/folder-view/action-popups/target-desk-action-popup/target-desk-action-popup.component';
import { GlobalSearchBarComponent } from './components/header/global-search-bar/global-search-bar.component';
import { StepStateIconPipe } from './utils/step-state-icon.pipe';
import { FolderViewPreferencesComponent } from './components/main/users/user-edit-page-subpanels/folder-view-preferences/folder-view-preferences.component';
import { FolderViewBlockNamePipe } from './utils/folder-view-block-name.pipe';
import { UserDashboardComponent } from './components/main/users/user-edit-page-subpanels/user-dashboard/user-dashboard.component';
import { FolderSortByPipe } from './utils/folder-sort-by.pipe';
import { ColumnNamePipe } from './utils/column-name.pipe';
import { DocumentIconPipe } from './utils/document-icon.pipe';
import { AdminUserSupervisingComponent } from './components/admin/admin-sub-panels/user-popup/subpanels/admin-user-supervising/admin-user-supervising.component';
import { TrashBinViewColumnsComponent } from './components/main/users/user-edit-page-subpanels/user-trash-bin-view-columns/trash-bin-view-columns.component';
import { GdprComponent } from './components/main/gdpr/gdpr.component';
import { SaveFilterPopupComponent } from './shared/components/folder-filter-panel/save-filter-popup/save-filter-popup.component';
import { UserDebugComponent } from './components/main/users/user-edit-page-subpanels/user-debug/user-debug.component';
import { SharedModule } from './shared/shared.module';
import { HistoryPopupComponent } from './components/main/history-popup/history-popup.component';
import { AdminAbsencesMenuComponent } from './components/admin/admin-sub-panels/absences/admin-absences-menu/admin-absences-menu.component';
import { AnnotationDisplayComponent } from './components/main/folder-view/annotation-display/annotation-display.component';
import { LiberSignCheckComponent } from './components/settings/liber-sign-check/liber-sign-check.component';
import { LiberSignHelpPopupComponent } from './components/settings/liber-sign-help-popup/liber-sign-help-popup.component';
import { MetadataTypeNamePipe } from './utils/metadata-type-name.pipe';
import { StepMetadataListComponent } from './components/main/folder-view/step-metadata-list/step-metadata-list.component';
import { SecureMailStatusPopupComponent } from './components/main/folder-view/action-popups/mail/secure-mail-status-popup/secure-mail-status-popup.component';
import { MetadataFormComponent } from './components/main/folder-view/metadata-form/metadata-form.component';
import { PrivilegeIconPipe } from './utils/privilege-icon.pipe';
import { PrivilegeNamePipe } from './utils/privilege-name.pipe';
import { ServiceNamePipe } from './components/admin/admin-sub-panels/server-data/service-name.pipe';
import { FolderVisibilityNamePipe } from './utils/folder-visibility-name.pipe';
import { SupervisorsDeskFormComponent } from './components/admin/admin-sub-panels/desk/forms/supervisors/supervisors-desk-form.component';
import { DelegationManagerDeskFormComponent } from './components/admin/admin-sub-panels/desk/forms/delegation-manager/delegation-manager-desk-form.component';
import { AdminUserDelegationManagedComponent } from './components/admin/admin-sub-panels/user-popup/subpanels/admin-user-delegation-managered/admin-user-delegation-managed.component';
import { AddDesksToNotifyPopupComponent } from './components/main/folder-view/action-popups/add-desks-to-notify-popup/add-desks-to-notify-popup.component';
import { FirstLoginNotificationsPopupComponent } from './components/settings/first-login-notifications-popup/first-login-notifications-popup.component';
import { CreateAndStartFolderPopupComponent } from './components/main/folder-creation/create-and-start-folder-popup/create-and-start-folder-popup.component';
import { IpngProofDisplayPopupComponent } from './components/main/ipng/ipng-proof-display-popup/ipng-proof-display-popup.component';
import { BASE_PATH as STD_BASE_PATH } from '@libriciel/iparapheur-standard';
import { BASE_PATH as PROVISIONNING_BASE_PATH } from '@libriciel/iparapheur-provisioning';
import { BASE_PATH as INTERNAL_BASE_PATH } from '@libriciel/iparapheur-internal';
import { StampTypeNamePipe } from './utils/stamp-type-name.pipe';
import { MetadataNamePipe } from './utils/metadata-name.pipe';
import { InternalMetadataKeyPipe } from './utils/internal-metadata-key.pipe';
import { ColumToFolderSortByPipe } from './utils/colum-to-folder-sort-by.pipe';
import { FortifyCertificatesSelectorComponent } from './components/main/folder-view/action-popups/fortify-certificates-selector/fortify-certificates-selector.component';
import { ExternalSignatureProviderPipe } from './utils/external-signature-provider.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


registerLocaleData(localeFr, 'fr');

dayjs.extend(isBetween);

dayjs.locale(fr);

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    DeskListComponent,
    TaskListComponent,
    FolderMainLayoutComponent,
    WorkflowEditorComponent,
    StepEditorComponent,
    ActionListComponent,
    AdminTenantUserListComponent,
    WorkflowListComponent,
    AdminDeskListComponent,
    TimesPipe,
    AdminDeskPopupComponent,
    SimpleActionPopupComponent,
    AnnotationPopupComponent,
    ImportWorkflowPopupComponent,
    ChunkPipe,
    AdminFolderListComponent,
    AdminMainLayoutComponent,
    HeaderComponent,
    DocumentListComponent,
    WorkflowInstanceSnapshotComponent,
    TypologyListComponent,
    TypologyTypePopupComponent,
    TypologySubtypePopupComponent,
    DraftFieldsComponent,
    SafePipe,
    AdminTenantListComponent,
    AdminTenantPopupComponent,
    AdminUserEditPopupComponent,
    AdminTenantUserInfosComponent,
    UserEditPasswordComponent,
    UserEditNotificationsComponent,
    EditSignatureComponent,
    AdminUserPrivilegesComponent,
    AdminTenantUserDesksComponent,
    SplitIconNamePipe,
    GlobalErrorPopupComponent,
    TrashBinListComponent,
    ProfilePageComponent,
    AdvancedAdminMainLayoutComponent,
    SealCertificatesListComponent,
    AdminMetadataListComponent,
    AdvancedConfigMainLayoutComponent,
    ConnectorsMainLayoutComponent,
    ExternalSignatureConnectorsComponent,
    EditMetadataPopupComponent,
    AddSealCertificatePopupComponent,
    SubtypeGeneralComponent,
    SubtypePermissionsComponent,
    SubtypeVisibilityComponent,
    SubtypeWorkflowComponent,
    SubtypeMetadataComponent,
    SubtypeLayersComponent,
    TypeGeneralComponent,
    TypeProtocolComponent,
    TypeSignatureStampComponent,
    AdminTenantUserCreatePopupComponent,
    AdminTenantUserMainCreationInfosComponent,
    UserPasswordDoubledInputComponent,
    UserFavoriteDesksComponent,
    CreateFolderPageComponent,
    GeneralDeskFormComponent,
    OwnersDeskFormComponent,
    SecureMailConnectorsComponent,
    AddSecureMailConnectorComponent,
    ExternalStatePipe,
    StatsComponent,
    AddExternalSignatureConnectorPopupComponent,
    MetadataInputComponent,
    MetadataListComponent,
    MonacoEditorComponent,
    ChainPopupComponent,
    PtToMmPipe,
    SigningFormatNamePipe,
    SignatureProtocolNamePipe,
    IpngFormComponent,
    IpngMainLayoutComponent,
    IpngHealthStateComponent,
    IpngTypologyComponent,
    TemplatesListComponent,
    TemplatesEditPopupComponent,
    TemplateTypeNamePipe,
    SendByMailPopupComponent,
    ServerDataMainLayoutComponent,
    JobGraphDisplayComponent,
    JobGraphListComponent,
    EditSealCertificatePopupComponent,
    IpngDeskboxNamePipe,
    IpngEntityNamePipe,
    PrintPopupComponent,
    AdminTenantDeletePopupComponent,
    LayersListComponent,
    EditLayerPopupComponent,
    EditLayerStampListComponent,
    EditLayerGeneralComponent,
    PdfExampleComponent,
    StampTypeIconPipe,
    MultipleActionListComponent,
    DragAndDropDirective,
    AdminAllUsersComponent,
    UserTenantsEditComponent,
    AllUsersDeletePopupComponent,
    SubtypeLayerAssociationNamePipe,
    BooleanNamePipe,
    AbsenceManagementComponent,
    AbsenceListComponent,
    AbsencePopupComponent,
    ExternalSignaturePopupComponent,
    AuthorisationsComponent,
    IpngDeskboxesComponent,
    IpngMetadataComponent,
    AddDeskboxPopupComponent,
    LinkDeskboxPopupComponent,
    IpngTypologyNamePipe,
    AddTypologyLinkPopupComponent,
    RemoveTypologyLinkPopupComponent,
    AddMetadataLinkPopupComponent,
    RemoveMetadataLinkPopupComponent,
    IpngMetadataNamePipe,
    IpngDeskboxDescriptionPipe,
    GlobalSearchBarComponent,
    FolderViewPreferencesComponent,
    FolderViewBlockNamePipe,
    UserDashboardComponent,
    FolderSortByPipe,
    ColumnNamePipe,
    DocumentIconPipe,
    AssociatesComponent,
    DeskMetadataComponent,
    TargetDeskActionPopupComponent,
    GlobalSearchBarComponent,
    StepStateIconPipe,
    AdminUserSupervisingComponent,
    TrashBinViewColumnsComponent,
    SaveFilterPopupComponent,
    GdprComponent,
    UserDebugComponent,
    HistoryPopupComponent,
    AdminAbsencesMenuComponent,
    AnnotationDisplayComponent,
    LiberSignCheckComponent,
    AnnotationDisplayComponent,
    MetadataTypeNamePipe,
    LiberSignCheckComponent,
    LiberSignHelpPopupComponent,
    MetadataTypeNamePipe,
    StepMetadataListComponent,
    SecureMailStatusPopupComponent,
    MetadataFormComponent,
    PrivilegeIconPipe,
    PrivilegeNamePipe,
    ServiceNamePipe,
    FolderVisibilityNamePipe,
    SupervisorsDeskFormComponent,
    DelegationManagerDeskFormComponent,
    AdminUserDelegationManagedComponent,
    AddDesksToNotifyPopupComponent,
    FirstLoginNotificationsPopupComponent,
    CreateAndStartFolderPopupComponent,
    IpngProofDisplayPopupComponent,
    StampTypeNamePipe,
    MetadataNamePipe,
    InternalMetadataKeyPipe,
    ColumToFolderSortByPipe,
    FortifyCertificatesSelectorComponent,
    ExternalSignatureProviderPipe,
  ],
  imports: [
    BrowserModule,
    KeycloakAngularModule,
    HttpClientModule,
    AppRoutingModule,
    LsComposantsModule,
    LsWorkflowsModule,
    CommonModule,
    NgbModule,
    DragDropModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    PdfJsViewerModule,
    ToastrModule.forRoot(),
    FontAwesomeModule,
    NgxDaterangepickerMd.forRoot(),
    SharedModule,
    BrowserAnimationsModule
  ],
  providers: [
    DatePipe,
    {provide: LOCALE_ID, useValue: 'fr'},
    {provide: HTTP_INTERCEPTORS, useClass: GlobalHttpInterceptorService, multi: true},
    {provide: STD_BASE_PATH, useFactory: getApplicationUrl}, // Injects the base URL into the ip-core standard library
    {provide: INTERNAL_BASE_PATH, useFactory: getApplicationUrl}, // Injects the base URL into the ip-core internal library
    {provide: PROVISIONNING_BASE_PATH, useFactory: getApplicationUrl}, // Injects the base URL into the ip-core provisioning library
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService, HttpClient]
    },
  ],
  exports: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})


export class AppModule {

  // ngDoBootstrap(app) {
  //   keycloakService
  //     .init()
  //     .then(() => {
  //       console.log('[ngDoBootstrap] bootstrap app');
  //       app.bootstrap(AppComponent);
  //       keycloakService.loadUserProfile().then((userInfo) => {
  //         console.log(JSON.stringify(userInfo));
  //       });
  //     })
  //     .catch(error => console.error('[ngDoBootstrap] init Keycloak failed', error));
  // }

}
