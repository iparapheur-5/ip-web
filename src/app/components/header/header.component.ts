/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { KeycloakService } from 'keycloak-angular';
import { NotificationsService } from '../../services/notifications.service';
import { faWrench, faUser, faSignOutAlt, faHome, faCaretDown, faUserSecret, faQuestion } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalErrorPopupComponent } from './global-error-popup/global-error-popup.component';
import { LegacyUserService } from '../../services/ip-core/legacy-user.service';
import { HeaderMessages } from './header-messages';
import { SelectedDeskService } from '../../services/resolvers/desk/selected-desk.service';
import { CommonMessages } from '../../shared/common-messages';
import { UserEditNotificationsComponent } from '../main/users/user-edit-page-subpanels/user-edit-notifications/user-edit-notifications.component';
import { FirstLoginNotificationsPopupComponent } from '../settings/first-login-notifications-popup/first-login-notifications-popup.component';
import { User } from '../../models/auth/user';
import { FolderUtils } from '../../utils/folder-utils';
import { WebsocketService } from '../../services/websockets/websocket.service';
import { State, DeskDto, DeskCount, DeskService, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { Subscription } from 'rxjs';
import { SelectedUserService } from '../../services/selected-user.service';
import { SelectedUserPreferencesService } from '../../services/selected-user-preferences.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  readonly commonMessages = CommonMessages;
  readonly messages = HeaderMessages;
  readonly commonIcons = CommonIcons;
  readonly logoutIcon = faSignOutAlt;
  readonly wrenchIcon = faWrench;
  readonly homeIcon = faHome;
  readonly userProfileIcon = faUser;
  readonly gdprIcon = faUserSecret;
  readonly aboutIcon = faQuestion;
  readonly arrowDownIcon = faCaretDown;
  readonly stateEnum = State;
  readonly computeDelegatedTotalFn = FolderUtils.computeDelegatedTotal;

  selectedDesk: DeskDto;
  deskCount: DeskCount;
  folderCountsSubscriptions: Subscription[] = [];
  isUserAdmin: boolean = false;
  isUserTenantAdmin: boolean = false;
  isUserSuperAdmin: boolean = false;
  isInGlobalErrorState: boolean = false;
  globalErrorMessage: string = '';
  currentUser: User = null;
  currentUserPreferences: UserPreferencesDto = null;
  showTrashBin: boolean = false;

  readonly globalErrorTitle = `Une erreur importante est survenue, et empêche l'application de fonctionner normalement`;
  readonly globalErrorTooltip = `Une erreur importante est survenue. Cliquez pour visualiser le message d'erreur`;


  // <editor-fold desc="LifeCycle">


  constructor(private keycloakService: KeycloakService,
              private notificationService: NotificationsService,
              private deskService: DeskService,
              private modalService: NgbModal,
              private notificationsService: NotificationsService,
              private selectedDeskService: SelectedDeskService,
              private selectedUserPreferencesService: SelectedUserPreferencesService,
              private selectedUserService: SelectedUserService,
              private legacyUserService: LegacyUserService,
              private websocketService: WebsocketService) {

    notificationsService.globalErrorState$.subscribe(errorState => this.isInGlobalErrorState = errorState);
    notificationsService.globalErrorMsg$.subscribe(errorMsg => this.globalErrorMessage = errorMsg);
    selectedUserService.currentUser$.subscribe(user => {
      console.log("Header received user:", user);
      this.currentUser = user;
      this.checkForFirstLogin();
    });
    selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      console.log("Header received userPreferences:", userPreferences);
      this.currentUserPreferences = userPreferences;
      this.checkForFirstLogin();
    });
  }


  ngOnInit(): void {

    this.isUserTenantAdmin = this.legacyUserService.isCurrentUserTenantAdminOfOneTenant();
    this.isUserAdmin = this.legacyUserService.isCurrentUserAdmin();
    this.isUserSuperAdmin = this.legacyUserService.isCurrentUserSuperAdmin();
    this.showTrashBin = this.isUserTenantAdmin || this.isUserSuperAdmin;

    this.selectedDeskService.selectedDesk.subscribe(desk => {
      this.selectedDesk = desk;

      if (!this.selectedDesk) return;

      this.deskService.getDeskFolderCount(this.selectedDesk?.tenantId, this.selectedDesk.id)
        .subscribe(deskCount => this.deskCount = deskCount);

      this.websocketService.watchForDeskCountChanges([this.selectedDesk])
        .subscribe(observables => {
          this.folderCountsSubscriptions = observables.map(observable => observable.subscribe(
            deskCount => {
              this.deskCount = deskCount;
              console.debug(`Folder count updated, tenant : ${deskCount.tenantId}, desk : ${deskCount.deskId}`);
            }
          ));
        });
    });
  }


  ngOnDestroy(): void {
    this.folderCountsSubscriptions.forEach(subscription => subscription.unsubscribe());
  }


  // </editor-fold desc="LifeCycle">


  openErrorPopup() {
    const modalRef = this.modalService.open(GlobalErrorPopupComponent, {size: 'lg'});
    (modalRef.componentInstance as GlobalErrorPopupComponent).errorMessage = this.globalErrorMessage;
    (modalRef.componentInstance as GlobalErrorPopupComponent).title = this.globalErrorTitle;
    modalRef.result.then().catch();
  }


  onLogoutButtonClicked() {
    this.keycloakService
      .logout(document.baseURI)
      .then(() => { /* Not used */ });
  }


  checkForFirstLogin(): void {

    const isInitPending = !this.currentUser || !this.currentUserPreferences;
    const userAlreadyHasNotificationSettings = !!this.currentUserPreferences?.notificationsCronFrequency;

    if (isInitPending || userAlreadyHasNotificationSettings) {
      // Nothing to do here...
      return;
    }

    this.currentUserPreferences.notificationsCronFrequency = UserEditNotificationsComponent.NONE_NOTIFICATION;
    this.modalService
      .open(FirstLoginNotificationsPopupComponent, {
        backdrop: 'static',
        injector: Injector.create({
          providers: [
            {provide: FirstLoginNotificationsPopupComponent.INJECTABLE_CURRENT_USER_KEY, useValue: this.currentUser},
            {provide: FirstLoginNotificationsPopupComponent.INJECTABLE_CURRENT_USER_PREFERENCES_KEY, useValue: this.currentUserPreferences}
          ]
        }),
        size: 'lg'
      })
      .result
      .then(
        () => {/* Not used */},
        () => {/* Dismissed */}
      );
  }


  getAdminPath() {
    const tenantId: string = this.legacyUserService.getFirstAdministeredTenantId()
    return this.isUserSuperAdmin ? '/admin' : `/admin/${tenantId}/${this.legacyUserService.isCurrentUserTenantAdminOfThisTenant(tenantId) ? 'tenant-users' : 'desks'}`
  }


}
