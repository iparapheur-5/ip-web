/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit } from '@angular/core';
import { faWrench, faUser, faDesktop, faRoad, faFolderOpen, faAngleDoubleRight, faAngleDoubleLeft, faBook, faCog, faTachometerAlt, faUsers, faBuilding, faChevronRight, faUserClock } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../services/notifications.service';
import { AdminMessages } from '../admin-sub-panels/admin-messages';
import { CommonMessages } from '../../../shared/common-messages';
import { Router, ActivatedRoute } from '@angular/router';
import { isNullOrUndefined, compareById, isNotNullOrUndefined } from '../../../utils/string-utils';
import { LegacyUserService } from '../../../services/ip-core/legacy-user.service';
import { TenantService, TenantSortBy, TenantRepresentation } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-main-layout',
  templateUrl: './admin-main-layout.component.html',
  styleUrls: ['./admin-main-layout.component.scss']
})
export class AdminMainLayoutComponent implements AfterViewInit {

  readonly commonMessages = CommonMessages;
  readonly messages = AdminMessages;
  readonly wrenchIcon = faWrench;
  readonly angleDoubleLeftIcon = faAngleDoubleLeft;
  readonly angleDoubleRightIcon = faAngleDoubleRight;
  readonly isNullOrUndefinedFn = isNullOrUndefined;
  readonly isNotNullNorUndefinedFn = isNotNullOrUndefined;
  readonly compareByIdFn = compareById;
  readonly tenantIcon = faBuilding;
  readonly arrowRightIcon = faChevronRight;

  readonly globalMenuRows = [
    {route: './server', label: AdminMessages.SERVER_INFOS, icon: faTachometerAlt},
    {route: './tenants', label: AdminMessages.TENANTS, icon: faBuilding},
    {route: './all-users', label: AdminMessages.ALL_USERS, icon: faUsers},
  ]

  readonly tenantMenuRows = [
    {route: 'tenant-users', label: AdminMessages.USERS, icon: faUser},
    {route: 'desks', label: AdminMessages.DESKS, icon: faDesktop},
    {route: 'workflows', label: AdminMessages.WORKFLOWS, icon: faRoad},
    {route: 'typology', label: AdminMessages.TYPOLOGY, icon: faBook},
    {route: 'absences', label: AdminMessages.ABSENCES, icon: faUserClock},
    {route: 'folders', label: AdminMessages.FOLDERS, icon: faFolderOpen},
    {route: 'advanced', label: AdminMessages.ADVANCED_ADMIN, icon: faCog}
  ];

  readonly functionalAdminsMenuRows = [
    {route: 'desks', label: AdminMessages.DESKS, icon: faDesktop},
    {route: 'absences', label: AdminMessages.ABSENCES, icon: faUserClock},
    {route: 'folders', label: AdminMessages.FOLDERS, icon: faFolderOpen},
  ];

  activeTenantMenuRows = this.tenantMenuRows;


  isSuperAdmin = false;
  navbarCollapsed = false;
  currentTenantId: string;
  selectedTenant: TenantRepresentation;
  tenantList: Array<TenantRepresentation> = [];


  // <editor-fold desc="LifeCycle">


  constructor(private tenantService: TenantService,
              private legacyUserService: LegacyUserService,
              private router: Router,
              private route: ActivatedRoute,
              private notificationsService: NotificationsService) {
    this.isSuperAdmin = legacyUserService.isCurrentUserSuperAdmin();
  }


  ngAfterViewInit(): void {
    this.tenantService
      .listTenantsForUser(0, 10000, [TenantSortBy.Name + ',ASC'], true)
      .pipe(catchError(this.notificationsService.handleHttpError('listTenantsForUser')))
      .subscribe(
        (tenants) => {
          if (!tenants || !tenants.content || tenants.content.length === 0) {
            this.notificationsService.setGlobalError(AdminMessages.NO_TENANT_FOR_CURRENT_USER);
            return;
          }

          this.tenantList = tenants.content;
          this.route.firstChild.params.subscribe(params => {
            const selectedTenantId = params['tenantId'];
            if (selectedTenantId) {
              this.selectedTenant = this.tenantList.find(t => t.id === selectedTenantId);
              this.currentTenantId = selectedTenantId;
            } else if (!this.selectedTenant || !this.tenantList.find(t => t.id === this.selectedTenant.id)) {
              this.selectedTenant = this.tenantList[0];
              this.currentTenantId = this.selectedTenant.id;
            }
            this.setMenuListForSelectedTenant();
          });
        },
        () => this.notificationsService.showErrorMessage(AdminMessages.ERROR_REQUESTING_TENANTS)
      );
  }


  // </editor-fold desc="LifeCycle">


  onTenantSelectionChanged(tenant: TenantRepresentation) {

    if (tenant.id === this.currentTenantId) {
      return;
    }

    this.selectedTenant = tenant;
    this.router
      .navigateByUrl(this.router.url.replace(`/${this.currentTenantId}/`, `/${tenant.id}/`))
      .then(() => {
        this.currentTenantId = tenant.id
        this.setMenuListForSelectedTenant();
      });
  }


  private setMenuListForSelectedTenant() {
    const isFunctionalAdminOnly = this.legacyUserService.isCurrentUserOnlyFunctionalAdminOfThisTenant(this.currentTenantId);
    if (isFunctionalAdminOnly) {
      this.activeTenantMenuRows = this.functionalAdminsMenuRows;
    } else {
      this.activeTenantMenuRows = this.tenantMenuRows;
    }
  }


}
