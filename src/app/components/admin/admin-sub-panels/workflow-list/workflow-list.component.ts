/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { faCopy } from '@fortawesome/free-regular-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from '../../../../services/notifications.service';
import { CommonMessages } from '../../../../shared/common-messages';
import { AdminMessages } from '../admin-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { CrudOperation } from '../../../../services/crud-operation';
import { AdminWorkflowDefinitionService, WorkflowDefinitionSortBy, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';
import { WorkflowDefinition } from '../../../../models/workflow-definition';


@Component({
  selector: 'app-workflow-list',
  templateUrl: './workflow-list.component.html',
  styleUrls: ['./workflow-list.component.scss']
})
export class WorkflowListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly sortByEnum = WorkflowDefinitionSortBy;
  readonly messages = AdminMessages;
  readonly copyIcon = faCopy;

  tenantId: string;
  currentSearchTerm = null;

  workflowList: WorkflowDefinition[] = [];
  page = 1;
  total = 0;
  pageSizeIndex = 1;
  sortBy: WorkflowDefinitionSortBy = WorkflowDefinitionSortBy.Name;
  asc = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              private route: ActivatedRoute,
              public globalPopupService: GlobalPopupService,
              private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              private modalService: NgbModal,
              private router: Router) { }


  ngOnInit(): void {
    this.route.parent.parent.data.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 4 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route.parent.params.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.requestWorkflowList();
    });
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearchTerm = newTerm;
    this.page = 1;
    this.requestWorkflowList();
  }


  requestWorkflowList() {

    this.loading = true;

    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];
    this.adminWorkflowDefinitionService
      .listWorkflowDefinitions(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), requestSortBy, this.currentSearchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('listWorkflowDefinitions')))
      .subscribe(
        workflowsRetrieved => {
          this.workflowList = workflowsRetrieved.content.map(dto => Object.assign(new WorkflowDefinition(), dto));
          this.total = workflowsRetrieved.totalElements;
        },
        error => this.notificationsService.showErrorMessage(AdminMessages.ERROR_REQUESTING_WORKFLOWS, error)
      )
      .add(() => this.loading = false);
  }


  onRowOrderClicked(row: WorkflowDefinitionSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.requestWorkflowList();
  }


  prettyPrintId(id: string) {
    return id.split(/[:]+/).pop();
  }


  onCreateWorkflowClicked() {
    this.router
      .navigate([`/admin/${this.tenantId}/workflows/new`])
      .then(() => console.log('navigated to workflow creation'));
  }

// FIXME import workflow
/*  onImportWorkflowClicked() {
    this.modalService
      .open(ImportWorkflowPopupComponent, {
        injector: Injector.create({
          providers: [{provide: ImportWorkflowPopupComponent.INJECTABLE_TENANT_KEY, useValue: this.tenantId}]
        })
      })
      .result
      .then(
        (result) => {
          if (result === CommonMessages.ACTION_RESULT_OK) {
            this.requestWorkflowList();
          }
        },
        () => { /!* Dismissed *!/ }
      );
  }*/


  onCloneButtonClicked(workflowDefinition: WorkflowDefinition) {
    this.router
      .navigate([`/admin/${this.tenantId}/workflows/definitions/${workflowDefinition.id}/clone`])
      .then(() => console.log('navigated to workflow edition'));
  }


  onEditButtonClicked(workflowDefinition: WorkflowDefinition) {
    this.router
      .navigate([`/admin/${this.tenantId}/workflows/definitions/${workflowDefinition.id}`])
      .then(() => console.log('navigated to workflow edition'));
  }


  onDeleteButtonClicked(workflowDefinition: WorkflowDefinition) {
    this.globalPopupService
      .showDeleteValidationPopup(this.messages.workflowValidationPopupLabel(workflowDefinition.name))
      .then(
        () => this.deleteWorkflow(workflowDefinition),
        () => {/* dismissed */}
      );
  }


  deleteWorkflow(workflowDefinition: WorkflowDefinition) {
    this.adminWorkflowDefinitionService
      .deleteWorkflowDefinition(this.tenantId, workflowDefinition.key)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteWorkflowDefinition')))
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(CrudOperation.Delete, workflowDefinition);
          this.requestWorkflowList();
        },
        error => {
          this.notificationsService.showCrudMessage(CrudOperation.Delete, workflowDefinition, error.message, false);
        });
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


}
