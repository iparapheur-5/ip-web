/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { LoggableUser } from '../../../../../models/auth/loggable-user';
import { CrudOperation } from '../../../../../services/crud-operation';
import { UserPopupMessages } from '../../user-popup/user-popup-messages';
import { DeskRepresentation } from '@libriciel/iparapheur-standard';
import { AdminTenantUserService } from '@libriciel/iparapheur-provisioning';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';
import { catchError } from 'rxjs/operators';
import { AuthUtils } from '../../../../../utils/auth-utils';


@Component({
  selector: 'app-admin-tenant-user-create-popup',
  templateUrl: './admin-tenant-user-create-popup.component.html',
  styleUrls: ['./admin-tenant-user-create-popup.component.scss']
})
export class AdminTenantUserCreatePopupComponent {


  readonly messages = UserPopupMessages;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;

  @Input() tenantId: string;

  modifiedUser: UserDto = AuthUtils.createNewUserDto();
  administeredDesks: DeskRepresentation[] = [];
  password: string;
  isGeneralFormValid: boolean = false;
  isProcessing = false;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              private adminUserService: AdminTenantUserService,
              public activeModal: NgbActiveModal) {
  }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="UI Callbacks">

  onSaveButtonClicked() {

    if (!this.password || !this.isGeneralFormValid) {
      return;
    }

    this.modifiedUser.password = this.password;
    this.modifiedUser.administeredDeskIds = this.administeredDesks.map(desk => desk.id);

    this.isProcessing = true;
    this.adminUserService
      .createUser(this.tenantId, this.modifiedUser)
      .pipe(catchError(this.notificationService.handleHttpError('createUser')))
      .subscribe(
        () => {
          this.notificationService.showCrudMessage(CrudOperation.Create, new LoggableUser(this.modifiedUser));
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => {
          this.notificationService.showCrudMessage(CrudOperation.Create, new LoggableUser(this.modifiedUser), error.message, false);
        }
      )
      .add(() => this.isProcessing = false);
  }


  // </editor-fold desc="UI Callbacks">


}
