/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { faCaretUp, faCaretDown, faUnlink } from '@fortawesome/free-solid-svg-icons';
import { AdminUserEditPopupComponent } from '../../user-popup/admin-user-edit-popup/admin-user-edit-popup.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { faAddressBook } from '@fortawesome/free-regular-svg-icons';
import { AdminTenantUserCreatePopupComponent } from '../admin-tenant-user-create-popup/admin-tenant-user-create-popup.component';
import { NotificationsService } from '../../../../../services/notifications.service';
import { TenantUsersMessages } from '../tenant-users-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { ActivatedRoute } from '@angular/router';
import { GlobalPopupService } from '../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../admin-messages';
import { LegacyUserService } from '../../../../../services/ip-core/legacy-user.service';
import { UserRepresentation, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { AdminTenantUserService, UserSortBy, UserPrivilege } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-tenant-user-list',
  templateUrl: './admin-tenant-user-list.component.html',
  styleUrls: ['./admin-tenant-user-list.component.scss']
})
export class AdminTenantUserListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly ldapUserIcon = faAddressBook;
  readonly unlinkIcon = faUnlink;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = TenantUsersMessages;
  readonly adminMessages = AdminMessages;
  readonly sortByEnum = UserSortBy;
  readonly privilegeEnum = UserPrivilege;

  tenantId: string;
  userList: UserRepresentation[] = [];
  page: number = 1;
  pageSizeIndex: number = 1;
  total: number = 0;
  firstTotal: number;
  sortBy: UserSortBy = UserSortBy.LastName;
  currentSearchTerm: string = '';
  asc: boolean = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  isSuperAdmin: boolean = false;
  loading: boolean = false;
  colspan: number;

  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              public modalService: NgbModal,
              public legacyUserService: LegacyUserService,
              public globalPopupService: GlobalPopupService,
              private route: ActivatedRoute,
              private adminTenantUserService: AdminTenantUserService) {
    this.isSuperAdmin = legacyUserService.isCurrentUserSuperAdmin();
  }


  ngOnInit(): void {
    this.route.parent.parent.data.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 6 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route.parent.params.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.refreshUserList();
    });
  }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="UI Callbacks">


  onUnlinkUserButtonClicked(user: UserRepresentation) {
    this.globalPopupService
      .showUnlinkValidationPopup(this.adminMessages.tenantUserUnlinkValidationPopupLabel(user.firstName + ' ' + user.lastName))
      .then(
        () => {
          this.adminTenantUserService
            .removeUser(this.tenantId, user.id)
            .subscribe(
              () => {
                this.notificationService.showSuccessMessage(TenantUsersMessages.getUnlinkSuccessMessage(user));
                this.refreshUserList();
              },
              error => {
                this.notificationService.showErrorMessage(TenantUsersMessages.getUnlinkErrorMessage(user), error.message);
              }
            );
        },
        () => {/* dismissed */}
      );
  }


  onEditUserButtonClicked(user: UserRepresentation) {

    this.adminTenantUserService
      .getUser(this.tenantId, user.id)
      .subscribe(user => {
          this.modalService
            .open(
              AdminUserEditPopupComponent,
              {
                injector: Injector.create({
                  providers: [
                    {provide: AdminUserEditPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                    {provide: AdminUserEditPopupComponent.INJECTABLE_USER_PREFERENCES_KEY, useValue: this.userPreferences},
                    {provide: AdminUserEditPopupComponent.INJECTABLE_USER_KEY, useValue: user},
                  ]
                }),
                size: 'xl'
              }
            )
            .result
            .then(
              () => this.refreshUserList(),
              () => { /* Dismissed */ }
            )
        },
        error => this.notificationService.showErrorMessage(TenantUsersMessages.getFetchUserErrorMessage(user), error.message)
      );
  }


  onCreateUserClicked() {
    const modalRef = this.modalService
      .open(AdminTenantUserCreatePopupComponent, {size: 'xl'});

    modalRef.componentInstance.tenantId = this.tenantId;
    modalRef.result.then(
      () => this.refreshUserList(),
      () => { /* Dismissed */ }
    );
  }


  onRowOrderClicked(row: UserSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshUserList();
  }


  updateSearchTerm(searchTerm: string) {
    this.currentSearchTerm = searchTerm;
    this.page = 1;
    this.total = 0;
    this.refreshUserList();
  }


  // </editor-fold desc="UI Callbacks">


  isGreaterAdmin(targetUser: UserRepresentation): boolean {
    const isCurrentUserNotGlobalAdmin = !this.isSuperAdmin;
    const isTargetUserGlobalAdmin = targetUser.privilege == UserPrivilege.SuperAdmin;
    return isCurrentUserNotGlobalAdmin && isTargetUserGlobalAdmin;
  }


  refreshUserList() {

    this.loading = true;

    const searchTerm = this.currentSearchTerm.length > 0 ? this.currentSearchTerm : null;
    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];

    this.adminTenantUserService
      .listTenantUsers(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), requestSortBy, searchTerm)
      .pipe(catchError(this.notificationService.handleHttpError('listTenantUsersAsAdmin')))
      .subscribe(
        userRetrieved => {
          this.userList = userRetrieved.content;
          this.total = userRetrieved.totalElements;

          if (!this.firstTotal) {
            this.firstTotal = this.total;
          }
        },
        error => this.notificationService.showErrorMessage(TenantUsersMessages.ERROR_RETRIEVING_USERS, error)
      )
      .add(() => this.loading = false);
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


}
