/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { AllUsersMessages } from '../all-users/all-users-messages';
import { UserRepresentation } from '@libriciel/iparapheur-standard';


export class TenantUsersMessages extends AllUsersMessages {


  static readonly USERS_MANAGEMENT = 'Gestion des utilisateurs';
  static readonly CREATE_USER = 'Créer un utilisateur';
  static readonly ONLY_AN_ADMIN_CAN_EDIT_AN_ADMIN = `Un super administrateur ne peut être modifié que par un autre super administrateur`;


  static readonly getUnlinkSuccessMessage = (user: UserRepresentation): string =>
    `L'utilisateur ${user.firstName} ${user.lastName} à été dissocié de l'entité avec succès`;


  static readonly getUnlinkErrorMessage = (user: UserRepresentation): string =>
    `Erreur lors de la dissociation de l'utilisateur ${user.firstName} ${user.lastName}`;

  static readonly getFetchUserErrorMessage = (user: UserRepresentation): string =>
    `Erreur lors de la récupération de l'utilisateur ${user.firstName + ' ' + user.lastName}`;

}
