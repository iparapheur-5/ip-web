/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { interval, Subscription } from 'rxjs';
import { AdminMonitoringService } from '../../../../../services/ip-core/admin-monitoring.service';
import { JobDataResponse } from '../../../../../models/monitoring/job-data-response';
import { ServerDataMessages } from '../server-data-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../../../../../services/notifications.service';
import { AdminAllUsersService } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-server-data-main-layout',
  templateUrl: './server-data-main-layout.component.html',
  styleUrls: ['./server-data-main-layout.component.scss']
})
export class ServerDataMainLayoutComponent implements OnInit, OnDestroy {

  readonly infoIcon = faInfoCircle;
  readonly messages = ServerDataMessages;
  readonly icons = CommonIcons;

  localSpaceUsage: number;
  jobDataResponses: JobDataResponse[];
  intervalSubscription: Subscription;
  loggedInUserCount: number;

  // <editor-fold desc="LifeCycle">


  constructor(public monitoringService: AdminMonitoringService,
              public notificationsService: NotificationsService,
              private adminAllUserService: AdminAllUsersService) {}


  ngOnInit(): void {
    this.loadData();
    const intervalSource = interval(5000);
    this.intervalSubscription = intervalSource.subscribe(() => {
      this.loadData();
    });
  }


  ngOnDestroy(): void {
    this.intervalSubscription.unsubscribe();
  }


  // </editor-fold desc="LifeCycle">


  private loadUserCount(): void {
    this.adminAllUserService
      .countLoggedInUsers()
      .pipe(catchError(this.notificationsService.handleHttpError('countLoggedInUsers')))
      .subscribe(result => this.loggedInUserCount = result);
  }


  private loadDiskUsage(): void {
    this.monitoringService
      .getLocalSpaceUsage()
      .subscribe(data => {
        this.localSpaceUsage = data;
      });
  }


  private loadJobData(): void {
    this.monitoringService
      .getJobsData()
      .subscribe(jobDataResponses => {
        this.jobDataResponses = jobDataResponses;
      });
  }


  loadData(): void {
    this.loadJobData();
    this.loadDiskUsage();
    this.loadUserCount();
  }


}
