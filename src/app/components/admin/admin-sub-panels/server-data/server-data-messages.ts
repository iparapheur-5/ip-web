/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class ServerDataMessages {

  static readonly SERVER_DATA = 'Informations du serveur';
  static readonly SERVICES = 'Services';
  static readonly MEMORY_JVM_USE = 'Utilisation mémoire de la JVM\u00a0:';
  static readonly SYSTEM_CPU_USE = 'Utilisation CPU du système\u00a0:';
  static readonly LOCAL_DISK_USE = 'Utilisation du disque\u00a0:';
  static readonly SESSIONS_TOOLTIP_LINE_1 = `- Ce nombre décompte les sessions Keycloak créées ou renouvelées lors des 30 dernières minutes.`;
  static readonly SESSIONS_TOOLTIP_LINE_2 = `- Par défaut, la session dure 30 minutes.`;
  static readonly SESSIONS_TOOLTIP_LINE_3 = `- Ce nombre est un indicateur, une limite haute du nombre d'utilisateurs vraiment actifs.`;

  static usersCount = (count: number) => `Sessions actives : ${count}`;

}
