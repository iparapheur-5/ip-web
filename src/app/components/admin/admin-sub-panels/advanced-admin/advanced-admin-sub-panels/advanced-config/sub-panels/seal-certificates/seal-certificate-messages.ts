/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class SealCertificateMessages {

  static readonly SEAL_TITLE = 'Gestion des certificats de cachet serveur';
  static readonly SEAL_EXP_DATE = 'Exp. date';
  static readonly SEAL_IMPORT = 'Importer un nouveau certificat';
  static readonly SEAL_EDIT = 'Édition du certificat de cachet serveur';
  static readonly SEAL_EDIT_CERT_IMAGE = 'Image de certificat';
  static readonly ADD_SEAL_CERTIFICATE_TITLE = `Ajout d'un certificat de Cachet serveur`;
  static readonly FILE = `Fichier`;

  static readonly ERROR_RETRIEVING_SEAL_CERTIFICATES = 'Erreur à la récupération des certificats de cachet serveur';

  static associateCertificate = (data: number) => `Le certificat est associé à ${data} sous-type${data == 1 ? "" : "s"}.`;
  static deletedCertificate = (data: string) => `Le certificat "${data}" a été supprimé avec succès.`;

}
