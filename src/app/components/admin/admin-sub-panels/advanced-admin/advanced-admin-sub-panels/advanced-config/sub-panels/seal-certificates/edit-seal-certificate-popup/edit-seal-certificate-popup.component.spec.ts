/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSealCertificatePopupComponent } from './edit-seal-certificate-popup.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SealCertificate } from '../../../../../../../../../models/seal-certificate';

describe('EditSealCertificatePopupComponent', () => {

  let component: EditSealCertificatePopupComponent;
  let fixture: ComponentFixture<EditSealCertificatePopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: EditSealCertificatePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant_id'},
          {provide: EditSealCertificatePopupComponent.INJECTABLE_SEAL_CERTIFICATE_KEY, useValue: {} as SealCertificate}
        ],
        declarations: [EditSealCertificatePopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(EditSealCertificatePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
