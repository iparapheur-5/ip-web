/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { GLOBAL_SETTINGS } from '../../../../../../../shared/models/global-settings';

@Component({
  selector: 'app-advanced-config-main-layout',
  templateUrl: './advanced-config-main-layout.component.html',
  styleUrls: ['./advanced-config-main-layout.component.scss']
})
export class AdvancedConfigMainLayoutComponent implements OnInit {


  readonly menuRows = [
    {route: './metadata', label: 'Dictionnaire personnalisé des métadonnées'},
    {route: './layers', label: `Calques d'impression des dossiers`},
    {route: './seal-certificates', label: 'Certificats de cachets serveurs'}
  ];

  ngOnInit(): void {
    if (GLOBAL_SETTINGS?.admin?.showTemplates) {
      this.menuRows.unshift(
        {
          route: './templates',
          label: 'Modèles (e-mail et bordereau)'
        });
    }
  }


}
