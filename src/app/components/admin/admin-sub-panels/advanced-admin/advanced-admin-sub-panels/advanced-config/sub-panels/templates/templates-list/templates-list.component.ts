/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TemplatesEditPopupComponent } from '../templates-edit-popup/templates-edit-popup.component';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { TemplatesMessages } from '../templates-messages';
import { ActivatedRoute } from '@angular/router';
import { AdminTemplateService, TemplateType } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { of, combineLatest } from 'rxjs';
import { CommonIcons, Style } from '@libriciel/ls-composants';


@Component({
  selector: 'app-model-templates-list',
  templateUrl: './templates-list.component.html',
  styleUrls: ['./templates-list.component.scss']
})
export class TemplatesListComponent implements OnInit {

  readonly CommonIcons = CommonIcons;
  readonly Style = Style;
  readonly style = Style;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;  readonly messages = TemplatesMessages;
  readonly templateTypeEnum = TemplateType;


  tenantId: string;


  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal,
              public route: ActivatedRoute,
              private adminTemplateService: AdminTemplateService,
              private notificationsService: NotificationsService) { }


  ngOnInit() {
    this.route?.parent?.parent?.parent?.params?.subscribe(params => this.tenantId = params['tenantId']);
  }


  // </editor-fold desc="LifeCycle">


  /**
   * We may fetch both custom and default template here.
   * If the custom one does exist, we open the popup to modify it.
   * If the custom one does not exist, we open the popup with the default template to modify.
   * @param templateKeyValue
   */
  onEditButtonClicked(templateKeyValue: any) {

    const template = TemplateType[templateKeyValue.key];

    const getCustomTemplate$ = this.adminTemplateService
      .getCustomTemplate(this.tenantId, template, 'body', true, {httpHeaderAccept: 'text/plain'})
      // On a 404 error, the tenant does not have any custom tenant defined.
      // We just continue into a creation mode
      // FIXME : For some reason, the error returned when we set the 'text/plain' response is null.
      //  We can't filter the 404 error only.
      .pipe(catchError(() => of(null)));

    const getDefaultTemplate$ = this.adminTemplateService
      .getDefaultTemplate(template, 'body', true, {httpHeaderAccept: 'text/plain'})
      .pipe(catchError(this.notificationsService.handleHttpError('getDefaultTemplate')));

    combineLatest([getCustomTemplate$, getDefaultTemplate$]).subscribe(
      ([customTemplate, defaultTemplate]) =>
        this.modalService
          .open(
            TemplatesEditPopupComponent,
            {
              injector: Injector.create({
                providers: [
                  {provide: TemplatesEditPopupComponent.INJECTABLE_CREATION_MODE_KEY, useValue: !customTemplate},
                  {provide: TemplatesEditPopupComponent.INJECTABLE_ORIGINAL_TEMPLATE_KEY, useValue: customTemplate ?? defaultTemplate},
                  {provide: TemplatesEditPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                  {provide: TemplatesEditPopupComponent.INJECTABLE_TEMPLATE_TYPE_KEY, useValue: template}
                ]
              }),
              size: 'xl'
            }
          ),
      error => this.notificationsService.showErrorMessage(TemplatesMessages.GET_DEFAULT_TEMPLATE_ERROR_MESSAGE, error.message)
    );
  }


}
