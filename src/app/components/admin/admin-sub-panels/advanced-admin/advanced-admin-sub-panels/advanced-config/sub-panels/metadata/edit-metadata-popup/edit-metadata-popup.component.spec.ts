/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { EditMetadataPopupComponent } from './edit-metadata-popup.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Metadata } from '../../../../../../../../../models/metadata';
import { MetadataTypeNamePipe } from '../../../../../../../../../utils/metadata-type-name.pipe';


describe('EditMetadataPopupComponent', () => {

  let component: EditMetadataPopupComponent;
  let fixture: ComponentFixture<EditMetadataPopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: EditMetadataPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant_01'},
          {provide: EditMetadataPopupComponent.INJECTABLE_EXISTING_METADATA_KEY, useValue: new Metadata()},
        ],
        declarations: [MetadataTypeNamePipe, EditMetadataPopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(EditMetadataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
