/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Metadata } from '../../../../../../../../../models/metadata';
import { faGripHorizontal, faSortAlphaDown, faSortNumericDown, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { MetadataType } from '../../../../../../../../../models/metadata-type.enum';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { trackByIndex, integerRegex, floatRegex, urlRfc3986Regex } from '../../../../../../../../../utils/string-utils';
import { CdkDragDrop, moveItemInArray, CDK_DRAG_CONFIG } from '@angular/cdk/drag-drop';
import { Observable } from 'rxjs';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { CommonIcons } from '@libriciel/ls-composants';
import { MetadataMessages } from '../metadata-messages';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { FormUtils } from '../../../../../../../../../utils/form-utils';
import { catchError } from 'rxjs/operators';
import { AdminMetadataService } from '@libriciel/iparapheur-standard';


const DragConfig = {
  dragStartThreshold: 0,
  pointerDirectionChangeThreshold: 5,
  zIndex: 10000
};

@Component({
  selector: 'app-edit-metadata-popup',
  templateUrl: './edit-metadata-popup.component.html',
  styleUrls: ['./edit-metadata-popup.component.scss'],
  providers: [{provide: CDK_DRAG_CONFIG, useValue: DragConfig}]
})
export class EditMetadataPopupComponent {

  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_EXISTING_METADATA_KEY = 'existingMetadata';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = MetadataMessages;
  readonly infoIcon = faInfoCircle;
  readonly gripHorizontalIcon = faGripHorizontal;
  readonly sortAlphabeticallyIcon = faSortAlphaDown;
  readonly sortNumericallyIcon = faSortNumericDown;
  readonly metadataTypeEnum = MetadataType;
  readonly trackByIndexFn = trackByIndex;
  readonly updateValueAndValidity = FormUtils.updateValueAndValidity;

  generalForm = new FormGroup({
    nameControl: new FormControl(this.metadata?.name, [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(64)
    ]),
    keyControl: new FormControl(
      {value: this.metadata?.key, disabled: !!this.metadata?.key},
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(64),
        Validators.pattern('^[a-zA-Z0-9_]+$')
      ]),
    typeControl: new FormControl(
      {value: this.metadata?.type, disabled: !!this.metadata?.key},
      [Validators.required]
    ),
    indexControl: new FormControl(this.metadata?.index, []),
    restrictedValues: new FormControl(this.metadata?.restrictedValues, this.restrictedValuesValidator()),
  });

  isProcessing = false;
  areValuesRestricted = false;
  sortIcon = this.sortAlphabeticallyIcon;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminMetadataService: AdminMetadataService,
              public activeModal: NgbActiveModal,
              @Inject(EditMetadataPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(EditMetadataPopupComponent.INJECTABLE_EXISTING_METADATA_KEY) public metadata: Metadata) {
    this.areValuesRestricted = (metadata.restrictedValues?.length > 0);
  }


  // </editor-fold desc="LifeCycle">


  onAddValueButtonClicked() {
    this.metadata.restrictedValues.push('');
    FormUtils.updateValueAndValidity(this.generalForm);
  }


  onDeleteValueButtonClicked(index: number) {
    this.metadata.restrictedValues.splice(index, 1);
    FormUtils.updateValueAndValidity(this.generalForm);
  }


  onSortValuesButtonClicked() {
    // Special case for numeric values :
    // We want to sort the number values, and not just the starting characters.
    // Strings and dates are natively sorted in the expected natural way.
    [MetadataType.Integer, MetadataType.Float].includes(this.metadata.type)
      ? this.metadata.restrictedValues.sort((a, b) => parseFloat(a) - parseFloat(b))
      : this.metadata.restrictedValues.sort();
  }


  onTypeChange() {

    this.sortIcon = [MetadataType.Date, MetadataType.Integer, MetadataType.Float].includes(this.metadata.type)
      ? this.sortNumericallyIcon
      : this.sortAlphabeticallyIcon;

    FormUtils.updateValueAndValidity(this.generalForm);
  }


  restrictedValuesValidator(): ValidatorFn {
    return (_: AbstractControl): { [key: string]: any } | null => {

      if ((!this.areValuesRestricted) || (this.metadata.restrictedValues.length === 0)) {
        return null;
      }

      // Kinda hacky, we use the error messages directly as keys.
      // TODO : have some kind of key/message map somewhere ?

      const isFloat = this.metadata.type === MetadataType.Float;
      const onlyFloats = this.metadata.restrictedValues.every(s => floatRegex.test(s));
      if (isFloat && !onlyFloats) {
        return {['notOnlyFloats:']: true};
      }

      const isInteger = this.metadata.type === MetadataType.Integer;
      const onlyIntegers = this.metadata.restrictedValues.every(s => integerRegex.test(s));
      if (isInteger && !onlyIntegers) {
        return {['notOnlyIntegers:']: true};
      }

      const isUrl = this.metadata.type === MetadataType.Url;
      const onlyUrl = this.metadata.restrictedValues.every(s => urlRfc3986Regex.test(s));
      if (isUrl && !onlyUrl) {
        return {['notOnlyUrl:']: true};
      }

      return null;
    }
  }


  validate() {

    if (!this.generalForm.valid) {
      return;
    }

    // Cleaning up restricted values prior to request.
    // We don't want those if the checkbox was de-ticked, or the selected type is un-applicable.

    if ((this.metadata.type === MetadataType.Boolean) || !this.areValuesRestricted) {
      this.metadata.restrictedValues = [];
    }

    if ([MetadataType.Integer, MetadataType.Float].includes(this.metadata.type)) {
      this.metadata.restrictedValues = this.metadata.restrictedValues.filter(v => v.length >= 1)
    }

    // Sending request

    this.isProcessing = true;
    const crudOperation = (!this.metadata.id) ? CrudOperation.Create : CrudOperation.Update;

    const observableRequest: Observable<Metadata> = (!this.metadata.id)
      ? this.adminMetadataService.createMetadata(this.tenantId, this.metadata)
      : this.adminMetadataService.updateMetadata(this.tenantId, this.metadata.id, this.metadata);

    observableRequest
      .pipe(catchError(this.notificationsService.handleHttpError('create/edit metadata')))
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(crudOperation, this.metadata);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationsService.showCrudMessage(crudOperation, this.metadata, error.message, false)
      )
      .add(() => this.isProcessing = false);
  }


  onDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.metadata.restrictedValues, event.previousIndex, event.currentIndex);
  }


}
