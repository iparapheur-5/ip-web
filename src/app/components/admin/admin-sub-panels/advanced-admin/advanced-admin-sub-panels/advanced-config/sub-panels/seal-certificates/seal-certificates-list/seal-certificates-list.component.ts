/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SealCertificate } from '../../../../../../../../../models/seal-certificate';
import { AddSealCertificatePopupComponent } from '../add-seal-certificate-popup/add-seal-certificate-popup.component';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { EditSealCertificatePopupComponent } from '../edit-seal-certificate-popup/edit-seal-certificate-popup.component';
import { isNotNullOrUndefined } from '../../../../../../../../../utils/string-utils';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { SealCertificateMessages } from '../seal-certificate-messages';
import { GlobalPopupService } from '../../../../../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../../../../../admin-messages';
import { catchError } from 'rxjs/operators';
import { AdminSealCertificateService, SealCertificateSortBy, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { LegacyAdminSealCertificateService } from '../../../../../../../../../services/ip-core/legacy-admin-seal-certificate.service';

@Component({
  selector: 'app-seal-certificates-list',
  templateUrl: './seal-certificates-list.component.html',
  styleUrls: ['./seal-certificates-list.component.scss']
})
export class SealCertificatesListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50];
  readonly usageSubtypesMaxSize = 5;
  readonly sortByEnum = SealCertificateSortBy
  readonly caretUpIcon = faCaretUp;
  readonly caretDownIcon = faCaretDown;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = SealCertificateMessages;
  readonly adminMessages = AdminMessages;


  tenantId: string;
  sealCertificateList: SealCertificate[] = [];
  sortBy = SealCertificateSortBy.Name;
  asc = true;
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;

  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public modalService: NgbModal,
              public globalPopupService: GlobalPopupService,
              public route: ActivatedRoute,
              public legacyAdminSealCertificateService: LegacyAdminSealCertificateService,
              public adminSealCertificateService: AdminSealCertificateService) { }


  ngOnInit() {
    this.route?.parent?.parent?.parent?.parent?.data?.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 3 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route?.parent?.parent?.parent?.params?.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.refreshSealCertificateList();
    });
  }


  // </editor-fold desc="LifeCycle">


  refreshSealCertificateList() {

    this.loading = true;
    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];
    this.adminSealCertificateService
      .listSealCertificateAsAdmin(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), requestSortBy)
      .pipe(catchError(this.notificationsService.handleHttpError('listSealCertificateAsAdmin')))
      .subscribe(
        sealCertificatesResult => {
          this.sealCertificateList = sealCertificatesResult.content.map(dto => Object.assign(new SealCertificate(), dto));
          this.total = sealCertificatesResult.totalElements;
        },
        error => this.notificationsService.showErrorMessage(SealCertificateMessages.ERROR_RETRIEVING_SEAL_CERTIFICATES, error)
      )
      .add(() => this.loading = false);
  }


  refreshUsage(sealCertificate: SealCertificate) {

    // Preventing useless request
    if ((sealCertificate.usageCount == 0)
      || (isNotNullOrUndefined(sealCertificate.usageSubtypes) && (sealCertificate.usageSubtypes.length > 0))) {
      return;
    }

    this.legacyAdminSealCertificateService
      .getSealCertificateUsage(this.tenantId, sealCertificate, 0, this.usageSubtypesMaxSize)
      .subscribe(subtypePaginated => sealCertificate.usageSubtypes = subtypePaginated.data);
  }


  onRowOrderClicked(row: SealCertificateSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshSealCertificateList();
  }


  onImportButtonClicked() {
    this.modalService
      .open(AddSealCertificatePopupComponent, {
          injector: Injector.create({
            providers: [{provide: AddSealCertificatePopupComponent.INJECTABLE_SELECTED_TENANT_ID_KEY, useValue: this.tenantId}]
          })
        }
      )
      .result
      .then(
        () => this.refreshSealCertificateList(),
        () => { /* Dismissed */ }
      );
  }


  onEditButtonClicked(sealCertificate: SealCertificate) {

    this.adminSealCertificateService
      .getSealCertificate(this.tenantId, sealCertificate.id)
      .pipe(catchError(this.notificationsService.handleHttpError('getSealCertificate')))
      .subscribe(fullObject => {
        this.modalService
          .open(EditSealCertificatePopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: EditSealCertificatePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                  {provide: EditSealCertificatePopupComponent.INJECTABLE_SEAL_CERTIFICATE_KEY, useValue: Object.assign(new SealCertificate(), fullObject)}
                ]
              })
            }
          )
          .result
          .then(
            () => this.refreshSealCertificateList(),
            () => { /* Dismissed */ }
          );
      });
  }


  onDeleteButtonClicked(certificate: SealCertificate) {
    this.globalPopupService
      .showDeleteValidationPopup(this.adminMessages.certificateValidationPopupLabel(certificate.name))
      .then(
        () => this.deleteSealCertificate(certificate),
        () => {/* dismissed */}
      );
  }


  deleteSealCertificate(certificate: SealCertificate): void {
    this.adminSealCertificateService
      .deleteSealCertificate(this.tenantId, certificate.id)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteSealCertificate')))
      .subscribe(_ => {
        this.notificationsService.showSuccessMessage(this.messages.deletedCertificate(certificate.name));
        this.refreshSealCertificateList();
      });
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  isExpired(sealCertificate: SealCertificate): boolean {
    return (sealCertificate.expirationDate != null) && (new Date(sealCertificate.expirationDate).getTime() < new Date().getTime());
  }


}
