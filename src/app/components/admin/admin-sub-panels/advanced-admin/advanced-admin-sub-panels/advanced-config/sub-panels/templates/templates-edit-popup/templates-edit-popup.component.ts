/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TemplatesMessages } from '../templates-messages';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { isRfc5322ValidMail } from '../../../../../../../../../utils/string-utils';
import { AdminTemplateService, TemplateTestRequest, TemplateType } from '@libriciel/iparapheur-standard';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap/nav/nav';


enum TabEnum {
  Model,
  Example
}

@Component({
  selector: 'app-templates-edit-popup',
  templateUrl: './templates-edit-popup.component.html',
  styleUrls: ['./templates-edit-popup.component.scss']
})
export class TemplatesEditPopupComponent {

  public static readonly INJECTABLE_CREATION_MODE_KEY = 'creationMode';
  public static readonly INJECTABLE_ORIGINAL_TEMPLATE_KEY = 'originalTemplate';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_TEMPLATE_TYPE_KEY = 'templateType';

  readonly tabEnum = TabEnum;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly messages = TemplatesMessages;
  readonly isRfc5322ValidMailFn = isRfc5322ValidMail;
  readonly styleEnum = Style;
  readonly mailIcon = faEnvelope;


  isTestMailProcessing = false;
  isSaveProcessing = false;
  isDeleteProcessing = false;
  testMail: string;
  editedTemplate: string;
  lastModificationDate: string = Date.now().toString();


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              public adminTemplateService: AdminTemplateService,
              public notificationsService: NotificationsService,
              @Inject(TemplatesEditPopupComponent.INJECTABLE_CREATION_MODE_KEY) public creationMode: boolean,
              @Inject(TemplatesEditPopupComponent.INJECTABLE_ORIGINAL_TEMPLATE_KEY) public originalTemplate: string,
              @Inject(TemplatesEditPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(TemplatesEditPopupComponent.INJECTABLE_TEMPLATE_TYPE_KEY) public templateType: TemplateType) {
    this.editedTemplate = this.originalTemplate;
  }


  // </editor-fold desc="LifeCycle">


  getTemplateExamplePdfFn = (): Observable<any> => {

    let request: TemplateTestRequest = {
      mail: null,
      template: this.editedTemplate
    }

    return this.adminTemplateService
      .testPdfTemplate(this.tenantId, request, 'body', true, {httpHeaderAccept: 'application/pdf'})
      .pipe(catchError(this.notificationsService.handleHttpError('get template example PDF')))
  }


  isPdfTemplate(): boolean {
    return this.templateType == TemplateType.Docket;
  }


  isMailTemplate(): boolean {
    return [TemplateType.MailActionSend, TemplateType.MailNotificationSingle, TemplateType.MailNotificationDigest].includes(this.templateType);
  }


  onTestMailButtonClicked() {

    this.isTestMailProcessing = true;
    const testTemplate: TemplateTestRequest = {
      mail: this.testMail,
      template: this.editedTemplate
    };

    this.adminTemplateService
      .testMailTemplate(this.tenantId, testTemplate)
      .subscribe(
        () => { /* Not used */ },
        error => this.notificationsService.showErrorMessage(this.messages.ERROR_SENDING_TEST_MAIL, error.message)
      )
      .add(() => this.isTestMailProcessing = false);
  }


  onSaveButtonClicked() {
    this.isSaveProcessing = true;

    let requestObservable$: Observable<any> = this.creationMode
      ? this.adminTemplateService.createCustomTemplate(this.tenantId, this.templateType, this.editedTemplate)
      : this.adminTemplateService.updateCustomTemplate(this.tenantId, this.templateType, this.editedTemplate);

    requestObservable$
      .pipe(catchError(this.notificationsService.handleHttpError('create/update template')))
      .subscribe(
        () => {
          const message = this.creationMode ? TemplatesMessages.CREATE_SUCCESS_MESSAGE : TemplatesMessages.UPDATE_SUCCESS_MESSAGE;
          this.notificationsService.showSuccessMessage(message);
          this.activeModal.dismiss(CommonMessages.ACTION_RESULT_OK);
        },
        error => {
          const message = this.creationMode ? TemplatesMessages.CREATE_ERROR_MESSAGE : TemplatesMessages.UPDATE_ERROR_MESSAGE;
          this.notificationsService.showErrorMessage(message + error.message);
        }
      )
      .add(() => this.isSaveProcessing = false);
  }


  onDeleteButtonClicked() {
    this.isDeleteProcessing = true;
    this.adminTemplateService
      .deleteCustomTemplate(this.tenantId, this.templateType)
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(TemplatesMessages.DELETE_SUCCESS_MESSAGE);
          this.activeModal.dismiss(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationsService.showErrorMessage(TemplatesMessages.DELETE_ERROR_MESSAGE + error.message)
      )
      .add(() => this.isDeleteProcessing = false);
  }


  onNavElementClicked(changeEvent: NgbNavChangeEvent) {
    // Forcing a refresh on example tab selection
    if (changeEvent.nextId === TabEnum.Example) {
      this.lastModificationDate = Date.now().toString();
    }
  }


}
