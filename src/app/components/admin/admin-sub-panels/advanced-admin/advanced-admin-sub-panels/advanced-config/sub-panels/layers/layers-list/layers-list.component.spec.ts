/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { LayersListComponent } from './layers-list.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';
import { HttpHandler, HttpClient } from '@angular/common/http';

describe('LayersListComponent', () => {

  let component: LayersListComponent;
  let fixture: ComponentFixture<LayersListComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          ToastrModule.forRoot(),
          RouterModule.forRoot([])
        ],
        providers: [HttpClient, HttpHandler],
        declarations: [LayersListComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(LayersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
