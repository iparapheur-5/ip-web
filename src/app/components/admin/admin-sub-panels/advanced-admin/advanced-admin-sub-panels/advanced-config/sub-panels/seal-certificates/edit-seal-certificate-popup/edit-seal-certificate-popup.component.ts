/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, ViewChild } from '@angular/core';
import { LegacyAdminSealCertificateService } from '../../../../../../../../../services/ip-core/legacy-admin-seal-certificate.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SealCertificate } from '../../../../../../../../../models/seal-certificate';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { User } from '../../../../../../../../../models/auth/user';
import { EditSignatureComponent } from '../../../../../../../../main/users/user-edit-page-subpanels/user-edit-signature/edit-signature.component';
import { isNotNullOrUndefined } from '../../../../../../../../../utils/string-utils';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { SealCertificateMessages } from '../seal-certificate-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { LoggableSignatureImage } from '../../../../../../../../../models/auth/loggable-signature-image';
import { AdminSealCertificateService } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-edit-seal-certificate-popup',
  templateUrl: './edit-seal-certificate-popup.component.html',
  styleUrls: ['./edit-seal-certificate-popup.component.scss']
})
export class EditSealCertificatePopupComponent {


  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenant';
  public static readonly INJECTABLE_SEAL_CERTIFICATE_KEY = 'sealCertificate';

  readonly timesIcon = faTimes;
  readonly messages = SealCertificateMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;

  @ViewChild('signatureComponent') signatureComponent: EditSignatureComponent;

  isProcessing: boolean = false;
  signatureImageSrc: string;
  modifiedUser = new User();


  // <editor-fold desc="LifeCycle">


  constructor(public legacyAdminSealCertificateService: LegacyAdminSealCertificateService,
              public adminSealCertificateService: AdminSealCertificateService,
              public notificationsService: NotificationsService,
              public activeModal: NgbActiveModal,
              @Inject(EditSealCertificatePopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(EditSealCertificatePopupComponent.INJECTABLE_SEAL_CERTIFICATE_KEY) public sealCertificate: SealCertificate) {

    if (isNotNullOrUndefined(this.sealCertificate.signatureImageContentId)) {
      this.signatureImageSrc = this.legacyAdminSealCertificateService
        .signatureImageUrl(this.tenantId, this.sealCertificate, Date.now().toString());
    }
  }


  // </editor-fold desc="LifeCycle">


  createSignature(event: { user: User, file: File }) {
    this.legacyAdminSealCertificateService
      .setSignatureImage(this.tenantId, this.sealCertificate, event.file)
      .subscribe(imageContentId => {
        this.sealCertificate.signatureImageContentId = imageContentId;
        this.notificationsService.showCrudMessage(CrudOperation.Update, this.sealCertificate);
        this.signatureImageSrc = this.legacyAdminSealCertificateService
          .signatureImageUrl(this.tenantId, this.sealCertificate, Date.now().toString());
      }, error => {
        this.notificationsService.showCrudMessage(CrudOperation.Update, this.sealCertificate, error.message, false)
      });
  }


  deleteSignature(): void {
    this.legacyAdminSealCertificateService
      .deleteSignatureImage(this.tenantId, this.sealCertificate)
      .subscribe(
        () => {
          this.signatureImageSrc = null;
          this.sealCertificate.signatureImageContentId = null;
          this.notificationsService.showCrudMessage(CrudOperation.Update, new LoggableSignatureImage());
        },
        error => this.notificationsService.showCrudMessage(CrudOperation.Update, new LoggableSignatureImage(), error.message, false)
      );
  }


  updateSignature(event: { user: User, file: File }): void {
    this.legacyAdminSealCertificateService
      .updateSignatureImage(this.tenantId, this.sealCertificate, event.file)
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(CrudOperation.Update, this.sealCertificate);
          this.signatureImageSrc = this.legacyAdminSealCertificateService
            .signatureImageUrl(this.tenantId, this.sealCertificate, Date.now().toString());
        },
        error => this.notificationsService.showCrudMessage(CrudOperation.Update, this.sealCertificate, error.message, false));
  }


  onSaveButtonClicked() {
    this.isProcessing = true;
    this.adminSealCertificateService
      .updateSealCertificate(this.tenantId, this.sealCertificate.id, this.sealCertificate)
      .pipe(catchError(this.notificationsService.handleHttpError('updateSealCertificate')))
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(CrudOperation.Update, this.sealCertificate);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => {
          this.notificationsService.showCrudMessage(CrudOperation.Update, this.sealCertificate, error.message, false)
        }
      )
      .add(() => this.isProcessing = false);
  }


}
