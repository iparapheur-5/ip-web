/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Layer } from '../../../../../../../../../models/pdf-stamp/layer';
import { EditLayerPopupComponent } from '../edit-layer-popup/edit-layer-popup.component';
import { LayersMessages } from '../layers-messages';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { ActivatedRoute } from '@angular/router';
import { GlobalPopupService } from '../../../../../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../../../../../admin-messages';
import { of } from 'rxjs';
import { AdminLayerService, LayerSortBy, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-layers-list',
  templateUrl: './layers-list.component.html',
  styleUrls: ['./layers-list.component.scss']
})
export class LayersListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50];
  readonly caretUpIcon = faCaretUp;
  readonly caretDownIcon = faCaretDown;
  readonly sortByEnum = LayerSortBy;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly adminMessages = AdminMessages;
  readonly messages = LayersMessages;


  tenantId: string;
  layerList: Layer[] = [];
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  sortBy: LayerSortBy = LayerSortBy.Name;
  asc = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public modalService: NgbModal,
              public globalPopupService: GlobalPopupService,
              public route: ActivatedRoute,
              public adminLayerService: AdminLayerService) { }


  ngOnInit() {
    this.route?.parent?.parent?.parent?.parent?.data?.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 2 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route?.parent?.parent?.parent?.params?.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.refreshLayerList();
    });
  }


  // </editor-fold desc="LifeCycle">


  refreshLayerList() {

    this.loading = true;

    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];
    this.adminLayerService
      .listLayers(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), requestSortBy)
      .pipe(catchError(this.notificationsService.handleHttpError('listLayers')))
      .subscribe(
        tenantsRetrieved => {
          this.layerList = tenantsRetrieved.content.map(dto => Object.assign(new Layer(), dto));
          this.total = tenantsRetrieved.totalElements;
        },
        error => this.notificationsService.showErrorMessage(LayersMessages.ERROR_RETRIEVING_LAYERS, error)
      )
      .add(() => this.loading = false);
  }


  openEditLayerPopup(targetLayer?: Layer) {

    const layerObservable$ = !!targetLayer
      ? this.adminLayerService
        .getLayer(this.tenantId, targetLayer.id)
        .pipe(
          map(dto => Object.assign(new Layer(), dto)),
          catchError(this.notificationsService.handleHttpError('getLayer'))
        )
      : of(new Layer());

    layerObservable$
      .subscribe(
        layer =>
          this.modalService
            .open(EditLayerPopupComponent, {
                injector: Injector.create({
                    providers: [
                      {provide: EditLayerPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                      {provide: EditLayerPopupComponent.INJECTABLE_LAYER_KEY, useValue: layer},
                      {provide: EditLayerPopupComponent.INJECTABLE_USER_PREFERENCES_KEY, useValue: this.userPreferences}
                    ]
                  }
                ),
                size: 'xl'
              }
            )
            .result
            .then(
              () => this.refreshLayerList(),
              () => { /* Dismissed */ }
            ),
        error => {
          this.notificationsService.showCrudMessage(CrudOperation.Read, targetLayer, error.message, false);
        }
      );
  }


  onDeleteButtonClicked(layer: Layer) {
    this.globalPopupService.showDeleteValidationPopup(this.adminMessages.layerValidationPopupLabel(layer.name))
      .then(
        () => this.deleteLayer(layer),
        () => {/* dismissed */}
      );
  }


  deleteLayer(layer: Layer): void {
    this.adminLayerService
      .deleteLayer(this.tenantId, layer.id)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteLayer')))
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(CrudOperation.Delete, layer);
          this.refreshLayerList();
        },
        error => this.notificationsService.showCrudMessage(CrudOperation.Delete, layer, error.message, false)
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  onRowOrderClicked(row: LayerSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshLayerList();
  }


}
