/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { LayersMessages } from '../../layers-messages';
import { faSquare, faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import { Layer } from '../../../../../../../../../../models/pdf-stamp/layer';
import { Stamp } from '../../../../../../../../../../models/pdf-stamp/stamp';
import { CommonMessages } from '../../../../../../../../../../shared/common-messages';
import { NotificationsService } from '../../../../../../../../../../services/notifications.service';
import { CDK_DRAG_CONFIG, moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';
import { CommonIcons } from '@libriciel/ls-composants';
import { AdminMetadataService, MetadataRepresentation, AdminLayerService, StampType, StampTextColor, MetadataSortBy } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';


const DragConfig = {
  dragStartThreshold: 0,
  pointerDirectionChangeThreshold: 5,
  zIndex: 10000
};

@Component({
  selector: 'app-edit-layer-stamp-list',
  templateUrl: './edit-layer-stamp-list.component.html',
  styleUrls: ['./edit-layer-stamp-list.component.scss'],
  providers: [{provide: CDK_DRAG_CONFIG, useValue: DragConfig}]
})
export class EditLayerStampListComponent implements OnInit {


  readonly stampTypeEnum = StampType;
  readonly stampTextColorEnum = StampTextColor;
  readonly messages = LayersMessages;
  readonly commonMessages = CommonMessages;
  readonly gripHorizontalIcon = faGripHorizontal;
  readonly squareIcon = faSquare;
  readonly commonIcons = CommonIcons;


  @Input() tenantId: string;
  @Input() layer: Layer;
  @Output() pendingImageIdAdded = new EventEmitter<string>();

  metadataList: MetadataRepresentation[] = [];
  isUploadingFile = false;


  // <editor-fold desc="LifeCycle">


  constructor(public adminLayerService: AdminLayerService,
              public adminMetadataService: AdminMetadataService,
              public notificationsService: NotificationsService) { }


  ngOnInit() {
    if (!this.tenantId) return;

    const sortBy = [
      `${MetadataSortBy.Index},ASC`,
      `${MetadataSortBy.Name},ASC`,
      `${MetadataSortBy.Key},ASC`,
      `${MetadataSortBy.Id},ASC`,
    ];
    this.adminMetadataService
      .listMetadataAsAdmin(this.tenantId, true, null, 0, 9999, sortBy)
      .pipe(catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin')))
      .subscribe(result => this.metadataList = result.content);
  }


  // </editor-fold desc="LifeCycle">


  onDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.layer.stampList, event.previousIndex, event.currentIndex);
  }


  handleFileInput(stamp: Stamp, event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList | null = element.files;
    if (fileList) {
      this.isUploadingFile = true;
      this.adminLayerService
        .createFileStamp(this.tenantId, this.layer.id, stamp, fileList.item(0))
        .pipe(catchError(this.notificationsService.handleHttpError('createFileStamp')))
        .subscribe(
          stampDto => {
            stamp.id = stampDto.id;
            stamp.value = stampDto.value;
            this.pendingImageIdAdded.emit(stamp.id);
          },
          error => this.notificationsService.showErrorMessage(LayersMessages.ERROR_SENDING_FILE, error.message)
        )
        .add(() => this.isUploadingFile = false);
    }
  }


  onDeleteButtonClicked(stamp: Stamp) {
    this.layer.stampList = this.layer.stampList.filter(s => s.id !== stamp.id);
  }


  addNewStamp() {

    let stamp = new Stamp();
    stamp.id = null;
    stamp.signatureRank = 0;
    stamp.type = StampType.Text;
    stamp.value = null;
    stamp.fontSize = 10;
    stamp.textColor = StampTextColor.Black;
    stamp.x = 0;
    stamp.y = 0;
    stamp.page = 0;
    stamp.afterSignature = false;

    this.layer.stampList.push(stamp);
  }


}
