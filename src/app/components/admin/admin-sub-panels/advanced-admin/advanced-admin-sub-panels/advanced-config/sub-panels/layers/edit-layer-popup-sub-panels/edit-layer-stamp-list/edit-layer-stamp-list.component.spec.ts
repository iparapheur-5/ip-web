/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { EditLayerStampListComponent } from './edit-layer-stamp-list.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Layer } from '../../../../../../../../../../models/pdf-stamp/layer';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';

describe('EditLayerStampListComponent', () => {

  let component: EditLayerStampListComponent;
  let fixture: ComponentFixture<EditLayerStampListComponent>;


  beforeEach(async () => {

    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [HttpClient, HttpHandler],
        declarations: [FaIconComponent, EditLayerStampListComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(EditLayerStampListComponent);
    component = fixture.componentInstance;
    component.tenantId = 'tenantId01';
    component.layer = new Layer();
    component.layer.stampList = [];
    component.layer.id = 'layerId01';
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
