/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class MetadataMessages {

  static readonly INDEX = 'Position';
  static readonly METADATA_TYPE = 'Nature';

  static readonly INDEX_TOOLTIP = `"Index" selon lequel les métadonnées seront affichées aux utilisateurs.
  Plusieurs métadonnées peuvent partager la même position, si c'est le cas elles seront affichées par ordre alphabétique`;

  static readonly LIMIT_ALLOWED_VALUES = 'Restreindre les valeurs autorisées';
  static readonly METADATA_ID_INFO = `Ne doit pas contenir d'espace. Seul le caractère spécial "_" est accepté`;
  static readonly ADD_VALUE = `Ajouter une valeur`;
  static readonly SORT = `Trier`;

  static readonly METADATA_MANAGEMENT = 'Gestion des métadonnées';
  static readonly METADATA_CREATE = 'Créer une métadonnée';
  static readonly METADATA_CREATION_POPUP_TITLE = `Ajout d'une nouvelle métadonnée`;
  static readonly METADATA_EDIT_POPUP_TITLE = `Édition de la métadonnée`;
  static readonly METADATA_SEARCH_PLACEHOLDER = 'Rechercher des métadonnées';

  static readonly ERROR_RETRIEVING_METADATA = `Erreur à la récupération des métadonnées`;


  static getDeleteMetadataErrorMessage(metadataName: string) {return `Erreur lors de la suppression de la métadonnée ${metadataName}`}

  static getDeleteMetadataSuccessMessage(metadataName: string) {return `La métadonnée ${metadataName} a été supprimée avec succès`}
}
