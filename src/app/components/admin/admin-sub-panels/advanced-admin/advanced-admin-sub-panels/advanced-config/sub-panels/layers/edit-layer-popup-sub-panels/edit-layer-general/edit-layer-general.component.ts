/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { LayersMessages } from '../../layers-messages';
import { CommonMessages } from '../../../../../../../../../../shared/common-messages';
import { Layer } from '../../../../../../../../../../models/pdf-stamp/layer';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserPreferencesDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-edit-layer-general',
  templateUrl: './edit-layer-general.component.html',
  styleUrls: ['./edit-layer-general.component.scss']
})
export class EditLayerGeneralComponent {

  readonly messages = LayersMessages;
  readonly commonMessages = CommonMessages;

  @Input() editMode: boolean;
  @Input() layer: Layer;
  @Input() userPreferences: UserPreferencesDto;
  @Output() validityStateChanged = new EventEmitter<boolean>();

  generalForm: FormGroup;


  // <editor-fold desc="LifeCycle">


  constructor() {
    this.generalForm = new FormGroup({
      idControl: new FormControl(this.layer?.id),
      nameControl: new FormControl(this.layer?.name, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(64)
      ])
    });

    this.generalForm.valueChanges.subscribe(() => this.validityStateChanged.emit(this.generalForm.valid))
  }


  // </editor-fold desc="LifeCycle">


}
