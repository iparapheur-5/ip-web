/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class TemplatesMessages {

  static readonly CUSTOM_TEMPLATES = 'Modèles personnalisés';
  static readonly TEST_TEMPLATE = 'Tester le modèle';
  static readonly MAIL_TEST_ = 'Adresse e-mail de test\u00a0:';
  static readonly TEMPLATE = 'Modèle\u00a0:';

  static readonly TAB_MODEL = 'Modèle';
  static readonly ERROR_SENDING_TEST_MAIL = `Erreur à l'envoi du mail de test`;

  static readonly GET_DEFAULT_TEMPLATE_ERROR_MESSAGE = 'Erreur à la récupération du modèle par défaut';

  static readonly CREATE_SUCCESS_MESSAGE = 'Le modèle personnalisé a été créé avec succès';
  static readonly CREATE_ERROR_MESSAGE = 'Erreur à la création du modèle personnalisé\u00a0: ';
  static readonly UPDATE_SUCCESS_MESSAGE = 'Le modèle personnalisé a été modifié avec succès';
  static readonly UPDATE_ERROR_MESSAGE = 'Erreur à la modification du modèle personnalisé\u00a0: ';
  static readonly DELETE_SUCCESS_MESSAGE = 'Le modèle personnalisé a été supprimé avec succès';
  static readonly DELETE_ERROR_MESSAGE = 'Erreur à la suppression du modèle personnalisé\u00a0: ';

}
