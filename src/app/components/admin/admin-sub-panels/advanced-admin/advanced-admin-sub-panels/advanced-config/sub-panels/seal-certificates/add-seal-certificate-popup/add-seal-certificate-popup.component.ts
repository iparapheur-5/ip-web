/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { SealCertificate } from '../../../../../../../../../models/seal-certificate';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { CommonIcons } from '@libriciel/ls-composants';
import { SealCertificateMessages } from '../seal-certificate-messages';
import { AdminSealCertificateService } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-add-seal-certificate-popup',
  templateUrl: './add-seal-certificate-popup.component.html',
  styleUrls: ['./add-seal-certificate-popup.component.scss']
})
export class AddSealCertificatePopupComponent {

  static readonly INJECTABLE_SELECTED_TENANT_ID_KEY = 'tenantId';

  readonly commonMessages = CommonMessages;
  readonly messages = SealCertificateMessages;
  readonly commonIcons = CommonIcons;


  password: string;
  selectedFile: File;
  isProcessing = false;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminSealCertificateService: AdminSealCertificateService,
              public activeModal: NgbActiveModal,
              @Inject(AddSealCertificatePopupComponent.INJECTABLE_SELECTED_TENANT_ID_KEY) public tenantId: string) { }


  // </editor-fold desc="LifeCycle">


  onSaveButtonClicked() {
    this.isProcessing = true;

    let sealCert = new SealCertificate();
    sealCert.name = this.selectedFile.name;

    this.adminSealCertificateService
      .createSealCertificate(this.tenantId, this.password, this.selectedFile)
      .pipe(catchError(this.notificationsService.handleHttpError('createSealCertificate')))
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(CrudOperation.Create, sealCert);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationsService.showCrudMessage(CrudOperation.Create, sealCert, error.message, false)
      )
      .add(() => this.isProcessing = false);
  }


  onFileChanged(event) {
    const selectedFiles: FileList = (event.target as HTMLInputElement).files;
    this.selectedFile = selectedFiles.item(0);
  }


}
