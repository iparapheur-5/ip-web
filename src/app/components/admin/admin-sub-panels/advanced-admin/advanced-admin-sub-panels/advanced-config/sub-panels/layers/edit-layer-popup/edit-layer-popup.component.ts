/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Layer } from 'src/app/models/pdf-stamp/layer';
import { Observable, combineLatest } from 'rxjs';
import { LayersMessages } from '../layers-messages';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap/nav/nav';
import { AdminLayerService, LayerDto, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { catchError, map } from 'rxjs/operators';


enum TabEnum {
  General,
  Stamps,
  Example
}


@Component({
  selector: 'app-edit-layer-popup',
  templateUrl: './edit-layer-popup.component.html',
  styleUrls: ['./edit-layer-popup.component.scss']
})
export class EditLayerPopupComponent {

  static readonly INJECTABLE_TENANT_ID_KEY = "tenantId";
  static readonly INJECTABLE_LAYER_KEY = "layer";
  static readonly INJECTABLE_USER_PREFERENCES_KEY = "userPreferences";

  readonly messages = LayersMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly tabEnum = TabEnum;

  isFormValid = false;
  isGeneralTabValid = false;
  isProcessing = false;
  lastModificationDate: string = Date.now().toString();
  pendingImageIds: string[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminLayerService: AdminLayerService,
              public activeModal: NgbActiveModal,
              @Inject(EditLayerPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(EditLayerPopupComponent.INJECTABLE_LAYER_KEY) public layer: Layer,
              @Inject(EditLayerPopupComponent.INJECTABLE_USER_PREFERENCES_KEY) public userPreferences: UserPreferencesDto) { }


  // </editor-fold desc="LifeCycle">


  onNavElementClicked(changeEvent: NgbNavChangeEvent) {
    // Forcing a refresh on example tab selection
    if (changeEvent.nextId === TabEnum.Example) {
      this.lastModificationDate = Date.now().toString();
    }
  }


  onPendingImageIdAdded(id: string): void {
    this.pendingImageIds.push(id);
  }


  getLayerExamplePdfFn = (): Observable<any> => {
    return this.adminLayerService
      .getLayerExamplePdf(this.tenantId, this.layer, 'body', true, {httpHeaderAccept: 'application/pdf'})
      .pipe(catchError(this.notificationsService.handleHttpError('get layer example PDF')))
  }


  onGeneralValidityChanged(isValid: boolean) {
    this.isGeneralTabValid = isValid;
    this.isFormValid = this.isGeneralTabValid;
  }


  onCancelButtonClicked(): void {

    let deletionList$: Observable<void>[] = this.pendingImageIds
      .map(imageId => this.adminLayerService
        .deleteFileStamp(this.tenantId, this.layer.id, imageId)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteFileStamp')))
      );

    combineLatest(deletionList$)
      .subscribe(
        () => console.log('Temporary images had been deleted'),
        error => this.notificationsService.showErrorMessage(LayersMessages.ERROR_CLEANING_TEMP_IMAGE_FILES, error)
      );

    this.activeModal.close(CommonMessages.ACTION_RESULT_CANCEL);
  }


  submit() {
    this.isProcessing = true;
    const crudOperation = !this.layer.id ? CrudOperation.Create : CrudOperation.Update;

    const observableRequest$: Observable<LayerDto> = (crudOperation === CrudOperation.Create)
      ? this.adminLayerService.createLayer(this.tenantId, this.layer)
      : this.adminLayerService.updateLayer(this.tenantId, this.layer.id, this.layer);

    observableRequest$
      .pipe(
        map(dto => Object.assign(new Layer(), dto)),
        catchError(this.notificationsService.handleHttpError('create/update layer'))
      )
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(crudOperation, this.layer);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationsService.showCrudMessage(crudOperation, this.layer, error.message, false)
      )
      .add(() => this.isProcessing = false);
  }


}
