/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { AddSealCertificatePopupComponent } from './add-seal-certificate-popup.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHandler } from '@angular/common/http';


describe('AddSealCertificatePopupComponent', () => {


  let component: AddSealCertificatePopupComponent;
  let fixture: ComponentFixture<AddSealCertificatePopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          NgbActiveModal, HttpClient, HttpHandler,
          {provide: AddSealCertificatePopupComponent.INJECTABLE_SELECTED_TENANT_ID_KEY, useValue: 'tenant_01'},
        ],
        declarations: [AddSealCertificatePopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AddSealCertificatePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
