/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { ConnectorMessages } from '../../../connector-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { urlRfc3986Regex } from '../../../../../../../../../utils/string-utils';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { AdminExternalSignatureService, ExternalSignatureConfigDto, ExternalSignatureProvider } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-add-external-signature-connector-popup',
  templateUrl: './add-external-signature-connector-popup.component.html',
  styleUrls: ['./add-external-signature-connector-popup.component.scss']
})
export class AddExternalSignatureConnectorPopupComponent implements OnInit {

  static readonly INJECTABLE_CURRENT_EXTERNAL_SIGNATURE_CONF_KEY = 'currentConfig';
  static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  static readonly INJECTABLE_EDIT_MODE_KEY = 'editMode';

  readonly messages = ConnectorMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly eyeIcon = faEye;
  readonly eyeSlashIcon = faEyeSlash;

  // FIXME docage 5.0
  readonly availableServices: ExternalSignatureProvider[] = [ExternalSignatureProvider.Yousign, ExternalSignatureProvider.Universign];

  showPasswordField: boolean = false;
  showTokenField: boolean = false;
  showLoginField: boolean = false;
  isProcessing: boolean = false;
  hidePassword: boolean = true;

  urlValidator: ValidatorFn = (control: AbstractControl) => {
    const malformedUrlError: ValidationErrors = {malformedUrlError: true};
    return urlRfc3986Regex.test(control.value) ? null : malformedUrlError;
  }

  extSigForm: FormGroup = new FormGroup({
    name: new FormControl(this.currentConfig?.name, [Validators.required, Validators.minLength(2)]),
    url: new FormControl(this.currentConfig?.url, [Validators.required, Validators.minLength(2), this.urlValidator]),
    serviceName: new FormControl(this.currentConfig?.serviceName, [Validators.required]),
    token: new FormControl(this.currentConfig?.token, [Validators.minLength(2)]),
    login: new FormControl(this.currentConfig?.login, [Validators.minLength(2)]),
    password: new FormControl(this.currentConfig?.password, [Validators.minLength(2)])
  });


  // <editor-fold desc="LifeCycle">


  constructor(private notificationService: NotificationsService,
              private adminExternalSignatureService: AdminExternalSignatureService,
              private activeModal: NgbActiveModal,
              @Inject(AddExternalSignatureConnectorPopupComponent.INJECTABLE_TENANT_ID_KEY) private tenantId: string,
              @Inject(AddExternalSignatureConnectorPopupComponent.INJECTABLE_EDIT_MODE_KEY) public editMode: boolean,
              @Inject(AddExternalSignatureConnectorPopupComponent.INJECTABLE_CURRENT_EXTERNAL_SIGNATURE_CONF_KEY) public currentConfig: ExternalSignatureConfigDto) {}

  ngOnInit(): void {
    this.onServiceChange();
  }


  // </editor-fold desc="LifeCycle">

  onServiceChange(): void {
    const serviceName: ExternalSignatureProvider = this.extSigForm.get('serviceName').value;
    this.showTokenField = serviceName === ExternalSignatureProvider.Yousign;
    this.showLoginField = [ExternalSignatureProvider.Docage, ExternalSignatureProvider.Universign].includes(serviceName);
    this.showPasswordField = [ExternalSignatureProvider.Docage, ExternalSignatureProvider.Universign].includes(serviceName);
  }

  executeForm(): void {

    if (!this.extSigForm.valid) return;

    this.isProcessing = true;

    let request$: Observable<ExternalSignatureConfigDto> = this.editMode
      ? this.adminExternalSignatureService.editExternalSignatureConfig(this.tenantId, this.currentConfig.id, this.currentConfig)
      : this.adminExternalSignatureService.createExternalSignatureConfig(this.tenantId, this.currentConfig);

    request$
      .subscribe(
        () => {
          this.notificationService.showSuccessMessage(
            this.editMode
              ? ConnectorMessages.SUCCESS_EDITING_CONFIGURATION
              : ConnectorMessages.SUCCESS_CREATING_CONFIGURATION,
          );
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationService.showErrorMessage(
          this.editMode
            ? ConnectorMessages.ERROR_EDITING_CONFIGURATION
            : ConnectorMessages.ERROR_CREATING_CONFIGURATION,
          error.message
        )
      )
      .add(() => this.isProcessing = false);
  }


  onCancelButtonClicked(): void {
    this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL);
  }


  cancel(): void {
    this.onCancelButtonClicked();
  }
}
