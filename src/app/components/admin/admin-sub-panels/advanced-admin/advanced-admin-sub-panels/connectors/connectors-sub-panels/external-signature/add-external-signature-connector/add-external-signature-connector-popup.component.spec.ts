/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddExternalSignatureConnectorPopupComponent } from './add-external-signature-connector-popup.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ExternalSignatureConfigDto } from '@libriciel/iparapheur-standard';
import { ExternalSignatureProviderPipe } from '../../../../../../../../../utils/external-signature-provider.pipe';

describe('AddExternalSignatureConnectorComponent', () => {

  let component: AddExternalSignatureConnectorPopupComponent;
  let fixture: ComponentFixture<AddExternalSignatureConnectorPopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenantId'},
          {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_EDIT_MODE_KEY, useValue: true},
          {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_CURRENT_EXTERNAL_SIGNATURE_CONF_KEY, useValue: {} as ExternalSignatureConfigDto}
        ],
        declarations: [ExternalSignatureProviderPipe, AddExternalSignatureConnectorPopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AddExternalSignatureConnectorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
