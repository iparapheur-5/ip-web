/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class ConnectorMessages {

  static readonly CONNECTOR_CREATE = 'Créer une configuration';

  static readonly EXT_SIG_TITLE = 'Gestion des configurations de signatures externes';
  static readonly MAIL_SEC_TITLE = 'Gestion des configurations de mail sécurisé';

  static readonly SERVICE = 'Service';
  static readonly URL = 'Url';
  static readonly LOGIN = 'Identifiant';
  static readonly PASSWORD = 'Mot de passe';
  static readonly TOKEN = 'Token';

  static readonly CONNECT = 'Connecter';
  static readonly DISCONNECT = 'Déconnecter';
  static readonly CONNECTED = 'Connecté';

  static readonly INVALID_LOG_INFO = 'Utilisateur ou mot de passe invalide';
  static readonly CHOOSE_AN_ENTITY = 'Choisissez une entité';

  static readonly ERROR_CREATING_CONFIGURATION = 'Une erreur est  survenue durant la création de la configuration.';
  static readonly ERROR_EDITING_CONFIGURATION = `Une erreur est  survenue durant l'édition de la configuration.`;
  static readonly ERROR_DELETING_CONFIGURATION = 'Une erreur est  survenue durant la suppression de la configuration.';
  static readonly SUCCESS_CREATING_CONFIGURATION = 'La configuration a été créée avec succès';
  static readonly SUCCESS_EDITING_CONFIGURATION = 'La configuration a été éditée avec succès';
  static readonly SUCCESS_DELETING_CONFIGURATION = 'La configuration a été supprimée avec succès';
  static getConnectorEditOrCreatePopupTitle = (editMode: boolean, name?: string) => editMode ? `Modification du connecteur "${name}"` : `Ajout d'un nouveau connecteur`;

}

