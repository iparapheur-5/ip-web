/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddExternalSignatureConnectorPopupComponent } from '../add-external-signature-connector/add-external-signature-connector-popup.component';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { ActivatedRoute } from '@angular/router';
import { ConnectorMessages } from '../../../connector-messages';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { GlobalPopupService } from '../../../../../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../../../../../admin-messages';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { ExternalSignatureConfigDto, ExternalSignatureConfigRepresentation, ExternalSignatureConfigSortBy, AdminExternalSignatureService, UserPreferencesDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-external-signature-connectors',
  templateUrl: './external-signature-connectors.component.html',
  styleUrls: ['./external-signature-connectors.component.scss']
})
export class ExternalSignatureConnectorsComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50];
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = ConnectorMessages;
  readonly adminMessages = AdminMessages;
  readonly sortByEnum = ExternalSignatureConfigSortBy;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly style = Style;

  tenantId: string;
  configurationList: ExternalSignatureConfigRepresentation[] = [];
  currentSearchTerm: string = '';
  page: number = 1;
  total: number = 0;
  pageSizeIndex: number = 1;
  sortBy: ExternalSignatureConfigSortBy = ExternalSignatureConfigSortBy.Name;
  asc: boolean = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              public modalService: NgbModal,
              public globalPopupService: GlobalPopupService,
              public route: ActivatedRoute,
              private adminExternalSignatureService: AdminExternalSignatureService) { }


  ngOnInit(): void {
    this.route?.parent?.parent?.parent?.parent?.data?.subscribe(data => {
      this.userPreferences = data["userPreferences"];
    });
    this.colspan = 5 + (this.userPreferences.showAdminIds ? 1 : 0)
    this.route?.parent?.parent?.parent?.params?.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.requestExternalConfigurationList();
    });
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(searchTerm: string) {
    this.currentSearchTerm = searchTerm;
    this.page = 1;
    this.total = 0;

    this.requestExternalConfigurationList();
  }


  requestExternalConfigurationList() {
    this.loading = true;

    const searchTerm = this.currentSearchTerm.length > 0 ? this.currentSearchTerm : null;
    const sortBy = [this.sortBy.toLowerCase() + (this.asc ? ',ASC' : ',DESC')];

    this.adminExternalSignatureService
      .listExternalSignatureConfigs(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), sortBy, searchTerm)
      .subscribe(configsRetrieved => {
        this.configurationList = configsRetrieved.content;
        this.total = configsRetrieved.totalElements;
      })
      .add(() => this.loading = false);
  }


  onCreateButtonClicked() {
    this.modalService
      .open(AddExternalSignatureConnectorPopupComponent, {
          injector: Injector.create(
            {
              providers: [
                {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_EDIT_MODE_KEY, useValue: false},
                {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_CURRENT_EXTERNAL_SIGNATURE_CONF_KEY, useValue: {}},
              ]
            })
        }
      )
      .result
      .then(
        () => this.requestExternalConfigurationList(),
        () => { /* Dismissed */ }
      );
  }


  onDeleteButtonClicked(config: ExternalSignatureConfigDto) {
    this.globalPopupService.showDeleteValidationPopup(this.adminMessages.externalSignatureConnectorPopupLabel(config.name))
      .then(result => {
        if (result === CommonMessages.ACTION_RESULT_OK) {
          this.deleteConnector(config);
        }
      });
  }

  onEditButtonClicked(server: ExternalSignatureConfigDto) {

    this.modalService
      .open(AddExternalSignatureConnectorPopupComponent, {
          injector: Injector.create(
            {
              providers: [
                {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_EDIT_MODE_KEY, useValue: true},
                {provide: AddExternalSignatureConnectorPopupComponent.INJECTABLE_CURRENT_EXTERNAL_SIGNATURE_CONF_KEY, useValue: server}
              ]
            })
        }
      )
      .result
      .then(
        () => this.requestExternalConfigurationList(),
        () => { /* Dismissed */ }
      );
  }


  deleteConnector(config: ExternalSignatureConfigDto): void {
    this.adminExternalSignatureService
      .deleteExternalSignatureConfig(this.tenantId, config.id)
      .subscribe(
        () => {
          this.notificationService.showSuccessMessage(ConnectorMessages.SUCCESS_DELETING_CONFIGURATION);
          this.requestExternalConfigurationList();
        },
        error => this.notificationService.showErrorMessage(ConnectorMessages.ERROR_DELETING_CONFIGURATION, error.message)
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }

  onRowOrderClicked(row: ExternalSignatureConfigSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.requestExternalConfigurationList();
  }
}
