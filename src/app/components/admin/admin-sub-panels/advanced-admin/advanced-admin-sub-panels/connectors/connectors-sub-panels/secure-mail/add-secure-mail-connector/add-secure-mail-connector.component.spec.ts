/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { AddSecureMailConnectorComponent } from './add-secure-mail-connector.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SecureMailServer } from '../../../../../../../../../models/securemail/secure-mail-server';

describe('AddSecureMailConnectorComponent', () => {

  let component: AddSecureMailConnectorComponent;
  let fixture: ComponentFixture<AddSecureMailConnectorComponent>;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: AddSecureMailConnectorComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenantId'},
          {provide: AddSecureMailConnectorComponent.INJECTABLE_EDIT_MODE_KEY, useValue: true},
          {provide: AddSecureMailConnectorComponent.INJECTABLE_CURRENT_SECURE_MAIL_SERVER_KEY, useValue: {} as SecureMailServer}
        ],
        declarations: [AddSecureMailConnectorComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AddSecureMailConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
