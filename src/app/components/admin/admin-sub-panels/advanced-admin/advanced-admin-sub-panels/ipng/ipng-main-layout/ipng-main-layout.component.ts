/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { AdminMessages } from '../../../../admin-messages';
import { IpngEntity } from '../../../../../../../models/ipng/ipng-entity';
import { CommonMessages } from '../../../../../../../shared/common-messages';
import { Router, ActivatedRoute } from '@angular/router';
import { IpngService } from '../../../../../../../services/ipng.service';
import { isNullOrUndefined, isNotNullOrUndefined } from '../../../../../../../utils/string-utils';
import { NotificationsService } from '../../../../../../../services/notifications.service';
import { IpngMessage } from '../ipng-message';

@Component({
  selector: 'app-ipng-main-layout',
  templateUrl: './ipng-main-layout.component.html',
  styleUrls: ['./ipng-main-layout.component.scss']
})
export class IpngMainLayoutComponent implements OnInit {

  readonly commonMessages = CommonMessages;
  readonly messages = AdminMessages;
  readonly ipngMessage = IpngMessage;
  readonly isNullOrUndefinedFn = isNullOrUndefined;
  readonly isNotNullNorUndefinedFn = isNotNullOrUndefined;
  selectedIpngEntity: IpngEntity;
  ipngEntityList: Array<IpngEntity> = [];
  tenantId: string;
  currentIpngEntityId: string;

  readonly menuRows = [
    {route: './health-state', label: 'Etat de santé'}
  ];

  readonly tenantMenuRows = [
    {route: 'deskboxes', label: 'Gestion des Deskbox'},
    {route: 'outgoing-typology', label: 'Liaison de typologies sortantes'},
    {route: 'incoming-typology', label: 'Liaison de typologies entrantes'},
    {route: 'metadata', label: 'Liaison des métadonnées'}
  ];


  constructor(private router: Router,
              private ipngService: IpngService,
              public notificationService: NotificationsService,
              public route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.parent.params.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.ipngService
        .getEntitiesManagedByTenant(this.tenantId)
        .subscribe((entities) => {
          this.ipngEntityList = entities;
          if (this.ipngEntityList && this.ipngEntityList.length > 0) {
            this.selectedIpngEntity = this.ipngEntityList[0]
            this.currentIpngEntityId = this.ipngEntityList[0].id
          } else {
            this.selectedIpngEntity = null;
            this.currentIpngEntityId = null;
          }
        }, () => {
          this.notificationService.showErrorMessage(this.ipngMessage.ERROR_LOADING_ENTITIES);
        });
    })

  }

  onIpngEntitySelectionChanged(entity: IpngEntity) {

    if (entity.id === this.currentIpngEntityId) {
      return;
    }

    this.selectedIpngEntity = entity;
    this.router
      .navigateByUrl(this.router.url.replace(`/${this.currentIpngEntityId}/`, `/${entity.id}/`))
      .then(() => this.currentIpngEntityId = entity.id);
  }

}
