/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector } from '@angular/core';
import { Deskbox, DeskboxAssociation } from '../../../../../../../../../models/ipng/deskbox';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { IpngService } from '../../../../../../../../../services/ipng.service';
import { AddDeskboxPopupComponent } from '../add-deskbox-popup/add-deskbox-popup.component';
import { LinkDeskboxPopupComponent } from '../link-deskbox-popup/link-deskbox-popup.component';
import { CommonIcons } from '@libriciel/ls-composants';
import { IpngMessage } from '../../../ipng-message';
import { faThumbtack, faUnlink } from '@fortawesome/free-solid-svg-icons';
import { combineLatest } from 'rxjs';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { DeskRepresentation } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-ipng-deskboxes',
  templateUrl: './ipng-deskboxes.component.html',
  styleUrls: ['./ipng-deskboxes.component.scss']
})
export class IpngDeskboxesComponent implements OnInit {


  readonly commonIcons = CommonIcons;
  readonly ipngMessage = IpngMessage;
  readonly commonMessages = CommonMessages;
  readonly unlinkIcon = faUnlink;
  readonly faThumbtack = faThumbtack;
  readonly pageSizes = [10, 15, 20, 50];


  deskboxList: {
    deskbox: Deskbox,
    linkedDesk: DeskRepresentation
  }[] = [];
  linkedDesks: DeskRepresentation[] = [];
  currentSearchTerm = '';
  page = 0;
  total = 0;
  pageSizeIndex = 1;
  tenantId: string;
  ipngEntityId: string;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              public modalService: NgbModal,
              public route: ActivatedRoute,
              public ipngService: IpngService) { }


  ngOnInit(): void {
    this.tenantId = this.route.snapshot.parent.parent.paramMap.get('tenantId');
    this.ipngEntityId = this.route.snapshot.paramMap.get('entityId');
    this.requestDeskboxList();
  }


  // </editor-fold desc="LifeCycle">


  requestDeskboxList() {

    const deskboxMappings$ = this.ipngService.getDeskboxesMapping(this.tenantId, this.ipngEntityId);
    const allDeskboxes$ = this.ipngService.getEntityDeskboxList(this.tenantId, this.ipngEntityId);
    combineLatest([allDeskboxes$, deskboxMappings$])
      .subscribe(res => {
        let deskboxes = res[0];
        let linkedDeskboxes: DeskboxAssociation[] = res[1];
        this.deskboxList = [];
        deskboxes.forEach(deskbox => {
          this.deskboxList.push({
            deskbox: deskbox,
            linkedDesk: linkedDeskboxes.find(asso => asso.ipngDeskbox.id === deskbox.id)?.tenantDesk
          });
        });
        this.total = this.deskboxList.length;
      });

  }


  onCreateButtonClicked() {
    this.modalService
      .open(AddDeskboxPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: AddDeskboxPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
              {provide: AddDeskboxPopupComponent.INJECTABLE_EDIT_MODE_KEY, useValue: false},
              {provide: AddDeskboxPopupComponent.INJECTABLE_DESKBOX_KEY, useValue: null}
            ]
          }),
          size: "xl"
        }
      )
      .result
      .then(
        () => this.requestDeskboxList(),
        () => { /* Dismissed */ }
      );
  }


  onLinkButtonClicked(deskbox: Deskbox) {
    this.modalService
      .open(LinkDeskboxPopupComponent, {
        injector: Injector.create({
          providers: [
            {provide: LinkDeskboxPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
            {provide: LinkDeskboxPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY, useValue: this.ipngEntityId},
            {provide: LinkDeskboxPopupComponent.INJECTABLE_DESKBOX_KEY, useValue: deskbox}
          ]
        }),
        size: "xl"
      })
      .result
      .then(
        () => this.requestDeskboxList(),
        () => { /* Dismissed */ }
      );
  }


  onUnlinkButtonClicked(obj: { deskbox: Deskbox, linkedDesk: DeskRepresentation }) {
    // TODO should have a popup before unlinking
    this.ipngService
      .unlinkDeskAndDeskbox(this.tenantId, this.ipngEntityId, obj.linkedDesk.id, obj.deskbox.id)
      .subscribe(
        () => this.requestDeskboxList(),
        error => this.notificationService.showErrorMessage(IpngMessage.ERROR_UNLINKING_DESKBOXES, error.message)
      );
  }


  onEditButtonClicked(deskbox: Deskbox): void {
    this.modalService
      .open(AddDeskboxPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: AddDeskboxPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
              {provide: AddDeskboxPopupComponent.INJECTABLE_EDIT_MODE_KEY, useValue: true},
              {provide: AddDeskboxPopupComponent.INJECTABLE_DESKBOX_KEY, useValue: deskbox}
            ]
          }),
          size: "xl"
        }
      )
      .result
      .then(
        () => this.requestDeskboxList(),
        () => { /* Dismissed */ }
      );
  }


  getPageSize(pageIndex: number): number {
    return this.pageSizes[pageIndex - 1];
  }


  setActive(deskbox: Deskbox, event: boolean): void {
    deskbox.active = event;
    this.ipngService
      .updateDeskbox(this.tenantId, deskbox)
      .subscribe(
        () => this.notificationService.showSuccessMessage(IpngMessage.DESKBOX_EDIT_SUCCESS),
        error => this.notificationService.showErrorMessage(IpngMessage.DESKBOX_EDIT_ERROR, error.message)
      );
  }


}
