/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { Metadata } from '../../../../../../../../../models/metadata';
import { IpngMetadata } from '../../../../../../../../../models/ipng/ipng-metadata';
import { faUnlink } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { IpngMessage } from '../../../ipng-message';
import { IpngService } from '../../../../../../../../../services/ipng.service';

@Component({
  selector: 'app-remove-metadata-link-popup',
  templateUrl: './remove-metadata-link-popup.component.html',
  styleUrls: ['./remove-metadata-link-popup.component.scss']
})
export class RemoveMetadataLinkPopupComponent {
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_IPNG_ENTITY_ID_KEY = 'ipngEntityId';
  public static readonly INJECTABLE_TENANT_METADATA_KEY = 'tenantMetadata';
  public static readonly INJECTABLE_IPNG_METADATA_KEY = 'ipngMetadata';
  readonly ipngMessage = IpngMessage;

  constructor(public activeModal: NgbActiveModal,
              public notificationService: NotificationsService,
              private ipngService: IpngService,
              @Inject(RemoveMetadataLinkPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(RemoveMetadataLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY) public ipngEntityId: string,
              @Inject(RemoveMetadataLinkPopupComponent.INJECTABLE_TENANT_METADATA_KEY) public tenantMetadata: Metadata,
              @Inject(RemoveMetadataLinkPopupComponent.INJECTABLE_IPNG_METADATA_KEY) public ipngMetadata: IpngMetadata) { }

  readonly commonIcons = CommonIcons;
  readonly faUnlink = faUnlink;
  isProcessing = false;

  validate() {
    this.isProcessing = true;

    this.ipngService.dissociateMetadata(this.tenantId, this.ipngEntityId, this.tenantMetadata.key).subscribe(res => {
      console.log("dissociateMetadata - result : ", res);
      this.notificationService.showSuccessMessage(this.ipngMessage.LINKS_SUCCESSFULLY_CREATED);
      this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
    }, () => {
      this.isProcessing = false;
      this.notificationService.showErrorMessage(this.ipngMessage.METADATA_ERROR_UNLINKING_METADATA);
    }).add(() => this.isProcessing = false);
  }

  onCancelButtonClicked() {
    this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL);
  }

  cancel() {
    this.onCancelButtonClicked();
  }
}
