/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { Deskbox } from '../../../../../../../../../models/ipng/deskbox';
import { IpngService } from '../../../../../../../../../services/ipng.service';
import { CommonIcons } from '@libriciel/ls-composants';
import { IpngMessage } from '../../../ipng-message';
import { IpngLocales } from '../../../../../../../../../models/ipng/ipng-locales.enum';

@Component({
  selector: 'app-add-deskbox-popup',
  templateUrl: './add-deskbox-popup.component.html',
  styleUrls: ['./add-deskbox-popup.component.scss']
})
export class AddDeskboxPopupComponent implements OnInit {

  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_EDIT_MODE_KEY = 'editMode';
  public static readonly INJECTABLE_DESKBOX_KEY = 'tenantId';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly ipngMessage = IpngMessage;
  readonly ipngLocales = IpngLocales;


  modalTitle: string = "";
  isFormValid = false;
  isProcessing = false;
  currentDeskbox: Deskbox = {
    id: undefined,
    mail: undefined,
    title: {},
    active: false,
    description: {},
    entityId: undefined,
    urlInstance: undefined
  };


  constructor(public notificationService: NotificationsService,
              private ipngService: IpngService,
              public activeModal: NgbActiveModal,
              @Inject(AddDeskboxPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(AddDeskboxPopupComponent.INJECTABLE_EDIT_MODE_KEY) public editMode: boolean,
              @Inject(AddDeskboxPopupComponent.INJECTABLE_DESKBOX_KEY) public deskBox: Deskbox) {}

  ngOnInit(): void {
    if (this.editMode === true) {
      this.currentDeskbox = this.deskBox;
    }
    this.setModalTitle()
  }

  onChange() {
    const isNameValid = (this.currentDeskbox.title[this.ipngLocales.DEFAULT] !== undefined) && (this.currentDeskbox.title[this.ipngLocales.DEFAULT].length > 1);
    const isMailValid = (this.currentDeskbox.mail !== undefined) && (this.currentDeskbox.mail.length > 1);
    const isDescriptionValid = (this.currentDeskbox.description[this.ipngLocales.DEFAULT] !== undefined) && (this.currentDeskbox.description[this.ipngLocales.DEFAULT].length > 1);
    this.isFormValid = isNameValid && isMailValid && isDescriptionValid;
  }

  validate() {

    if (!this.isFormValid) {
      return;
    }

    this.isProcessing = true;
    if (!this.editMode) {
      this.ipngService
        .createDeskbox(this.tenantId, this.currentDeskbox)
        .subscribe(() => {
            this.notificationService.showSuccessMessage(CrudOperation.Create)
            this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
          },
          error => this.notificationService.showErrorMessage(CrudOperation.Create, error.message)
        )
        .add(() => this.isProcessing = false);
    } else {
      this.ipngService
        .createDeskbox(this.tenantId, this.currentDeskbox)
        .subscribe(() => {
            this.notificationService.showSuccessMessage(CrudOperation.Update)
            this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
          },
          error => this.notificationService.showErrorMessage(CrudOperation.Update, error.message)
        )
        .add(() => this.isProcessing = false);
    }

  }

  setModalTitle(): void {
    this.modalTitle = this.editMode ? this.ipngMessage.UPDATE_DESKBOX_POPUP_TITLE : this.ipngMessage.ADD_DESKBOX_POPUP_TITLE;
  }


  onCancelButtonClicked() {
    this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL);
  }


  cancel() {
    this.onCancelButtonClicked();
  }

}
