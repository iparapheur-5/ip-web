/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkDeskboxPopupComponent } from './link-deskbox-popup.component';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Deskbox } from '../../../../../../../../../models/ipng/deskbox';

describe('LinkDeskboxPopupComponent', () => {


  let component: LinkDeskboxPopupComponent;
  let fixture: ComponentFixture<LinkDeskboxPopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: LinkDeskboxPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: '1234'},
          {provide: LinkDeskboxPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY, useValue: '4321'},
          {provide: LinkDeskboxPopupComponent.INJECTABLE_DESKBOX_KEY, useValue: new Deskbox()}
        ],
        declarations: [LinkDeskboxPopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(LinkDeskboxPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });


});
