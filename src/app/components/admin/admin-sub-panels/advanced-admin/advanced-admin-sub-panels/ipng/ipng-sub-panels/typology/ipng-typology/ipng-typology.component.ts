/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector } from '@angular/core';
import { IpngService } from '../../../../../../../../../services/ipng.service';
import { IpngType } from '../../../../../../../../../models/ipng/ipng-type';
import { ActivatedRoute } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { CommonIcons } from '@libriciel/ls-composants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddTypologyLinkPopupComponent } from '../add-typology-link-popup/add-typology-link-popup.component';
import { RemoveTypologyLinkPopupComponent } from '../remove-typology-link-popup/remove-typology-link-popup.component';
import { faUnlink } from '@fortawesome/free-solid-svg-icons';
import { IpngMessage } from '../../../ipng-message';
import { TypeAssociation } from '../../../../../../../../../models/ipng/ipng-typology';
import { TypologySortBy } from '@libriciel/iparapheur-standard';
import { map, catchError } from 'rxjs/operators';
import { PaginatedResult } from '../../../../../../../../../models/paginated-result';
import { TypologyEntity } from '../../../../../../../../../models/typology-entity';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { AdminTypologyService } from '@libriciel/iparapheur-provisioning';
import { AdminTypologyService as InternalAdminTypologyService } from '@libriciel/iparapheur-internal';

@Component({
  selector: 'app-ipng-typology',
  templateUrl: './ipng-typology.component.html',
  styleUrls: ['./ipng-typology.component.scss']
})
export class IpngTypologyComponent implements OnInit {


  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly commonIcons = CommonIcons;
  readonly faUnlink = faUnlink;
  readonly ipngMessage = IpngMessage;


  tenantId: string;
  ipngEntityId: string;
  isForIncomingTypes: boolean;
  page = 1;
  total = 0;
  pageSizeIndex = 1;
  tenantTypes: any[];
  ipngTypes: IpngType[];
  listData: TypeAssociation[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(private ipngService: IpngService,
              private route: ActivatedRoute,
              public modalService: NgbModal,
              public notificationsService: NotificationsService,
              private adminTypologyService: AdminTypologyService,
              private internalAdminTypologyService: InternalAdminTypologyService) { }


  ngOnInit(): void {
    const params$ = this.route.params;
    const parentParams$ = this.route.parent.parent.params;
    const data$ = this.route.data;
    combineLatest([params$, parentParams$, data$]).subscribe(result => {
      console.log('ngOnInit - result : ', result);
      this.ipngEntityId = result[0]['entityId'];
      this.tenantId = result[1]['tenantId'];
      this.isForIncomingTypes = result[2]['forIncomingTypes'];
      this.requestTypeAssociations();
    });
  }


  private requestTypes(): Observable<any> {
    let observableList: Observable<any>[] = []
    observableList.push(this.ipngService
      .getLatestTypologyList(this.tenantId));

    const pageNumber = this.page - 1;
    const pageSize = this.getPageSize(this.pageSizeIndex);

    observableList.push(
      this.internalAdminTypologyService
        .getTypologyHierarchy(this.tenantId, false, new Set<string>(), pageNumber, pageSize, [TypologySortBy.Name + ',ASC'])
        .pipe(
          map(page => {
            let paginatedResult = new PaginatedResult<TypologyEntity>();
            paginatedResult.data = page.content.map(representation => Object.assign(new TypologyEntity(), representation));
            paginatedResult.page = page.pageable.pageNumber;
            paginatedResult.pageSize = page.pageable.pageSize;
            paginatedResult.total = page.totalElements;
            return paginatedResult;
          }),
          catchError(this.notificationsService.handleHttpError('getTypologyHierarchy'))
        )
    );

    return combineLatest(observableList);
  }


  requestTypeAssociations(): void {
    if (this.isForIncomingTypes) {
      this.ipngService.getIncomingTypeMapping(this.tenantId, this.ipngEntityId).subscribe(associationList => {
        this.listData = associationList;
      })
    } else {
      this.ipngService.getOutgoingTypeMapping(this.tenantId, this.ipngEntityId).subscribe(associationList => {
        this.listData = associationList;
      })
    }
  }


  // </editor-fold desc="LifeCycle">


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  onCreateButtonClicked() {

    this.requestTypes().subscribe((data) => {
      this.ipngTypes = data[0].types;
      this.tenantTypes = data[1].data;

      this.modalService
        .open(AddTypologyLinkPopupComponent, {
            injector: Injector.create(
              {
                providers: [
                  {provide: AddTypologyLinkPopupComponent.INJECTABLE_IS_FOR_INCOMING_KEY, useValue: this.isForIncomingTypes},
                  {provide: AddTypologyLinkPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                  {provide: AddTypologyLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY, useValue: this.ipngEntityId},
                  {provide: AddTypologyLinkPopupComponent.INJECTABLE_TENANT_TYPES_KEY, useValue: this.tenantTypes},
                  {provide: AddTypologyLinkPopupComponent.INJECTABLE_IPNG_TYPES_KEY, useValue: this.ipngTypes},
                  {provide: AddTypologyLinkPopupComponent.INJECTABLE_CURRENT_LINKS_KEY, useValue: this.listData},
                ]
              }),
            size: "xl"
          }
        )
        .result
        .then(
          () => this.requestTypeAssociations(),
          () => { /* Dismissed */ }
        );
    });
  }


  onDissociateButtonClicked(data: TypeAssociation) {
    this.modalService
      .open(RemoveTypologyLinkPopupComponent, {
          injector: Injector.create(
            {
              providers: [
                {provide: AddTypologyLinkPopupComponent.INJECTABLE_IS_FOR_INCOMING_KEY, useValue: this.isForIncomingTypes},
                {provide: RemoveTypologyLinkPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: AddTypologyLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY, useValue: this.ipngEntityId},
                {provide: RemoveTypologyLinkPopupComponent.INJECTABLE_SUBTYPE_KEY, useValue: data.tenantType},
                {provide: RemoveTypologyLinkPopupComponent.INJECTABLE_IPNG_TYPE_KEY, useValue: data.ipngType},
              ]
            }),
          size: "lg"
        }
      )
      .result
      .then(
        () => this.requestTypeAssociations(),
        () => { /* Dismissed */ }
      );
  }


  formatTenantTypeText(data: TypeAssociation): string {
    return data.tenantType.name;
  }


}
