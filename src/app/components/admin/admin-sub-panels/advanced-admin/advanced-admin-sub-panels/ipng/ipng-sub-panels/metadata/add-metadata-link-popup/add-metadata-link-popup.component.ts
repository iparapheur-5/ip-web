/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Metadata } from '../../../../../../../../../models/metadata';
import { IpngMetadata, MetadataAssociation } from '../../../../../../../../../models/ipng/ipng-metadata';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { faThumbtack, faEye, faUnlink } from '@fortawesome/free-solid-svg-icons';
import { IpngMessage } from '../../../ipng-message';
import { IpngLocales } from '../../../../../../../../../models/ipng/ipng-locales.enum';
import { IpngService } from '../../../../../../../../../services/ipng.service';

@Component({
  selector: 'app-add-metadata-link-popup',
  templateUrl: './add-metadata-link-popup.component.html',
  styleUrls: ['./add-metadata-link-popup.component.scss']
})
export class AddMetadataLinkPopupComponent implements OnInit {

  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_IPNG_ENTITY_ID_KEY = 'ipngEntityId';
  public static readonly INJECTABLE_TENANT_METADATA_KEY = 'tenantMetadata';
  public static readonly INJECTABLE_IPNG_METADATA_KEY = 'ipngMetadata';
  public static readonly INJECTABLE_CURRENT_LINKS_KEY = 'currentLinks';

  readonly commonIcons = CommonIcons;
  readonly faThumbtack = faThumbtack;
  readonly faEye = faEye;
  readonly unlinkIcon = faUnlink;
  readonly ipngMessage = IpngMessage;
  readonly ipngLocales = IpngLocales;

  isProcessing: boolean = false;
  selectedIpngMetadata: IpngMetadata;
  selectedTenantMetadata: Metadata;
  sortedTenantMetadata: Metadata[] = [];
  sortedIpngMetadata: IpngMetadata[] = [];

  links: {
    tenantMetadata: Metadata,
    ipngMetadata: IpngMetadata
  }[] = [];

  constructor(public activeModal: NgbActiveModal,
              public notificationService: NotificationsService,
              private ipngService: IpngService,
              @Inject(AddMetadataLinkPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(AddMetadataLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY) public ipngEntityId: string,
              @Inject(AddMetadataLinkPopupComponent.INJECTABLE_TENANT_METADATA_KEY) public tenantMetadata: Metadata[],
              @Inject(AddMetadataLinkPopupComponent.INJECTABLE_IPNG_METADATA_KEY) public ipngMetadata: IpngMetadata[],
              @Inject(AddMetadataLinkPopupComponent.INJECTABLE_CURRENT_LINKS_KEY) public previousLinks: MetadataAssociation[]) { }

  ngOnInit(): void {
    console.log('ngOnInit - tenantMetadata : ', this.tenantMetadata);
    console.log('ngOnInit - ipngMetadata : ', this.ipngMetadata);
    const alreadyLinkedTenantMetadataKeys: string[] = this.previousLinks.map(ta => ta.tenantMetadata.key);
    this.tenantMetadata = this.tenantMetadata.filter(metadata => !alreadyLinkedTenantMetadataKeys.includes(metadata.key));

    this.sortLists();
  }

  sortLists(): void {
    if (this.selectedIpngMetadata && this.selectedTenantMetadata) {
      this.sortedTenantMetadata = [this.selectedTenantMetadata].concat(this.tenantMetadata.filter(tenantMetadata => tenantMetadata.id !== this.selectedTenantMetadata.id));
      this.sortedIpngMetadata = [this.selectedIpngMetadata].concat(this.ipngMetadata.filter(ipngMetadata => ipngMetadata.key !== this.selectedIpngMetadata.key));
    } else {
      this.sortedTenantMetadata = this.tenantMetadata.sort();
      this.sortedIpngMetadata = this.ipngMetadata.sort();
    }
  }

  isTenantMetadataSelected(metadata: Metadata): boolean {
    return this.selectedTenantMetadata && metadata.id === this.selectedTenantMetadata.id;

  }

  isIpngMetadataSelected(ipngMetadata: IpngMetadata): boolean {
    return this.selectedIpngMetadata && ipngMetadata.key === this.selectedIpngMetadata.key;
  }

  selectTenantMetadata(metadata: Metadata): void {
    if (this.selectedTenantMetadata && this.selectedTenantMetadata.id === metadata.id) {
      this.selectedTenantMetadata = null;
    } else {
      this.selectedTenantMetadata = metadata;
    }
    this.sortLists();
  }

  selectIpngMetadata(metadata: IpngMetadata): void {
    if (this.selectedIpngMetadata && this.selectedIpngMetadata.key === metadata.key) {
      this.selectedIpngMetadata = null;
    } else {
      this.selectedIpngMetadata = metadata;
    }
    this.sortLists();
  }

  canAddLink(): boolean {
    return !!(this.selectedIpngMetadata && this.selectedTenantMetadata);
  }

  addLink(): void {
    if (this.canAddLink()) {
      this.links.push({
        tenantMetadata: this.selectedTenantMetadata,
        ipngMetadata: this.selectedIpngMetadata
      });
      this.selectedTenantMetadata = null;
      this.selectedIpngMetadata = null;
      this.sortLists();
    }
  }

  removeLink(index: number): void {
    this.links.splice(index, 1);
  }

  validate(): void {
    this.isProcessing = true;

    this.ipngService.addMetadataMapping(this.tenantId, this.ipngEntityId, this.links).subscribe(res => {
      console.log("addMetadataMapping - got result : ", res);
      this.notificationService.showSuccessMessage(this.ipngMessage.LINKS_SUCCESSFULLY_CREATED);
      this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
    }, () => {
      this.notificationService.showErrorMessage(this.ipngMessage.METADATA_ERROR_LINKING_METADATA);
    }).add(() => this.isProcessing = false);
  }

  updateSearchTerm(newTerm: string) {
    if (!!newTerm) {
      const lowercaseNewTerm = newTerm.toLowerCase();
      this.sortedIpngMetadata = this.sortedIpngMetadata.filter(value => {
        for (let ipngLocalesKey in this.ipngLocales) {
          const name = value.name[this.ipngLocales[ipngLocalesKey]]
          if (name && name.toLowerCase().includes(lowercaseNewTerm)) {
            return value;
          }
        }
      });

      this.sortedTenantMetadata = this.tenantMetadata.filter(value => {
        if (value.name.toLowerCase().includes(lowercaseNewTerm)) {
          return value;
        }
      });
    } else {
      if (this.sortedTenantMetadata.length !== this.tenantMetadata.length) {
        this.sortedTenantMetadata = this.tenantMetadata.sort();
      }
      if (this.sortedIpngMetadata.length !== this.ipngMetadata.length) {
        this.sortedIpngMetadata = this.ipngMetadata.sort();
      }
    }
  }

  onCancelButtonClicked() {
    this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL);
  }


  cancel() {
    this.onCancelButtonClicked();
  }
}
