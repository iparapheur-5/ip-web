/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { ActivatedRoute } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { IpngService } from '../../../../../../../../../services/ipng.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IpngMetadata, MetadataAssociation } from '../../../../../../../../../models/ipng/ipng-metadata';
import { Metadata } from '../../../../../../../../../models/metadata';
import { AddMetadataLinkPopupComponent } from '../add-metadata-link-popup/add-metadata-link-popup.component';
import { RemoveMetadataLinkPopupComponent } from '../remove-metadata-link-popup/remove-metadata-link-popup.component';
import { faUnlink } from '@fortawesome/free-solid-svg-icons';
import { IpngMessage } from '../../../ipng-message';
import { AdminMetadataService, MetadataSortBy } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';

@Component({
  selector: 'app-ipng-metadata',
  templateUrl: './ipng-metadata.component.html',
  styleUrls: ['./ipng-metadata.component.scss']
})
export class IpngMetadataComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50, 100];

  readonly commonIcons = CommonIcons;
  readonly unlinkIcon = faUnlink;
  readonly ipngMessage = IpngMessage;

  tenantId: string;
  ipngEntityId: string;
  page = 1;
  total = 0;
  pageSizeIndex = 1;
  // tenantMetadata: Metadata[];
  // ipngMetadata: IpngMetadata[];
  listData: MetadataAssociation[] = [];


  constructor(private ipngService: IpngService,
              private notificationsService: NotificationsService,
              private route: ActivatedRoute,
              public modalService: NgbModal,
              private adminMetadataService: AdminMetadataService) { }


  ngOnInit(): void {
    const params$ = this.route.params;
    const parentParams$ = this.route.parent.parent.params;
    combineLatest([params$, parentParams$])
      .subscribe(result => {
        console.log('ngOnInit - result : ', result);
        this.ipngEntityId = result[0]['entityId'];
        this.tenantId = result[1]['tenantId'];
        this.requestMetadataAssociations();
      });
  }


  requestMetadata(): Observable<any> {
    let observableList: Observable<any>[] = []
    observableList.push(this.ipngService
      .getLatestMetadataList(this.tenantId));

    observableList.push(
      this.adminMetadataService
        .listMetadataAsAdmin(
          this.tenantId,
          false,
          null,
          this.page - 1,
          this.getPageSize(this.pageSizeIndex),
          [MetadataSortBy.Key + ",ASC"]
        )
        .pipe(catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin')))
    );

    return combineLatest(observableList);

  }


  requestMetadataAssociations() {
    this.ipngService.getMetadataMapping(this.tenantId, this.ipngEntityId).subscribe(associations => {
      this.listData = associations;
    });
  }


  onCreateButtonClicked() {
    this.requestMetadata().subscribe(combinedData => {
      const ipngMetadata: IpngMetadata[] = combinedData[0].metadatas;
      const tenantMetadata: Metadata[] = combinedData[1].data;

      this.modalService
        .open(AddMetadataLinkPopupComponent, {
            injector: Injector.create(
              {
                providers: [
                  {provide: AddMetadataLinkPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                  {provide: AddMetadataLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY, useValue: this.ipngEntityId},
                  {provide: AddMetadataLinkPopupComponent.INJECTABLE_TENANT_METADATA_KEY, useValue: tenantMetadata},
                  {provide: AddMetadataLinkPopupComponent.INJECTABLE_IPNG_METADATA_KEY, useValue: ipngMetadata},
                  {provide: AddMetadataLinkPopupComponent.INJECTABLE_CURRENT_LINKS_KEY, useValue: this.listData},
                ]
              }),
            size: "xl"
          }
        )
        .result
        .then(
          () => this.requestMetadataAssociations(),
          () => { /* Dismissed */ }
        );
    });
  }


  onDissociateButtonClicked(data: {
    tenantMetadata: Metadata,
    ipngMetadata: IpngMetadata
  }) {
    this.modalService
      .open(RemoveMetadataLinkPopupComponent, {
          injector: Injector.create(
            {
              providers: [

                {provide: RemoveMetadataLinkPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: RemoveMetadataLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY, useValue: this.ipngEntityId},
                {provide: RemoveMetadataLinkPopupComponent.INJECTABLE_TENANT_METADATA_KEY, useValue: data.tenantMetadata},
                {provide: RemoveMetadataLinkPopupComponent.INJECTABLE_IPNG_METADATA_KEY, useValue: data.ipngMetadata},
              ]
            }),
          size: "lg"
        }
      )
      .result
      .then(
        () => this.requestMetadataAssociations(),
        () => { /* Dismissed */ }
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


}
