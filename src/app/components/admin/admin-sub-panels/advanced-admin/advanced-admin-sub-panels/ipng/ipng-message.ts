/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class IpngMessage {

  // Entities

  static readonly ERROR_LOADING_ENTITIES = 'Erreur pendant le chargement des entités IPNG';

  // Deskboxes

  static readonly UPDATE_DESKBOX_POPUP_TITLE = 'Modification de la deskbox';
  static readonly ADD_DESKBOX_POPUP_TITLE = `Ajout d'une deskbox`;
  static readonly ADD_DESKBOX_BUTTON_TITLE = 'Ajouter une deskbox';
  static readonly DESKBOX_LIST_TITLE = 'Liste des Deskbox';
  static readonly DESKBOX_NAME = 'Deskbox';
  static readonly LINKED_DESK = 'Bureau lié';
  static readonly ADD_DESKBOX_POPUP_TITLE_FIELD_UNIQUE_RESTRICTION = 'Le titre doit être unique';
  static readonly ADD_DESKBOX_POPUP_MAIL_FIELD = 'Adresse mail';
  static readonly ADD_DESKBOX_POPUP_DESCRIPTION_FIELD = 'Description';
  static readonly LINK_DESKBOX_POPUP_TITLE = 'Gestion des liaisons de'
  static readonly LINK_DESKBOX_LINK_DESK_TITLE = 'Lier un bureau à';
  static readonly ERROR_LINKING_DESKBOXES = 'Erreur lors de la liaison Bureau <-> Deskbox';
  static readonly ERROR_UNLINKING_DESKBOXES = 'Erreur lors de la suppression de la liaison Bureau <-> Deskbox';
  static readonly DESKBOX_EDIT_SUCCESS = `Erreur lors de l'édition de la Deskbox`;
  static readonly DESKBOX_EDIT_ERROR = 'Deskbox modifiée avec succès.';

  // Health state

  static readonly HEALTH_STATE_TITLE = 'Etat de santé';
  static readonly OPEN_NEW_MONITORING_WINDOW_BUTTON = 'ouvre une nouvelle fenêtre de monitoring';

  // Metadata

  static readonly METADATA_LIST_TITLE = 'Liaisons de métadonnées';
  static readonly METADATA_TABLE_TITLE_METADATA_IPARAPHEUR = 'Métadonnée iparapheur';
  static readonly METADATA_TABLE_TITLE_METADATA_IPNG = 'Métadonnée ipng';
  static readonly METADATA_DISSOCIATE_LINK_TITLE = 'Dissociation des métadonnées';
  static readonly METADATA_DISSOCIATE_ARE_YOU_SURE = 'Êtes-vous sûr de vouloir dissocier ces deux métadonnées ?';
  static readonly METADATA_SEARCH_BAR_TITLE = 'Rechercher une métadonnée';
  static readonly METADATA_ERROR_LINKING_METADATA = 'Erreur lors de la liaison des métadonnées';
  static readonly METADATA_ERROR_UNLINKING_METADATA = 'Erreur lors de la dissociation des métadonnées';

  // Typology

  static readonly OUTGOING_TYPOLOGY_LIST_TITLE = 'Liaisons de typologies sortantes';
  static readonly INCOMING_TYPOLOGY_LIST_TITLE = 'Liaisons de typologies entrantes';
  static readonly TYPOLOGY_DISSOCIATE_LINK_TITLE = 'Dissociation des types';
  static readonly TYPOLOGY_DISSOCIATE_ARE_YOU_SURE = 'Êtes-vous sûr de vouloir dissocier ces deux types ?';
  static readonly TYPOLOGY_SEARCH_BAR_TITLE = 'Rechercher un type';
  static readonly TYPOLOGY_ERROR_LINKING_TYPES = 'Erreur lors de la liaison des types';
  static readonly TYPOLOGY_ERROR_UNLINKING_TYPES = 'Erreur lors de la dissociation des types';

  // Metadata + Typology

  static readonly ADD_OUTGOING_LINK_POPUP_TITLE = 'Ajout de liaisons sortantes';
  static readonly ADD_INCOMING_LINK_POPUP_TITLE = 'Ajout de liaisons entrantes';
  static readonly ADD_LINK_POPUP_TITLE = 'Ajout de liaison';
  static readonly ADD_LINK_POPUP_TABLE_TITLE_LINKS = 'Liaisons';
  static readonly ADD_LINK_POPUP_TABLE_SUBTITLE_ACTION = 'Action';
  static readonly ADD_LINK_BUTTON_TITLE = 'Ajouter la liaison';
  static readonly TABLE_NO_LINKS = 'Aucune liaison';
  static readonly TABLE_SUBTITLE_IPARAPHEUR_SUBTYPE = 'Sous-type iparapheur';
  static readonly TABLE_SUBTITLE_IPNG_TYPE = 'Type ipng';
  static readonly CREATE_LINK_BUTTON_TITLE = 'Ajouter des liaisons';
  static readonly DISSOCIATE_BUTTON = 'Dissocier';
  static readonly LINKS_SUCCESSFULLY_CREATED = 'Liens créés avec succès.';

}
