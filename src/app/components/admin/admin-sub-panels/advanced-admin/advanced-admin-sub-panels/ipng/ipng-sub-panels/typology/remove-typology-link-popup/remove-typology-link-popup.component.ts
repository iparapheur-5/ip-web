/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { IpngType } from '../../../../../../../../../models/ipng/ipng-type';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { faUnlink } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { IpngMessage } from '../../../ipng-message';
import { IpngService } from '../../../../../../../../../services/ipng.service';
import { SubtypeDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-remove-typology-link-popup',
  templateUrl: './remove-typology-link-popup.component.html',
  styleUrls: ['./remove-typology-link-popup.component.scss']
})
export class RemoveTypologyLinkPopupComponent {


  public static readonly INJECTABLE_IS_FOR_INCOMING_KEY = 'isForIncomingTypes';
  public static readonly INJECTABLE_SUBTYPE_KEY = 'subtype';
  public static readonly INJECTABLE_IPNG_TYPE_KEY = 'ipngType';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_IPNG_ENTITY_ID_KEY = 'ipngEntityId';


  readonly faUnlink = faUnlink;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly ipngMessage = IpngMessage;

  isProcessing = false;


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              public ipngService: IpngService,
              public notificationService: NotificationsService,
              @Inject(RemoveTypologyLinkPopupComponent.INJECTABLE_IS_FOR_INCOMING_KEY) public isForIncomingTypes: boolean,
              @Inject(RemoveTypologyLinkPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(RemoveTypologyLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY) public ipngEntityId: string,
              @Inject(RemoveTypologyLinkPopupComponent.INJECTABLE_SUBTYPE_KEY) public subtype: SubtypeDto,
              @Inject(RemoveTypologyLinkPopupComponent.INJECTABLE_IPNG_TYPE_KEY) public ipngType: IpngType) { }


  // </editor-fold desc="LifeCycle">


  formatTenantTypeText(): string {
    return this.subtype.name;
  }


  validate() {
    this.isProcessing = true;
    const deleteMethod$ = this.isForIncomingTypes
      ? this.ipngService.dissociateIncomingTypes(this.tenantId, this.ipngEntityId, this.ipngType.key)
      : this.ipngService.dissociateOutgoingTypes(this.tenantId, this.ipngEntityId, this.subtype.id);

    deleteMethod$
      .subscribe(
        () => {
          this.notificationService.showSuccessMessage(this.ipngMessage.LINKS_SUCCESSFULLY_CREATED);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => {
          this.notificationService.showErrorMessage(this.ipngMessage.TYPOLOGY_ERROR_UNLINKING_TYPES, error.message);
        })
      .add(() => this.isProcessing = false);
  }


}
