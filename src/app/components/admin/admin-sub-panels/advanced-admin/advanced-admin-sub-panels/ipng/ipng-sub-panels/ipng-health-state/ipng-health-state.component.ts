/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { IpngService } from '../../../../../../../../services/ipng.service';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons } from '@libriciel/ls-composants';
import { faCheck, faRedo } from '@fortawesome/free-solid-svg-icons';
import { IpngMessage } from '../../ipng-message';

@Component({
  selector: 'app-ipng-health-state',
  templateUrl: './ipng-health-state.component.html',
  styleUrls: ['./ipng-health-state.component.scss']
})
export class IpngHealthStateComponent implements OnInit {


  readonly commonIcons = CommonIcons;
  readonly faCheck = faCheck;
  readonly faRedo = faRedo;
  readonly ipngMessage = IpngMessage;

  healthStates: any[];


  // <editor-fold desc="LifeCycle">


  constructor(private ipngService: IpngService,
              private route: ActivatedRoute) { }


  ngOnInit(): void {
    this.route.parent.parent.params.subscribe(params => {
      // TODO when we have a healthState
      // this.ipngService
      //   .getHealthStates(tenantId)
      //   .subscribe(healthStates => this.healthStates = healthStates)
    });
  }


  // </editor-fold desc="LifeCycle">


}
