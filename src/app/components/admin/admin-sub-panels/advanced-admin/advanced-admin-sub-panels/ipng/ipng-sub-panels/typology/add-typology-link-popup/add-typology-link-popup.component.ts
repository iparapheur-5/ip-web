/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { IpngType } from '../../../../../../../../../models/ipng/ipng-type';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { faThumbtack, faEye, faUnlink } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { IpngMessage } from '../../../ipng-message';
import { IpngLocales } from '../../../../../../../../../models/ipng/ipng-locales.enum';
import { IpngService } from '../../../../../../../../../services/ipng.service';
import { TypeAssociation } from '../../../../../../../../../models/ipng/ipng-typology';
import { SubtypeDto } from '@libriciel/iparapheur-standard';

export type QualifiedNamedSubtype = SubtypeDto & { parentTypeName: string };

@Component({
  selector: 'app-add-typology-link-popup',
  templateUrl: './add-typology-link-popup.component.html',
  styleUrls: ['./add-typology-link-popup.component.scss']
})
export class AddTypologyLinkPopupComponent implements OnInit {

  public static readonly INJECTABLE_IS_FOR_INCOMING_KEY = 'isForIncomingTypes';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_IPNG_ENTITY_ID_KEY = 'ipngEntityId';
  public static readonly INJECTABLE_TENANT_TYPES_KEY = 'tenantTypes';
  public static readonly INJECTABLE_IPNG_TYPES_KEY = 'ipngTypes';
  public static readonly INJECTABLE_CURRENT_LINKS_KEY = 'currentLinks';


  readonly commonIcons = CommonIcons;
  readonly faThumbtack = faThumbtack;
  readonly faEye = faEye;
  readonly unlinkIcon = faUnlink;
  readonly ipngMessage = IpngMessage;
  readonly ipngLocales = IpngLocales;

  isProcessing = false;
  // sortedTenantSubtypes: Subtype[];
  allTenantSubtypes: QualifiedNamedSubtype[] = [];
  sortedTenantSubtypes: QualifiedNamedSubtype[];
  searchTerm = '';
  sortedIpngTypes: IpngType[];
  selectedIpngType: IpngType;
  selectedSubtype: QualifiedNamedSubtype;
  links: TypeAssociation[] = [];


  constructor(public activeModal: NgbActiveModal,
              private ipngService: IpngService,
              public notificationService: NotificationsService,
              @Inject(AddTypologyLinkPopupComponent.INJECTABLE_IS_FOR_INCOMING_KEY) public isForIncomingTypes: boolean,
              @Inject(AddTypologyLinkPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(AddTypologyLinkPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY) public ipngEntityId: string,
              @Inject(AddTypologyLinkPopupComponent.INJECTABLE_TENANT_TYPES_KEY) public tenantTypes: any[],
              @Inject(AddTypologyLinkPopupComponent.INJECTABLE_IPNG_TYPES_KEY) public ipngTypes: IpngType[],
              @Inject(AddTypologyLinkPopupComponent.INJECTABLE_CURRENT_LINKS_KEY) public previousLinks: TypeAssociation[]) { }

  ngOnInit(): void {
    //initialize subtype list
    const types = this.tenantTypes.filter(type => !type.parentId);
    this.allTenantSubtypes = this.tenantTypes.filter(subtype => !!subtype.parentId)
      .map(subtype => {
        const res = subtype as QualifiedNamedSubtype;
        let parentType = types.find(t => t.id === subtype.parentId);
        if (parentType) {
          res.parentTypeName = parentType.name;
        }
        return res;
      });

    if (this.isForIncomingTypes) {
      const alreadyLinkedIpngTypesKeys: string[] = this.previousLinks.map(ta => ta.ipngType.key);
      this.ipngTypes = this.ipngTypes.filter(ipngType => !alreadyLinkedIpngTypesKeys.includes(ipngType.key));

    } else {
      const alreadyLinkedSubtypesIds: string[] = this.previousLinks.map(ta => ta.tenantType.id);
      this.allTenantSubtypes = this.allTenantSubtypes.filter(subtype => !alreadyLinkedSubtypesIds.includes(subtype.id));
    }


    this.sortLists();
  }

  sortLists(): void {

    if (this.selectedIpngType && this.selectedSubtype) {
      this.sortedTenantSubtypes = [this.selectedSubtype].concat(this.getFilteredSubtypes().filter(subtype => subtype.id !== this.selectedSubtype.id))
      this.sortedIpngTypes = [this.selectedIpngType].concat(this.getFilteredIpngTypes().filter(ipngType => ipngType.key !== this.selectedIpngType.key))
    } else {
      this.sortedTenantSubtypes = this.getFilteredSubtypes().sort();
      this.sortedIpngTypes = this.getFilteredIpngTypes().sort();
    }
  }

  private getFilteredSubtypes(): QualifiedNamedSubtype[] {
    let filteredSubtypes;

    // We only filter out already linked tenantSubtypes on outgoing mapping
    if (!this.isForIncomingTypes) {
      const linkedSubtypeIds = this.links.map(l => l.tenantType.id);
      filteredSubtypes = this.allTenantSubtypes.filter(subtype => !linkedSubtypeIds.includes(subtype.id));
    } else {
      filteredSubtypes = [...this.allTenantSubtypes];
    }


    if (!!this.searchTerm) {
      filteredSubtypes = filteredSubtypes.filter(subtype =>
        this.formatTenantTypeText(subtype).toLowerCase().includes(this.searchTerm)
      );
    }

    return filteredSubtypes;
  }

  private getFilteredIpngTypes(): IpngType[] {
    let filteredIpngTypes;

    // We only filter out already linked ipngTypes on incoming mapping
    if (this.isForIncomingTypes) {
      const linkedIpngTypeKeys = this.links.map(l => l.ipngType.key);
      filteredIpngTypes = this.ipngTypes.filter(ipngType => !linkedIpngTypeKeys.includes(ipngType.key));
    } else {
      filteredIpngTypes = [...this.ipngTypes];
    }


    if (!!this.searchTerm) {
      // TODO either this goes away (handling a current locale), or it must be rewritten to be more elegant
      filteredIpngTypes = filteredIpngTypes.filter(value => {
        for (let ipngLocalesKey in this.ipngLocales) {
          const name = value.name[this.ipngLocales[ipngLocalesKey]]
          if (name && name.toLowerCase().includes(this.searchTerm)) {
            return true;
          }
        }
      });
    }

    return filteredIpngTypes
  }


  isSubtypeSelected(type: SubtypeDto): boolean {
    return this.selectedSubtype && type.id === this.selectedSubtype.id;
  }

  isIpngTypeSelected(type: IpngType): boolean {
    return this.selectedIpngType && type.key === this.selectedIpngType.key;
  }


  formatTenantTypeText(subtype: QualifiedNamedSubtype): string {
    return subtype.parentTypeName + " - " + subtype.name;
  }

  selectSubtype(type: QualifiedNamedSubtype): void {
    if (this.selectedSubtype && this.selectedSubtype.id === type.id) {
      this.selectedSubtype = null;
    } else {
      this.selectedSubtype = type;
    }
    this.sortLists();
  }

  selectIpngType(type: IpngType): void {
    if (this.selectedIpngType && this.selectedIpngType.key === type.key) {
      this.selectedIpngType = null;
    } else {
      this.selectedIpngType = type;
    }
    this.sortLists();
  }

  canAddLink(): boolean {
    return !!(this.selectedIpngType && this.selectedSubtype);
  }

  addLink(): void {
    if (this.canAddLink()) {
      this.links.push({
        tenantType: this.selectedSubtype,
        ipngType: this.selectedIpngType
      });
      this.selectedSubtype = null;
      this.selectedIpngType = null;
      this.sortLists();
    }
  }

  removeLink(index: number): void {
    this.links.splice(index, 1);
    this.sortLists();
  }

  validate(): void {
    this.isProcessing = true;

    const creationMethod$ = this.isForIncomingTypes
      ? this.ipngService.addIncomingTypeMapping(this.tenantId, this.ipngEntityId, this.links)
      : this.ipngService.addOutgoingTypeMapping(this.tenantId, this.ipngEntityId, this.links);

    creationMethod$.subscribe(() => {
      this.notificationService.showSuccessMessage(this.ipngMessage.LINKS_SUCCESSFULLY_CREATED)
      this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
    }, () => {
      this.notificationService.showErrorMessage(this.ipngMessage.TYPOLOGY_ERROR_LINKING_TYPES);
    }).add(() => this.isProcessing = false);
  }

  updateSearchTerm(newTerm: string) {
    this.searchTerm = newTerm?.toLowerCase() || '';
    this.sortLists();
  }

  onCancelButtonClicked() {
    this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL);
  }

  cancel() {
    this.onCancelButtonClicked();
  }
}
