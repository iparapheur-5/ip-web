/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, Input, ViewChild } from '@angular/core';
import { AbsencesMessages } from '../absences-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { Delegation } from '../../../../../models/auth/delegation';
import { NotificationsService } from '../../../../../services/notifications.service';
import { AbsenceManagementComponent } from '../absence-management/absence-management.component';
import { Observable } from 'rxjs';
import { DeskService, AdminDeskService } from '@libriciel/iparapheur-standard';
import { DeskDto } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-absence-creation-popup',
  templateUrl: './absence-popup.component.html',
  styleUrls: ['./absence-popup.component.scss']
})
export class AbsencePopupComponent {


  public static readonly INJECTABLE_DESK_KEY = 'desk';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_AS_ADMIN_KEY = 'asAdmin';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = AbsencesMessages;

  @Input() currentScheduledAbsences: Delegation[] = [];
  @ViewChild('absenceManagementComponent') absenceManagementComponent: AbsenceManagementComponent;

  isProcessing: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(protected adminDeskService: AdminDeskService,
              protected deskService: DeskService,
              public activeModal: NgbActiveModal,
              protected notificationsService: NotificationsService,
              @Inject(AbsencePopupComponent.INJECTABLE_DESK_KEY) public desk: DeskDto,
              @Inject(AbsencePopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(AbsencePopupComponent.INJECTABLE_AS_ADMIN_KEY) public asAdmin: boolean) {}


  // </editor-fold desc="LifeCycle">


  validate() {

    if (this.absenceManagementComponent?.selectedSubstituteDesks?.length === 0) {
      return;
    }

    const absenceToSetup = new Delegation();
    absenceToSetup.substituteDeskId = this.absenceManagementComponent.selectedSubstituteDesks[0].id;
    absenceToSetup.delegatingDeskId = this.desk.id;
    absenceToSetup.typeId = this.absenceManagementComponent.type?.id;
    absenceToSetup.subtypeId = this.absenceManagementComponent.subtype?.id;
    absenceToSetup.start = this.absenceManagementComponent.start;
    absenceToSetup.end = this.absenceManagementComponent.end;

    this.isProcessing = true;
    let addDelegationRequest$: Observable<void>;
    if (this.asAdmin) {
      addDelegationRequest$ = this.adminDeskService.createDelegationAsAdmin(this.tenantId, this.desk.id, absenceToSetup);
    } else {
      addDelegationRequest$ = this.deskService.createDelegation(this.tenantId, this.desk.id, absenceToSetup);
    }

    addDelegationRequest$.subscribe(
      () => {
        this.notificationsService.showSuccessMessage(AbsencesMessages.DELEGATION_ADD_SUCCESS);
        this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
      },
      error => {
        this.notificationsService.showErrorMessage(AbsencesMessages.DELEGATION_ADD_ERROR, error);
      }
    )
      .add(() => this.isProcessing = false);

  }


  isValid(): boolean {
    const hasPickedSomeDesk = this.absenceManagementComponent?.selectedSubstituteDesks?.length !== 0;
    const hasPickedSomeStartDate = !!this.absenceManagementComponent?.start;
    return hasPickedSomeDesk && hasPickedSomeStartDate;
  }


}
