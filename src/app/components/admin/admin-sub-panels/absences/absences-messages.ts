/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class AbsencesMessages {

  // List
  static readonly LIST_TITLE = 'Gestion des absences';
  static readonly CREATE_ABSENCE_BUTTON_TITLE = 'Déclarer une absence';

  static readonly ACTIONS = 'Actions';

  static readonly BEGINNING = 'Début';
  static readonly END = 'Fin';
  static readonly REPLACEMENT_DESK = 'Bureau remplaçant';
  static readonly TYPOLOGY = 'Typologie';

  static readonly SELECT_DESK_INFO_TEXT = `Sélectionnez d'abord le bureau dont vous voulez gérer les absences`;

  static readonly DELEGATIONS_GET_ERROR = 'Erreur à la récupération des absences';
  static readonly DELEGATION_ADD_SUCCESS = `L'absence a été déclarée avec succès`;
  static readonly DELEGATION_ADD_ERROR = `Erreur lors de la déclaration de l'absence`;
  static readonly DELEGATION_DELETE_SUCCESS = `L'absence a été supprimée avec succès`;
  static readonly DELEGATION_DELETE_ERROR = `Erreur à la suppression de l'absence`;

}
