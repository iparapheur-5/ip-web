/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAbsencesMenuComponent } from './admin-absences-menu.component';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ColumnNamePipe } from '../../../../../utils/column-name.pipe';
import { ToastrModule } from 'ngx-toastr';
import { KeycloakService } from 'keycloak-angular';

describe('AdminAbsencesMenuComponent', () => {


  let component: AdminAbsencesMenuComponent;
  let fixture: ComponentFixture<AdminAbsencesMenuComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        declarations: [ColumnNamePipe, AdminAbsencesMenuComponent],
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [HttpClient, HttpHandler, KeycloakService]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAbsencesMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
