/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DeskMessages } from '../../desk/desk-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { Delegation } from '../../../../../models/auth/delegation';
import { DeskService as LegacyDeskService } from '../../../../../services/ip-core/desk.service';
import { TypologyService, PageTypeRepresentation, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { DeskService, PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-internal';
import { DeskDto, PageSubtypeRepresentation, SubtypeRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-absence-management',
  templateUrl: './absence-management.component.html',
  styleUrls: ['./absence-management.component.scss']
})
export class AbsenceManagementComponent implements OnInit {

  readonly commonMessages = CommonMessages;
  readonly messages = DeskMessages;

  @Input() desk: DeskDto;
  @Input() tenantId: string;
  @Input() forbiddenDateRanges: Delegation[] = [];

  // @ViewChild('typeSelectorComponent') typeSelectorComponent: TypeSelectorComponent;

  start: Date = new Date();
  end: Date = null;
  type: TypeRepresentation = null;
  subtype: SubtypeRepresentation = null;
  label = '';
  isProcessing = false;
  selectedSubstituteDesks: DeskRepresentation[] | { id: string, name: string }[] = [];

  retrieveTypesFn = (page: number, pageSize: number) => this.retrieveTypes(page, pageSize);
  retrieveSubtypesFn = (page: number, pageSize: number) => this.retrieveSubtypes(page, pageSize);

  // <editor-fold desc="LifeCycle">


  constructor(private legacyDeskService: LegacyDeskService,
              private deskService: DeskService,
              private typologyService: TypologyService) {}


  ngOnInit(): void {
    this.retrieveAssociatedDesksFn(0, 20, '');
  }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="FormGroup">

  // TODO : a proper formGroup

  // readonly typeValidator = (): ValidatorFn => (this.subtype) ? Validators.required : null;
  //
  //
  // readonly startValidator = (): ValidatorFn => (this.end) ? Validators.max(this.end.getUTCDate()) : null;
  //
  //
  // readonly endValidator = (): ValidatorFn => (this.start) ? Validators.min(this.start.getUTCDate()) : null;
  //
  //
  // readonly isConflictingDate = (date: Date): boolean =>
  //   this.forbiddenDateRanges
  //     .filter(existing => (existing.type?.id === this.type?.id))
  //     .filter(existing => (existing.subtype?.id === this.subtype?.id))
  //     .some(existing => dayjs(date).isBetween(existing.start, existing.end, 'd', '[]'));
  //
  //
  // readonly conflictingDateValidator = (date: Date): ValidatorFn =>
  //   (control: AbstractControl): { [key: string]: any } | null =>
  //     this.isConflictingDate(date) ? null : {invalidDate: control.value};
  //
  //
  // absenceForm = new FormGroup({
  //   targetDeskInput: new FormControl(null, [Validators.required]),
  //   targetTypeInput: new FormControl(null, this.typeValidator),
  //   targetSubtypeInput: new FormControl(),
  //   startInput: new FormControl(null, [this.startValidator, this.conflictingDateValidator(this.start)]),
  //   endInput: new FormControl(null, [this.endValidator, this.conflictingDateValidator(this.end)]),
  // });


  // </editor-fold desc="FormGroup">


  onTypeChanged(type: TypeRepresentation): void {
    if (!type) {
      this.subtype = null;
    }
  }


  startDateChangedFromInput(evt) {
    this.start = evt.target.valueAsDate;
  }


  endDateChangedFromInput(evt) {
    this.end = evt.target.valueAsDate;
  }


  retrieveAssociatedDesksFn = (page: number, pageSize: number, searchTerm: string): Observable<PageDeskRepresentation> => {
    if (!this.tenantId) return of({});
    return this.deskService.getAssociatedDesks(this.tenantId, this.desk.id, page, pageSize, [], searchTerm);
  }


  /**
   * By default, we don't want to retroactively declare something in the past
   */
  todayDate(): string {
    return new Date().toISOString().slice(0, 10)
  }


  minDate(startDate: number): string {
    // start date + 1  day
    let minDate = new Date(startDate + 24 * 60 * 60 * 1000);
    return minDate.toISOString().slice(0, 10);
  }


  // end date - 1  day
  maxDate(endDate: number) {
    if (!endDate) {
      return endDate;
    }

    let maxDate = new Date(endDate - 24 * 60 * 60 * 1000);
    return maxDate.toISOString().slice(0, 10);
  }

  retrieveTypes(page: number, pageSize: number): Observable<PageTypeRepresentation> {
    console.debug(`retrieve type : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getTypes(this.tenantId, this.desk.id);
  }

  retrieveSubtypes(page: number, pageSize: number): Observable<PageSubtypeRepresentation> {
    console.debug(`retrieve subtypes : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getSubtypes(this.tenantId, this.desk.id, this.type?.id);
  }

}
