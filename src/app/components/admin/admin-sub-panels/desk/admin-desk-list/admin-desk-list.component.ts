/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { of, Observable } from 'rxjs';
import { faCaretRight, faCaretDown, faCompress, faExpand } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminDeskPopupComponent } from '../admin-desk-popup/admin-desk-popup.component';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { DeskMessages } from '../desk-messages';
import { ActivatedRoute } from '@angular/router';
import { GlobalPopupService } from '../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../admin-messages';
import { LegacyUserService } from '../../../../../services/ip-core/legacy-user.service';
import { AdminDeskService, DeskRepresentation, HierarchisedDeskRepresentation, DeskService, CurrentUserService, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { AdminDeskService as InternalAdminDeskService } from '@libriciel/iparapheur-internal';
import { AdminDeskService as ProvisioningAdminDeskService, DeskDto } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-desk-list',
  templateUrl: './admin-desk-list.component.html',
  styleUrls: ['./admin-desk-list.component.scss']
})
export class AdminDeskListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretRightIcon = faCaretRight;
  readonly caretDownIcon = faCaretDown;
  readonly expandIcon = faExpand;
  readonly compressIcon = faCompress;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly style = Style;
  readonly messages = DeskMessages;
  readonly adminMessages = AdminMessages;

  tenantId: string;
  deskList: HierarchisedDeskRepresentation[];
  deskPage: number = 1;
  deskTotal: number = 0;
  pageSizeIndex: number = 1;
  currentSearchTerm: string = '';
  collapseAll: boolean = false;
  asFunctionalAdmin: boolean = false;
  reversedIds: Set<string> = new Set<string>();
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;

  // <editor-fold desc="LifeCycle">


  constructor(public adminDeskService: AdminDeskService,
              public deskService: DeskService,
              public globalPopupService: GlobalPopupService,
              public legacyUserService: LegacyUserService,
              public currentUserService: CurrentUserService,
              public modalService: NgbModal,
              public notificationService: NotificationsService,
              public internalAdminDeskService: InternalAdminDeskService,
              public provisioningAdminDeskService: ProvisioningAdminDeskService,
              public route: ActivatedRoute) { }


  ngOnInit() {
    this.route.parent.parent.data.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 2 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route.parent.params.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.asFunctionalAdmin = this.legacyUserService.isCurrentUserOnlyFunctionalAdminOfThisTenant(this.tenantId);
      this.requestDesks();
    });
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(searchTerm: string) {
    this.currentSearchTerm = searchTerm;
    this.deskPage = 1;
    this.deskTotal = 0;

    this.requestDesks();
  }

  /**
   * Fetching the folder's list, updating the UI.
   */
  requestDesks() {

    this.loading = true;

    let searchTerm = this.currentSearchTerm.length > 0 ? this.currentSearchTerm : null;

    const deskRequest$ = this.asFunctionalAdmin
      ? this.currentUserService.getAdministeredDesksForTenant(
        this.tenantId,
        this.deskPage - 1,
        this.getPageSize(this.pageSizeIndex),
        [],
        searchTerm
      )
      : this.internalAdminDeskService.listHierarchicDesks(
        this.tenantId,
        this.collapseAll,
        [...this.reversedIds.values()],
        this.deskPage - 1,
        this.getPageSize(this.pageSizeIndex),
        [],
        searchTerm
      );

    deskRequest$.subscribe(
      deskRetrieved => {
        this.deskList = deskRetrieved.content;
        this.deskTotal = deskRetrieved.totalElements;
      },
      error => this.notificationService.showErrorMessage(DeskMessages.ERROR_FETCHING_DESKS, error)
    ).add(() => this.loading = false);
  }


  onCaretClicked(desk: HierarchisedDeskRepresentation) {
    if (desk.directChildrenCount === 0) {
      return;
    }

    this.reversedIds.has(desk.id)
      ? this.reversedIds.delete(desk.id)
      : this.reversedIds.add(desk.id);

    this.requestDesks();
  }


  onCollapseButtonClicked(collapseAll: boolean) {
    this.collapseAll = collapseAll;
    this.reversedIds.clear();
    this.requestDesks();
  }


  onDeleteButtonClicked(desk: DeskRepresentation) {
    this.globalPopupService
      .showDeleteValidationPopup(this.adminMessages.deskPopupLabel(desk.name))
      .then(
        () => this.deleteDesk(desk),
        () => {/* dismissed */}
      );
  }


  deleteDesk(desk: HierarchisedDeskRepresentation): void {
    this.provisioningAdminDeskService
      .deleteDesk(this.tenantId, desk.id)
      .subscribe(() => {
          this.notificationService.showSuccessMessage(`Le bureau ${desk.name} a été supprimé avec succès`)
          this.requestDesks();
        },
        error => this.notificationService.showErrorMessage(`Erreur lors de la suppression du bureau ${desk.name}`, error.message)
      );
  }


  openModalWithComponent(selectedDesk?: DeskRepresentation) {
    const getDeskInfo$: Observable<DeskDto> = selectedDesk
      ? this.provisioningAdminDeskService.getDeskAsAdmin(this.tenantId, selectedDesk.id)
      : of({
        id: undefined,
        name: undefined,
        shortName: undefined,
        folderCreationAllowed: true,
        actionAllowed: true,
        archivingAllowed: true,
        chainAllowed: true,
        ownerIds: [],
        owners: [],
        supervisors: [],
        delegationManagers: [],
        filterableMetadata: [],
        associatedDesks: [],
      });

    getDeskInfo$.subscribe(
      completeDesk => this.modalService
        .open(
          AdminDeskPopupComponent,
          {
            injector: Injector.create({
              providers: [
                {provide: AdminDeskPopupComponent.INJECTABLE_DESK_KEY, useValue: completeDesk},
                {provide: AdminDeskPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
              ]
            }),
            size: 'xl'
          }
        )
        .result
        .then(
          () => this.requestDesks(),
          () => { /* Dismissed */ }
        ),
      error => this.notificationService.showErrorMessage(`Erreur lors de la récupération du bureau ${selectedDesk.name}`, error.message)
    );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


}
