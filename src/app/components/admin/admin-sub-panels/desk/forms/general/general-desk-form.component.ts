/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DeskMessages } from '../../desk-messages';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { AdminDeskService, PageDeskRepresentation, DeskDto } from '@libriciel/iparapheur-provisioning';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-general-desk-form',
  templateUrl: './general-desk-form.component.html',
  styleUrls: ['./general-desk-form.component.scss']
})
export class GeneralDeskFormComponent {

  @Input() desk: DeskDto;
  @Input() tenantId: string;
  @Output() valid = new EventEmitter<boolean>();

  readonly commonMessages = CommonMessages;
  readonly messages = DeskMessages;
  readonly descriptionMaxLength = 300;
  readonly generalForm = new FormGroup({
    nameControl: new FormControl('', [Validators.required, Validators.minLength(1)]),
    shortNameControl: new FormControl('', [Validators.required, Validators.minLength(1)]),
    descriptionControl: new FormControl('', [Validators.maxLength(this.descriptionMaxLength)]),
  });


  // <editor-fold desc="LifeCycle">


  constructor(protected adminDeskService: AdminDeskService) {
    this.generalForm.valueChanges
      .subscribe(() => this.valid.emit(this.generalForm.valid));
  }


  // </editor-fold desc="LifeCycle">


  /**
   * Retrieving everything, filtering the current desk.
   *
   * @param page
   * @param pageSize
   * @param searchTerm
   */
  requestDesksFn = (page: number, pageSize: number, searchTerm: string): Observable<PageDeskRepresentation> => {
    return this.adminDeskService
      .listDesks(this.tenantId, page, pageSize, [], searchTerm)
      .pipe(tap(result => result.content = result.content.filter(d => d.id !== this.desk.id)));
  }


  onDeskSelectionChanged(event: any) {
    this.desk.parentDesk = event[0];
  }


}
