/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { DeskMessages } from '../../desk-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { stringifyUser } from '../../../../../../utils/string-utils';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { UserSortBy, UserRepresentation, DeskRepresentation } from '@libriciel/iparapheur-standard';
import { AdminTenantUserService, PageUserRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-delegation-manager-desk-form',
  templateUrl: './delegation-manager-desk-form.component.html',
  styleUrls: ['./delegation-manager-desk-form.component.scss']
})
export class DelegationManagerDeskFormComponent implements OnChanges {

  readonly commonIcons = CommonIcons;
  readonly messages = DeskMessages;
  readonly commonMessages = CommonMessages;
  readonly infoIcon = faInfoCircle;
  readonly stringifyUserFn = stringifyUser;

  @Input() desk: DeskRepresentation;
  @Input() tenantId: string;
  @Input() delegationManagers: UserRepresentation[] = [];
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<UserRepresentation>;

  currentSearch = null;
  unmodifiableElements: UserRepresentation[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(private adminTenantUserService: AdminTenantUserService) {}


  ngOnChanges(): void {
    // FIXME : Link this to the KEYCLOAK_CLIENT env variable
    this.unmodifiableElements = this.delegationManagers.filter(o => o.userName === "service-account-ipcore-api");
  }


  // </editor-fold desc="LifeCycle">


  onSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearch = newTerm;
    this.doubleListComponent.requestElements(true);
  }


  requestDeskUsersFn = (page: number, pageSize: number): Observable<PageUserRepresentation> => {
    const searchTerm = this.currentSearch?.length > 0 ? this.currentSearch : null;
    const sortBy = [`${UserSortBy.Username.toString()},ASC`];

    return this.adminTenantUserService
      .listTenantUsers(this.tenantId, page, pageSize, sortBy, searchTerm, this.desk.id);
  }


}
