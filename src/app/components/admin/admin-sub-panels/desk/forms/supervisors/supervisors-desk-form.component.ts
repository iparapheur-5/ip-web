/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { DeskMessages } from '../../desk-messages';
import { stringifyUser } from '../../../../../../utils/string-utils';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { Observable } from 'rxjs';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { DeskDto, AdminTenantUserService, PageUserRepresentation, UserSortBy, UserRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-supervisors-desk-form',
  templateUrl: './supervisors-desk-form.component.html',
  styleUrls: ['./supervisors-desk-form.component.scss']
})
export class SupervisorsDeskFormComponent {

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = DeskMessages;
  readonly infoIcon = faInfoCircle;
  readonly stringifyUserFn = stringifyUser;

  @Input() desk: DeskDto;
  @Input() tenantId: string;
  @Input() supervisors: UserRepresentation[] = [];
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<UserRepresentation>;

  currentSearch: string = null;
  unmodifiableElements: UserRepresentation[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(private adminTenantUserService: AdminTenantUserService) {}


  // </editor-fold desc="LifeCycle">


  onSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearch = newTerm;
    this.doubleListComponent.requestElements(true);
  }


  requestDeskSupervisorsFn = (page: number, pageSize: number): Observable<PageUserRepresentation> => {
    const searchTerm = this.currentSearch?.length > 0 ? this.currentSearch : null;
    const sortBy = [`${UserSortBy.Username.toString()},ASC`];

    return this.adminTenantUserService
      .listTenantUsers(this.tenantId, page, pageSize, sortBy, searchTerm, this.desk.id);
  }


}
