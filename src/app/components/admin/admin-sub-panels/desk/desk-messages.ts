/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


import { DeskRepresentation } from '@libriciel/iparapheur-standard';

export class DeskMessages {

  static readonly USERS = 'Utilisateurs';
  static readonly USER_ROLE_TAB_SUB_TITLE = 'Attribuer le rôle à un nouvel utilisateur';
  static readonly USER_PERMISSION_TAB_SUB_TITLE = 'Attribuer la permission à un nouvel utilisateur';

  // List
  static readonly LIST_TITLE = 'Gestion des bureaux';
  static readonly CREATE_DESK_BUTTON_TITLE = 'Créer un Bureau';

  // General form
  static readonly GENERAL_TAB_TITLE = 'Général';
  static readonly SHORT_NAME = 'Nom court';
  static readonly DESCRIPTION = 'Description';
  static readonly SUPERIOR = 'Supérieur hiérarchique';

  // Authorisations form
  static readonly AUTHORISATIONS_TAB_TITLE = 'Habilitations';
  static readonly THE_CURRENT_DESK_CAN_ = 'Le bureau peut\u00a0:';
  static readonly CREATE_FOLDERS = 'Créer des dossiers';
  static readonly PERFORM_ACTIONS = 'Traiter des dossiers';
  static readonly FINISHED_FOLDERS = 'Traiter des dossiers en fin de circuit';
  static readonly CHAIN_FOLDERS = 'Enchaîner des dossiers terminés dans un nouveau circuit';

  // Owner form
  static readonly OWNERS_TAB_TITLE = 'Acteurs';
  static readonly OWNERS = 'Propriétaires';

  // Supervisor form
  static readonly SUPERVISORS_TAB_TITLE = 'Superviseurs';
  static readonly SUPERVISORS_INFO = `Les superviseurs sont des acteurs en lecture seule sur un bureau.`;
  static readonly SUPERVISORS = 'Superviseurs';

  // Delegation managers form
  static readonly DELEGATION_MANAGERS_TAB_TITLE = `Gestionnaires d'absences`;
  static readonly DELEGATION_MANAGERS = `Gestionnaires d'absence`;

  // Associated desks form
  static readonly ASSOCIATED_DESKS_INFO = `Les bureaux associés sont les bureaux proposés dans les formulaires d'absences, de transferts, de demande d'avis complémentaire, de bureaux variables, et de bureaux notifiés.`;

  // Metadata form
  static readonly METADATA_TAB_TITLE = 'Métadonnées';
  static readonly METADATA_INFO = 'Métadonnées visibles dans les filtres utilisateur';
  static readonly ERROR_FETCHING_METADATA = 'Erreur à la récupération des métadonnées';

  static readonly METADATA_LIST = 'Liste des métadonnées';
  static readonly METADATA_LINKED = 'Métadonnées associées';

  // Absence form
  static readonly DELEGATION_TO = 'Bureau remplaçant\u00a0:';
  static readonly DELEGATION_FOR_TYPE = 'Restreindre aux dossiers de type\u00a0:';
  static readonly DELEGATION_FOR_SUBTYPE = 'Restreindre aux dossiers de sous-type\u00a0:';
  static readonly ABSENCE_START = `Début de l'absence`;
  static readonly ABSENCE_END = `Fin de l'absence`;

  static readonly ERROR_FETCHING_DESKS = 'Erreur à la récupération des bureaux';

  static getAdminDeskPopupModalTitle(desk: DeskRepresentation) {
    return desk.id
      ? `Modification du bureau ${desk.name ? desk.name : ''} `
      : `Création du bureau ${desk.name ? desk.name : ''} `;
  }


}
