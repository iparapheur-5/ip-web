/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DeskMessages } from '../desk-messages';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CrudOperation } from '../../../../../services/crud-operation';
import { Observable } from 'rxjs';
import { SupervisorsDeskFormComponent } from '../forms/supervisors/supervisors-desk-form.component';
import { DelegationManagerDeskFormComponent } from '../forms/delegation-manager/delegation-manager-desk-form.component';
import { AdminDeskService, DeskDto } from '@libriciel/iparapheur-provisioning';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-desk-popup',
  templateUrl: './admin-desk-popup.component.html',
  styleUrls: ['./admin-desk-popup.component.scss']
})
export class AdminDeskPopupComponent {

  public static readonly INJECTABLE_DESK_KEY = 'desk';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = DeskMessages;

  // TODO : A proper banana-in-the-box method, maybe
  @ViewChild('supervisorsDeskForm') supervisorsDeskFormComponent: SupervisorsDeskFormComponent;
  @ViewChild('delegationManagerDeskForm') delegationManagerDeskFormComponent: DelegationManagerDeskFormComponent;

  generalDeskFormValid: boolean = true;
  ownersDeskFormValid: boolean = true;
  isProcessing: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(protected adminDeskService: AdminDeskService,
              protected notificationService: NotificationsService,
              protected toastr: ToastrService,
              public activeModal: NgbActiveModal,
              @Inject(AdminDeskPopupComponent.INJECTABLE_DESK_KEY) public desk: DeskDto,
              @Inject(AdminDeskPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string) {}


  // </editor-fold desc="LifeCycle">


  validate(): void {

    if (!this.validForms()) {
      console.log('Invalid form, cancelling...');
      return;
    }

    this.isProcessing = true;
    const crudOperation = this.desk.id ? CrudOperation.Update : CrudOperation.Create;

    const requestObservable$: Observable<any> = this.desk.id
      ? this.adminDeskService.editDesk(this.tenantId, this.desk.id, this.createDeskDtoFromCurrentDesk())
      : this.adminDeskService.createDesk(this.tenantId, this.createDeskDtoFromCurrentDesk());

    requestObservable$
      .subscribe(
        () => {
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
          this.notificationService.showSuccessMessage(
            `Le bureau ${this.desk.name} a été ${crudOperation === CrudOperation.Create ? 'créé' : 'modifié'} avec succès`
          );
        },
        error => this.notificationService.showErrorMessage(
          `Erreur lors de la ${crudOperation === CrudOperation.Create ? 'création' : 'modification'} du bureau ${this.desk.name}`,
          error.message
        )
      )
      .add(() => this.isProcessing = false);
  }


  validForms(): boolean {
    return this.generalDeskFormValid && this.ownersDeskFormValid;
  }


  validateGeneralForm($event: boolean) {
    this.generalDeskFormValid = $event;
  }


  createDeskDtoFromCurrentDesk(): DeskDto {
    return {
      shortName: this.desk.shortName,
      name: this.desk.name,
      description: this.desk.description || null,
      parentDeskId: this.desk.parentDesk?.id || null,
      folderCreationAllowed: this.desk.folderCreationAllowed,
      actionAllowed: this.desk.actionAllowed,
      archivingAllowed: this.desk.archivingAllowed,
      chainAllowed: this.desk.chainAllowed,
      ownerIds: this.desk.owners.map(u => u.id),
      associatedDeskIds: this.desk.associatedDesks.map(d => d.id),
      filterableMetadataIds: this.desk.filterableMetadata.map(m => m.id),
      supervisorIds: this.desk.supervisors.map(u => u.id),
      delegationManagerIds: this.desk.delegationManagers.map(u => u.id),
    };
  }


}
