/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { Folder } from '../../../../models/folder/folder';
import { AdminFolderService } from '../../../../services/ip-core/admin-folder.service';
import { NotificationsService } from '../../../../services/notifications.service';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../shared/common-messages';
import { AdminMessages } from '../admin-messages';
import { TypologyEntity } from '../../../../models/typology-entity';
import { CrudOperation } from '../../../../services/crud-operation';
import { isNotEmpty } from '../../../../utils/string-utils';
import { ActivatedRoute } from '@angular/router';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { HistoryPopupComponent } from '../../../main/history-popup/history-popup.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NamePipe } from '../../../../shared/utils/name.pipe';
import { TargetDeskActionPopupComponent } from '../../../main/folder-view/action-popups/target-desk-action-popup/target-desk-action-popup.component';
import { Task } from '../../../../models/task';
import { FolderUtils } from '../../../../utils/folder-utils';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { Action, State, FolderSortBy, CurrentUserService, PageDeskRepresentation, TaskViewColumn, UserPreferencesDto, SubtypeDto } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { TasksMessages } from '../../../main/tasks/tasks-messages';
import { WebsocketService } from '../../../../services/websockets/websocket.service';
import { AdminTypologyService, PageSubtypeRepresentation, AdminDeskService, PageTypeRepresentation, DeskRepresentation } from '@libriciel/iparapheur-provisioning';


@Component({
  selector: 'app-admin-folder-list',
  templateUrl: './admin-folder-list.component.html',
  styleUrls: ['./admin-folder-list.component.scss']
})
export class AdminFolderListComponent implements OnInit {


  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly sortByEnum = FolderSortBy;
  readonly stateEnum = State;
  readonly actionsEnum = Action;
  readonly secondaryActionsEnum = SecondaryAction;
  readonly messages = AdminMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly columnsEnum = TaskViewColumn;
  readonly styles = Style;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;

  tenantId: string;
  asFunctionalAdmin: boolean = false;

  selectedType: TypologyEntity;
  selectedSubtype: SubtypeDto;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;

  folders: Folder[] = [];
  foldersDesks: DeskRepresentation[] = [];
  deskList: DeskRepresentation[] = [];

  emitBefore: string = null;
  stillSince: string = null;
  stateFilter: State = null;

  showFolderSearchBar: boolean = true;
  isCollapsed: boolean = false;

  page = 1;
  pageSizeIndex = 1;
  total = 0;
  sortBy: FolderSortBy = FolderSortBy.FolderName;
  asc = true;
  currentSearchTerm = '';
  loading: boolean = false;
  colspan: number;

  retrieveTypesAsAdminFn = (page: number, pageSize: number) => this.retrieveTypesAsAdmin(page, pageSize);
  retrieveSubtypesAsAdminFn = (page: number, pageSize: number) => this.retrieveSubtypesAsAdmin(page, pageSize);

  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              public adminFolderService: AdminFolderService,
              private websocketService: WebsocketService,
              public adminDeskService: AdminDeskService,
              public legacyUserService: LegacyUserService,
              public currentUserService: CurrentUserService,
              public globalPopupService: GlobalPopupService,
              public adminTypologyService: AdminTypologyService,
              public modalService: NgbModal,
              public route: ActivatedRoute) { }


  ngOnInit(): void {
    this.route?.parent?.parent?.data.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 8 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route?.parent?.params.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.asFunctionalAdmin = this.legacyUserService.isCurrentUserOnlyFunctionalAdminOfThisTenant(this.tenantId);
      this.refreshFolderList();
    });
  }


  // </editor-fold desc="LifeCycle">


  transfer(folder: Folder): void {

    let taskToDo: Task = Object.assign(new Task(), folder.stepList[0]);

    this.modalService
      .open(TargetDeskActionPopupComponent, {
        injector: Injector.create({
          providers: [
            {provide: TargetDeskActionPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folder]},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: Action.Transfer},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: taskToDo.desks[0].id},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: true}
          ]
        }),
        size: 'md'
      })
      .result
      .then(
        result => {
          if (result.value === CommonMessages.ACTION_RESULT_OK) {
            this.refreshFolderList();
          }
        },
        () => {/* Not used */}
      );
  }


  retrieveDesksAsAdminFn = (page: number, pageSize: number, searchTerm): Observable<PageDeskRepresentation> => {

    if (!this.tenantId) return of({});

    const search = isNotEmpty(searchTerm) ? searchTerm : null;
    return this.asFunctionalAdmin
      ? this.currentUserService.getAdministeredDesksForTenant(this.tenantId, page, pageSize, [], search)
      : this.adminDeskService.listDesks(this.tenantId, page, pageSize, [], search);
  }


  onTypeSelectionChanged(): void {
    this.selectedSubtype = null;
  }


  onRowOrderClicked(column: TaskViewColumn): void {
    let row = this.mapColumnToSortBy(column);
    if (!!row) {
      this.asc = (row === this.sortBy) ? !this.asc : true;
      this.sortBy = row;
      this.refreshFolderList();
    }
  }


  updateSearchTerm(newTerm: string): void {
    this.currentSearchTerm = newTerm;
    this.page = 1;
  }


  refreshFolderList(): void {

    this.loading = true;

    let searchTerm = this.currentSearchTerm.length > 0 ? this.currentSearchTerm : null;
    const emitBeforeDate = isNotEmpty(this.emitBefore) ? new Date(this.emitBefore.valueOf()) : null;
    const stillSinceDate = isNotEmpty(this.stillSince) ? new Date(this.stillSince.valueOf()) : null;
    const deskId = !!this.deskList ? this.deskList[0]?.id : null;

    this.adminFolderService
      .getFolders(
        this.tenantId,
        this.page - 1,
        this.getPageSize(this.pageSizeIndex),
        this.sortBy,
        this.asc,
        deskId,
        this.stateFilter,
        this.selectedType,
        this.selectedSubtype,
        searchTerm,
        emitBeforeDate,
        stillSinceDate
      )
      .subscribe(
        foldersRetrieved => {
          this.folders = foldersRetrieved.data;
          this.total = foldersRetrieved.total;
          this.getFoldersDesks();
        },
        error => this.notificationService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FOLDERS, error)
      )
      .add(() => this.loading = false);
  }


  getCurrentDeskNameIfAppropriate(deskId: string): string {
    return NamePipe.compute(this.foldersDesks?.find(desk => desk.id === deskId)?.name);
  }


  onHistoryButtonClicked(folder: Folder) {
    this.modalService
      .open(HistoryPopupComponent, {
        injector: Injector.create({
          providers: [
            {provide: HistoryPopupComponent.injectableFolderKey, useValue: folder},
            {provide: HistoryPopupComponent.injectableDeskIdKey, useValue: null},
            {provide: HistoryPopupComponent.injectableTenantIdKey, useValue: this.tenantId}
          ]
        }),
        size: 'xl'
      })
      .result
      .then(
        () => {/* Not used */},
        () => {/* Dismissed */}
      );
  }


  isTransferAvailable(folder: Folder): boolean {
    return folder.stepList[0].desks.length === 1
      && FolderUtils.isActive(folder)
      && FolderUtils.TRANSFERABLE_ACTIONS.includes(folder.stepList[0].action);
  }


  onDeleteButtonClicked(folder: Folder) {
    this.globalPopupService
      .showDeleteValidationPopup(CommonMessages.foldersValidationPopupLabel([folder]), CommonMessages.foldersValidationPopupTitle([folder]))
      .then(
        () => this.deleteFolder(folder),
        () => {/* Dismissed */}
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  resetSearch(): void {
    this.page = 1;
    this.total = 0;
    this.sortBy = FolderSortBy.FolderName;
    this.asc = true;
    this.currentSearchTerm = "";
    this.selectedType = null;
    this.selectedSubtype = null;
    this.emitBefore = null;
    this.stillSince = null;
    this.stateFilter = null;
    this.deskList = [];
    this.showFolderSearchBar = false;
    setTimeout(() => this.showFolderSearchBar = true);
    this.refreshFolderList();
  }


  mapColumnToSortBy(column: TaskViewColumn): FolderSortBy {
    switch (column) {
      case TaskViewColumn.FolderName: { return FolderSortBy.FolderName; }
      // FIXME: case TaskViewColumn.Type: { return FolderSortBy.TypeName; }
      // FIXME: case TaskViewColumn.Subtype: { return FolderSortBy.SubtypeName; }
      case TaskViewColumn.LimitDate: { return FolderSortBy.LateDate; }
      case TaskViewColumn.CreationDate: { return FolderSortBy.CreationDate; }
      case TaskViewColumn.TaskId: { return FolderSortBy.TaskId; }
      case TaskViewColumn.FolderId: { return FolderSortBy.FolderId; }
      default: { return null; }
    }
  }



  retrieveTypesAsAdmin(page: number, pageSize: number): Observable<PageTypeRepresentation> {
    return this.adminTypologyService.listTypes(this.tenantId, page, pageSize);
  }

  retrieveSubtypesAsAdmin(page: number, pageSize: number): Observable<PageSubtypeRepresentation> {
    return this.adminTypologyService.listSubtypes(this.tenantId, this.selectedType.id, page, pageSize);
  }


  private getFoldersDesks(): void {
    let foldersDesksIds: Set<string> = new Set<string>();
    this.folders
      .map(folder => folder.stepList[0].desks)
      .forEach(desks => desks.forEach(desk => foldersDesksIds.add(desk.id)));
    this.adminDeskService.listDesks(this.tenantId, 0, 10000, [])
      .subscribe(result => this.foldersDesks = result.content);
  }


  private deleteFolder(folder: Folder) {
    this.adminFolderService
      .deleteFolder(this.tenantId, folder)
      .subscribe(
        () => {
          this.refreshFolderList();
          folder.stepList[0].desks.forEach(desk => this.websocketService.notifyFolderAction(this.tenantId, desk.id));
          this.notificationService.showCrudMessage(CrudOperation.Delete, folder);
        },
        error => this.notificationService.showCrudMessage(CrudOperation.Delete, folder, error.message, false)
      );
  }


}
