/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, AfterContentChecked, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { TypologyMessages } from '../../typology-messages';
import { Weight, CommonIcons } from '@libriciel/ls-composants';
import { Config } from '../../../../../../config';
import { PdfSignaturePosition } from '../../../../../../models/pdf-signature-position';
import { TypeDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-type-signature-stamp',
  templateUrl: './type-signature-stamp.component.html',
  styleUrls: ['./type-signature-stamp.component.scss']
})
export class TypeSignatureStampComponent implements OnInit, AfterContentChecked {

  readonly messages = TypologyMessages;
  readonly commonIcons = CommonIcons;
  readonly config = Config;
  readonly weight = Weight;
  readonly math = Math;
  readonly defaultSignaturePosition = {
    x: 50,
    y: 50,
    page: 1
  };
  readonly pageWidthInMM = 210;
  readonly pageHeightInMM = 297;
  readonly pageWidthInPt = this.pageWidthInMM * Config.POINTS_PER_MM;
  readonly pageHeightInPt = this.pageHeightInMM * Config.POINTS_PER_MM;
  readonly previewWidthInPx = 315;
  readonly ratio = this.previewWidthInPx / this.pageWidthInPt;
  readonly previewHeightInPx = this.pageHeightInPt * this.ratio;
  readonly showStampValues = [
    {name: TypologyMessages.SHOW_STAMP, value: true},
    {name: TypologyMessages.DONT_SHOW_STAMP, value: false}
  ];


  @Input() type: TypeDto;
  @Output() valid = new EventEmitter<boolean>();

  originalStampCustomization: PdfSignaturePosition;


  // <editor-fold desc="LifeCycle">


  constructor(private changeDetector: ChangeDetectorRef) {}


  ngOnInit(): void {
    if (!this.type.signaturePosition) {
      this.type.signaturePosition = this.defaultSignaturePosition;
    }
    if (!this.type.signatureVisible) {
      this.type.signatureVisible = false;
    }
    this.originalStampCustomization = {
      x: this.type.signaturePosition.x,
      y: this.type.signaturePosition.y,
      page: this.type.signaturePosition.page,
    };
    this.resetStampCustomization();
  }


  ngAfterContentChecked(): void {
    if (!this.type.signaturePosition) {
      this.type.signaturePosition = this.defaultSignaturePosition;
    }
    this.changeDetector.detectChanges();
  }


  // </editor-fold desc="LifeCycle">


  resetStampCustomization() {
    this.type.signaturePosition.x = this.originalStampCustomization.x;
    this.type.signaturePosition.y = this.originalStampCustomization.y;
    this.type.signaturePosition.page = this.originalStampCustomization.page;
  }


  onChange() {
    this.valid.emit(true);
  }


}
