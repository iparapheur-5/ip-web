/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeSignatureStampComponent } from './type-signature-stamp.component';
import { PtToMmPipe } from '../../../../../../utils/pt-to-mm.pipe';
import { SignatureFormat, SignatureProtocol, TypeDto } from '@libriciel/iparapheur-standard';


describe('TypeSignatureStampComponent', () => {


  let component: TypeSignatureStampComponent;
  let fixture: ComponentFixture<TypeSignatureStampComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        declarations: [TypeSignatureStampComponent, PtToMmPipe]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(TypeSignatureStampComponent);
    component = fixture.componentInstance;

    let testType: TypeDto = {
      name: 'testType',
      protocol: SignatureProtocol.Actes,
      signatureFormat: SignatureFormat.Pades
    };
    component.type = testType;

    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
