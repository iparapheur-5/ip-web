/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TypologyMessages } from '../../typology-messages';
import { Weight, CommonIcons } from '@libriciel/ls-composants';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { SignatureProtocol, SignatureFormat, TypeDto } from '@libriciel/iparapheur-standard';


@Component({
  selector: 'app-type-protocol',
  templateUrl: './type-protocol.component.html',
  styleUrls: ['./type-protocol.component.scss']
})
export class TypeProtocolComponent implements OnInit {

  readonly messages = TypologyMessages;
  readonly commonMessages = CommonMessages;
  readonly weight = Weight;
  readonly availableProtocols = Object.values(SignatureProtocol);
  readonly commonIcons = CommonIcons;
  readonly zipCodeValidator = Validators.pattern(/^\d{5}$/);

  @Input() type: TypeDto;
  @Output() valid = new EventEmitter<boolean>();
  @Output() goToCustomizeStampTabEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() showCustomizeStampTab: EventEmitter<boolean> = new EventEmitter<boolean>();

  availableSigningFormats = Object.values(SignatureFormat);
  showLocationFieldset: boolean;
  isCityMandatory: boolean;
  showZipCodeField: boolean;
  isZipCodeMandatory: boolean;
  showCustomSigningStamp: boolean;
  protocolForm = new FormGroup({
    protocolInput: new FormControl(Validators.required),
    signingFormatInput: new FormControl(Validators.required),
    signatureLocationInput: new FormControl(),
    signatureZipCodeInput: new FormControl(null, [this.zipCodeValidator])
  });


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    if (!this.type.protocol) {
      this.type.protocol = SignatureProtocol.None;
    }
  }


  // </editor-fold desc="LifeCycle">


  private static getAvailableSigningFormats(protocol: SignatureProtocol): SignatureFormat[] {
    switch (protocol) {
      case SignatureProtocol.Actes:
        return [SignatureFormat.Pkcs7, SignatureFormat.Pades];
      case SignatureProtocol.Helios:
        return [SignatureFormat.PesV2];
      case SignatureProtocol.None:
      default:
        return [SignatureFormat.Pkcs7, SignatureFormat.Pades, SignatureFormat.Auto, SignatureFormat.XadesDetached];
    }
  }


  private static getDefaultSigningFormat(protocol: SignatureProtocol): SignatureFormat {
    switch (protocol) {
      case SignatureProtocol.Actes:
        return SignatureFormat.Pades;
      case SignatureProtocol.Helios:
        return SignatureFormat.PesV2;
      case SignatureProtocol.None:
        return SignatureFormat.Pades;
      default:
        return SignatureFormat.Pades;
    }
  }


  onChange(): void {
    this.availableSigningFormats = TypeProtocolComponent.getAvailableSigningFormats(this.type.protocol);
    if (!this.availableSigningFormats.includes(this.type.signatureFormat)) {
      this.type.signatureFormat = TypeProtocolComponent.getDefaultSigningFormat(this.type.protocol);
    }

    this.showLocationFieldset = [SignatureFormat.XadesDetached, SignatureFormat.PesV2, SignatureFormat.Pades, SignatureFormat.Auto].includes(this.type.signatureFormat);
    this.isCityMandatory = [SignatureFormat.XadesDetached, SignatureFormat.PesV2, SignatureFormat.Auto].includes(this.type.signatureFormat);
    this.protocolForm.controls["signatureLocationInput"].setValidators(this.isCityMandatory ? Validators.required : null);
    this.protocolForm.controls["signatureLocationInput"].updateValueAndValidity();

    this.showZipCodeField = [SignatureFormat.XadesDetached, SignatureFormat.PesV2, SignatureFormat.Auto].includes(this.type.signatureFormat);
    this.isZipCodeMandatory = [SignatureFormat.XadesDetached].includes(this.type.signatureFormat);
    this.protocolForm.controls["signatureZipCodeInput"].setValidators(this.isZipCodeMandatory ? [Validators.required, this.zipCodeValidator] : this.zipCodeValidator);
    this.protocolForm.controls["signatureZipCodeInput"].updateValueAndValidity();

    this.showCustomSigningStamp = [SignatureFormat.Pades, SignatureFormat.Auto].includes(this.type.signatureFormat);
    if (!this.showCustomSigningStamp) {
      this.type.signaturePosition = undefined;
      this.type.signatureVisible = undefined;
    }

    this.valid.emit(this.protocolForm.valid);
    this.showCustomizeStampTab.emit(this.showCustomSigningStamp);
  }


}
