/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { CommonMessages } from '../../../../shared/common-messages';

export class AdminTypologyMessages {

  static readonly ADDITIONAL_FIELDS = 'Options supplémentaires';
  static readonly READING_MANDATORY = 'Lecture obligatoire';
  static readonly MULTI_DOCUMENTS = 'Multi-documents principaux';
  static readonly INCLUDE_ANNEXES = `Inclure les annexes lors de le génération du PDF d'impression ou l'envoi dans la corbeille`;

  // Permissions

  static readonly PUBLIC = 'Public';
  static readonly AUTHORIZED_DESKS = 'Bureaux autorisés';

  // Workflow

  static readonly ERROR_RETRIEVING_STEPS = 'Erreur à la récupération des étapes du circuit';

  static readonly SIGNATURE_OPTIONS = 'Options de signature';
  static readonly DIGITAL_SIGNATURE_MANDATORY = 'Signature électronique obligatoire';

  static readonly SEAL = 'Cachet serveur';
  static readonly AUTOMATIC_SEAL = 'Cachet automatique';
  static readonly CHOOSE_A_SEAL_CERTIFICATE = 'Choisissez un certificat de cachet';

  static readonly EXTERNAL_SIGNATURE_CONFIGURATION = 'Configuration de signature externe';
  static readonly SECURE_MAIL_CONFIGURATION = 'Configuration du mail sécurisé';
  static readonly CHOOSE_A_CONFIGURATION = 'Choisissez une configuration';
  static readonly AUTOMATIC_EXTERNAL_SIGNATURE = 'Signature externe automatique';
  static readonly ANNOTATIONS_ALLOWED = 'Autoriser les commentaires intégrés au PDF';

  static readonly SELECTION_SCRIPT = 'Script de sélection de circuit';
  static readonly CHOOSE_A_WORKFLOW = 'Choisissez un circuit';
  static readonly CHOOSE_A_VALIDATION_WORKFLOW_FIRST = `Choisissez d'abord un circuit de validation`;
  static readonly CREATION_WORKFLOW_CAN_ONLY_CONTAIN_SINGLE_STEPS = `Le circuit de création ne doit pas contenir d'étapes parallèles`;
  static readonly CREATION_WORKFLOW_CAN_ONLY_CONTAIN_VISAS = `Le circuit de création ne doit contenir que des visas`;
  static readonly WORKFLOW_TYPE_CANNOT_CONTAIN_SEAL = `Le format de signature ne permet pas l'utilisation d'une étape de cachet serveur`;
  static readonly WORKFLOW_TYPE_CANNOT_CONTAIN_EXTERNAL_SIGNATURE = `Le format de signature ne permet pas l'utilisation d'une étape de signature externe`;
  static readonly WORKFLOW_TYPE_CANNOT_CONTAIN_SECURE_MAIL = `Le format de signature ne permet pas l'utilisation d'une étape de mail sécurisé`;
  static readonly MUST_SELECT_VALIDATION_WORKFLOW_OR_FILL_SELECTION_SCRIPT = `Un circuit de validation doit être sélectionné, ou un script de sélection de circuit doit être fourni`;
  static readonly MULTI_DOC_TOOLTIP = 'Permet de signer plusieurs documents (hors annexes) regroupés dans un même dossier.';

  static readonly EXTERNAL_SIGNATURE_TOOLTIP_INTRO =
    `Lors d'une étape manuelle de signature externe, les coordonnées du signataire devront être renseignées.
    Si cette option est activée, et que toutes les informations nécessaires sont présentes sur le dossier, le ${CommonMessages.APP_NAME} déclenchera automatiquement l'action.
    
    Les métadonnées suivantes seront lues :`;

  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_FIRST_NAME_META_LABEL = 'i_Parapheur_reserved_ext_sig_firstname';
  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_LAST_NAME_META_LABEL = 'i_Parapheur_reserved_ext_sig_lastname';
  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_MAIL_META_LABEL = 'i_Parapheur_reserved_ext_sig_mail';
  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_PHONE_META_LABEL = 'i_Parapheur_reserved_ext_sig_phone';

  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_FIRST_NAME = 'Le prénom du signataire';
  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_LAST_NAME = 'Le nom de famille du signataire';
  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_MAIL = `L'adresse e-mail du signataire`;
  static readonly EXTERNAL_SIGNATURE_TOOLTIP_LIST_PHONE = `Le numéro de téléphone du signataire`;

  static readonly EXTERNAL_SIGNATURE_TOOLTIP_OUTRO =
    `Si une de ces métadonnées est manquante, l'étape sera traitée comme une étape manuelle.
    L'action apparaîtra sur le bureau cible, et offrira le formulaire standard.`;

}
