/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { faUnlink } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Layer } from '../../../../../../models/pdf-stamp/layer';
import { TypologyMessages } from '../../typology-messages';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { AdminLayerService, SubtypeLayerAssociation, LayerSortBy, SubtypeDto, SubtypeLayerDto } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-subtype-layers',
  templateUrl: './subtype-layers.component.html',
  styleUrls: ['./subtype-layers.component.scss']
})
export class SubtypeLayersComponent implements OnInit {

  readonly pageSizes = [6];
  readonly unlinkIcon = faUnlink;
  readonly subtypeLayerAssociationEnum = SubtypeLayerAssociation;
  readonly messages = TypologyMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;

  @Input() tenantId: string;
  @Input() subtype: SubtypeDto;

  layersList: Layer[] = [];
  filteredLayersList: Layer[] = [];
  selectedLayer: Layer = null;

  page = 1;
  pageSizeIndex = 1;
  total = 0;

  loading: boolean = false;

  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal,
              public adminLayerService: AdminLayerService,
              public notificationsService: NotificationsService) {}


  ngOnInit() {
    if(!this.subtype.subtypeLayers) {
      this.subtype.subtypeLayers = [];
    }

    this.loading = true;
    this.adminLayerService
      .listLayers(this.tenantId, 0, 500, [LayerSortBy.Name + ',ASC'])
      .pipe(catchError(this.notificationsService.handleHttpError('listLayers')))
      .subscribe(
        layersRetrieved => {
          this.layersList = layersRetrieved.content.map(dto => Object.assign(new Layer(), dto));
          this.filterAlreadyLinkedLayersFromList();
        },
        error => this.notificationsService.showErrorMessage(TypologyMessages.ERROR_RETRIEVING_LAYER_LIST, error.message)
      )
      .add(() => this.loading = false);
  }


  // </editor-fold desc="LifeCycle">


  filterAlreadyLinkedLayersFromList() {
    this.filteredLayersList = this.layersList.filter(l => !this.subtype.subtypeLayers.some(sl => sl.layer.id === l.id));
  }


  onAddLayerButtonClicked() {
    const subtypeLayer = {
      layerId: this.selectedLayer.id,
      layer: this.selectedLayer,
      association: SubtypeLayerAssociation.MainDocument
    };
    this.selectedLayer = null;
    this.subtype.subtypeLayers.push(subtypeLayer);
    this.filterAlreadyLinkedLayersFromList();
  }


  onUnlinkButtonClicked(subtypeLayer: SubtypeLayerDto) {
    const index = this.subtype.subtypeLayers.indexOf(subtypeLayer);
    this.subtype.subtypeLayers.splice(index, 1);
    this.filterAlreadyLinkedLayersFromList();
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


}
