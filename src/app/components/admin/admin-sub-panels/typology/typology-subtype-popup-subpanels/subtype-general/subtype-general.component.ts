/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { AdminTypologyMessages } from '../../admin-typology-messages';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { TypologyMessages } from '../../typology-messages';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonIcons } from '@libriciel/ls-composants';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { SignatureProtocol, UserPreferencesDto, TypeDto, SubtypeDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-subtype-general',
  templateUrl: './subtype-general.component.html',
  styleUrls: ['./subtype-general.component.scss']
})
export class SubtypeGeneralComponent {

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly adminTypologyMessages = AdminTypologyMessages;
  readonly protocolEnum = SignatureProtocol;
  readonly messages = TypologyMessages;
  readonly infoIcon = faInfoCircle;

  readonly nameMinLength = 2;
  readonly nameMaxLength = 255;
  readonly descriptionMinLength = 3;
  readonly descriptionMaxLength = 255;

  @Output() valid = new EventEmitter<boolean>();
  @Input() subtype: SubtypeDto = null;
  @Input() type: TypeDto;
  @Input() tenantId: string;
  @Input() userPreferences: UserPreferencesDto;

  generalForm = new FormGroup({
    nameInput: new FormControl(this.subtype?.name, [
      Validators.required,
      Validators.minLength(this.nameMinLength),
      Validators.maxLength(this.nameMaxLength)
    ]),
    descriptionInput: new FormControl(this.subtype?.description, [
      Validators.required,
      Validators.minLength(this.descriptionMinLength),
      Validators.maxLength(this.descriptionMaxLength)
    ]),
  });


  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService) {
    this.generalForm.valueChanges.subscribe(() => this.valid.emit(this.generalForm.valid));
  }


  // </editor-fold desc="LifeCycle">


}
