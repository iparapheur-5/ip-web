/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { WorkflowDefinition } from '../../../../../../models/workflow-definition';
import { map, catchError } from 'rxjs/operators';
import { SealCertificate } from '../../../../../../models/seal-certificate';
import { SecureMailServer } from '../../../../../../models/securemail/secure-mail-server';
import { AdminSecureMailService } from '../../../../../../services/ip-core/admin-secure-mail.service';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { AdminTypologyMessages } from '../../admin-typology-messages';
import { isNullOrUndefined, isNotEmpty } from '../../../../../../utils/string-utils';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { CrudOperation } from '../../../../../../services/crud-operation';
import { CommonIcons } from '@libriciel/ls-composants';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { GENERIC_DESK_IDS, GENERIC_DESK_NAMES } from '../../../../../../models/workflows/ip-workflow-actor';
import { FormGroup, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { FormUtils } from '../../../../../../utils/form-utils';
import { SignatureFormat, Action, AdminSealCertificateService, SealCertificateSortBy, AdminWorkflowDefinitionService, WorkflowDefinitionSortBy, SignatureProtocol, ExternalSignatureConfigDto, AdminExternalSignatureService, ExternalSignatureConfigSortBy, DeskRepresentation, TypeDto, SubtypeDto } from '@libriciel/iparapheur-standard';


@Component({
  selector: 'app-subtype-workflow',
  templateUrl: './subtype-workflow.component.html',
  styleUrls: ['./subtype-workflow.component.scss']
})
export class SubtypeWorkflowComponent implements OnInit {

  readonly commonMessages = CommonMessages;
  readonly messages = AdminTypologyMessages;
  readonly commonIcons = CommonIcons;
  readonly infoIcon = faInfoCircle;
  readonly actionEnum = Action;
  readonly searchPageSize = 50;
  readonly dummyOriginDesk: DeskRepresentation = {id: GENERIC_DESK_IDS.EMITTER_ID, name: GENERIC_DESK_NAMES.EMITTER};
  readonly protocolEnum = SignatureProtocol;

  @Output() valid = new EventEmitter<boolean>();
  @Input() tenantId: string;
  @Input() type: TypeDto;
  @Input() subtype: SubtypeDto = null;
  @Input() displayMonacoEditor = false;

  validationWorkflowDefinition: WorkflowDefinition;
  creationWorkflowDefinition: WorkflowDefinition;
  availableExternalSignatureConfigurations: ExternalSignatureConfigDto[];
  availableSecureMailServers: SecureMailServer[];
  sealCertificates: SealCertificate[] = [];
  availableWorkflows: WorkflowDefinition[];
  initialScript: string;

  workflowForm: FormGroup = new FormGroup({
    selectCreationWorkflow: new FormControl(this.subtype?.creationWorkflowId, this.creationWorkflowValidator()),
    selectValidationWorkflow: new FormControl(this.subtype?.validationWorkflowId, this.validationWorkflowValidator()),
    selectSealCertificate: new FormControl(this.subtype?.sealCertificateId, this.actionConfigurationValidator(Action.Seal)),
    selectExternalSignatureConfig: new FormControl(this.subtype?.externalSignatureConfigId, this.actionConfigurationValidator(Action.ExternalSignature)),
    selectSecureMailServer: new FormControl(this.subtype?.secureMailServerId, this.actionConfigurationValidator(Action.SecureMail)),
    isExternalSignatureAutomatic: new FormControl(),
    isDigitalSignatureMandatory: new FormControl(),
    isSealAutomatic: new FormControl(),
  });


  // <editor-fold desc="LifeCycle">


  constructor(private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              public adminSecureMailService: AdminSecureMailService,
              public notificationsService: NotificationsService,
              public adminSealCertificateService: AdminSealCertificateService,
              public adminExternalSignatureService: AdminExternalSignatureService) {

    this.workflowForm.valueChanges.subscribe(() => this.valid.emit(this.workflowForm.valid));
  }


  ngOnInit(): void {
    console.log(this.subtype)
    this.initialScript = this.subtype.workflowSelectionScript;
    this.requestWorkflows(0, null);
    this.requestExternalSignatureConfigs(0, null);
    this.requestSealCertificates(0);
    this.requestSecureMailServers(0, null);
    this.updateCurrentValidationWorkflowSteps(this.subtype.validationWorkflowId);
    this.updateCurrentCreationWorkflowSteps(this.subtype.creationWorkflowId);
  }


  // </editor-fold desc="LifeCycle">


  getWorkflowCreationTooltip(): string {

    if (!this.validationWorkflowDefinition) {
      return AdminTypologyMessages.CHOOSE_A_VALIDATION_WORKFLOW_FIRST;
    }

    return FormUtils.getFirstErrorKey(this.workflowForm, 'selectCreationWorkflow');
  }


  getWorkflowValidationTooltip(): string {
    return FormUtils.getFirstErrorKey(this.workflowForm, 'selectValidationWorkflow');
  }


  updateAvailableWorkflowsList(searchEvt: { term: string, items: any[] }) {
    this.requestWorkflows(0, searchEvt.term);
  }


  updateCurrentCreationWorkflowSteps(workflowId: string) {
    this.creationWorkflowDefinition = null;
    this.workflowForm.controls['selectCreationWorkflow'].updateValueAndValidity();
    if (isNullOrUndefined(workflowId)) {
      return;
    }

    this.adminWorkflowDefinitionService
      .getWorkflowDefinitionByKey(this.tenantId, workflowId)
      .pipe(catchError(this.notificationsService.handleHttpError('getWorkflowDefinitionByKey')))
      .subscribe(
        workflow => {
          this.creationWorkflowDefinition = Object.assign(new WorkflowDefinition(), workflow);
          FormUtils.updateValueAndValidity(this.workflowForm);
        },
        error => {
          this.notificationsService.showErrorMessage(this.messages.ERROR_RETRIEVING_STEPS, error.message);
        }
      );
  }


  updateCurrentValidationWorkflowSteps(workflowId: string) {
    this.validationWorkflowDefinition = null;

    if (!workflowId) {

      // Special case : If we remove the validation workflow,
      // there is no creation one neither
      this.creationWorkflowDefinition = null;
      this.subtype.creationWorkflowId = null;

      // Back to the selection script, nothing else to do
      return;
    }

    this.adminWorkflowDefinitionService
      .getWorkflowDefinitionByKey(this.tenantId, workflowId)
      .pipe(catchError(this.notificationsService.handleHttpError('getWorkflowDefinitionByKey')))
      .subscribe(
        workflow => {
          this.validationWorkflowDefinition = Object.assign(new WorkflowDefinition(), workflow);
          FormUtils.updateValueAndValidity(this.workflowForm);
        },
        error => {
          this.notificationsService.showErrorMessage(this.messages.ERROR_RETRIEVING_STEPS, error.message);
        }
      );
  }


  requestWorkflows(page: number, searchTerm: string) {
    this.adminWorkflowDefinitionService
      .listWorkflowDefinitions(this.tenantId, page, 50, [WorkflowDefinitionSortBy.Name + ',ASC'], searchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('listWorkflowDefinitions')))
      .subscribe(paginatedResult => this.availableWorkflows = paginatedResult.content.map(dto => Object.assign(new WorkflowDefinition(), dto)));
  }


  requestExternalSignatureConfigs(page: number, searchTerm: string) {
    this.adminExternalSignatureService
      .listExternalSignatureConfigs(this.tenantId, page, this.searchPageSize, [ExternalSignatureConfigSortBy.Name.toLowerCase() + ',ASC'], searchTerm)
      .subscribe(configs => this.availableExternalSignatureConfigurations = configs.content);
  }


  requestSecureMailServers(page: number, searchTerm: string) {
    this.adminSecureMailService
      .findSecureMailServersByPage(this.tenantId, page, this.searchPageSize, searchTerm)
      .subscribe(
        servers => this.availableSecureMailServers = servers.data,
        error => this.notificationsService.showCrudMessage(CrudOperation.Read, SecureMailServer, error.message, false)
      );
  }


  requestSealCertificates(page: number) {
    this.adminSealCertificateService
      .listSealCertificateAsAdmin(this.tenantId, page, this.searchPageSize, [SealCertificateSortBy.Name + ',ASC'])
      .pipe(map(paginatedResult => paginatedResult.content.map(dto => Object.assign(new SealCertificate(), dto))))
      .subscribe(configs => this.sealCertificates = configs);
  }


  updateSealCertificates(event: any): void {
    if (event !== undefined) {
      this.subtype.sealCertificateId = event.id;
    }
  }


  updateAvailableExternalSignatureConfigsList(searchEvt: { term: string, items: any[] }) {
    this.requestExternalSignatureConfigs(0, searchEvt.term);
  }


  updateAvailableSealCertificates(searchEvt: { term: string, items: any[] }) {
    this.requestExternalSignatureConfigs(0, searchEvt.term);
  }


  updateAvailableSecureMailServerList(searchEvt: { term: string, items: any[] }) {
    this.requestSecureMailServers(0, searchEvt.term);
  }


  /**
   * Special case: when nothing's selected, we're in a selection-script mode.
   * We can't determine anything. So we'll assume that every action may be triggered.
   * @param workflowDefinition workflow to be tested
   * @param action availability to test
   */
  doesWorkflowDefinitionContainsAction(workflowDefinition: WorkflowDefinition, action: Action): boolean {
    return !(workflowDefinition?.steps)
      ? true
      : workflowDefinition.steps
        .some(s => s.type == action);
  }


  updateSelectionScript(evt) {
    this.subtype.workflowSelectionScript = evt;
    FormUtils.updateValueAndValidity(this.workflowForm);
  }


  actionConfigurationValidator(action: Action): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {

      if (!this.validationWorkflowDefinition) {
        return null;
      }

      const noAction = !this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, action);
      const isConfProperlySet = !!control.value;
      const isValid = noAction || isConfProperlySet;

      // console.log('actionValidator ' + action + ' noAction:' + noAction);
      // console.log('actionValidator ' + action + ' isConfProperlySet:' + isConfProperlySet);
      // console.log('actionValidator ' + action + ' isValid:' + isValid)

      if (isValid) {
        return null;
      }

      const errors = {};
      if (!isConfProperlySet) {
        errors['extSigConfNotProperlySet:'] = true;
      }

      return errors;
    };
  }


  creationWorkflowValidator(): ValidatorFn {
    return (_: AbstractControl): { [key: string]: any } | null => {

      if (!this.creationWorkflowDefinition?.steps) {
        return null;
      }

      const onlyVisa = this.creationWorkflowDefinition.steps
        .every(a => a.type === Action.Visa);

      const onlySingleSteps = this.creationWorkflowDefinition.steps
        .every(s => s.validatingDesks.length === 1);

      // Kinda hacky, we use the error messages directly as keys.
      // TODO : have some kind of key/message map somewhere ?

      const errors = {};

      if (!onlyVisa) {
        errors[AdminTypologyMessages.CREATION_WORKFLOW_CAN_ONLY_CONTAIN_VISAS] = true;
      }

      if (!onlySingleSteps) {
        errors[AdminTypologyMessages.CREATION_WORKFLOW_CAN_ONLY_CONTAIN_SINGLE_STEPS] = true;
      }

      return errors;
    }
  }


  validationWorkflowValidator(): ValidatorFn {
    return (_: AbstractControl): { [key: string]: any } | null => {

      const errors = {};
      if (!this.validationWorkflowDefinition?.steps) {
        if (isNotEmpty(this.subtype?.workflowSelectionScript)) {
          return null;
        } else {
          errors[AdminTypologyMessages.MUST_SELECT_VALIDATION_WORKFLOW_OR_FILL_SELECTION_SCRIPT] = true;
          return errors;
        }
      }

      const containsSeal = this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, Action.Seal);
      const containsSecureMail = this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, Action.SecureMail);
      const containsExternalSignature = this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, Action.ExternalSignature);
      const isPadesType = (this.type?.signatureFormat === SignatureFormat.Pades) || (this.type?.signatureFormat === SignatureFormat.Auto);

      // console.log('validationWorkflowValidator containsSeal:' + containsSeal);
      // console.log('validationWorkflowValidator containsSecureMail:' + containsSecureMail);
      // console.log('validationWorkflowValidator containsExternalSignature:' + containsExternalSignature);
      // console.log('validationWorkflowValidator isPadesType:' + isPadesType);

      // Kinda hacky, we use the error messages directly as keys.
      // TODO : have some kind of key/message map somewhere ?

      if (isPadesType) {
        return null;
      }

      if (containsSeal) {
        errors[AdminTypologyMessages.WORKFLOW_TYPE_CANNOT_CONTAIN_SEAL] = true;
      }

      if (containsSecureMail) {
        errors[AdminTypologyMessages.WORKFLOW_TYPE_CANNOT_CONTAIN_SECURE_MAIL] = true;
      }

      if (containsExternalSignature) {
        errors[AdminTypologyMessages.WORKFLOW_TYPE_CANNOT_CONTAIN_EXTERNAL_SIGNATURE] = true;
      }

      return errors;
    }
  }


}
