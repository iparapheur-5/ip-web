/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild, ElementRef, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { AdminTypologyMessages } from '../../admin-typology-messages';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { CommonIcons } from '@libriciel/ls-composants';
import { stringifyNamedElement } from '../../../../../../utils/string-utils';
import { AdminDeskService, DeskRepresentation } from '@libriciel/iparapheur-provisioning';
import { PageDeskRepresentation } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-subtype-permissions',
  templateUrl: './subtype-permissions.component.html',
  styleUrls: ['./subtype-permissions.component.scss']
})
export class SubtypePermissionsComponent implements AfterViewInit {

  readonly commonMessages = CommonMessages;
  readonly messages = AdminTypologyMessages;
  readonly commonIcons = CommonIcons;
  readonly stringifyNamedElementFn = stringifyNamedElement;

  @Input() tenantId: string;
  @Input() isCreationPermittedPublic: boolean;
  @Output() isCreationPermittedPublicChange = new EventEmitter<boolean>();
  @Input() creationPermittedDesks: DeskRepresentation[];

  @ViewChild('desksSearchInput') desksSearchInput: ElementRef<HTMLInputElement>;
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<DeskRepresentation>;


  // <editor-fold desc="LifeCycle">


  constructor(public adminDeskService: AdminDeskService) {}


  ngAfterViewInit(): void {
    fromEvent(this.desksSearchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(350),
        distinctUntilChanged()
      )
      .subscribe(() => this.doubleListComponent.requestElements(true));
  }


  // </editor-fold desc="LifeCycle">


  requestDesksFn = (page: number, pageSize: number): Observable<PageDeskRepresentation> => {

    let searchTerm = this.desksSearchInput?.nativeElement.value ?? '';
    searchTerm = searchTerm.length > 0 ? searchTerm : null;

    return this.adminDeskService
      .listDesks(this.tenantId, page, pageSize, [], searchTerm);
  }

}
