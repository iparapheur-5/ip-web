/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { faCaretRight, faCaretDown, faExpand, faCompress, faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TypologyTypePopupComponent } from '../typology-type-popup/typology-type-popup.component';
import { TypologySubtypePopupComponent } from '../typology-subtype-popup/typology-subtype-popup.component';
import { combineLatest, of, Observable } from 'rxjs';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonIcons } from '@libriciel/ls-composants';
import { TypologyMessages } from '../typology-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { ActivatedRoute } from '@angular/router';
import { GlobalPopupService } from '../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../admin-messages';
import { catchError } from 'rxjs/operators';
import { TypologySortBy, UserPreferencesDto, TypeDto, SubtypeDto, TypologyRepresentation, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { AdminTypologyService } from '@libriciel/iparapheur-provisioning';
import { AdminTypologyService as InternalAdminTypologyService } from '@libriciel/iparapheur-internal';


@Component({
  selector: 'app-typology-list',
  templateUrl: './typology-list.component.html',
  styleUrls: ['./typology-list.component.scss']
})
export class TypologyListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretRight = faCaretRight;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly expandIcon = faExpand;
  readonly compressIcon = faCompress;
  readonly commonMessages = CommonMessages;
  readonly adminMessages = AdminMessages;
  readonly typologyMessages = TypologyMessages;
  readonly commonIcons = CommonIcons;

  tenantId: string;
  typologyList: TypologyRepresentation[] = [];
  page = 1;
  total = 0;
  asc = true;
  currentSearchTerm = '';
  pageSizeIndex = 1;
  collapseAll = false;
  reversedIds: Set<string> = new Set<string>();
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;

  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminTypologyService: AdminTypologyService,
              public internalAdminTypologyService: InternalAdminTypologyService,
              public globalPopupService: GlobalPopupService,
              public route: ActivatedRoute,
              public modalService: NgbModal) { }


  ngOnInit() {
    this.route?.parent?.parent?.data?.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 2 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route?.parent?.params?.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.requestTypology();
    });
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(searchTerm: string) {
    this.currentSearchTerm = searchTerm;
    this.collapseAll = false;
    this.reversedIds.clear();
    this.page = 1;
    this.total = 0;
    this.requestTypology();
  }


  /**
   * Fetching the folder's list, updating the UI.
   */
  requestTypology() {

    this.loading = true;

    const searchTerm = this.currentSearchTerm.length > 0 ? this.currentSearchTerm : null;
    const sortBy = [TypologySortBy.Name + (this.asc ? ',ASC' : ',DESC')];

    this.internalAdminTypologyService
      .getTypologyHierarchy(this.tenantId, this.collapseAll, this.reversedIds, this.page - 1, this.getPageSize(this.pageSizeIndex), sortBy, searchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('getTypologyHierarchy')))
      .subscribe(
        page => {
          this.typologyList = page.content;
          this.total = page.totalElements;
        },
        error => this.notificationsService.showErrorMessage(TypologyMessages.ERROR_FETCHING_TYPES, error)
      )
      .add(() => this.loading = false);
  }


  onCaretClicked(typologyEntity: TypologyRepresentation) {

    if (typologyEntity.childrenCount == 0) {
      return;
    }

    if (this.reversedIds.has(typologyEntity.id)) {
      this.reversedIds.delete(typologyEntity.id);
    } else {
      this.reversedIds.add(typologyEntity.id);
    }

    this.requestTypology();
  }


  onCollapseButtonClicked(collapseAll: boolean) {
    this.collapseAll = collapseAll;
    this.reversedIds.clear();
    this.requestTypology();
  }


  onDeleteButtonClicked(typology: TypologyRepresentation) {
    this.globalPopupService
      .showDeleteValidationPopup(this.adminMessages.typologyPopupLabel(typology.name))
      .then(
        () => this.deleteTypology(typology),
        () => {/* dismissed */}
      );
  }


  deleteTypology(typology: TypologyRepresentation) {
    let observable = this.isType(typology)
      ? this.adminTypologyService.deleteType(this.tenantId, typology.id)
      : this.adminTypologyService.deleteSubtype(this.tenantId, typology.parentId, typology.id);

    observable
      .pipe(catchError(this.notificationsService.handleHttpError('delete type/subtype')))
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(
            this.isType(typology) ? TypologyMessages.TYPE_DELETION_SUCCESS : TypologyMessages.SUBTYPE_DELETION_SUCCESS
          );
          this.requestTypology();
        },
        error => this.notificationsService.showErrorMessage(
          this.isType(typology) ? TypologyMessages.TYPE_DELETION_ERROR : TypologyMessages.SUBTYPE_DELETION_ERROR,
          error.message
        )
      );
  }


  fetchAndOpenTypeModal(typeRepresentation?: TypeRepresentation) {
    !!typeRepresentation
      ? this.adminTypologyService
        .getType(this.tenantId, typeRepresentation.id)
        .pipe(catchError(this.notificationsService.handleHttpError('getType')))
        .subscribe(
          t => this.openTypeModal(t),
          error => this.notificationsService.showErrorMessage(TypologyMessages.TYPE_READ_ERROR, error.message)
        )
      : this.openTypeModal();
  }


  fetchAndOpenSubtypeModal(typeId: string, subtype?: TypologyRepresentation) {
    !!subtype
      ? this.openSubtypeModal(subtype.parentId, {id: subtype.id, name: subtype.name})
      : this.openSubtypeModal(typeId, null);
  }


  openTypeModal(typeDto?: TypeDto) {
    this.modalService
      .open(TypologyTypePopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: TypologyTypePopupComponent.INJECTABLE_TYPE_KEY, useValue: typeDto},
              {provide: TypologyTypePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId}
            ]
          }),
          size: 'xl'
        }
      )
      .result
      .then(
        () => this.requestTypology(),
        () => { /* Dismissed */ }
      );
  }


  openSubtypeModal(typeId: string, subtype?: TypologyRepresentation) {
    const subtypeId = subtype?.id;

    const fullType$: Observable<TypeDto> = this.adminTypologyService
      .getType(this.tenantId, typeId)
      .pipe(catchError(this.notificationsService.handleHttpError('getTypeInfo')));

    const fullSubtype$: Observable<SubtypeDto> = subtype
      ? this.adminTypologyService
        .getSubtype(this.tenantId, typeId, subtypeId)
        .pipe(catchError(this.notificationsService.handleHttpError('getSubtypeAsAdmin')))
      : of(null);

    combineLatest([fullType$, fullSubtype$])
      .subscribe(
        ([type, subtype]) => this.modalService
          .open(TypologySubtypePopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: TypologySubtypePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                  {provide: TypologySubtypePopupComponent.INJECTABLE_TYPE_KEY, useValue: type},
                  {provide: TypologySubtypePopupComponent.INJECTABLE_SUBTYPE_KEY, useValue: subtype},
                  {provide: TypologySubtypePopupComponent.INJECTABLE_USER_PREFERENCES_KEY, useValue: this.userPreferences},
                ]
              }),
              size: 'xl'
            }
          )
          .result
          .then(
            () => this.requestTypology(),
            () => { /* Dismissed */ }
          ),
        error => this.notificationsService.showErrorMessage(TypologyMessages.SUBTYPE_READ_ERROR, error.messages)
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  /**
   * This controller can only be sorted by Name.
   * The hierarchy cannot be maintained if the rows are sorted by any other way
   */
  onRowOrderClicked() {
    this.asc = !this.asc;
    this.requestTypology();
  }

  isType(typology: TypologyRepresentation): boolean {
    return !typology.parentId;
  }


  isSubtype(typology: TypologyRepresentation): boolean {
    return !!typology.parentId;
  }

}
