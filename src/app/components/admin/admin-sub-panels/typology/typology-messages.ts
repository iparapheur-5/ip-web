/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { faSync } from '@fortawesome/free-solid-svg-icons/faSync';
import { TypeRepresentation } from '@libriciel/iparapheur-standard';
import { SubtypeRepresentation } from '@libriciel/iparapheur-provisioning';

export class TypologyMessages {

  static readonly TYPOLOGY_MANAGEMENT_TITLE = 'Gestion des typologies';

  static readonly CREATE_TYPOLOGY_BUTTON_TITLE = 'Créer un type';
  static readonly ADD_SUBTYPE_BUTTON_TITLE = 'Ajouter un sous-type';

  static readonly GENERAL_TAB_TITLE = 'Général';
  static readonly PERMISSION_TAB_TITLE = 'Permissions de création';
  static readonly VISIBILITY_TAB_TITLE = 'Visibilité en filtre';
  static readonly WORKFLOWS_TAB_TITLE = 'Circuits';
  static readonly METADATA_TAB_TITLE = 'Métadonnées';
  static readonly LAYER_TAB_TITLE = 'Calques';

  static readonly SIGNING_FORMAT_TAB_TITLE = 'Protocole et format de signature';
  static readonly SIGNING_LAYOUT_TAB_TITLE = 'Tampon de signature';

  static readonly PROTOCOL_LABEL = 'Protocole';
  static readonly SIGNING_FORMAT_LABEL = 'Format de signature';
  static readonly CITY_LABEL = 'Ville de signature';
  static readonly ZIP_CODE_LABEL = 'Code postal';

  static readonly SEARCH_TYPES = 'Rechercher des types / sous-types';

  static readonly ERROR_FETCHING_TYPES = 'Erreur à la récupération des types';

  // Stamp tab

  static readonly SHOW_STAMP_LABEL = 'Afficher le tampon de signature ?';
  static readonly SHOW_STAMP = 'Afficher';
  static readonly DONT_SHOW_STAMP = 'Ne pas afficher';
  static readonly SHOW_STAMP_ON_PAGE_LABEL = 'Sur la page';
  static readonly COORDINATES_LABEL = 'Coordonnées';
  static readonly COORDINATES_LABEL_INFO = `Les techniques d'insertion du tampon dans le document sont susceptibles de faire varier sa position finale.`;
  static readonly COORDINATES_X_PT_LABEL = 'X (pt)\u00a0: ';
  static readonly COORDINATES_Y_PT_LABEL = 'Y (pt)\u00a0: ';
  static readonly COORDINATES_X_MM_LABEL = 'X (mm)\u00a0: ';
  static readonly COORDINATES_Y_MM_LABEL = 'Y (mm)\u00a0: ';
  static readonly SIZE_LABEL = 'Dimensions';
  static readonly SIZE_W_PT_LABEL = 'L (pt)\u00a0: ';
  static readonly SIZE_H_PT_LABEL = 'H (pt)\u00a0: ';
  static readonly SIZE_W_MM_LABEL = 'L (mm)\u00a0: ';
  static readonly SIZE_H_MM_LABEL = 'H (mm)\u00a0: ';
  static readonly STAMP_PREVIEW_LABEL = 'Aperçu du positionnement';
  static readonly RESET_STAMP_CUSTOMIZATION_BUTTON_TITLE = 'Réinitialiser la position';
  static readonly RESET_STAMP_CUSTOMIZATION_BUTTON_ICON = faSync;

  // Metadata tab

  static readonly LINK_METADATA_ = 'Associer une nouvelle métadonnée\u00a0:';
  static readonly LINKED_METADATA_ = 'Métadonnées associées\u00a0:';
  static readonly DEFAULT_VALUE = 'Valeur par défaut';
  static readonly MANDATORY = 'Requis';
  static readonly EDITABLE = 'Éditable';

  // Layer tab

  static readonly LINK_LAYER_ = 'Associer un nouveau calque\u00a0:';
  static readonly LINKED_LAYERS_ = 'Calques associés\u00a0:';
  static readonly ASSOCIATION_TYPE = `Type d'association`;
  static readonly ERROR_RETRIEVING_LAYER_LIST = 'Erreur à la récupération de la liste de calques';
  static readonly PAGE_TOOLTIP = `
      <p class="mt-2">
        L'index des pages commence à <span class="badge badge-secondary text-monospace">1</span>.<br/>
        Si une valeur dépasse le nombre de pages, la signature sera posée à la dernière page.
        <ul>
            <li><span class="badge badge-secondary text-monospace">1</span>\u00a0: La première page</li>
            <li><span class="badge badge-secondary text-monospace">2</span>\u00a0: La deuxième page</li>
            <li><span class="badge badge-secondary text-monospace">3</span>\u00a0: La troisième page...</li>
        </ul>
      </p>
      <p>
        L'index peut également être nul, ou négatif.<br/>
        Si une valeur négative dépasse le nombre de pages, la signature sera posée à la première page.
        <ul>
          <li><span class="badge badge-secondary text-monospace">0</span>\u00a0: La dernière page, par défaut</li>
          <li><span class="badge badge-secondary text-monospace">-1</span>\u00a0: La dernière page</li>
          <li><span class="badge badge-secondary text-monospace">-2</span>\u00a0: L'avant-dernière page</li>
          <li><span class="badge badge-secondary text-monospace">-3</span>\u00a0: L'avant-avant-dernière page...</li>
        </ul>
      </p>
  `;

  static readonly SUBTYPE_CREATION_SUCCESS = 'Le sous-type a été créé avec succès.';
  static readonly SUBTYPE_EDITION_SUCCESS = 'Le sous-type a été modifié avec succès.';
  static readonly SUBTYPE_DELETION_SUCCESS = 'Le sous-type a été supprimé avec succès.';

  static readonly SUBTYPE_CREATION_ERROR = 'Erreur lors de la création du sous-type.';
  static readonly SUBTYPE_EDITION_ERROR = 'Erreur lors de la modification du sous-type.';
  static readonly SUBTYPE_DELETION_ERROR = 'Erreur lors de la suppression du sous-type.';
  static readonly SUBTYPE_READ_ERROR = 'Erreur lors de la récupération du sous-type.';

  static readonly TYPE_CREATION_SUCCESS = 'Le type a été créé avec succès.';
  static readonly TYPE_EDITION_SUCCESS = 'Le type a été modifié avec succès.';
  static readonly TYPE_DELETION_SUCCESS = 'Le type a été supprimé avec succès.';

  static readonly TYPE_CREATION_ERROR = 'Erreur lors de la création du type.';
  static readonly TYPE_EDITION_ERROR = 'Erreur lors de la modification du type.';
  static readonly TYPE_DELETION_ERROR = 'Erreur lors de la suppression du type.';
  static readonly TYPE_READ_ERROR = 'Erreur lors de la récupération du type.';

  static getNameLabelInfo(min: number, max: number) {
    return min + ' à ' + max + ' caractères requis';
  }


  static getAdminSubtypeTypologyPopupModalTitle(type: TypeRepresentation, subtype: SubtypeRepresentation) {
    return subtype?.id
      ? `"${type?.name}" Modification d'un sous-type ${subtype?.name ?? ''}`
      : `"${type?.name}" Ajout d'un sous-type`;
  }


  static getAdminTypeTypologyPopupModalTitle(type: TypeRepresentation) {
    if (!type) return '';

    return type.id
      ? `Modification du type ${type?.name ? type?.name : ''} `
      : `Création du type ${type?.name ? type?.name : ''} `;
  }


}
