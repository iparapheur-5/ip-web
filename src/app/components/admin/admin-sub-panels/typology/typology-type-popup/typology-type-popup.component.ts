/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, ViewChild, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import { NgbActiveModal, NgbNav } from '@ng-bootstrap/ng-bootstrap';
import { TypologyMessages } from '../typology-messages';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CrudOperation } from '../../../../../services/crud-operation';
import { catchError } from 'rxjs/operators';
import { AdminTypologyService } from '@libriciel/iparapheur-provisioning';
import { TypeDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-typology-type-popup',
  templateUrl: './typology-type-popup.component.html',
  styleUrls: ['./typology-type-popup.component.scss']
})
export class TypologyTypePopupComponent implements AfterContentChecked {

  public static readonly INJECTABLE_TYPE_KEY = 'type';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = TypologyMessages;

  isProcessing = false;
  @ViewChild('nav') tabs: NgbNav;
  generalFormValid = true;
  protocolFormValid = true;
  showCustomizeStampTab;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminTypologyService: AdminTypologyService,
              public activeModal: NgbActiveModal,
              private changeDetector: ChangeDetectorRef,
              @Inject(TypologyTypePopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(TypologyTypePopupComponent.INJECTABLE_TYPE_KEY) public type?: TypeDto) {
    if (!type) {
      this.type = {name: null};
    }
  }


  ngAfterContentChecked(): void {
    // avoid ExpressionChangedAfterItHasBeenCheckedError
    this.changeDetector.detectChanges();
  }


  // </editor-fold desc="LifeCycle">


  onSaveButtonClicked() {

    // Trim, to ease things on every other software
    this.type.name = this.type.name.trim();
    this.type.description = this.type.description.trim();
    this.type.signatureLocation = this.type.signatureLocation?.trim() ?? null;
    this.type.signatureZipCode = this.type.signatureZipCode?.trim() ?? null;

    const createOrUpdateType = this.type.id
      ? this.adminTypologyService.updateType(this.tenantId, this.type.id, this.type)
      : this.adminTypologyService.createType(this.tenantId, this.type);

    this.isProcessing = true;
    const crudOperation = this.type.id ? CrudOperation.Update : CrudOperation.Create;

    createOrUpdateType
      .pipe(catchError(this.notificationsService.handleHttpError('update/create type')))
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(
            crudOperation === CrudOperation.Create ? TypologyMessages.TYPE_CREATION_SUCCESS : TypologyMessages.TYPE_EDITION_SUCCESS
          );
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationsService.showErrorMessage(
          crudOperation === CrudOperation.Create ? TypologyMessages.TYPE_CREATION_ERROR : TypologyMessages.TYPE_EDITION_ERROR,
          error.message
        )
      )
      .add(() => this.isProcessing = false);
  }


  setShowCustomizeStampTab(value: boolean) {
    this.showCustomizeStampTab = value;
  }


  goToCustomizeStampTab() {
    this.tabs.select('tab-customize-stamp');
  }


  validForms() {
    return this.generalFormValid && this.protocolFormValid;
  }


  setProtocolFormValid($event: boolean) {
    this.protocolFormValid = $event;
  }


  setGeneralFormValid($event: boolean) {
    this.generalFormValid = $event;
  }


}
