/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { TypologyMessages } from '../typology-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CrudOperation } from '../../../../../services/crud-operation';
import { Observable } from 'rxjs';
import { UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { AdminTypologyService, TypeDto, SubtypeDto, DeskRepresentation } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-typology-subtype-popup',
  templateUrl: './typology-subtype-popup.component.html',
  styleUrls: ['./typology-subtype-popup.component.scss']
})
export class TypologySubtypePopupComponent {

  public static readonly INJECTABLE_SUBTYPE_KEY = 'subtype';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_TYPE_KEY = 'type';
  public static readonly INJECTABLE_USER_PREFERENCES_KEY = 'userPreferences';

  readonly commonMessages = CommonMessages;
  readonly messages = TypologyMessages;
  readonly commonIcons = CommonIcons;

  isProcessing = false;
  isGeneralTabValid = true;
  isWorkflowTabValid = true;
  canDisplayMonacoEditor = false;

  /**
   * Note : In the API, a null list will make the permission public.
   * It is actually different from an empty list.
   *
   * Yet, we still want to have a list in the code, to make the double-list work, and the "Is public" checkbox mapping easy.
   * That's why we split these lists in two distinct values.
   */
  isCreationPermittedPublic = true;
  creationPermittedDesks: DeskRepresentation[] = [];

  isFilterablePublic = true;
  filterableByDesks: DeskRepresentation[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminTypologyService: AdminTypologyService,
              public activeModal: NgbActiveModal,
              @Inject(TypologySubtypePopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(TypologySubtypePopupComponent.INJECTABLE_TYPE_KEY) public type: TypeDto,
              @Inject(TypologySubtypePopupComponent.INJECTABLE_USER_PREFERENCES_KEY) public userPreferences: UserPreferencesDto,
              @Inject(TypologySubtypePopupComponent.INJECTABLE_SUBTYPE_KEY) public subtype?: SubtypeDto) {

    if (!this.subtype) {
      this.subtype = {name: null};
    }

    if (!!this.subtype.creationPermittedDesks) {
      this.isCreationPermittedPublic = false;
      this.creationPermittedDesks = this.subtype.creationPermittedDesks;
    }

    if (!!this.subtype.filterableByDesks) {
      this.isFilterablePublic = false;
      this.filterableByDesks = this.subtype.filterableByDesks;
    }
  }


  // </editor-fold desc="LifeCycle">


  onSaveButtonClicked() {

    if (!this.validForms()) return;

    // Trim, to ease things on every other software
    this.subtype.name = this.subtype.name.trim();
    this.subtype.description = this.subtype.description.trim();

    // Cleanup data before the actual request
    this.subtype.creationPermittedDeskIds = this.creationPermittedDesks.map(desk => desk.id);
    if (this.isCreationPermittedPublic) {
      this.subtype.creationPermittedDeskIds = null;
    }

    this.subtype.filterableByDeskIds = this.filterableByDesks.map(desk => desk.id);
    if (this.isFilterablePublic) {
      this.subtype.filterableByDeskIds = null;
    }

    const createOrUpdateSubtype: Observable<any> = this.subtype.id
      ? this.adminTypologyService.updateSubtype(this.tenantId, this.type.id, this.subtype.id, this.subtype)
      : this.adminTypologyService.createSubtype(this.tenantId, this.type.id, this.subtype);

    this.isProcessing = true;

    const crudOperation = this.subtype.id ? CrudOperation.Update : CrudOperation.Create;
    createOrUpdateSubtype
      .pipe(catchError(this.notificationsService.handleHttpError('update/create subtype')))
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(
            crudOperation === CrudOperation.Create ? TypologyMessages.SUBTYPE_CREATION_SUCCESS : TypologyMessages.SUBTYPE_EDITION_SUCCESS
          );
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => {
          this.notificationsService.showErrorMessage(
            crudOperation === CrudOperation.Create ? TypologyMessages.SUBTYPE_CREATION_ERROR : TypologyMessages.SUBTYPE_EDITION_ERROR,
            error.message
          );
        }
      )
      .add(() => this.isProcessing = false);
  }


  validForms() {
    return this.isGeneralTabValid && this.isWorkflowTabValid;
  }


  tabChanged(evt: { nextId: string }) {

    // monaco editor must be enabled only after the tab is completely drawn, or it won't display
    // failure observed with timeout < 100ms, 200ms seems to work
    if (evt.nextId === this.messages.WORKFLOWS_TAB_TITLE) {
      setTimeout(() => this.canDisplayMonacoEditor = true, 200);
    } else {
      this.canDisplayMonacoEditor = false;
    }
  }


}
