/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonMessages } from '../../../../../shared/common-messages';
import { AllUsersMessages } from '../all-users-messages';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { NotificationsService } from '../../../../../services/notifications.service';
import { LoggableUser } from '../../../../../models/auth/loggable-user';
import { CrudOperation } from '../../../../../services/crud-operation';
import { AdminAllUsersService } from '@libriciel/iparapheur-provisioning';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';

@Component({
  selector: 'app-all-users-delete-popup',
  templateUrl: './all-users-delete-popup.component.html',
  styleUrls: ['./all-users-delete-popup.component.scss']
})
export class AllUsersDeletePopupComponent {


  static readonly INJECTABLE_SELECTED_USER = 'selected_user';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = AllUsersMessages;
  readonly styleEnum = Style;

  isProcessing = false;


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              public adminAllUsersService: AdminAllUsersService,
              public notificationsService: NotificationsService,
              @Inject(AllUsersDeletePopupComponent.INJECTABLE_SELECTED_USER) public selectedUser: UserDto) { }


  // </editor-fold desc="LifeCycle">


  submit() {
    this.isProcessing = true;

    this.adminAllUsersService
      .deleteUserAsSuperAdmin(this.selectedUser.id)
      .subscribe(
        () => {
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
          this.notificationsService.showCrudMessage(CrudOperation.Delete, new LoggableUser(this.selectedUser));
        },
        error => {
          this.notificationsService.showCrudMessage(CrudOperation.Delete, new LoggableUser(this.selectedUser), error.message, false);
        }
      )
      .add(() => this.isProcessing = false);
  }


}
