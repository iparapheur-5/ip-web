/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { User } from '../../../../../models/auth/user';
import { NotificationsService } from '../../../../../services/notifications.service';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { faAddressBook } from '@fortawesome/free-regular-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AllUsersMessages } from '../all-users-messages';
import { AllUsersDeletePopupComponent } from '../all-users-delete-popup/all-users-delete-popup.component';
import { CommonMessages } from '../../../../../shared/common-messages';
import { AdminUserEditPopupComponent } from '../../user-popup/admin-user-edit-popup/admin-user-edit-popup.component';
import { ActivatedRoute } from '@angular/router';
import { UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';
import { AdminAllUsersService, UserSortBy, UserPrivilege, UserRepresentation } from '@libriciel/iparapheur-provisioning';


@Component({
  selector: 'app-admin-all-users',
  templateUrl: './admin-all-users.component.html',
  styleUrls: ['./admin-all-users.component.scss']
})
export class AdminAllUsersComponent implements OnInit {


  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly ldapUserIcon = faAddressBook;
  readonly privilegeEnum = UserPrivilege;
  readonly sortByEnum = UserSortBy;
  readonly commonMessages = CommonMessages;
  readonly messages = AllUsersMessages;
  readonly commonIcons = CommonIcons;


  userList: UserRepresentation[] = [];
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  sortBy: UserSortBy = UserSortBy.LastName;
  currentSearchTerm = null;
  asc = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              private adminAllUsersService: AdminAllUsersService,
              private route: ActivatedRoute,
              public modalService: NgbModal) {}


  ngOnInit(): void {
    this.route.parent.data.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 6 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.refreshUserList();
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearchTerm = newTerm;
    this.page = 1;

    this.refreshUserList();
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  onRowOrderClicked(row: UserSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshUserList();
  }


  refreshUserList() {

    this.loading = true;

    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];

    this.adminAllUsersService
      .listUsersAsSuperAdmin(this.page - 1, this.getPageSize(this.pageSizeIndex), requestSortBy, this.currentSearchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('listUsers')))
      .subscribe(
        userRetrieved => {
          this.userList = userRetrieved.content;
          this.total = userRetrieved.totalElements;
        },
        error => this.notificationsService.showErrorMessage(AllUsersMessages.ERROR_RETRIEVING_USERS, error)
      )
      .add(() => this.loading = false);
  }


  onUserEditButtonClicked(user: User | UserRepresentation) {

    this.adminAllUsersService
      .getUserAsSuperAdmin(user.id)
      .subscribe(populatedUser => {
          this.modalService
            .open(AdminUserEditPopupComponent, {
                injector: Injector.create({
                  providers: [
                    {provide: AdminUserEditPopupComponent.INJECTABLE_USER_KEY, useValue: populatedUser},
                    {provide: AdminUserEditPopupComponent.INJECTABLE_USER_PREFERENCES_KEY, useValue: this.userPreferences},
                    {provide: AdminUserEditPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: null}
                  ]
                }),
                size: 'xl'
              }
            )
            .result
            .then(
              () => this.refreshUserList(),
              () => { /* Dismissed */ }
            );
        },
        error => this.notificationsService.showErrorMessage(AllUsersMessages.ERROR_RETRIEVING_USERS, error)
      );
  }


  onDeleteUserButtonClicked(user: User | UserRepresentation) {
    this.adminAllUsersService.getUserAsSuperAdmin(user.id)
      .subscribe(
        userDto => this.modalService
          .open(AllUsersDeletePopupComponent, {
              injector: Injector.create({
                providers: [{provide: AllUsersDeletePopupComponent.INJECTABLE_SELECTED_USER, useValue: userDto}]
              }),
              size: 'lg'
            }
          )
          .result
          .then(
            () => this.refreshUserList(),
            () => { /* Dismissed */ }
          ),
        error => this.notificationsService.showErrorMessage(AllUsersMessages.ERROR_RETRIEVING_USERS, error)
      );
  }


}
