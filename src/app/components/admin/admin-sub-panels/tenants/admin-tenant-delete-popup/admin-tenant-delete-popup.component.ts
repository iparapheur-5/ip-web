/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TenantMessages } from '../tenant-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { forkJoin, Observable, of, BehaviorSubject, throwError } from 'rxjs';
import { NotificationsService } from '../../../../../services/notifications.service';
import { AdminFolderService } from '../../../../../services/ip-core/admin-folder.service';
import * as freeSolidIcons from '@fortawesome/free-solid-svg-icons';
import { faCheck, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { tap, catchError, map } from 'rxjs/operators';
import { BatchResult } from '../../../../../models/commons/batch-result';
import { CrudOperation } from '../../../../../services/crud-operation';
import { AdminSecureMailService } from '../../../../../services/ip-core/admin-secure-mail.service';
import { AdminTemplatesService } from '../../../../../services/ip-core/admin-templates.service';
import { AdminMetadataService, AdminLayerService, AdminSealCertificateService, MetadataSortBy, LayerSortBy, TenantService, AdminWorkflowDefinitionService, AdminTrashBinService, FolderSortBy, AdminDeskService, ExternalSignatureConfigDto, AdminExternalSignatureService, UserRepresentation, DeskRepresentation, TenantRepresentation, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { Metadata } from '../../../../../models/metadata';
import { PaginatedResult } from '../../../../../models/paginated-result';
import { IpService } from '../../../../../shared/service/ip-service';
import { Layer } from '../../../../../models/pdf-stamp/layer';
import { SealCertificate } from '../../../../../models/seal-certificate';
import { WorkflowDefinition } from '../../../../../models/workflow-definition';
import { Folder } from '../../../../../models/folder/folder';
import { HttpErrorResponse } from '@angular/common/http';
import { AdminDeskService as ProvisioningAdminDeskService, AdminTypologyService, AdminTenantUserService, AdminTenantService } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-admin-tenant-delete-popup',
  templateUrl: './admin-tenant-delete-popup.component.html',
  styleUrls: ['./admin-tenant-delete-popup.component.scss']
})
export class AdminTenantDeletePopupComponent implements OnInit {

  static readonly injectableTenantKey = 'tenant';

  readonly messages = TenantMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly processState = ProcessState;
  readonly styles = Style;
  readonly successIcon = faCheck;
  readonly toBeProcessIcon = faCaretRight;
  readonly failureIcon = CommonIcons.CLOSE_ICON;
  readonly batchSize = 10;

  state: ProcessState;
  isFormValid = false;
  completedCalls = 0;
  totalCalls = 0;
  confirmTenantName = '';

  items: TobeDeletedItem[] = [
    new TobeDeletedItem(CommonMessages.FOLDER_NAME, this.adminFolderService.deleteFolders(this.tenant.id, this.batchSize), this.adminFolderService.getFoldersCount(this.tenant.id)),
    new TobeDeletedItem(CommonMessages.TRASH_BIN_NAME, this.deleteAllTrashBinFolders(this.tenant.id, this.batchSize), this.adminTrashBinService.listTrashBinFolders(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.TYPE_AND_SUBTYPE_NAME, this.deleteAllTypes(this.tenant.id, this.batchSize), this.adminTypologyService.listTypes(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.SEAL_CERTIFICATE_NAME, this.deleteAllSealCertificates(this.tenant.id, this.batchSize), this.adminSealCertificateService.listSealCertificateAsAdmin(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.SECURE_MAIL_SERVER_NAME, this.adminSecureMailService.deleteSecureMailServers(this.tenant.id, this.batchSize), this.adminSecureMailService.getSecureMailServersCount(this.tenant.id)),
    new TobeDeletedItem(CommonMessages.EXTERNAL_SIGNATURE_CONFIG_NAME, this.deleteAllExternalSignatureConfigs(this.tenant.id, this.batchSize), this.adminExternalSignatureService.listExternalSignatureConfigs(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.LAYER_NAME, this.deleteAllLayers(this.tenant.id, this.batchSize), this.adminLayerService.listLayers(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.WORKFLOW_NAME, this.deleteAllWorkflowDefinitions(this.tenant.id, this.batchSize), this.adminWorkflowDefinitionService.listWorkflowDefinitions(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.DESK, this.deleteAllDesks(this.tenant.id, this.batchSize), this.provisioningAdminDeskService.listDesks(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.METADATA_NAME, this.deleteAllMetadata(this.tenant.id, this.batchSize), this.adminMetadataService.listMetadataAsAdmin(this.tenant.id, false, null, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.USER, this.deleteAllUsers(this.tenant.id, this.batchSize), this.adminTenantUserService.listTenantUsers(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))),
    new TobeDeletedItem(CommonMessages.TEMPLATE_NAME, this.adminTemplatesService.deleteTemplates(this.tenant.id), this.adminTemplatesService.getCustomTemplatesCount(this.tenant.id)),
  ];


  // <editor-fold desc="LifeCycle">


  constructor(public adminExternalSignatureService: AdminExternalSignatureService,
              public adminTrashBinService: AdminTrashBinService,
              public adminSecureMailService: AdminSecureMailService,
              public adminSealCertificateService: AdminSealCertificateService,
              public adminTenantService: AdminTenantService,
              public adminFolderService: AdminFolderService,
              public adminLayerService: AdminLayerService,
              private adminTypologyService: AdminTypologyService,
              public adminMetadataService: AdminMetadataService,
              public adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              public adminDeskService: AdminDeskService,
              private provisioningAdminDeskService: ProvisioningAdminDeskService,
              private adminTenantUserService: AdminTenantUserService,
              private tenantService: TenantService,
              public adminTemplatesService: AdminTemplatesService,
              public activeModal: NgbActiveModal,
              private notificationsService: NotificationsService,
              @Inject(AdminTenantDeletePopupComponent.injectableTenantKey) public tenant?: TenantRepresentation) { }


  ngOnInit(): void {
    this.state = ProcessState.loading;
    const requests = [];
    this.items.forEach(item => requests.push(item.getCount$.pipe(tap(result => item.value = result))))

    forkJoin(requests)
      .subscribe(
        () => {
          this.totalCalls = this.items
            .map(item => Math.ceil(item.value / this.batchSize))
            .reduce((a, b) => a + b, 0);
          this.state = ProcessState.tobeProcessed;
        },
        error => this.notificationsService.showErrorMessage(this.messages.ErrorLoadingTenantOrDependencies(this.tenant.name), error.message)
      )
  }


  // <editor-fold desc="LifeCycle">


  /**
   * Removing the last admin user of a tenant is forbidden and returns a 406 code.
   * But when we are in a tenant deletion process, we can let a last admin amongst tenant user.
   * It  won't affect the final tenant deletion.
   */
  deleteAllUsers(tenantId: string, batchSize: number): Observable<BatchResult> {

    // batch of size 1 will be a problem since we must let the last admin user
    batchSize = Math.max(batchSize, 2);

    return new Observable(observer => {
      let dataObservable: BehaviorSubject<UserRepresentation[]> = new BehaviorSubject<UserRepresentation[]>([]);
      // Retrieving a batch of users to be deleted to initiate the process
      this.adminTenantUserService.listTenantUsers(tenantId, 0, batchSize)
        .subscribe(data => {
          dataObservable.next(data.content);
          console.debug('Récupéré : ' + data.content.map(element => element.id).join(', '));
          const total = Math.ceil((data.totalElements - 1) / batchSize);
          let completed = 0;
          dataObservable.subscribe(users => {
            if (users.length > 1) {
              // Building a batch of deletion
              const removeCalls = [];
              users.forEach(user => removeCalls.push(this.adminTenantUserService.removeUser(tenantId, user.id)
                .pipe(catchError((err: HttpErrorResponse) => err.status === 406 ? of(null) : throwError(err))),
              ));
              console.debug('Suppression : ' + users.map(user => user.id).join(', '));
              // Send a batch of deletions
              forkJoin(removeCalls)
                .subscribe(() => {
                    // All deletions have succeeded
                    console.debug('Supprimés');
                    completed++;
                    // Send a status report
                    observer.next({completed: completed, total: total, done: false});
                    console.debug('Récupération');
                    // Retrieve other elements to be deleted
                    this.adminTenantUserService.listTenantUsers(tenantId, 0, batchSize)
                      .subscribe(data => {
                        console.debug('Récupéré : ' + data.content.map(user => user.id).join(', '));
                        dataObservable.next(data.content);
                      });
                  },
                  error => observer.error(error)
                );
            } else {
              // No more elements of type T, deletion process is complete
              observer.next({completed: completed, total: total, done: true});
              observer.complete();
            }
          });
        });
    });
  }


  deleteAllExternalSignatureConfigs(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<ExternalSignatureConfigDto>(
      tenantId,
      batchSize,
      (tid, p, b) => this.adminExternalSignatureService.listExternalSignatureConfigs(tid, p, b)
        .pipe(map(page => {
            return {
              data: page.content,
              page: page.pageable.pageNumber,
              pageSize: page.size,
              total: page.totalElements
            }
          }
        )),
      (tid, s) => this.adminExternalSignatureService.deleteExternalSignatureConfig(tid, s.id)
    );
  }

  deleteAllDesks(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<DeskRepresentation>(
      tenantId,
      batchSize,
      (t, p, b) => this.provisioningAdminDeskService.listDesks(tenantId, p, b)
        .pipe(map(page => {
            let result: PaginatedResult<DeskRepresentation> = new PaginatedResult<DeskRepresentation>();
            result.data = page.content;
            result.page = page.number;
            result.total = page.totalElements;
            result.pageSize = page.size;
            return result
          }),
          catchError(this.notificationsService.handleHttpError('listDesks'))
        ),
      (tid, d) => this.provisioningAdminDeskService.deleteDesk(tid, d.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteDesk')))
    );
  }


  deleteAllMetadata(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<Metadata>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminMetadataService
        .listMetadataAsAdmin(tenantId, false, null, currentPage, batchSize, [MetadataSortBy.Id + ',ASC'])
        .pipe(
          map(data => {
            const result = new PaginatedResult<Metadata>();
            result.data = data.content.map(dto => Object.assign(new Metadata(), dto));
            result.total = data.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin'))
        ),
      (_, metadata) => this.adminMetadataService
        .deleteMetadata(tenantId, metadata.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteMetadata')))
    );
  }


  deleteAllLayers(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<Layer>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminLayerService
        .listLayers(tenantId, currentPage, batchSize, [LayerSortBy.Id + ',ASC'])
        .pipe(
          map(data => {
            const result = new PaginatedResult<Layer>();
            result.data = data.content.map(dto => Object.assign(new Layer(), dto));
            result.total = data.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin'))
        ),
      (_, layer) => this.adminLayerService
        .deleteLayer(tenantId, layer.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteLayer')))
    );
  }


  deleteAllTrashBinFolders(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<Folder>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminTrashBinService
        .listTrashBinFolders(tenantId, currentPage, batchSize, [`${FolderSortBy.FolderId},ASC`])
        .pipe(
          map(data => {
            const result = new PaginatedResult<Folder>();
            result.data = data.content.map(dto => Object.assign(new Folder(), dto));
            result.total = data.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('getTrashBinFolders'))
        ),
      (_, folder) => this.adminTrashBinService
        .deleteTrashBinFolder(tenantId, folder.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteTrashBinFolder')))
    );
  }


  deleteAllTypes(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<TypeRepresentation>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminTypologyService.listTypes(tenantId, currentPage, batchSize)
        .pipe(
          map(page => {
            const result = new PaginatedResult<TypeRepresentation>();
            result.data = page.content;
            result.total = page.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listSealCertificateAsAdmin'))
        ),
      (_, type) => this.adminTypologyService
        .deleteType(tenantId, type.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteType')))
    );
  }


  deleteAllWorkflowDefinitions(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<WorkflowDefinition>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminWorkflowDefinitionService
        .listWorkflowDefinitions(tenantId, currentPage, batchSize)
        .pipe(
          map(page => {
            const result = new PaginatedResult<WorkflowDefinition>();
            result.data = page.content.map(dto => Object.assign(new WorkflowDefinition(), dto));
            result.total = page.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listWorkflowDefinitions'))
        ),
      (_, workflowDefinition) => this.adminWorkflowDefinitionService
        .deleteWorkflowDefinition(tenantId, workflowDefinition.key)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteWorkflowDefinition')))
    );
  }


  deleteAllSealCertificates(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<SealCertificate>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminSealCertificateService
        .listSealCertificateAsAdmin(tenantId, currentPage, batchSize)
        .pipe(
          map(page => {
            const result = new PaginatedResult<SealCertificate>();
            result.data = page.content.map(dto => Object.assign(new SealCertificate(), dto));
            result.total = page.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listSealCertificateAsAdmin'))
        ),
      (_, sealCertificate) => this.adminSealCertificateService
        .deleteSealCertificate(tenantId, sealCertificate.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteSealCertificate')))
    );
  }


  onDeleteButtonClicked() {
    this.state = ProcessState.processing;
    const itemsToBeDeleted = this.items.slice();
    const deleteProcess: BehaviorSubject<TobeDeletedItem> = new BehaviorSubject<TobeDeletedItem>(itemsToBeDeleted.shift());
    new Observable(observer => deleteProcess
      .subscribe(item => {
        if (item) {
          console.debug('suppression des ' + item.name);
          if (item.value > 0) {
            item.state = ProcessState.processing;
            // For each batch, number of completed batch is incremented (for progress bars)
            item.deleteBatch$.subscribe(
              batchResult => {
                if (!batchResult.done) {
                  this.completedCalls++;
                  item.completed++;
                }
                console.debug('Lot terminé : ' + batchResult.completed + '/' + batchResult.total);
                if (batchResult.done) {
                  item.state = ProcessState.done;
                  deleteProcess.next(itemsToBeDeleted.shift());
                }
              },
              error => {
                item.state = ProcessState.failure;
                observer.error(error);
              }
            )
          } else {
            // Nothing to do with this item
            item.state = ProcessState.done;
            deleteProcess.next(itemsToBeDeleted.shift());
          }
        } else {
          // last item has been processed, all deletions have been done
          observer.next();
          observer.complete();
        }
      }))
      .subscribe(
        // All dependencies have been deleted, deletion of the tenant
        () => {
          this.adminTenantService
            .deleteTenant(this.tenant.id)
            .pipe(catchError(this.notificationsService.handleHttpError('delete tenant')))
            .subscribe(
              () => {
                this.state = ProcessState.done;
              },
              error => {
                this.state = ProcessState.tobeProcessed;
                this.notificationsService.showCrudMessage(CrudOperation.Delete, this.tenant, error.message, false);
              }
            )
        },
        // There was a problem during dependencies deletions
        error => {
          this.state = ProcessState.failure;
          this.notificationsService.showCrudMessage(CrudOperation.Delete, this.tenant, error.message, false);
        }
      )
  }


  onChange($event: string) {
    this.isFormValid = this.tenant.name.trim() === $event.trim();
  }


  getProgressPerCent(): number {
    return (this.totalCalls === 0 || this.state === ProcessState.done)
      ? 100
      : Math.round(this.completedCalls * 100 / this.totalCalls);
  }


  getProgressPerCentForItem(item: TobeDeletedItem) {
    return (item.value === 0 || item.state === ProcessState.done)
      ? 100
      : Math.round(item.completed * 100 / Math.ceil(item.value / this.batchSize));
  }


  getIcon(item: TobeDeletedItem): freeSolidIcons.IconDefinition {
    switch (item.state) {
      case ProcessState.tobeProcessed:
        return this.toBeProcessIcon;
      case ProcessState.processing:
        return CommonIcons.SPINNER_ICON
      case ProcessState.done:
        return this.successIcon;
      case ProcessState.failure:
        return this.failureIcon;
    }
  }


}


class TobeDeletedItem {

  name: string;
  value: number;
  state: ProcessState;
  completed: number;
  getCount$: Observable<number>
  deleteBatch$: Observable<BatchResult>


  constructor(name: string, deleteBatch$: Observable<BatchResult>, getCount$?: Observable<number>) {
    this.name = name;
    this.state = ProcessState.tobeProcessed;
    this.value = 0;
    this.completed = 0;
    this.getCount$ = getCount$ ? getCount$ : of(undefined);
    this.deleteBatch$ = deleteBatch$;
  }

}


enum ProcessState {
  tobeProcessed = 'tobeProcessed', processing = 'Processing', done = 'Done', failure = 'Failure', loading = 'Loading'
}


