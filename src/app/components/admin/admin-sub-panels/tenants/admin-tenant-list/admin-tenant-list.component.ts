/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit, Injector } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminTenantPopupComponent } from '../admin-tenant-popup/admin-tenant-popup.component';
import { NotificationsService } from '../../../../../services/notifications.service';
import { AdminTenantDeletePopupComponent } from '../admin-tenant-delete-popup/admin-tenant-delete-popup.component';
import { CommonMessages } from '../../../../../shared/common-messages';
import { AdminMessages } from '../../admin-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { ActivatedRoute } from '@angular/router';
import { AdminTenantService, TenantSortBy, UserPreferencesDto, TenantRepresentation } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-admin-tenant-list',
  templateUrl: './admin-tenant-list.component.html',
  styleUrls: ['./admin-tenant-list.component.scss']
})
export class AdminTenantListComponent implements AfterViewInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretUpIcon = faCaretUp;
  readonly caretDownIcon = faCaretDown;
  readonly sortByEnum = TenantSortBy;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = AdminMessages;


  tenantList: TenantRepresentation[] = [];
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  searchTerm = '';
  sortBy: TenantSortBy = TenantSortBy.Name;
  asc = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;

  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminTenantService: AdminTenantService,
              private route: ActivatedRoute,
              public modalService: NgbModal) {}


  ngAfterViewInit() {
    this.route.parent.data.subscribe(data => this.userPreferences = data["userPreferences"]);
    this.colspan = 2 + (this.userPreferences.showAdminIds ? 1 : 0);
    this.refreshTenantList();
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(searchTerm: string) {
    this.searchTerm = searchTerm;
    this.page = 1;
    this.refreshTenantList();
  }


  refreshTenantList() {

    this.loading = true;

    let searchTerm = this.searchTerm;
    searchTerm = searchTerm.length > 0 ? searchTerm : null;

    this.adminTenantService
      .listTenantsAsAdmin(this.page - 1, this.getPageSize(this.pageSizeIndex), [this.sortBy + (this.asc ? ',ASC' : ',DESC')], searchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('listTenantsAsAdmin')))
      .subscribe(
        tenantsRetrieved => {
          this.tenantList = tenantsRetrieved.content;
          this.total = tenantsRetrieved.totalElements;
        },
        error => this.notificationsService.showErrorMessage(AdminMessages.ERROR_REQUESTING_TENANTS, error)
      )
      .add(() => this.loading = false);
  }


  onCreateButtonClicked() {

    this.modalService
      .open(
        AdminTenantPopupComponent,
        {
          injector: Injector.create({
            providers: [
              {provide: AdminTenantPopupComponent.injectableTenantKey, useValue: undefined},
              {provide: AdminTenantPopupComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
            ]
          })
        }
      )
      .result
      .then(
        () => this.refreshTenantList(),
        () => { /* Dismissed */ }
      );
  }


  onEditButtonClicked(tenant: TenantRepresentation) {

    this.modalService
      .open(
        AdminTenantPopupComponent,
        {
          injector: Injector.create({
            providers: [
              {provide: AdminTenantPopupComponent.injectableTenantKey, useValue: tenant},
              {provide: AdminTenantPopupComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
            ]
          })
        }
      )
      .result
      .then(
        () => this.refreshTenantList(),
        () => { /* Dismissed */ }
      );
  }


  onDeleteButtonClicked(tenant: TenantRepresentation) {
    this.modalService
      .open(
        AdminTenantDeletePopupComponent,
        {
          injector: Injector.create({providers: [{provide: AdminTenantDeletePopupComponent.injectableTenantKey, useValue: tenant}]}),
          size: 'lg'
        }
      )
      .result
      .then(
        () => this.refreshTenantList(),
        () => { /* Dismissed */ }
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  onRowOrderClicked(row: TenantSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshTenantList();
  }


}
