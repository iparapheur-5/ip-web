/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class TenantMessages {

  static readonly CREATE_TENANT_POPUP_TITLE = `Création d'une entité`;

  static readonly DELETE_TENANT_FORM_LIST_LABEL = 'Cette action entrainera la suppression de :';
  static readonly DELETE_TENANT_FORM_CONFIRM_LABEL = `Veuillez saisir le nom de l'entité afin de confirmer votre choix`;
  static readonly DELETE_TENANT = `Supprimer définitivement l'entité`;
  static readonly DELETE_TENANT_PROCESSING_MESSAGE = `Veuillez ne pas fermer l'onglet pendant la suppression.`;
  static readonly DELETE_TENANT_FAILURE_MESSAGE = `L'entité n'a pas pu être supprimée entièrement ----`;
  static readonly DELETE_TENANT_SUCCESS_MESSAGE = `L'entité a été supprimée avec succès.`;

  static readonly DELETE_TENANT_ALERT_MESSAGE = '<b>Vous êtes sur le point de supprimer de façon définitive cette entité.</b>' +
    '<br> Une fois cette entité définitivement supprimée, il ne sera plus possible de la récupérer.' +
    '<br>Cette action est <b>irréversible</b>.' +
    '<br>Supprimer définitivement cette entité sera <b>immédiat</b>.';

  static editTenantPopupTitle = (tenantName: string): string => `Modification de l'entité ${tenantName}`;

  static deleteTenantPopupTitle = (tenantName: string): string => `Suppression de l'entité ${tenantName}`;

  static ErrorLoadingTenantOrDependencies = (tenantName: string) => `Erreur lors du chargement des informations de l'entité ${tenantName}`;

}
