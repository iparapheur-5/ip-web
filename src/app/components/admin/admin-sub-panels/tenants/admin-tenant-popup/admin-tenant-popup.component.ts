/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { TenantMessages } from '../tenant-messages';
import { AdminTenantService } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';
import { UserPreferencesDto, TenantRepresentation } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-admin-tenant-popup',
  templateUrl: './admin-tenant-popup.component.html',
  styleUrls: ['./admin-tenant-popup.component.scss']
})
export class AdminTenantPopupComponent {

  public static readonly injectableTenantKey = 'tenant';
  public static readonly injectableUserPreferencesKey = 'userPreferences';


  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = TenantMessages;

  isProcessing = false;
  pendingTenant: TenantRepresentation;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminTenantService: AdminTenantService,
              public activeModal: NgbActiveModal,
              @Inject(AdminTenantPopupComponent.injectableUserPreferencesKey) public userPreferences: UserPreferencesDto,
              @Inject(AdminTenantPopupComponent.injectableTenantKey) public targetTenant?: TenantRepresentation) {
    this.pendingTenant = !!targetTenant ? targetTenant : {id: null, name: null};
  }


  // </editor-fold desc="LifeCycle">


  onSaveButtonClicked() {

    this.isProcessing = true;

    let observable = (this.targetTenant)
      ? this.adminTenantService.updateTenant(this.targetTenant.id, this.pendingTenant)
      : this.adminTenantService.createTenant(this.pendingTenant);

    observable
      .pipe(catchError(this.notificationsService.handleHttpError('update/create tenant')))
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(`L'entité ${this.pendingTenant.name} a été ${this.targetTenant ? `édité` : 'créé'} avec succès`);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationsService.showErrorMessage(`Erreur lors de la création de l'entité ${this.pendingTenant.name}`, error.message)
      )
      .add(() => this.isProcessing = false);
  }


}
