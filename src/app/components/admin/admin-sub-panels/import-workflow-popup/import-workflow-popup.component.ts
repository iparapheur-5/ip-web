/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../services/notifications.service';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../shared/common-messages';
import { TenantRepresentation } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-import-workflow-popup',
  templateUrl: './import-workflow-popup.component.html',
  styleUrls: ['./import-workflow-popup.component.scss']
})
export class ImportWorkflowPopupComponent {

  public static readonly INJECTABLE_TENANT_KEY = 'tenant';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;

  xmlId: string;
  xmlName: string;
  xmlSize: string;


  constructor(public notificationService: NotificationsService,
              public activeModal: NgbActiveModal,
              @Inject(ImportWorkflowPopupComponent.INJECTABLE_TENANT_KEY) public targetTenant?: TenantRepresentation) {
  }


  getFileDetails(event: any) {

    const fileReader = new FileReader();

    for (const file of event.target.files) {

      fileReader.onload = () => {

        // TODO : find value with the XPath, the proper way
        // Something like `/definitions/process/@name`...
        // I can't make doc.evaluate work...
        const nameRegex = /<(?:.*?:)?process\s+.*?name="(.*?)".*?>/;
        const idRegex = /<(?:.*?:)?process\s+.*?id="(.*?)".*?>/;

        const xmlString = fileReader.result.toString();

        this.xmlId = xmlString.match(idRegex)[1];
        this.xmlName = xmlString.match(nameRegex)[1];
      };

      fileReader.readAsText(file);

      this.xmlSize = Math.round(file.size / 1024) + ' Ko';
    }
  }


  /*sendRequest(name: string, files: FileList) {
    // this.adminWorkflowDefinitionService
    //   .importWorkflowDefinition(this.targetTenant, name, files.item(0))
    //   .subscribe(() => {
    //     this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
    //     this.notificationService.showSuccessMessage('Le circuit "' + name + '" a été importé avec succès.');
    //   });
  }*/


}
