/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { WorkflowModelLocalizedMessages } from '@libriciel/ls-composants/workflows';
import { CommonMessages } from '../../../../shared/common-messages';

export class WorkflowEditorMessages extends WorkflowModelLocalizedMessages {

  static readonly WORKFLOW_NAME_LABEL = 'Nom du circuit\u00A0:';

  static readonly CREATE_WORKFLOW_TITLE = 'Création du circuit';
  static readonly EDIT_WORKFLOW_TITLE = 'Modification du circuit';
  static readonly CREATE_WORKFLOW = 'Créer le circuit';
  static readonly SAVE_WORKFLOW = 'Enregistrer les modifications';

  static readonly COPY_TXT = ' (copie)';
  // Step editor

  static readonly PICK_A_METADATA = 'Choisissez une métadonnée...';
  static readonly POP_CREATE_TITLE = `Nouvelle étape ${CommonMessages.APP_NAME}`;

  static readonly STEP_TYPE_LABEL = `Type d'étape\u00A0:`;
  static readonly VALIDATION_MODE_LABEL = 'Mode de validation\u00A0:';
  static readonly DESK_SEARCH_LABEL = 'Choix du bureau\u00A0:';
  static readonly NOTIFIED_DESKS_LABEL = 'Bureaux notifiés\u00A0:';
  static readonly MANDATORY_METADATA_VALIDATION_LABEL = 'Métadonnées obligatoires\u00A0:';
  static readonly MANDATORY_METADATA_REJECTION_LABEL = 'Métadonnées de rejet obligatoires\u00A0:';

}
