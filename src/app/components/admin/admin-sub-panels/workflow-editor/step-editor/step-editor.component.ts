/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IpStepModel, STEP_TYPE_LIST, StepType, StepValidationMode } from '../../../../../models/workflows/ip-step-model';
import { Observable } from 'rxjs';
import { CommonMessages } from '../../../../../shared/common-messages';
import { WorkflowEditorMessages } from '../workflow-editor-messages';
import { ActivatedRoute } from '@angular/router';
import { compareById } from '../../../../../utils/string-utils';
import { Metadata } from '../../../../../models/metadata';
import { StepToActionPipe } from '../../../../../shared/utils/step-to-action.pipe';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { GENERIC_DESK_IDS } from '../../../../../models/workflows/ip-workflow-actor';
import { AdminDeskService, PageDeskRepresentation } from '@libriciel/iparapheur-provisioning';


@Component({
  selector: 'app-step-editor',
  templateUrl: './step-editor.component.html',
  styleUrls: ['./step-editor.component.scss']
})
export class StepEditorComponent implements OnInit {

  readonly messages = WorkflowEditorMessages;
  readonly commonMessages = CommonMessages;
  readonly compareByIdFn = compareById;
  readonly enumToArrayFn = Object.keys;
  readonly validationModesEnum = StepValidationMode;
  readonly availableStepTypes: StepType[] = STEP_TYPE_LIST;


  @Input() stepModel = new IpStepModel();
  @Input() tenantMetadataList: Metadata[] = [];
  @Input() lastStep = false;
  @Output() validateChange = new EventEmitter();
  @Output() cancelChange = new EventEmitter();


  updatedStepModel: IpStepModel;
  tenantId: string;
  validateBtnDisabled = true;


  // <editor-fold desc="LifeCycle">


  constructor(public adminDeskService: AdminDeskService,
              private route: ActivatedRoute) {}


  ngOnInit(): void {
    this.updatedStepModel = new IpStepModel(this.stepModel).clone();

    if (this.updatedStepModel) {

      if (!this.updatedStepModel.type) {
        this.selectType(StepType.Visa);
      }
      if (!this.updatedStepModel.validationMode) {
        this.updatedStepModel.validationMode = StepValidationMode.Simple;
      }
    }

    this.route.params.subscribe(params => this.tenantId = params['tenantId']);
    this.checkDataValidity();
  }


  // </editor-fold desc="LifeCycle">


  retrieveDesksAsAdminFn = (page: number, pageSize: number, searchTerm): Observable<PageDeskRepresentation> => {
    return this.adminDeskService.listDesks(this.tenantId, page, pageSize, [], searchTerm);
  }


  selectType(type: StepType) {

    const action = StepToActionPipe.compute(type);

    this.updatedStepModel.type = type;
    this.updatedStepModel.name = ActionNamePipe.compute(action);

    if (this.isExternal(type)) {
      this.updatedStepModel.validationMode = StepValidationMode.Simple;
      this.updatedStepModel.validators = this.updatedStepModel.validators.slice(0, 1);
    }
  }


  isSelectedType(type: StepType): boolean {
    return this.updatedStepModel.type === type;
  }


  isExternal(type: StepType): boolean {
    return [StepType.ExternalSignature, StepType.Ipng, StepType.SecureMail].indexOf(type) !== -1;
  }


  isMultiple(mode: StepValidationMode): boolean {
    return mode !== this.validationModesEnum.Simple;
  }


  isModeDisabledForStepType(mode: StepValidationMode, type: StepType): boolean {
    if (this.isMultiple(mode) && this.isExternal(type)) {
      return true;
    }

    // We forbid 'collaborative' seal steps, for now.
    return mode === this.validationModesEnum.And
      && type === StepType.Seal;
  }


  deskSelectionChanged(evt) {
    if (this.isMultiple(this.updatedStepModel.validationMode)) {
      this.updatedStepModel.validators = evt;
    }
    // Filtering out nulls
    this.updatedStepModel.validators = this.updatedStepModel.validators.filter(v => !!v);

    this.checkDataValidity();
  }


  validationModeChanged(mode) {
    if (this.isMultiple(mode)) {
      this.updatedStepModel.validators = this.updatedStepModel.validators.filter(v => Object.values(GENERIC_DESK_IDS).indexOf(v?.id as string) === -1);
      this.checkDataValidity();
    } else {
      this.updatedStepModel.validators = this.updatedStepModel.validators.slice(0, 1);
    }
  }


  /**
   * TODO : a FormGroup/GroupControl instead
   */
  checkDataValidity() {

    if (!this.lastStep && !this.updatedStepModel.type) {
      this.validateBtnDisabled = true;
      return;
    }

    if (!this.updatedStepModel.validators || this.updatedStepModel.validators.length === 0 || !this.updatedStepModel.validators[0]) {
      this.validateBtnDisabled = true;
      return;
    }

    this.validateBtnDisabled = false;
  }


  addOrUpdateStep() {
    this.validateChange.emit(this.updatedStepModel);
  }


  cancel() {
    this.cancelChange.emit();
  }


}
