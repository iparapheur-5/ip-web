/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Output, EventEmitter, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { WorkflowModelOptions, StepModel, WorkflowModelLocalizedMessages, WorkflowActor } from '@libriciel/ls-composants/workflows';
import { IpStepModel, StepValidationMode } from '../../../../../models/workflows/ip-step-model';
import { ActionIconPipe } from '../../../../../shared/utils/action-icon.pipe';
import { IpWorkflowModel } from '../../../../../models/workflows/ip-workflow-model';
import { GenericWorkflowActors } from '../../../../../models/workflows/ip-workflow-actor';
import { Subscription, combineLatest } from 'rxjs';
import { debounceTime, catchError } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { WorkflowEditorMessages } from '../workflow-editor-messages';
import { Metadata } from '../../../../../models/metadata';
import { CrudOperation } from '../../../../../services/crud-operation';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { StepToActionPipe } from '../../../../../shared/utils/step-to-action.pipe';
import { Action, AdminMetadataService, MetadataSortBy, AdminWorkflowDefinitionService } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../../shared/models/secondary-action.enum';
import { WorkflowDefinition } from '../../../../../models/workflow-definition';
import { DataConversionService } from '../../../../../services/data-conversion.service';

@Component({
  selector: 'app-workflow-editor',
  templateUrl: './workflow-editor.component.html',
  styleUrls: ['./workflow-editor.component.scss']
})
export class WorkflowEditorComponent implements AfterViewInit, OnDestroy {

  readonly messages = WorkflowEditorMessages;
  readonly commonMessages = CommonMessages;
  readonly workflowEditorLocalizedMessages: WorkflowModelLocalizedMessages = {
    popCreateTitle: WorkflowEditorMessages.POP_CREATE_TITLE,
    // popEditTitle: 'The title of the edition popover',
  };

  @Output() finished = new EventEmitter();

  @ViewChild('popEditContent') popEditContent: ElementRef;
  @ViewChild('popCreateContent') popCreateContent: ElementRef;
  @ViewChild('endpointPopoverEditContent') endpointPopoverEditContent: ElementRef;
  @ViewChild('stepDisplayContent') stepDisplayContent: ElementRef;
  @ViewChild('lastStepDisplayContent') lastStepDisplayContent: ElementRef;

  options: WorkflowModelOptions = {
    editorGlobalLayoutClass: 'workflow-editor-custom-layout',
    startIconClass: ActionIconPipe.computeToString(Action.Start),
    endIconClass: ActionIconPipe.computeToString(SecondaryAction.End),
    additionalFinalEndpointClasses: 'with-content'
  };

  workflowDefinition: WorkflowDefinition = new WorkflowDefinition();
  tenantMetadataList: Metadata[] = [];

  updatedIpWorkflowModel: IpWorkflowModel = new IpWorkflowModel();
  updatedLastStepData: IpStepModel = new IpStepModel();

  updatedWorkflowIsValid = false;
  waitingForAnswer = false;
  editMode = false;
  tenantId: string = null;

  workflowNameSubscription: Subscription;
  workflowNameCtrl: FormControl = new FormControl(
    this.workflowDefinition?.name,
    [
      Validators.required,
      Validators.minLength(2),
    ],
    // FIXME : pass the tenantId somehow : workflowNameValidatorFn(this.tenantId, this.adminWorkflowDefinitionService, this.debounceSubject, this.debouncedInput$)
  );


  // <editor-fold desc="LifeCycle">


  constructor(private adminMetadataService: AdminMetadataService,
              private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              private dataConversionService: DataConversionService,
              private notificationsService: NotificationsService,
              private route: ActivatedRoute,
              private router: Router) {}


  ngAfterViewInit(): void {
    combineLatest([this.route.params, this.route.data])
      .subscribe(([params, data]) => {

        this.tenantId = params['tenantId'];

        if (data.workflowDefinition!!) {
          this.workflowDefinition = data.workflowDefinition;
        }

        this.updatedIpWorkflowModel = this.dataConversionService.createWorkflowModelFromBackendData(this.workflowDefinition);

        if (this.router.url.endsWith("/clone")) {
          this.editMode = false;
          this.updatedIpWorkflowModel.id = null;
          this.updatedIpWorkflowModel.name = this.updatedIpWorkflowModel.name + this.messages.COPY_TXT;
        } else {
          this.editMode = !!this.workflowDefinition.id;
        }

        this.updatedLastStepData.validators = (this.workflowDefinition.finalDesk as WorkflowActor)
          ? [this.workflowDefinition.finalDesk as WorkflowActor]
          : [GenericWorkflowActors.EMITTER];

        this.updatedLastStepData.notifiedDesks = this.workflowDefinition.finalNotifiedDesks ?? [];

        // Name setup

        if (this.updatedIpWorkflowModel.name) {
          this.workflowNameCtrl.setValue(this.updatedIpWorkflowModel.name.trim());
        }

        this.workflowNameSubscription = this.workflowNameCtrl.valueChanges
          .pipe(debounceTime(600))
          .subscribe(newValue => {
            console.log('valueChanges.next, val : ', newValue);
            this.updatedIpWorkflowModel.name = newValue;
            this.checkCurrentWorkflowValidity();
          });

        // Metadata setup

        this.adminMetadataService
          .listMetadataAsAdmin(this.tenantId, false, null, 0, 250, [MetadataSortBy.Name, ",ASC"])
          .pipe(catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin')))
          .subscribe(data => {
            this.tenantMetadataList = data.content.map(dto => Object.assign(new Metadata(), dto));
          });
      });
  }


  ngOnDestroy(): void {
    this.workflowNameSubscription.unsubscribe();
  }


  // </editor-fold desc="LifeCycle">


  getStepIconClass(step: StepModel) {
    const action = StepToActionPipe.compute(step.type);
    return ActionIconPipe.computeToString(action);
  }


  getStepTooltip(step: StepModel) {
    const action = StepToActionPipe.compute(step.type);
    return ActionNamePipe.compute(action);
  }


  workflowWasUpdated() {
    console.log('workflowWasUpdated : ', this.updatedIpWorkflowModel);
    this.checkCurrentWorkflowValidity();
  }


  updateLastStep(data: IpStepModel) {
    this.updatedLastStepData = data;
    this.checkCurrentWorkflowValidity();
  }


  saveCurrent() {
    console.log('saving workflow, data : ', this.updatedIpWorkflowModel);

    // Checks and cleanups

    const updatedWorkflowDefinition = this.dataConversionService
      .createBackendDataFromWorkflowModel(this.workflowNameCtrl.value, this.updatedIpWorkflowModel, this.updatedLastStepData);

    updatedWorkflowDefinition.name = updatedWorkflowDefinition.name.trim();

    if (!this.updatedWorkflowIsValid || !(this.editMode && this.canSaveModifications())) {
      console.log('Not supposed to happen, cancelling...')
    }

    // Request

    this.waitingForAnswer = true;
    const crudOperation = this.editMode ? CrudOperation.Update : CrudOperation.Create;

    if (!this.editMode) {
      const key = this.dataConversionService.workflowNameToKey(updatedWorkflowDefinition.name);
      updatedWorkflowDefinition.id = key;
      updatedWorkflowDefinition.key = key;
    } else {
      updatedWorkflowDefinition.id = this.workflowDefinition.id;
      updatedWorkflowDefinition.key = this.workflowDefinition.key;
    }

    const request = this.editMode
      ? this.adminWorkflowDefinitionService.updateWorkflowDefinition(this.tenantId, updatedWorkflowDefinition.id, updatedWorkflowDefinition)
      : this.adminWorkflowDefinitionService.createWorkflowDefinition(this.tenantId, updatedWorkflowDefinition);

    request
      .pipe(catchError(this.notificationsService.handleHttpError('create/update workflow definition error')))
      .subscribe(
        () => {
          this.notificationsService.showCrudMessage(crudOperation, this.updatedIpWorkflowModel);
          this.finish();
        },
        error => this.notificationsService.showCrudMessage(crudOperation, this.updatedIpWorkflowModel, error.message, false)
      )
      .add(() => this.waitingForAnswer = false);
  }


  shouldDisplayStepValidationType(step: IpStepModel): boolean {
    return (step?.validationMode === StepValidationMode.And) || (step?.validationMode === StepValidationMode.Or);
  }


  canSaveModifications(): boolean {
    const alreadyInstantiated = this.editMode && this.updatedIpWorkflowModel.deploymentChildrenCount > 1;
    return this.updatedWorkflowIsValid && !alreadyInstantiated;
  }


  leaveEditor() {
    console.log('leave editor, back to list');
    this.finish();
  }


  checkCurrentWorkflowValidity() {
    this.updatedWorkflowIsValid = this.workflowIsValid()
      && !this.workflowNameCtrl.invalid;
  }


  workflowIsValid(): boolean {
    return this.updatedIpWorkflowModel && !!this.updatedIpWorkflowModel.steps;
  }


  private finish() {
    this.waitingForAnswer = false;
    this.router
      .navigate([`/admin/${this.tenantId}/workflows/`])
      .then(() => console.log('nav back'));
  }


}
