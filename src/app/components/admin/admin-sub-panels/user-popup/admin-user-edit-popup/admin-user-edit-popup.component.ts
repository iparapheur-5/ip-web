/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../../../models/auth/user';
import { AdminTenantUserService as LegacyAdminTenantUserService } from '../../../../../services/ip-core/admin-tenant-user.service';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { LoggableSignatureImage } from '../../../../../models/auth/loggable-signature-image';
import { LoggableUser } from '../../../../../models/auth/loggable-user';
import { CrudOperation } from '../../../../../services/crud-operation';
import { UserPopupMessages } from '../user-popup-messages';
import { Observable } from 'rxjs';
import { UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { AdminTenantUserService as ProvisioningAdminTenantUserService, AdminAllUsersService } from '@libriciel/iparapheur-provisioning';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-admin-user-edit-popup',
  templateUrl: './admin-user-edit-popup.component.html',
  styleUrls: ['./admin-user-edit-popup.component.scss']
})
export class AdminUserEditPopupComponent implements OnInit {


  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_USER_KEY = 'user';
  public static readonly INJECTABLE_USER_PREFERENCES_KEY = 'userPreferences';

  readonly messages = UserPopupMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;


  signatureImageSrc: string;
  isProcessing = false;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              private legacyAdminTenantUserService: LegacyAdminTenantUserService,
              private provisioningAdminTenantUserService: ProvisioningAdminTenantUserService,
              private adminAllUsersService: AdminAllUsersService,
              public activeModal: NgbActiveModal,
              @Inject(AdminUserEditPopupComponent.INJECTABLE_USER_KEY) public user: UserDto,
              @Inject(AdminUserEditPopupComponent.INJECTABLE_USER_PREFERENCES_KEY) public userPreferences: UserPreferencesDto,
              @Inject(AdminUserEditPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string) {
  }


  ngOnInit(): void {
    if (!!this.userPreferences.signatureImageContentId) {
      this.signatureImageSrc = this.legacyAdminTenantUserService.signatureImageUrl(this.tenantId, this.user.id, Date.now().toString());
    }
  }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="UI Callbacks">


  onSaveButtonClicked() {

    this.user.administeredDeskIds = this.user.administeredDesks?.map(desk => desk.id) ?? null;
    this.user.administeredTenantIds = this.user.administeredTenants?.map(tenant => tenant.id) ?? null;
    this.user.associatedTenantIds = this.user.associatedTenants?.map(tenant => tenant.id) ?? null;
    this.user.associatedDeskIds = this.user.associatedDesks?.map(desk => desk.id) ?? null;
    this.user.supervisedDeskIds = this.user.supervisedDesks?.map(desk => desk.id) ?? null;
    this.user.delegationManagedDeskIds = this.user.delegationManagedDesks?.map(desk => desk.id) ?? null;

    const requestObservable$: Observable<any> = !this.tenantId
      ? this.adminAllUsersService.updateUserAsSuperAdmin(this.user.id, this.user)
      : this.provisioningAdminTenantUserService.updateUser(this.tenantId, this.user.id, this.user);

    this.isProcessing = true;

    requestObservable$
      .pipe(catchError(this.notificationService.handleHttpError('updateUser')))
      .subscribe(
        () => {
          this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableUser(this.user));
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => {
          this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableUser(this.user), error.message, false);
        }
      )
      .add(() => this.isProcessing = false);
  }


  createSignature(event: { user: User, file: File }): void {
    this.legacyAdminTenantUserService
      .setSignatureImage(this.tenantId, event.user.id, event.file)
      .subscribe(
        signatureContentStringResult => {
          // FIXME Adrien : proper refresh the userPreferences
          this.userPreferences['signatureImageContentId' as UserPreferencesDto['signatureImageContentId']] = signatureContentStringResult.value;
          this.notificationService.showCrudMessage(CrudOperation.Create, new LoggableSignatureImage());
          this.signatureImageSrc = this.legacyAdminTenantUserService.signatureImageUrl(this.tenantId, this.user.id, Date.now().toString());
        },
        error => this.notificationService.showCrudMessage(CrudOperation.Create, new LoggableSignatureImage(), error.message, false)
      );
  }


  deleteSignature(): void {
    this.legacyAdminTenantUserService
      .deleteSignatureImage(this.tenantId, this.user.id)
      .subscribe(
        () => {
          this.notificationService.showCrudMessage(CrudOperation.Delete, new LoggableSignatureImage());
          this.userPreferences['signatureImageContentId' as UserPreferencesDto['signatureImageContentId']] = null; // FIXME Adrien : proper refresh the userPreferences
          this.signatureImageSrc = null;
        },
        error => this.notificationService.showCrudMessage(CrudOperation.Delete, new LoggableSignatureImage(), error.message, false)
      );
  }


  updateSignature(event: { user: User, file: File }): void {
    this.legacyAdminTenantUserService
      .updateSignatureImage(this.tenantId, event.user.id, event.file)
      .subscribe(
        () => {
          this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableSignatureImage());
          this.signatureImageSrc = this.legacyAdminTenantUserService.signatureImageUrl(this.tenantId, this.user.id, Date.now().toString());
        },
        error => this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableSignatureImage(), error.message, false)
      );
  }


  // </editor-fold desc="UI Callbacks">


}
