/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AdminUserSupervisingComponent } from './admin-user-supervising.component';
import { UserPrivilege } from '@libriciel/iparapheur-provisioning';


describe('AdminUserSupervisingComponent', () => {


  let component: AdminUserSupervisingComponent;
  let fixture: ComponentFixture<AdminUserSupervisingComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [HttpClient, HttpHandler, NgbActiveModal],
        declarations: [AdminUserSupervisingComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUserSupervisingComponent);
    component = fixture.componentInstance;
    component.user = {
      id: '01',
      userName: 'userName01',
      firstName: 'firstName01',
      lastName: 'lastName01',
      email: 'email@dom.local',
      privilege: UserPrivilege.None,
      associatedTenants: [],
      supervisedDesks: [],
      administeredDesks: [],
    };
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
