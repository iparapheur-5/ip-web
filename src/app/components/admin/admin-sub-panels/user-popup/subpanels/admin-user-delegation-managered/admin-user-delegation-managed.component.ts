/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { stringifyNamedElement } from '../../../../../../utils/string-utils';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { Observable } from 'rxjs';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { AdminDeskService, DeskRepresentation, PageDeskRepresentation } from '@libriciel/iparapheur-provisioning';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';
import { UserPopupMessages } from '../../user-popup-messages';

@Component({
  selector: 'app-admin-user-delegation-managed',
  templateUrl: './admin-user-delegation-managed.component.html',
  styleUrls: ['./admin-user-delegation-managed.component.scss']
})
export class AdminUserDelegationManagedComponent {


  readonly commonIcons = CommonIcons;
  readonly infoIcon = faInfoCircle;
  readonly commonMessages = CommonMessages;
  readonly messages = UserPopupMessages;
  readonly stringifyNamedElementFn = stringifyNamedElement;


  @Input() user: UserDto;
  @Input() tenantId: string;
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<DeskRepresentation>;

  currentSearch: string = null;


  // <editor-fold desc="LifeCycle">


  constructor(public adminDeskService: AdminDeskService) {}


  // </editor-fold desc="LifeCycle">


  onSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearch = newTerm;
    this.doubleListComponent.requestElements(true);
  }


  requestDesksFn = (page: number, pageSize: number): Observable<PageDeskRepresentation> => {
    let searchTerm = this.currentSearch?.length > 0 ? this.currentSearch : null;
    return this.adminDeskService
      .listDesks(this.tenantId, page, pageSize, [], searchTerm);
  }


}
