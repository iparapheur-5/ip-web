/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild } from '@angular/core';
import { faUnlink } from '@fortawesome/free-solid-svg-icons';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { UserPopupMessages } from '../../user-popup-messages';
import { PageDeskRepresentation, AdminDeskService, DeskRepresentation } from '@libriciel/iparapheur-provisioning';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { Observable } from 'rxjs';
import { stringifyNamedElement } from '../../../../../../utils/string-utils';

@Component({
  selector: 'app-admin-tenant-user-desks',
  templateUrl: './admin-tenant-user-desks.component.html',
  styleUrls: ['./admin-tenant-user-desks.component.scss']
})
export class AdminTenantUserDesksComponent {


  readonly pageSize = 8;
  readonly unlinkIcon = faUnlink;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = UserPopupMessages;
  readonly stringifyNamedElementFn = stringifyNamedElement;

  @Input() tenantId: string;
  @Input() modifiedUser: UserDto;
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<DeskRepresentation>;

  currentSearch: string = null;


  // <editor-fold desc="LifeCycle">


  constructor(public adminDeskService: AdminDeskService) {}


  // </editor-fold desc="LifeCycle">


  onSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearch = newTerm;
    this.doubleListComponent.requestElements(true);
  }


  requestDesksFn = (page: number, pageSize: number): Observable<PageDeskRepresentation> => {
    let searchTerm = this.currentSearch?.length > 0 ? this.currentSearch : null;
    return this.adminDeskService.listDesks(this.tenantId, page, pageSize, null, searchTerm);
  }


}
