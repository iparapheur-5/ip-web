/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild } from '@angular/core';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { UserPopupMessages } from '../../user-popup-messages';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { stringifyNamedElement } from '../../../../../../utils/string-utils';
import { AdminTenantService, TenantSortBy } from '@libriciel/iparapheur-standard';
import { AdminDeskService } from '@libriciel/iparapheur-provisioning';
import { UserDto, DeskRepresentation, TenantRepresentation, UserPrivilege } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-admin-user-privileges',
  templateUrl: './admin-user-privileges.component.html',
  styleUrls: ['./admin-user-privileges.component.scss']
})
export class AdminUserPrivilegesComponent {


  readonly messages = UserPopupMessages;
  readonly commonMessages = CommonMessages;
  readonly userPrivilegeEnum = UserPrivilege;
  readonly orderedUserPrivilegeList: UserPrivilege[] = [
    UserPrivilege.SuperAdmin,
    UserPrivilege.TenantAdmin,
    UserPrivilege.FunctionalAdmin,
    UserPrivilege.None
  ];

  readonly stringifyNamedElementFn = stringifyNamedElement;

  @ViewChild('tenantDoubleListComponent') tenantDoubleListComponent: DoubleListComponent<TenantRepresentation>;
  @ViewChild('deskDoubleListComponent') deskDoubleListComponent: DoubleListComponent<DeskRepresentation>;

  @Input() modifiedUser: UserDto;
  @Input() tenantId: string;

  currentTenantSearch: string = null;
  currentDeskSearch: string = null;


  // <editor-fold desc="LifeCycle">


  constructor(public adminTenantService: AdminTenantService,
              public notificationsService: NotificationsService,
              public adminDeskService: AdminDeskService) {}


  // </editor-fold desc="LifeCycle">


  isPrivilegeSelectionDisabled(privilege: UserPrivilege): boolean {
    const isForbiddenInTenantlessMode = !this.tenantId && (privilege === UserPrivilege.FunctionalAdmin);
    const isForbiddenInTenantMode = !!this.tenantId && (privilege === UserPrivilege.SuperAdmin);
    return isForbiddenInTenantlessMode || isForbiddenInTenantMode;
  }


  // TODO: Remove that hardcoded "NAME", once we'll have a DeskSortBy
  requestDesksFn = (page: number, pageSize: number) => this.adminDeskService
    .listDesks(this.tenantId, page, pageSize, ['NAME,ASC'], this.currentDeskSearch);


  requestTenantFn = (page: number, pageSize: number) => this.adminTenantService
    .listTenantsAsAdmin(page, pageSize, [TenantSortBy.Name + ',ASC'], this.currentTenantSearch);


  onTenantSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentTenantSearch = newTerm;
    this.tenantDoubleListComponent.requestElements(true);
  }


  onDeskSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentDeskSearch = newTerm;
    this.deskDoubleListComponent.requestElements(true);
  }


}
