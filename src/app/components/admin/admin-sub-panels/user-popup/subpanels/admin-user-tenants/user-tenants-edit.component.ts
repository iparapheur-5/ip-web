/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild } from '@angular/core';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { AllUsersMessages } from '../../../all-users/all-users-messages';
import { UserPreferencesDto, PageHierarchisedDeskRepresentation, AdminTenantService } from '@libriciel/iparapheur-standard';
import { ActivatedRoute } from '@angular/router';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';
import { AdminAllUsersService, TenantRepresentation } from '@libriciel/iparapheur-provisioning';
import { stringifyNamedElement } from '../../../../../../utils/string-utils';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-admin-user-tenants',
  templateUrl: './user-tenants-edit.component.html',
  styleUrls: ['./user-tenants-edit.component.scss']
})
export class UserTenantsEditComponent {

  readonly commonMessages = CommonMessages;
  readonly messages = AllUsersMessages;
  readonly stringifyNamedElementFn = stringifyNamedElement;

  @Input() user: UserDto;
  @Input() userPreferences: UserPreferencesDto;
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<TenantRepresentation>;

  currentSearch: string = null;


  // <editor-fold desc="LifeCycle">


  constructor(private adminAllUsersService: AdminAllUsersService,
              private adminTenantService: AdminTenantService,
              public notificationsService: NotificationsService,
              public route: ActivatedRoute) { }


  // <editor-fold desc="LifeCycle">


  onSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearch = newTerm;
    this.doubleListComponent.requestElements(true);
  }


  // </editor-fold desc="LifeCycle">


  requestTenantsFn = (page: number, pageSize: number): Observable<PageHierarchisedDeskRepresentation> => {
    let searchTerm = this.currentSearch?.length > 0 ? this.currentSearch : null;
    return this.adminTenantService.listTenantsAsAdmin(page, pageSize, [], searchTerm);
  }


}
