/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserPopupMessages } from '../../user-popup-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';

@Component({
  selector: 'app-admin-tenant-user-infos',
  templateUrl: './admin-tenant-user-infos.component.html',
  styleUrls: ['./admin-tenant-user-infos.component.scss']
})
export class AdminTenantUserInfosComponent implements OnInit {

  readonly commonMessages = CommonMessages;
  readonly messages = UserPopupMessages;
  readonly commonIcons = CommonIcons;
  readonly infoIcon = faInfoCircle;

  @Input() modifiedUser: UserDto;
  @Output() valid = new EventEmitter<boolean>();

  creationMode = false;

  readonly generalForm: FormGroup;


  // <editor-fold desc="LifeCycle">


  constructor() {
    this.generalForm = new FormGroup({
      userNameControl: new FormControl(this.modifiedUser?.userName ?? null, [Validators.required]),
      firstNameControl: new FormControl(this.modifiedUser?.firstName ?? null, [Validators.required]),
      lastNameControl: new FormControl(this.modifiedUser?.lastName ?? null, [Validators.required]),
      mailControl: new FormControl(this.modifiedUser?.email ?? null, [Validators.required, Validators.email])
    });
  }


  ngOnInit(): void {
    this.creationMode = !this.modifiedUser?.id;
    this.generalForm.valueChanges.subscribe(() => this.valid.emit(this.generalForm.valid));
  }


  // </editor-fold desc="LifeCycle">


}
