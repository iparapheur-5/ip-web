/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { AdminTenantUserInfosComponent } from './admin-tenant-user-infos.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { KeycloakService } from 'keycloak-angular';
import { UserPrivilege } from '@libriciel/iparapheur-provisioning';

describe('AdminTenantUserInfosComponent', () => {

  let component: AdminTenantUserInfosComponent;
  let fixture: ComponentFixture<AdminTenantUserInfosComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [HttpClient, HttpHandler, KeycloakService],
        declarations: [AdminTenantUserInfosComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTenantUserInfosComponent);
    component = fixture.componentInstance;

    component.modifiedUser = {
      complementaryField: 'complementaryField',
      email: 'email@email.email',
      firstName: 'firstName',
      isLdapSynchronized: false,
      lastName: 'lastName',
      privilege: UserPrivilege.None,
      id: 'userId',
      userName: 'userName'
    }

    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
