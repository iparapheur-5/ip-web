/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class UserPopupMessages {


  static readonly NEW_USER_CREATION = `Ajout d'un utilisateur`;
  static readonly USER_MODIFICATION = `Modification de l'utilisateur`;

  static readonly BAD_MAIL_FORMAT = `L'adresse mail saisie n'est pas correcte`;

  static readonly ONLY_AVAILABLE_IN_TENANTS = `Ce paramètre est disponible dans l'administration par entité.`;
  static readonly ONLY_AVAILABLE_IN_TENANTLESS = `Ce paramètre est disponible dans l'administration globale.`;
  static readonly COMPLEMENTARY_FIELD = 'Informations complémentaires';

  static readonly ADMINISTER_DESKS = 'Administrer des bureaux';
  static readonly ADMINISTER_TENANTS = 'Administrer des entités';
  static readonly ASSOCIATE_DESKS = 'Associer des bureaux';
  static readonly SUPERVISE_DESKS = 'Superviser des bureaux';
  static readonly MANAGE_DELEGATIONS = 'Gérer les absences des bureaux';

  static readonly SUPERVISED_DESKS = 'Bureaux supervisés';
  static readonly MANAGED_DESKS = 'Bureaux gérés';

  static readonly TAB_GENERAL = 'Général';
  static readonly TAB_PRIVILEGES = 'Droits';
  static readonly TAB_SUPERVISING = 'Supervision';
  static readonly TAB_DELEGATION_MANAGED = `Gestion d'absences`;
  static readonly TAB_SIGNATURE = 'Signature';
  static readonly TAB_DESKS = 'Bureaux';
  static readonly TAB_TENANTS = 'Entités associées';

  static readonly USER_PRIVILEGES_LABEL = `Droits de l'utilisateur\u00a0:`;

  static readonly TENANTS_LIST = 'Liste des entités';
  static readonly ASSOCIATED_TENANTS = 'Entités associées';
  static readonly ADMINISTERED_DESKS = `Bureaux administrés`;

  static readonly COMPLEMENTARY_FIELD_TOOLTIP = 'CSV clé="valeur". Exemple : TITRE="Directeur des achats"';

}
