/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { SettingsMessages } from '../settings-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../shared/common-messages';
import { BROWSERS, DeviceDetectorService } from 'ngx-device-detector';
import { GLOBAL_SETTINGS } from '../../../shared/models/global-settings';

@Component({
  selector: 'app-liber-sign-help-popup',
  templateUrl: './liber-sign-help-popup.component.html',
  styleUrls: ['./liber-sign-help-popup.component.scss']
})
export class LiberSignHelpPopupComponent {


  readonly browsers = BROWSERS;
  readonly messages = SettingsMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;


  browser = this.deviceService.getDeviceInfo().browser;


  constructor(public activeModal: NgbActiveModal,
              public deviceService: DeviceDetectorService) {}


  checkInstallation() {
    location.reload();
  }


  getInstallExtensionLink(): string {
    return this.browser === BROWSERS.FIREFOX
      ? GLOBAL_SETTINGS?.libersign?.firefoxExtensionUrl
      : GLOBAL_SETTINGS?.libersign?.chromeExtensionUrl
  }


  getInstallApplicationLink(): string {
    return GLOBAL_SETTINGS?.libersign?.nativeApplicationUrl;
  }


}
