/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { CommonMessages } from '../../shared/common-messages';

export class SettingsMessages {

  static readonly BROWSER_DETECTED_LABEL = 'Navigateur basé sur\u00a0:';
  static readonly ELECTRONIC_SIGNING_COMPATIBILITY_LABEL = 'Signature électronique\u00a0:';
  static readonly NATIVE_APPLICATION_LABEL = 'Logiciel companion\u00a0:';
  static readonly NOT_COMPATIBLE = 'Non compatible';
  static readonly COMPATIBLE = 'Compatible';
  static readonly NATIVE_APPLICATION_NOT_INSTALLED = 'Logiciel companion LiberSign non installée';
  static readonly EXTENSION_LABEL = 'Extension\u00a0:';
  static readonly LIBERSIGN_LABEL = 'LiberSign\u00a0:';
  static readonly INSTALLATION_HELP_BUTTON_LABEL = `Aide d'installation`;
  static readonly INSTALLATION_HELP_TEXT = `Afin de rendre votre navigateur compatible avec la signature électronique, merci de suivre l'aide d'installation.`;
  static readonly LIBERSIGN_EXTENSION_OBSOLETE = `Votre version de l'extension LiberSign est obsolète.<br/>Firefox lancera une mise à jour automatiquement sous 24h. Cela n'affectera pas l'action de signature.<br/>Si ce message apparait toujours 24h après l'installation de l'extension, veuillez contacter votre administrateur.`;
  static readonly INFORMATION_TITLE = 'Informations du poste';
  static readonly LIBERSIGN_APPLICATION_CHECKING = 'Vérification de la version de Libersign en cours, veuillez patienter.';
  static readonly LIBERSIGN_HELP_POPUP_TITLE = `Aide d'installation de l'extension LiberSign`;
  static readonly INSTALLATION_HELP_STEP1_PART1 = `Il est possible que vous obteniez le message ci-dessous lors de l'installation.
<br> Dans ce cas cliquez sur <em>Autoriser</em> puis <em>Installer</em>.`;
  static readonly INSTALLATION_HELP_STEP2_TITLE = `Étape 2 : Installation de l'application LiberSign`;
  static readonly INSTALLATION_HELP_STEP2_PART1 = `Un logiciel compagnon est à télécharger puis installer`;
  static readonly INSTALLATION_HELP_STEP2_PART2 = `Une fois le logiciel compagnon installé, vous pouvez passer à l'étape 3 de l'installation`;
  static readonly INSTALLATION_HELP_STEP3_PART1 = `Une fois les deux étapes précédentes effectuées, vous devez tester la bonne installation de LiberSign.`;
  static readonly INSTALLATION_HELP_STEP3_TITLE = `Étape 3 : Vérification de l'installation`;
  static readonly INSTALLATION_HELP_EXTENSION_BUTTON_TITLE = `Installer l'extension`;
  static readonly INSTALLATION_HELP_CHECK_BUTTON_TITLE = 'Tester LiberSign';
  static readonly INSTALLATION_HELP_APPLICATION_BUTTON_TITLE = 'Télécharger le logiciel compagnon';

  static readonly INFO_ICON = 'info-circle';
  static readonly HELP_ICON = 'question';
  static readonly ABOUT_PAGE_TITLE = 'À propos - Aide';
  static readonly FIREFOX_HELP_TXT = 'Attention permission Firefox';

  static readonly NOTIFICATION_EDIT_SUCCESS = 'Vos préférences de notification ont été modifiées avec succès.';
  static readonly NOTIFICATION_EDIT_ERROR = 'Une erreur est survenue lors de la modification de vos préférences de notification.';

  static readonly NOTIFICATION_POPUP_TITLE = `Première connexion sur le ${CommonMessages.APP_NAME}`;

  static readonly NOTIFICATION_POPUP_TEXT =
    'En conformité avec le RGPD, veuillez confirmer vos préférences de notification. '
    + 'Vous pourrez à nouveau les modifier ultérieurement depuis le menu "Préférences".';


  static installationHelpStep1Title(browser: string): string {
    return `Étape 1 : Installation de l'extension ${browser}`;
  }

  static version(version: string): string {
    return `Version ${version}`;
  }

  static installationHelpStep1Part1(browser: string): string {
    return `La version 2 de LiberSign fonctionne avec une extension de navigateur. Cette extension est spécifique au navigateur <em>${browser}</em>.`;
  }

}
