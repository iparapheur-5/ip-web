/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, HostBinding, OnInit } from '@angular/core';
import { BROWSERS, DeviceDetectorService, OS } from 'ngx-device-detector';
import { SettingsMessages } from '../settings-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LiberSignHelpPopupComponent } from '../liber-sign-help-popup/liber-sign-help-popup.component';


@Component({
  selector: 'app-liber-sign-check',
  templateUrl: './liber-sign-check.component.html',
  styleUrls: ['./liber-sign-check.component.scss']
})
export class LiberSignCheckComponent implements OnInit {

  constructor(public deviceService: DeviceDetectorService, public modalService: NgbModal) {
  }

  static authorizedBrowsers = [BROWSERS.FIREFOX, BROWSERS.CHROME, BROWSERS.MS_EDGE_CHROMIUM];
  static authorizedOss = [OS.WINDOWS];
  // noinspection SpellCheckingInspection
  static liberSignEventReady = 'libersignready';
  static checkIfExtensionInstalledTimeout = 5000;

  @HostBinding('class') hostClass = 'ls-article';
  messages = SettingsMessages;
  commonIcons = CommonIcons;
  LiberSignExtensionStatus = LiberExtensionSignStatus;
  LiberSignApplicationStatus = LiberSignApplicationStatus;

  signingCompatibility = false;
  liberSignExtension: { status: LiberExtensionSignStatus, version: string, error: string } = {
    status: null,
    version: null,
    error: null
  };
  liberSignApplication: { status: LiberSignApplicationStatus, version: string } = {
    status: LiberSignApplicationStatus.Checking,
    version: null,
  };
  liberSignVersion: string;

  static guessLiberSignExtensionVersion(liberSign: any): LiberExtensionSignStatus {
    return typeof liberSign.getFullVersion === 'function'
      ? LiberExtensionSignStatus.ExtensionInstalledUpToDate
      : LiberExtensionSignStatus.ExtensionInstalledObsolete
  }

  ngOnInit(): void {
    this.signingCompatibility =
      LiberSignCheckComponent.authorizedOss.includes(this.deviceService.getDeviceInfo().os)
      && LiberSignCheckComponent.authorizedBrowsers.includes(this.deviceService.getDeviceInfo().browser);

    window.addEventListener(LiberSignCheckComponent.liberSignEventReady, () => this.testLiberSignStatus());

    setTimeout(() => {
      if (this.liberSignApplication.status === LiberSignApplicationStatus.Checking) {
        this.liberSignExtension.status = LiberExtensionSignStatus.ExtensionNotInstalled;
      }
    }, LiberSignCheckComponent.checkIfExtensionInstalledTimeout);

    setTimeout(() => window.dispatchEvent(new Event(LiberSignCheckComponent.liberSignEventReady)), 1000);
  }

  testLiberSignStatus() {
    // tslint:disable-next-line:no-string-literal
    const liberSign = window['LiberSign'];
    if (liberSign) {
      liberSign.setUpdateUrl('/libersign/');
      this.liberSignApplication.status = LiberSignApplicationStatus.Checking;
      from(liberSign.getCertificates()).subscribe(
        () => LiberSignCheckComponent.getVersions(liberSign).subscribe(
          versions => {
            this.liberSignVersion = versions.libersignVersion;
            this.liberSignApplication.version = versions.applicationVersion;
            this.liberSignExtension.version = versions.extensionVersion;
            this.liberSignApplication.status = LiberSignApplicationStatus.ApplicationInstalled;
            this.liberSignExtension.status = LiberSignCheckComponent.guessLiberSignExtensionVersion(liberSign);
          }),
        error => {
          if (error.result === 'no_implementation') {
            this.liberSignApplication.status = LiberSignApplicationStatus.ApplicationNotInstalled;
            this.liberSignExtension.version = error.extension;
            this.liberSignExtension.status = LiberSignCheckComponent.guessLiberSignExtensionVersion(liberSign);
          } else {
            console.error('erreur libersign: ');
            console.error(error);
            this.liberSignExtension.status = LiberExtensionSignStatus.CommunicationWithExtensionProblem;
            this.liberSignExtension.error = error.result;
          }
        });
    }
  }

  private static getVersions(liberSign): Observable<{ libersignVersion: string, applicationVersion: string, extensionVersion: string }> {
    const getVersion = LiberSignCheckComponent.guessLiberSignExtensionVersion(liberSign) === LiberExtensionSignStatus.ExtensionInstalledUpToDate
      ? liberSign.getFullVersion()
      : liberSign.getVersion();
    return from(getVersion)
      .pipe(map((version: { version: string, libersign: string, extension: string } | string) =>
        typeof version === 'string'
          ? {
            libersignVersion: null,
            applicationVersion: version.split('/')[1],
            extensionVersion: version.split('/')[0]
          } : {
            libersignVersion: version.libersign,
            applicationVersion: version.version,
            extensionVersion: version.extension
          }));
  }

  openHelpPopup() {
    this.modalService.open(LiberSignHelpPopupComponent, {size: 'xl', backdrop: 'static', keyboard: false});
  }
}

export enum LiberExtensionSignStatus {
  ExtensionNotInstalled = 'EXTENSION_NOT_INSTALLED',
  ExtensionInstalledObsolete = 'EXTENSION_INSTALLED_OBSOLETE',
  ExtensionInstalledUpToDate = 'EXTENSION_INSTALLED_UP_TO_DATE',
  CommunicationWithExtensionProblem = 'COMMUNICATION_WITH_EXTENSION_PROBLEM',
}

export enum LiberSignApplicationStatus {
  Checking = 'CHECKING',
  ApplicationInstalled = 'APPLICATION_INSTALLED',
  ApplicationNotInstalled = 'APPLICATION_NOT_INSTALLED'
}
