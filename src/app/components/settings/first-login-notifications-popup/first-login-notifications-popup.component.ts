/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../shared/common-messages';
import { SettingsMessages } from '../settings-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../models/auth/user';
import { NotificationsService } from '../../../services/notifications.service';
import { CurrentUserService, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-first-login-notifications-popup',
  templateUrl: './first-login-notifications-popup.component.html',
  styleUrls: ['./first-login-notifications-popup.component.scss']
})
export class FirstLoginNotificationsPopupComponent {

  static readonly INJECTABLE_CURRENT_USER_KEY = 'currentUser';
  static readonly INJECTABLE_CURRENT_USER_PREFERENCES_KEY = 'currentUserPreferences';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = SettingsMessages;

  isProcessing: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              public currentUserService: CurrentUserService,
              public notificationsService: NotificationsService,
              @Inject(FirstLoginNotificationsPopupComponent.INJECTABLE_CURRENT_USER_KEY) public currentUser: User,
              @Inject(FirstLoginNotificationsPopupComponent.INJECTABLE_CURRENT_USER_PREFERENCES_KEY) public currentUserPreferences: UserPreferencesDto) { }


  // </editor-fold desc="LifeCycle">


  validate(): void {
    this.currentUserService
      .updatePreferences(this.currentUserPreferences)
      .pipe(catchError(this.notificationsService.handleHttpError('updateUserPreferences')))
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(SettingsMessages.NOTIFICATION_EDIT_SUCCESS)
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => this.notificationsService.showErrorMessage(SettingsMessages.NOTIFICATION_EDIT_ERROR, error.message)
      );
  }


}
