/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../../../models/auth/user';
import { EditSignatureComponent } from '../user-edit-page-subpanels/user-edit-signature/edit-signature.component';
import { NotificationsService } from '../../../../services/notifications.service';
import { UserEditSubPanelsMessages } from '../user-edit-page-subpanels/user-edit-sub-panels-messages';
import { ActivatedRoute } from '@angular/router';
import { FilterService } from '../../../../services/filter.service';
import { GLOBAL_SETTINGS } from '../../../../shared/models/global-settings';
import { CrudOperation } from '../../../../services/crud-operation';
import { LoggableSignatureImage } from '../../../../models/auth/loggable-signature-image';
import { CurrentUserService, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { FolderFilter } from '../../../../models/folder/folder-filter';
import { SelectedUserPreferencesService } from '../../../../services/selected-user-preferences.service';

type UserSignatureImageChangeEvent = { user: User, file: File };

@Component({
  selector: 'app-profile-popup',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {


  readonly messages = UserEditSubPanelsMessages;
  readonly globalSettings = GLOBAL_SETTINGS;

  currentUser: User = new User();
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  signatureImageSrc: string = null;
  newPassword: string;
  isTenantAdminOrMore: boolean = false;
  loading: boolean = false;

  @ViewChild('signatureComponent') signatureComponent: EditSignatureComponent;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              public currentUserService: CurrentUserService,
              public legacyUserService: LegacyUserService,
              private selectedUserPreferencesService: SelectedUserPreferencesService,
              private filterService: FilterService,
              private route: ActivatedRoute) { }


  ngOnInit(): void {

    this.isTenantAdminOrMore =
      this.legacyUserService.isCurrentUserTenantAdminOfOneTenant() || this.legacyUserService.isCurrentUserSuperAdmin();

    this.currentUser = this.route.snapshot.data["currentUser"];
    this.userPreferences = this.route.snapshot.data["userPreferences"];

    if (!!this.userPreferences?.signatureImageContentId) {
      this.signatureImageSrc = this.legacyUserService.signatureImageUrl(Date.now().toString());
    }
  }


  // </editor-fold desc="LifeCycle">


  onSaveButtonClicked(navActiveId: string) {

    navActiveId === 'password'
      ? this.savePassword()
      : this.saveForm();
  }


  saveForm() {

    if (!this.userPreferences.currentFilter?.id)
      this.userPreferences.currentFilter = null;

    this.userPreferences.currentFilterId = this.userPreferences.currentFilter?.id ?? null;
    this.filterService.setDefaultFilter(Object.assign(new FolderFilter(), this.userPreferences.currentFilter));

    this.loading = true;
    this.currentUserService
      .updatePreferences(this.userPreferences)
      .subscribe(
        () => {
          // FIXME return userprefs on Update or get it before update
          this.selectedUserPreferencesService.update(this.userPreferences);
          this.notificationService.showSuccessMessage(UserEditSubPanelsMessages.PROFILE_SAVE_USER_PREF_SUCCESS);
        },
        error => this.notificationService.showErrorMessage(UserEditSubPanelsMessages.PROFILE_SAVE_USER_PREF_ERROR, error.message)
      )
      .add(() => this.loading = false);
  }


  savePassword() {
    if (!this.newPassword) return;

    this.loading = true;

    this.legacyUserService
      .resetUserPassword(this.newPassword)
      .subscribe(
        () => this.notificationService.showSuccessMessage(UserEditSubPanelsMessages.PROFILE_SAVE_PASSWORD_SUCCESS),
        error => this.notificationService.showErrorMessage(UserEditSubPanelsMessages.PROFILE_SAVE_PASSWORD_ERROR, error.message)
      )
      .add(() => this.loading = false);

  }


  createSignature(event: UserSignatureImageChangeEvent): void {
    this.legacyUserService
      .setSignatureImage(event.file)
      .subscribe(
        signatureContentId => {
          // That's a strange syntax, but we actually reset the readonly property,
          // to avoid a useless request to the server
          this.userPreferences['signatureImageContentId' as UserPreferencesDto['signatureImageContentId']] = signatureContentId;
          this.notificationService.showCrudMessage(CrudOperation.Create, new LoggableSignatureImage());
          this.signatureImageSrc = this.legacyUserService.signatureImageUrl(Date.now().toString());
        },
        error => this.notificationService.showCrudMessage(CrudOperation.Create, new LoggableSignatureImage(), error.message, false)
      );
  }


  deleteSignature(): void {
    this.legacyUserService
      .deleteSignatureImage()
      .subscribe(
        () => {
          this.notificationService.showCrudMessage(CrudOperation.Delete, new LoggableSignatureImage());
          // That's a strange syntax, but we actually reset the readonly property,
          // to avoid a useless request to the server
          this.userPreferences['signatureImageContentId' as UserPreferencesDto['signatureImageContentId']] = null;
          this.signatureImageSrc = null;
        },
        error => this.notificationService.showCrudMessage(CrudOperation.Delete, new LoggableSignatureImage(), error.message, false)
      );
  }


  updateSignature(event: UserSignatureImageChangeEvent): void {
    this.legacyUserService
      .updateSignatureImage(event.file)
      .subscribe(
        () => {
          this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableSignatureImage());
          this.signatureImageSrc = this.legacyUserService.signatureImageUrl(Date.now().toString());
        },
        error => this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableSignatureImage(), error.message, false)
      );
  }


}
