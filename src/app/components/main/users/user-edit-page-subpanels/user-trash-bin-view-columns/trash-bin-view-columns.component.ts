/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../../../models/auth/user';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { FolderSortBy, ArchiveViewColumn, UserPreferencesDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-user-trash-bin-view-columns',
  templateUrl: './trash-bin-view-columns.component.html',
  styleUrls: ['./trash-bin-view-columns.component.scss']
})
export class TrashBinViewColumnsComponent implements OnInit {


  readonly messages = UserEditSubPanelsMessages;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly gripHorizontalIcon = faGripHorizontal;
  readonly columns = ArchiveViewColumn;
  readonly possibleResultsPerPage: number[] = [10, 15, 20, 50, 100];


  @Input() modifiedUser: User;
  @Input() userPreferences: UserPreferencesDto;


  availableColumns: ArchiveViewColumn[] = [
    ArchiveViewColumn.Id,
    ArchiveViewColumn.Name,
    ArchiveViewColumn.OriginDesk,
    ArchiveViewColumn.TypeSubtype,
    ArchiveViewColumn.CreationDate,
    ArchiveViewColumn.Actions
  ];


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    this.availableColumns = this.availableColumns.filter(item => !this.userPreferences.archiveViewColumnList.includes(item));
  }


  // </editor-fold desc="LifeCycle">


  drop(event: CdkDragDrop<FolderSortBy>): void {
    this.userPreferences.archiveViewColumnList
      .splice(
        event.currentIndex,
        0,
        this.userPreferences.archiveViewColumnList
          .splice(event.previousIndex, 1)[0]
      );
  }


  removeFromUsedColumns(column: ArchiveViewColumn) {
    if (column !== ArchiveViewColumn.Name) {
      this.userPreferences.archiveViewColumnList = this.userPreferences.archiveViewColumnList.filter(col => col !== column);
      this.availableColumns.push(column);
    }
  }


  addToUsedColumns(index: number) {
    this.userPreferences.archiveViewColumnList.push(this.availableColumns.splice(index, 1)[0]);
  }


}
