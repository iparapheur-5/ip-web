/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../../../models/auth/user';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import { CommonIcons } from '@libriciel/ls-composants';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { FolderFilter } from '../../../../../models/folder/folder-filter';
import { LegacyUserService } from '../../../../../services/ip-core/legacy-user.service';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CrudOperation } from '../../../../../services/crud-operation';
import { LoggableUser } from '../../../../../models/auth/loggable-user';
import { compareById } from '../../../../../utils/string-utils';
import { FolderSortBy, TaskViewColumn, UserPreferencesDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {


  readonly messages = UserEditSubPanelsMessages;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly gripHorizontalIcon = faGripHorizontal;
  readonly columns = TaskViewColumn;
  readonly possibleResultsPerPage: number[] = [10, 15, 20, 50, 100];
  readonly taskViewColumEnum = TaskViewColumn;
  readonly compareByIdFn = compareById;


  @Input() modifiedUser: User;
  @Input() userPreferences: UserPreferencesDto;


  filters: FolderFilter[] = [];
  availableColumns: TaskViewColumn[] = [
    TaskViewColumn.FolderName,
    TaskViewColumn.State,
    TaskViewColumn.Type,
    TaskViewColumn.Subtype,
    TaskViewColumn.CurrentDesk,
    TaskViewColumn.LimitDate,
    TaskViewColumn.CreationDate,
    TaskViewColumn.TaskId,
    TaskViewColumn.FolderId,
    TaskViewColumn.OriginDesk,
  ];


  // <editor-fold desc="LifeCycle">


  constructor(private legacyUserService: LegacyUserService,
              private notificationsService: NotificationsService) { }


  ngOnInit(): void {

    this.legacyUserService
      .getCurrentUserFolderFilters()
      .subscribe(
        filters => {
          this.filters = filters.map(f => f as FolderFilter);
          this.userPreferences.currentFilter = filters.find(filter => filter.id === this.userPreferences.currentFilter?.id);
        },
        error => this.notificationsService.showCrudMessage(CrudOperation.Read, new LoggableUser(this.modifiedUser), error.message, false)
      )
    this.availableColumns = this.availableColumns.filter(item => !this.userPreferences.taskViewColumnList.includes(item));
  }


  // </editor-fold desc="LifeCycle">


  drop(event: CdkDragDrop<FolderSortBy>): void {
    this.userPreferences.taskViewColumnList
      .splice(event.currentIndex,
        0,
        this.userPreferences.taskViewColumnList
          .splice(event.previousIndex, 1)[0]
      );
  }


  removeFromUsedColumns(column: TaskViewColumn) {
    if (column !== TaskViewColumn.FolderName) {
      this.userPreferences.taskViewColumnList = this.userPreferences.taskViewColumnList.filter(col => col !== column);
      this.availableColumns.push(column);
    }
  }


  addToUsedColumns(index: number) {
    this.userPreferences.taskViewColumnList.push(this.availableColumns.splice(index, 1)[0]);
  }


}
