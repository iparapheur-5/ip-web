/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class UserEditSubPanelsMessages {

  static readonly NO_SIGNATURE_IMAGE = `Pas d'image`;
  static readonly ADD_SIGNATURE_IMAGE = 'Ajouter une image';
  static readonly REPLACE_SIGNATURE_IMAGE = `Remplacer l'image`;
  static readonly DELETE_SIGNATURE_IMAGE = `Supprimer l'image`;

  static readonly REDIRECTION_MAIL_LABEL = 'Courriel destinataire des notifications';
  static readonly NOTIFICATION_FREQUENCY_LABEL = 'Fréquence des notifications';
  static readonly NOTIFICATION_FREQUENCY_NONE = 'Aucune';
  static readonly NOTIFICATION_FREQUENCY_SINGLE = 'Unitaire';
  static readonly NOTIFICATION_FREQUENCY_HOURLY = 'Heure';
  static readonly NOTIFICATION_FREQUENCY_DAILY = 'Jour';
  static readonly NOTIFICATION_FREQUENCY_WEEKLY = 'Semaine';
  static readonly EVERY_HOURS = 'Toutes les ';
  static readonly HOUR_S = 'heure(s)';
  static readonly EVERY_DAYS = 'Tous les jours à ';
  static readonly HOURS = 'heures';
  static readonly EVERY_WEEK = 'Toutes les semaines, le';
  static readonly MONDAY = 'lundi';
  static readonly TUESDAY = 'mardi';
  static readonly WEDNESDAY = 'mercredi';
  static readonly THURSDAY = 'jeudi';
  static readonly FRIDAY = 'vendredi';
  static readonly SATURDAY = 'samedi';
  static readonly SUNDAY = 'dimanche';

  static readonly PROFILE_EDIT_USER = `Préférences de l'utilisateur`;
  static readonly PROFILE_PASSWORD = 'Mot de passe';
  static readonly PROFILE_NOTIFICATION = 'Notifications';
  static readonly PROFILE_SIGNATURE = 'Signature';
  static readonly PROFILE_FAVORITE_DESKS = 'Bureaux favoris';
  static readonly PROFILE_DASHBOARD = 'Tableau de bord';
  static readonly PROFILE_ABSENCES = 'Absences';
  static readonly PROFILE_TRASH_BIN = 'Corbeille';
  static readonly PROFILE_DEBUG = 'Débogage';
  static readonly PROFILE_FOLDER_VIEW_PREFERENCES = 'Visualisation de dossier';
  static readonly PROFILE_VALIDATE = 'Valider';
  static readonly PROFILE_SAVE_USER_PREF_SUCCESS = `Préférences d'utilisateur éditées avec succès.`;
  static readonly PROFILE_SAVE_USER_PREF_ERROR = `Erreur lors de l'édition des préférences d'utilisateur`;
  static readonly PROFILE_SAVE_PASSWORD_SUCCESS = 'Mot de passe édité avec succès.';
  static readonly PROFILE_SAVE_PASSWORD_ERROR = `Erreur lors de l'édition du mot de passe`;

  static readonly FOLDER_VIEW_PREFERENCES_TITLE = 'Personnalisation de la visualisation de dossier';
  static readonly FOLDER_VIEW_PREFERENCES_SUBTITLE = 'Ordre des blocs';

  static readonly NEW_PASSWORD_LABEL = 'Nouveau mot de passe';
  static readonly CONFIRM_PASSWORD_LABEL = 'Confirmer le mot de passe';
  static readonly EDIT_PASSWORD_TITLE = 'Modification du mot de passe';
  static readonly SAVE_NEW_PASSWORD = 'Enregistrer le nouveau mot de passe';

  static readonly EDIT_NOTIFICATIONS_TITLE = 'Modifications des notifications';

  static readonly EDIT_SPECIFIC_NOTIFICATIONS_TITLE = 'Notifications qui me concernent';
  static readonly EDIT_SPECIFIC_NOTIFICATIONS_NEW_FOLDER_INVOLVING_ME = 'Un dossier est arrivé sur un bureau me concernant';
  static readonly EDIT_SPECIFIC_NOTIFICATIONS_ACTION_ON_FOLLOWED_FOLDER = 'Une action a eu lieu sur un dossier suivi';
  static readonly EDIT_SPECIFIC_NOTIFICATIONS_LATE_FOLDER_INVOLVING_ME = 'Un dossier me concernant est en retard';

  static readonly EDIT_DESK_ORDER_FAVORITE_DESKS = 'Bureaux favoris (max. 15)';
  static readonly EDIT_DESK_ORDER_ORDER = 'Ordre';

  static readonly CANNOT_RETRIEVE_DESKS = 'Erreur lors de la récupération des bureaux';
  static readonly FAVORITE_DESK_LIMIT_REACHED = `Impossible d'ajouter un bureau favori. La limite a été atteinte.`;

  static readonly TRASH_BIN_FOLDERS_TITLE = 'Modification de la corbeille';
  static readonly DASHBOARD_TITLE = 'Modification du tableau de bord';
  static readonly DASHBOARD_RESULTS_PER_PAGE = 'Nombre de résultats à afficher par page';
  static readonly DASHBOARD_DEFAULT_SORT = 'Tri par défaut';
  static readonly DASHBOARD_ASC = 'Ascendant';
  static readonly DASHBOARD_DESC = 'Descendant';
  static readonly DASHBOARD_DEFAULT_FILTER = 'Filtre par défaut';
  static readonly DASHBOARD_SORT_ELEMENTS = 'Réorganiser les colonnes';
  static readonly DASHBOARD_AVAILABLE_COLUMNS = 'Colonnes Disponible';
  static readonly DASHBOARD_USED_COLUMNS = 'Colonnes affichées';
  static readonly DASHBOARD_ORDER = 'Ordre';

  static readonly SHOW_ADMIN_IDS = 'Afficher les identifiants';
  static readonly SHOW = 'Afficher';
  static readonly DO_NOT_SHOW = 'Ne pas afficher';

}
