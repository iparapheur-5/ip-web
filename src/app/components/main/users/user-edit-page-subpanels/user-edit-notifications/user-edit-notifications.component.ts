/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { isNullOrUndefined } from '../../../../../utils/string-utils';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { Options } from "@libriciel/ls-composants";
import { User, UserPreferencesDto } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-user-edit-notifications',
  templateUrl: './user-edit-notifications.component.html',
  styleUrls: ['./user-edit-notifications.component.scss']
})
export class UserEditNotificationsComponent implements OnInit {


  static readonly SINGLE_NOTIFICATION = 'single_notifications';
  static readonly NONE_NOTIFICATION = 'none';

  readonly messages = UserEditSubPanelsMessages;
  readonly regexHourly = new RegExp(/0 0 \*(?:\/\d+)? \* \* \*/);
  readonly regexDaily = new RegExp(/0 0 \d+ \* \* \*/);
  readonly regexWeekly = new RegExp(/0 0 \d+ \* \* \d+/);

  @Input() user: User;
  @Input() userPreferences: UserPreferencesDto;

  notificationOptions: Options


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    this.notificationOptions = {
      name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_TITLE,
      values: [
        {name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_NEW_FOLDER_INVOLVING_ME, active: this.userPreferences.notifiedOnConfidentialFolders},
        {name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_ACTION_ON_FOLLOWED_FOLDER, active: this.userPreferences.notifiedOnFollowedFolders},
        {name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_LATE_FOLDER_INVOLVING_ME, active: this.userPreferences.notifiedOnLateFolders}
      ]
    };
  }


  // </editor-fold desc="LifeCycle">


  isNotificationSingle(): boolean {
    return this.userPreferences.notificationsCronFrequency == UserEditNotificationsComponent.SINGLE_NOTIFICATION;
  }


  isNotificationHourly(): boolean {
    return this.regexHourly.test(this.userPreferences.notificationsCronFrequency);
  }


  isNotificationDaily(): boolean {
    return this.regexDaily.test(this.userPreferences.notificationsCronFrequency)
  }


  isNotificationWeekly(): boolean {
    return this.regexWeekly.test(this.userPreferences.notificationsCronFrequency)
  }


  isNotificationDisabled(): boolean {
    return isNullOrUndefined(this.userPreferences.notificationsCronFrequency)
      || this.userPreferences.notificationsCronFrequency == UserEditNotificationsComponent.NONE_NOTIFICATION;
  }


  onSpecificNotificationValueChange() {

    this.userPreferences.notifiedOnConfidentialFolders = this.notificationOptions.values
      .find(option => option.name === UserEditSubPanelsMessages.EDIT_SPECIFIC_NOTIFICATIONS_NEW_FOLDER_INVOLVING_ME)
      .active;

    this.userPreferences.notifiedOnFollowedFolders = this.notificationOptions.values
      .find(option => option.name === UserEditSubPanelsMessages.EDIT_SPECIFIC_NOTIFICATIONS_ACTION_ON_FOLLOWED_FOLDER)
      .active;

    this.userPreferences.notifiedOnLateFolders = this.notificationOptions.values
      .find(option => option.name === UserEditSubPanelsMessages.EDIT_SPECIFIC_NOTIFICATIONS_LATE_FOLDER_INVOLVING_ME)
      .active;
  }


}
