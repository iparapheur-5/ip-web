/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { faGripHorizontal, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CurrentUserService, UserPreferencesDto, DeskRepresentation } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-user-favorite-desks',
  templateUrl: './user-favorite-desks.component.html',
  styleUrls: ['./user-favorite-desks.component.scss']
})
export class UserFavoriteDesksComponent implements OnInit {


  readonly gripHorizontalIcon = faGripHorizontal;
  readonly messages = UserEditSubPanelsMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly infoIcon = faInfoCircle;


  @Input() userPreferences: UserPreferencesDto;

  currentSearchTerm = null;
  page = 1;
  pageSize = 100;
  total = 0;

  allRequestedDesks: DeskRepresentation[] = [];
  pendingNonFavoriteDesks: DeskRepresentation[] = [];
  pendingFavoriteDesks: DeskRepresentation[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(private currentUserService: CurrentUserService,
              private notificationService: NotificationsService) { }


  ngOnInit() {

    console.log('favoriteDesksIds: ' + this.userPreferences.favoriteDeskIds);
    console.log('favoriteDesks: ' + this.userPreferences.favoriteDesks);

    this.pendingFavoriteDesks = this.userPreferences.favoriteDesks;
    this.requestDesks();
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearchTerm = newTerm;
    this.page = 1;
    this.requestDesks();
  }


  requestDesks() {
    this.currentUserService
      .getDesks(this.currentSearchTerm, this.page - 1, this.pageSize)
      .subscribe(
        desksRetrieved => {
          this.allRequestedDesks = desksRetrieved.content;
          this.total = desksRetrieved.totalElements;
          this.refreshNonFavoriteDesksAndPreferences();
        },
        error => this.notificationService.showErrorMessage(UserEditSubPanelsMessages.CANNOT_RETRIEVE_DESKS, error.message)
      );
  }


  drop(event: CdkDragDrop<string>): void {
    this.pendingFavoriteDesks
      .splice(event.currentIndex, 0, this.pendingFavoriteDesks.splice(event.previousIndex, 1)[0]);
    this.refreshNonFavoriteDesksAndPreferences();
  }


  removeFromFavorite(desk: DeskRepresentation) {
    this.pendingFavoriteDesks = this.pendingFavoriteDesks.filter(d => d.id !== desk.id);
    this.refreshNonFavoriteDesksAndPreferences();
  }


  addToFavorite(desk: DeskRepresentation) {
    if (this.userPreferences?.favoriteDeskIds?.length >= 15) {
      this.notificationService.showErrorMessage(this.messages.FAVORITE_DESK_LIMIT_REACHED);
      return;
    }

    this.pendingFavoriteDesks.push(desk);
    this.refreshNonFavoriteDesksAndPreferences();
  }


  private refreshNonFavoriteDesksAndPreferences() {

    this.pendingNonFavoriteDesks = this.allRequestedDesks
      .filter(allDesk => !this.pendingFavoriteDesks.some(favDesk => allDesk.id === favDesk.id));

    this.userPreferences.favoriteDeskIds = this.pendingFavoriteDesks.map(desk => desk.id);
  }


}
