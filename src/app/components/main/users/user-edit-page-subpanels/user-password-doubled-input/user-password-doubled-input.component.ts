/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';

@Component({
  selector: 'app-user-password-doubled-input',
  templateUrl: './user-password-doubled-input.component.html',
  styleUrls: ['./user-password-doubled-input.component.scss']
})
export class UserPasswordDoubledInputComponent implements AfterViewInit {
  @Input() isLdapSynchronized: boolean = false;
  @Output() passwordChanged = new EventEmitter<string>();

  @ViewChild('passwordInput') passwordInput: ElementRef<HTMLInputElement>;
  @ViewChild('passwordConfirmationInput') passwordConfirmationInput: ElementRef<HTMLInputElement>;

  readonly eyeIcon = faEye;
  readonly eyeSlashIcon = faEyeSlash;
  readonly messages = UserEditSubPanelsMessages;

  hidePassword = true;


  ngAfterViewInit(): void {
    this.registerKeyupOnInput(this.passwordInput.nativeElement);
    this.registerKeyupOnInput(this.passwordConfirmationInput.nativeElement);
  }


  registerKeyupOnInput(inputElement: HTMLInputElement) {
    fromEvent(inputElement, 'keyup')
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => this.passwordInputChanged());
  }


  private passwordInputChanged() {
    const valuesMatch = this.passwordInput.nativeElement.value === this.passwordConfirmationInput.nativeElement.value;
    const valueIsEmpty = !this.passwordInput.nativeElement.value;

    if (!valuesMatch || valueIsEmpty) return;

    this.passwordChanged.emit(this.passwordInput.nativeElement.value);
  }


}
