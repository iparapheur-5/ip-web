/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, ViewChild, ElementRef } from '@angular/core';
import { StatsGraphType } from '../../../models/stats-graph-type.enum';
import { StatsGraphPeriod } from '../../../models/stats-graph-period.enum';
import { Observable } from 'rxjs';
import { DeskService } from '../../../services/ip-core/desk.service';
import { CurrentUserService, PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../../../services/notifications.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent {

  readonly ratio = 1.777;  // 16/9
  readonly statsGraphPeriodEnum = StatsGraphPeriod;
  readonly statsGraphTypeEnum = StatsGraphType;


  @ViewChild('chartImageElement') chartImageElement: ElementRef;

  chartImageSrc = null;

  desks: DeskRepresentation[] = [];
  page = 0;
  pageSize = 10;
  total = 0;

  selectedDesk: DeskRepresentation;
  startDate: string;
  endDate: string;
  periodicity: StatsGraphPeriod = StatsGraphPeriod.Day;
  graphType: StatsGraphType = StatsGraphType.Lines;

  // <editor-fold desc="LifeCycle">


  constructor(public currentUserService: CurrentUserService,
              public notificationsService: NotificationsService,
              public deskService: DeskService) {
    const theDate = new Date();
    this.endDate = theDate.toISOString().substr(0, 10);

    theDate.setDate(theDate.getDate() - 30);
    this.startDate = theDate.toISOString().substr(0, 10);
  }


  // </editor-fold desc="LifeCycle">


  retrieveUserDesksFn = (page: number, pageSize: number, searchTerm?: string): Observable<PageDeskRepresentation> => {
    return this.currentUserService.getDesks(searchTerm, page, pageSize)
      .pipe(catchError(this.notificationsService.handleHttpError('getDesk')));
  }


  onDeskSelectionChanged(event: any) {
    this.selectedDesk = event[0];
    this.refreshGraph();
  }


  refreshGraph() {

    if (this.selectedDesk === null) {
      return;
    }

    let height = this.chartImageElement.nativeElement.getBoundingClientRect().height;
    const width = this.chartImageElement.nativeElement.getBoundingClientRect().width;

    // Forcing a 16/9 ratio
    height = Math.max(height, width / this.ratio);
    height = Math.round(height);

    this.chartImageSrc = this.deskService.getDeskChartUrl(this.selectedDesk.tenantId, this.selectedDesk, width, height, this.startDate, this.endDate,
      this.periodicity, this.graphType);
  }


  compareById(o1: DeskRepresentation, o2: DeskRepresentation) {
    return (!!o1 === !!o2) && (o1.id === o2.id);
  }


}
