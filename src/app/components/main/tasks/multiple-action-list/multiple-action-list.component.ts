/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Injector, Output, EventEmitter } from '@angular/core';
import { SendByMailPopupComponent } from '../../folder-view/action-popups/mail/send-by-mail-popup/send-by-mail-popup.component';
import { SimpleActionPopupComponent } from '../../folder-view/action-popups/simple-action-popup/simple-action-popup.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { CommonMessages } from '../../../../shared/common-messages';
import { Folder } from '../../../../models/folder/folder';
import { ExternalSignaturePopupComponent } from '../../folder-view/action-popups/external-signature-popup/external-signature-popup.component';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { combineLatest, Observable, forkJoin } from 'rxjs';
import { NotificationsService } from '../../../../services/notifications.service';
import { TargetDeskActionPopupComponent } from '../../folder-view/action-popups/target-desk-action-popup/target-desk-action-popup.component';
import { Action } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { TasksMessages } from '../tasks-messages';
import { WebsocketService } from '../../../../services/websockets/websocket.service';
import { FolderUtils } from '../../../../utils/folder-utils';

@Component({
  selector: 'app-multiple-action-list',
  templateUrl: './multiple-action-list.component.html',
  styleUrls: ['./multiple-action-list.component.scss']
})
export class MultipleActionListComponent {


  public static readonly forbiddenActions: (Action | SecondaryAction)[] = [Action.Chain, SecondaryAction.Print];

  public readonly actionEnum = Action;
  public readonly messages = TasksMessages;

  @Input() folders: Folder[];
  @Input() actions: (Action | SecondaryAction)[];
  @Input() deskId: string;
  @Input() tenantId: string;
  @Output() onSuccess: EventEmitter<Action | SecondaryAction> = new EventEmitter<Action | SecondaryAction>();


  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal,
              public folderService: FolderService,
              private websocketService: WebsocketService,
              public globalPopupService: GlobalPopupService,
              public notificationsService: NotificationsService,
              public router: Router) {}


  // </editor-fold desc="LifeCycle">


  public openModal(action: Action | SecondaryAction): void {

    if (MultipleActionListComponent.forbiddenActions.includes(action)) {
      return;
    }

    let folderRequestList: Observable<Folder>[] = [];

    this.folders.forEach(folder => folderRequestList.push(this.folderService.getFolder(this.tenantId, folder.id, null, true)));

    forkJoin(folderRequestList)
      .subscribe(populatedFolders => {
          populatedFolders.forEach(
            folder => folder.stepList = this.folders.find(f => f.id === folder.id)?.stepList || []
          );
          this.switchModal(populatedFolders, action);
        },
        error => this.notificationsService.showErrorMessage(this.messages.ERROR_RETRIEVING_FOLDERS, error)
      );
  }


  private switchModal(folders: Folder[], action: Action | SecondaryAction) {
    let modalResult: Promise<any>;
    let modalSize = FolderUtils.getModalSize(action, folders);

    switch (action) {

      case Action.Delete:
        modalResult = this.globalPopupService
          .showDeleteValidationPopup(
            CommonMessages.foldersValidationPopupLabel(folders),
            CommonMessages.foldersValidationPopupTitle(folders))
          .then(
            () => combineLatest(
              folders
                .map(d => d.id)
                .map(id => this.folderService.deleteFolder(this.tenantId, id)))
              .toPromise(),
            () => { /* Dismissed */ }
          );
        break;

      case SecondaryAction.Mail:
      case Action.SecureMail:
        modalResult = this.modalService
          .open(SendByMailPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: SendByMailPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.deskId},
                {provide: SendByMailPopupComponent.INJECTABLE_CURRENT_ACTION_KEY, useValue: action},
                {provide: SendByMailPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: folders},
                {provide: SendByMailPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case Action.ExternalSignature:
        modalResult = this.modalService
          .open(ExternalSignaturePopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: ExternalSignaturePopupComponent.INJECTABLE_FOLDERS_KEY, useValue: folders},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.deskId},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: action},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_TENANT_KEY, useValue: this.tenantId},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY, useValue: null}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case Action.Transfer:
      case Action.SecondOpinion:
        modalResult = this.modalService
          .open(TargetDeskActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: TargetDeskActionPopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: action},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: folders},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.deskId},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: false},
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      default:
        modalResult = this.modalService
          .open(SimpleActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: SimpleActionPopupComponent.injectableFoldersKey, useValue: folders},
                {provide: SimpleActionPopupComponent.injectableDeskIdKey, useValue: this.deskId},
                {provide: SimpleActionPopupComponent.injectablePerformedActionKey, useValue: action},
                {provide: SimpleActionPopupComponent.injectableTenantKey, useValue: this.tenantId}
              ]
            }),
            size: modalSize
          })
          .result;
        break;
    }

    modalResult
      .then(result => {
        this.websocketService.notifyFolderAction(this.tenantId, this.deskId);

        // Delete was dismissed
        if (result === undefined) {
          return;
        }

        // Delete was successful
        if (Array.isArray(result) && result.every(res => res === null)) {
          this.notificationsService.showSuccessMessage(CommonMessages.getMultipleDeleteCreationMessage(folders));
          this.onSuccess.emit(action);
        }

        if ((result === CommonMessages.ACTION_RESULT_OK) || (result.value === CommonMessages.ACTION_RESULT_OK)) {
          this.onSuccess.emit(action);
        }
      });
  }


  doesOneFolderHaveToBeRead(): boolean {
    return !this.folders.filter(folder => folder.selected).every(folder => !this.doesFolderHaveToBeRead(folder))
  }


  doesFolderHaveToBeRead(folder: Folder): boolean {
    return folder?.subtype?.readingMandatory && !folder?.readByCurrentUser && folder?.stepList[0]?.action === Action.Signature;
  }


}
