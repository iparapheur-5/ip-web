/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faCaretUp, faCaretDown, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeskService as LegacyDeskService } from '../../../../services/ip-core/desk.service';
import { WorkflowService } from '../../../../services/ip-core/workflow.service';
import { ExternalState } from '../../../../models/external-state.enum';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../shared/common-messages';
import { Folder } from '../../../../models/folder/folder';
import { MultipleActionListComponent } from '../multiple-action-list/multiple-action-list.component';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { FolderFilter } from '../../../../models/folder/folder-filter';
import { NamePipe } from '../../../../shared/utils/name.pipe';
import { TasksMessages } from '../tasks-messages';
import { FilterPanelIcons } from '../../../../shared/components/folder-filter-panel/filter-panel-icons';
import { of } from 'rxjs';
import { NotificationsService } from '../../../../services/notifications.service';
import { Action, State, FolderSortBy, DeskDto, TaskViewColumn, UserPreferencesDto, FolderFilterDto } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { ColumToFolderSortByPipe } from '../../../../utils/colum-to-folder-sort-by.pipe';
import { FolderUtils } from '../../../../utils/folder-utils';
import { SelectedDeskService } from '../../../../services/resolvers/desk/selected-desk.service';
import { tap, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {


  readonly pageSizes: number[] = [10, 15, 20, 50, 100];
  readonly commonMessages = CommonMessages;
  readonly messages = TasksMessages;
  readonly commonIcons = CommonIcons;
  readonly icons = FilterPanelIcons;
  readonly columnsEnum = TaskViewColumn;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly eyeIcon = faEye;
  readonly eyeSlashIcon = faEyeSlash;
  readonly actionEnum = Action;
  readonly sortByEnum = FolderSortBy;
  readonly stateEnum = State;
  readonly folderState: State;
  readonly filterFromNavigation: FolderFilterDto;
  readonly filterIdFromNavigation: string;

  deskId: string;
  tenantId: string;
  userPreferences: UserPreferencesDto;
  desk: DeskDto;
  folders: Folder[];
  loading: boolean = false;
  allFoldersSelected: boolean = false;
  showFilterPanel = false;
  actionsToDisplay: (Action | SecondaryAction)[] = [];
  selectedFolders: Folder[] = [];
  sortBy: FolderSortBy = FolderSortBy.CreationDate;
  asc = true;
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  selectedFilterId: string;
  userFolderFilters: FolderFilterDto[];
  colspan: number;

  private _appliedFilter: FolderFilterDto;
  // Original filter is used to manage url changing depending on state folder in order to keep trace of the original search and be able to come back
  originalFilter: FolderFilterDto;


  doesFolderHaveToBeRead(folder: Folder): boolean {
    return folder.subtype?.readingMandatory && !folder.readByCurrentUser && folder.stepList[0].action === Action.Signature;
  }


  selectedFilterChange(filterId: string) {
    this.filterChanged({folderFilter: this.getFolderFilter(filterId), reloadFilters: false, changeOriginalFilter: true});
  }


  get appliedFilter(): FolderFilterDto {
    return this._appliedFilter;
  }


  set appliedFilter(value: FolderFilterDto) {
    value.state = value.state || this.folderState;
    this._appliedFilter = value;
  }


  // <editor-fold desc="LifeCycle">


  constructor(public workflowService: WorkflowService,
              public notificationsService: NotificationsService,
              public selectedDeskService: SelectedDeskService,
              public legacyDeskService: LegacyDeskService,
              public legacyUserService: LegacyUserService,
              public modalService: NgbModal,
              public route: ActivatedRoute,
              protected router: Router) {

    this.deskId = this.route.snapshot.paramMap.get('deskId');
    this.tenantId = this.route.snapshot.paramMap.get('tenantId');
    this.showFilterPanel = this.router.getCurrentNavigation()?.extras?.state?.panelOpened;
    this.userPreferences = this.route.snapshot.data['userPreferences'];

    this.colspan = 1 + this.userPreferences.taskViewColumnList.length;

    if (!this.userPreferences.showAdminIds) {
      this.userPreferences.taskViewColumnList =
        this.userPreferences.taskViewColumnList
          .filter(column => column != TaskViewColumn.TaskId)
          .filter(column => column != TaskViewColumn.FolderId)
    }
    this.asc = this.userPreferences.taskViewDefaultAsc;
    this.sortBy = ColumToFolderSortByPipe.compute(this.userPreferences.taskViewDefaultSortBy as TaskViewColumn);
    this.pageSizeIndex = this.router.getCurrentNavigation()?.extras?.state?.pageSizeIndex || this.pageSizes.findIndex(item => item === this.userPreferences.taskViewDefaultPageSize) | 1;

    this.folderState = this.route.snapshot.url.pop().toString().toUpperCase() as State;

    this.filterFromNavigation = this.router.getCurrentNavigation()?.extras?.state?.filter;
    this.filterIdFromNavigation = this.router.getCurrentNavigation()?.extras?.state?.filterId;
    this.originalFilter = this.router.getCurrentNavigation()?.extras?.state?.originalFilter;

  }

  ngOnInit(): void {
    this.selectedDeskService.selectedDesk
      .pipe(
        tap(selectedDesk => this.desk = selectedDesk),
        mergeMap(() => this.legacyUserService.getCurrentUserFolderFilters())
      )
      .subscribe(
        filters => {
          this.userFolderFilters = filters.map(f => f as FolderFilter);
          this.appliedFilter = this.filterFromNavigation ?? this.getFolderFilter(this.filterIdFromNavigation);
          this.selectedFilterId = this.appliedFilter.id;
          this.originalFilter = this.originalFilter ?? FolderFilter.getFolderFilterForState(this.folderState);
          this.requestFolders();
        },
        error => this.notificationsService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FILTERS, error.message)
      );
  }


  // </editor-fold desc="LifeCycle">


  public getCurrentDeskNameIfAppropriate(folder: Folder): string {
    const task = folder.stepList[0];

    // Nothing set yet for a delegation coming from multiple desks.
    // This is a very special case, probably impossible to reach.
    // We won't display anything in that case.
    const isOriginDeskSingle = (task.desks.length === 1);

    return (isOriginDeskSingle) ? NamePipe.compute(task.desks[0].name) : '';
  }


  public getTargetDeskId(folder: Folder): string {
    const possibleTargetDeskIds: string[] = [];
    folder.stepList.forEach(task => {
      possibleTargetDeskIds.push(...task.desks.map(d => d.id));
    });

    if (possibleTargetDeskIds.includes(this.deskId) || possibleTargetDeskIds.length == 0) {
      return this.deskId;
    }

    return possibleTargetDeskIds.pop();
  }


  public isDateBeforeNow(date: Date): boolean {
    return (date != null) && (new Date(date).getTime() < Date.now());
  }


  public isCreateFolderAvailable() {

    const isTenantSet = !!this.desk?.tenantId;
    const isAllowed = this.desk?.folderCreationAllowed;
    const isCurrentUserOwner = this.legacyUserService.isCurrentUserDeskOwner(this.tenantId, this.deskId);

    return isTenantSet && isAllowed && isCurrentUserOwner;
  }


  onMultipleActionSuccess(action: Action | SecondaryAction) {
    console.debug(`Success on action ${action}`);

    this.requestFolders();
  }

  // <editor-fold desc="UI callbacks">


  public filterChanged(event: { folderFilter: FolderFilterDto; reloadFilters: boolean; changeOriginalFilter: boolean }): void {
    const folderFilter = event.folderFilter || FolderFilter.getFolderFilterForState(this.folderState);
    const reloadFilters = event.reloadFilters;
    const changeOriginalFilter = event.changeOriginalFilter;

    if (changeOriginalFilter) {
      this.originalFilter = Object.assign(new FolderFilter(), folderFilter);
    }

    if (folderFilter.state !== this.folderState) {
      this.router
        .navigate(
          ['tenant', this.tenantId, 'desk', this.deskId, folderFilter.state.toString().toLowerCase()],
          {state: {filter: folderFilter, originalFilter: this.originalFilter, panelOpened: true, pageSizeIndex: this.pageSizeIndex}}
        )
        .then();
    } else {
      const call = reloadFilters
        ? this.legacyUserService.getCurrentUserFolderFilters().subscribe(filters => this.userFolderFilters = filters)
        : of().subscribe();

      call.add(
        () => {
          folderFilter.typeId = (folderFilter as FolderFilterDto).type?.id;
          folderFilter.subtypeId = (folderFilter as FolderFilterDto).subtype?.id;
          this.appliedFilter = folderFilter;
          this.selectedFilterId = folderFilter.id;
          this.requestFolders();
        }
      )
    }
  }


  public onRowOrderClicked(column: TaskViewColumn): void {
    let row = this.mapColumnToSortBy(column);
    if (!!row) {
      this.asc = (row === this.sortBy) ? !this.asc : true;
      this.sortBy = row;
      this.requestFolders();
    }
  }


  // </editor-fold desc="UI callbacks">


  public mapColumnToSortBy(column: TaskViewColumn): FolderSortBy {
    switch (column) {
      case TaskViewColumn.FolderName: { return FolderSortBy.FolderName; }
      // FIXME: case TaskViewColumn.Type: { return FolderSortBy.TypeName; }
      // FIXME: case TaskViewColumn.Subtype: { return FolderSortBy.SubtypeName; }
      case TaskViewColumn.LimitDate: { return FolderSortBy.LateDate; }
      case TaskViewColumn.CreationDate: { return FolderSortBy.CreationDate; }
      case TaskViewColumn.TaskId: { return FolderSortBy.TaskId; }
      case TaskViewColumn.FolderId: { return FolderSortBy.FolderId; }
      default: { return null; }
    }
  }


  /**
   * Re-fetching the task list, updating the UI.
   */
  public requestFolders(): void {
    this.uncheckAll();
    this.loading = true;
    this.workflowService
      .getFolders(
        this.tenantId,
        this.deskId,
        this.appliedFilter,
        this.sortBy,
        this.asc,
        this.page - 1,
        this.getPageSize(this.pageSizeIndex)
      )
      .subscribe(
        foldersRetrieved => {
          this.folders = foldersRetrieved.data;
          this.total = foldersRetrieved.total;
        },
        error => this.notificationsService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FOLDERS, error.message)
      )
      .add(() => this.loading = false);
  }


  pageChange(newPage: number): void {
    this.page = newPage;
    this.requestFolders();
  }


  pageSizeChange(newPageSizeIndex: number): void {
    this.pageSizeIndex = newPageSizeIndex;
    this.requestFolders();
  }


  public getPageSize(pageIndex: number): number {
    return this.pageSizes[pageIndex - 1];
  }


  // TODO : unit-test that !
  public setActionsToDisplay(): void {
    this.actionsToDisplay = [];

    const selectedFolders = this.folders.filter(f => f.selected === true);
    const oneFolderIsUnsafe: boolean = selectedFolders.filter(f => !f.type || !f.subtype).length > 0;
    this.allFoldersSelected = this.folders.length === selectedFolders.length;

    if (selectedFolders.length === 0 || oneFolderIsUnsafe) {
      this.actionsToDisplay = [];
      return;
    }

    const foldersActions = selectedFolders.map(folder => folder.stepList[0].action);
    const uniqueAction = foldersActions[0];


    const deskHasArchivingRights: boolean = this.desk?.archivingAllowed
    const inDownstreamState = this.appliedFilter?.state === State.Downstream;
    const inRetrievableState = this.appliedFilter?.state === State.Retrievable;
    const mainActionsAllowedInCurrentState = !inDownstreamState && !inRetrievableState;

    const actionValidInState = !(uniqueAction === Action.Undo && !inRetrievableState) && !(uniqueAction === Action.Archive && !deskHasArchivingRights);

    const isThereOnlyStackableActions = foldersActions
      .every(action => FolderUtils.STACKABLE_ACTIONS.includes(action));

    const isThereMultipleStackableActions = foldersActions
      .filter(action => FolderUtils.STACKABLE_ACTIONS.includes(action))
      .filter((value, index, self) => self.indexOf(value) === index) // == distinct
      .length >= 2;

    const areAllActionsTheSame = foldersActions
      .every(action => action === foldersActions[0]);

    const areAllActionsRelevant = selectedFolders
      .filter(folder => !!folder.stepList[0].externalState)
      .filter(folder => folder.stepList[0].externalState !== ExternalState.Form)
      .length === 0;

    if (isThereOnlyStackableActions && isThereMultipleStackableActions && !inDownstreamState) {
      this.actionsToDisplay = [SecondaryAction.StackedValidation];
    } else if (isThereOnlyStackableActions && !isThereMultipleStackableActions && !inDownstreamState) {
      this.actionsToDisplay = [uniqueAction];
    } else if (areAllActionsTheSame
      && areAllActionsRelevant
      && !inDownstreamState
      && actionValidInState) {
      this.actionsToDisplay = [uniqueAction];
    }

    if (areAllActionsTheSame && areAllActionsRelevant && uniqueAction === Action.Start) {
      this.actionsToDisplay.push(Action.Delete);
    }

    if (areAllActionsTheSame
      && areAllActionsRelevant
      && uniqueAction === Action.Delete
      && mainActionsAllowedInCurrentState
      && deskHasArchivingRights) {
      this.actionsToDisplay.push(Action.Archive);
    }

    if (!foldersActions.includes(Action.SecondOpinion)
      && !foldersActions.includes(Action.Delete)
      && !foldersActions.includes(Action.Start)
      && !foldersActions.includes(Action.Archive)
      && mainActionsAllowedInCurrentState) {
      this.actionsToDisplay.push(Action.Reject);
    }

    if (areAllActionsTheSame && uniqueAction === Action.Visa && mainActionsAllowedInCurrentState) {
      this.actionsToDisplay.push(Action.Transfer)
    }


    const areAllTasksExternal = foldersActions.every(action => FolderUtils.EXTERNAL_ACTIONS.includes(action));
    if (areAllTasksExternal && mainActionsAllowedInCurrentState) {
      this.actionsToDisplay.push(Action.Bypass);
    }

    this.actionsToDisplay.push(SecondaryAction.Mail);
    this.actionsToDisplay = this.actionsToDisplay.filter(action => !MultipleActionListComponent.forbiddenActions.includes(action));

    const oneFolderCannotPerformMainAction = selectedFolders.some(f => this.isMainActionUnavailable(f));
    if (oneFolderCannotPerformMainAction) {
      this.actionsToDisplay.splice(0, 1);
    }

    this.selectedFolders = this.folders.filter(folder => folder.selected === true);
  }


  private isMainActionUnavailable(folder: Folder): boolean {
    return !folder.type || !folder.subtype;
  }


  public checkAll(): void {
    const areNoneSelected = this.folders.filter(folder => folder.selected === false).length === 0;
    this.folders.forEach(folder => folder.selected = !areNoneSelected);
    this.setActionsToDisplay();
  }

  uncheckAll(): void {
    if (!this.folders) {
      return;
    }

    this.folders.forEach(folder => folder.selected = false);
    this.setActionsToDisplay();
  }

  public toggleFilterPanel(): void {
    this.showFilterPanel = !this.showFilterPanel;
  }


  private getFolderFilter(folderFilterId?: string): FolderFilterDto {
    return this.userFolderFilters.find(filter => filter.id === folderFilterId) || FolderFilter.getFolderFilterForState(this.folderState);
  }


}
