/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { faDesktop, faCaretRight, faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { isNullOrUndefined } from '../../../utils/string-utils';
import { CommonMessages } from '../../../shared/common-messages';
import { DeskListMessages } from './desk-list-messages';
import { NotificationsService } from '../../../services/notifications.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FolderUtils } from '../../../utils/folder-utils';
import { FilterService } from '../../../services/filter.service';
import { State, TenantService, TenantSortBy, CurrentUserService, DeskRepresentation, PageDeskRepresentation, DeskCount, UserPreferencesDto, TenantRepresentation } from '@libriciel/iparapheur-standard';
import { from, Subscription, Observable } from 'rxjs';
import { WebsocketService } from '../../../services/websockets/websocket.service';
import { tap, catchError } from 'rxjs/operators';
import { ChunkPipe } from '../../../utils/chunk.pipe';

export class FlatTreeNode {
  id: string;
  name: string;
  actualElement: TenantRepresentation | DeskRepresentation;
  isDesk: boolean = false;
  expanded = true;

  constructor(elem: DeskRepresentation | TenantRepresentation, isDesk: boolean = false) {
    this.name = elem ? elem.name : '';
    this.id = elem ? elem.id : '';
    this.actualElement = elem;
    this.isDesk = isDesk;
  }
}

export enum DisplayModeEnum {
  THUMBS,
  LIST
}


@Component({
  selector: 'app-desk-list',
  templateUrl: './desk-list.component.html',
  styleUrls: ['./desk-list.component.scss']
})
export class DeskListComponent implements OnInit, OnDestroy {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretRightIcon = faCaretRight;
  readonly caretDownIcon = faCaretDown;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly deskIcon = faDesktop;
  readonly commonMessages = CommonMessages;
  readonly messages = DeskListMessages;
  readonly stateEnum = State;
  readonly THUMB_MODE_PAGE_SIZE = 15;
  readonly LIST_MODE_PAGE_SIZE = 250;
  readonly DISPLAY_MODE_ENUM = DisplayModeEnum;
  readonly computeDelegatedTotalFn = FolderUtils.computeDelegatedTotal;


  currentSearchTerm = null;
  flattenTreeStructureUnfiltered: FlatTreeNode[] = [];
  flattenTreeStructure: FlatTreeNode[] = [];
  tenants: TenantRepresentation[] = [];
  desks: DeskRepresentation[] = [];
  deskCounts: DeskCount[];
  favoriteDesks: DeskRepresentation[] = [];
  folderCountsSubscriptions: Subscription[] = [];
  userPreferences: UserPreferencesDto;
  page = 1;
  pageSize = this.THUMB_MODE_PAGE_SIZE;
  pageSizeIndex = 3;
  total = 0;
  displayMode: DisplayModeEnum = DisplayModeEnum.THUMBS;
  disableDisplayMode: boolean = false;
  showOnlyFavorites: boolean = false;
  asc: boolean = true;
  loading: boolean = false;
  colspan: number = 2;


  // <editor-fold desc="LifeCycle">


  constructor(public tenantService: TenantService,
              public currentUserService: CurrentUserService,
              public notificationService: NotificationsService,
              public modalService: NgbModal,
              private route: ActivatedRoute,
              private router: Router,
              private filterService: FilterService,
              private websocketService: WebsocketService) {}

  ngOnDestroy(): void {
    this.folderCountsSubscriptions.forEach(subscription => subscription.unsubscribe());
  }


  ngOnInit() {
    this.userPreferences = this.route.snapshot.data["userPreferences"];
    this.pageSizeIndex = this.pageSizes.findIndex(value => value === this.userPreferences.taskViewDefaultPageSize);
    this.asc = this.userPreferences.taskViewDefaultAsc;
    this.requestTenants();
    this.requestDesks()
      .subscribe(() => {
        this.redirectToOnlyDeskIfNeeded();
        this.websocketService.watchForDeskCountChanges(this.desks)
          .subscribe(observables => {
            this.folderCountsSubscriptions = observables.map(observable => observable.subscribe(
              newDeskCount => {
                const index = this.deskCounts.findIndex(existingDeskCount => existingDeskCount.deskId === newDeskCount.deskId);
                if (index === -1) return;
                console.debug(`Folder count updated, tenant : ${newDeskCount.tenantId}, desk : ${newDeskCount.deskId}`);
                this.deskCounts[index] = newDeskCount;
              }
            ));
          });
      })
      .add(() => this.loading = false);
  }


  // </editor-fold desc="LifeCycle">


  changeDisplayMode(): void {
    if (this.displayMode === DisplayModeEnum.LIST) {
      this.displayMode = DisplayModeEnum.THUMBS;
    } else {
      this.displayMode = DisplayModeEnum.LIST;
    }
  }


  forceDisplayMode() {
    this.showOnlyFavorites = false;
    if (this.userPreferences.favoriteDesks.length > 0 && this.total > this.THUMB_MODE_PAGE_SIZE && !this.currentSearchTerm) {
      this.showOnlyFavorites = true;
    } else if (this.total > this.THUMB_MODE_PAGE_SIZE && !this.currentSearchTerm) {
      this.disableDisplayMode = true;
      this.displayMode = DisplayModeEnum.LIST;
    }
  }


  /**
   * Re-fetching the desk list, updating the UI.
   */
  requestDesks(): Observable<PageDeskRepresentation> {
    // We have to request all desks (max is 250.)
    // So we can force the style to LIST if there is more than 15 results
    // Or retrieve all the favorites if we are in THUMB mode
    // page size is only used after the request in LIST mode

    this.loading = true;
    const pageSize = (!!this.currentSearchTerm && this.displayMode === DisplayModeEnum.THUMBS) ? this.THUMB_MODE_PAGE_SIZE : this.LIST_MODE_PAGE_SIZE;
    const page = !!this.currentSearchTerm ? 0 : this.page - 1;


    this.currentUserService.getDesksFolderCount(this.currentSearchTerm, page, pageSize)
      .subscribe(deskCounts => {
        this.deskCounts = deskCounts.content
      });

    return this.currentUserService
      .getDesks(this.currentSearchTerm, page, pageSize)
      .pipe(
        tap(desksRetrieved => {
          this.desks = desksRetrieved.content;
          this.total = desksRetrieved.totalElements;
          this.forceDisplayMode();
          this.setDeskOrder();
          this.buildFlattenTreeStructure();
        }),
        catchError(this.notificationService.handleHttpError('getDesks'))
      );
  }


  requestTenants() {
    this.tenantService
      .listTenantsForUser(0, 10000, [TenantSortBy.Name + ',ASC'], false)
      .subscribe(tenantsRetrieved => {
        this.tenants = tenantsRetrieved.content;
        this.tenants = this.sortTenants();
        this.buildFlattenTreeStructure();
      });
  }


  redirectToOnlyDeskIfNeeded(): void {
    if (this.desks.length !== 1) return;

    setTimeout(() => {
        const desk = this.desks[0];
        from(this.router.navigate([`/tenant/${desk.tenantId}/desk/${desk.id}/${this.getDefaultFilterRoutePath()}`]))
          .subscribe(
            () => console.log(`Redirected to desk ${desk.id}`),
            () => console.log(`Error redirecting to desk ${desk.id}`)
          );
      },
      500
    );

  }


  buildFlattenTreeStructure() {

    if (!this.desks || this.desks.length === 0) {
      return;
    }

    if (!this.tenants || this.tenants.length === 0) {
      this.flattenTreeStructureUnfiltered = [new FlatTreeNode({id: 'defaultId', name: 'defaultName'})];
      this.flattenTreeStructureUnfiltered.concat(this.desks.map(d => new FlatTreeNode(d, true)));
      return;
    }

    let desksWithoutTenant: DeskRepresentation[] = this.desks.filter(desk => isNullOrUndefined(desk.tenantId));
    if (desksWithoutTenant.length > 0) {
      console.warn('Desks without matched tenant : ', desksWithoutTenant);
    }
    const newTree: FlatTreeNode[] = [];
    this.tenants.forEach(tenant => {

      let tenantDesks: FlatTreeNode[] = this.desks
        .filter(desk => desk.tenantId === tenant.id)
        .sort((a, b) => this.sortByName(a.name, b.name))
        .map(desk => new FlatTreeNode(desk, true));

      if (tenantDesks.length > 0) {
        newTree.push(new FlatTreeNode(tenant));
        newTree.push(...tenantDesks);
      }
    });

    this.total = newTree.length;

    this.flattenTreeStructureUnfiltered = newTree;
    this.updatePage();
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  setDeskOrder() {
    let favoriteDeskInOrder = [];

    this.userPreferences.favoriteDesks.forEach(desk => {
      const deskIndex = this.desks.findIndex(dd => desk.id === dd.id);
      if (deskIndex !== -1) {
        favoriteDeskInOrder.push(this.desks[deskIndex]);
      }
    });
    this.favoriteDesks = favoriteDeskInOrder;

    this.desks = this.favoriteDesks.concat(
      this.desks.filter(desk => !this.userPreferences.favoriteDesks.some(dd => dd.id === desk.id))
    );
  }


  sortTenants(): TenantRepresentation[] {
    return this.tenants.sort((a, b) => this.sortByName(a.name, b.name));
  }


  sort() {
    this.asc = !this.asc;

    this.tenants = this.sortTenants();
    this.buildFlattenTreeStructure();
  }


  sortByName(objectAName: string, objectBName: string): number {
    return this.asc
      ? ((objectAName.toUpperCase() < objectBName.toUpperCase()) ? -1 : (objectAName.toUpperCase() > objectBName.toUpperCase()) ? 1 : 0)
      : ((objectBName.toUpperCase() < objectAName.toUpperCase()) ? -1 : (objectBName.toUpperCase() > objectAName.toUpperCase()) ? 1 : 0);
  }


  updateSearchTerm(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearchTerm = newTerm;
    this.page = 1;
    this.requestDesks().subscribe().add(() => this.loading = false);
  }


  computeTotal(): number {
    return this.flattenTreeStructureUnfiltered.filter(node => !node.isDesk || (node.isDesk && node.expanded)).length;
  }

  getThumbsDesks(): DeskRepresentation[] {
    return this.showOnlyFavorites
      ? this.favoriteDesks
      : this.desks;
  }


  updatePage() {
    this.pageSize = this.pageSizes[this.pageSizeIndex - 1];

    let startIndex: number = this.pageSize * (this.page - 1);
    let endIndex = startIndex + this.pageSize;

    this.flattenTreeStructure = this.flattenTreeStructureUnfiltered.filter(node => !node.isDesk || (node.isDesk && node.expanded)).slice(startIndex, endIndex);
  }


  expandOrCollapse(tenantNode: FlatTreeNode) {
    if (tenantNode.isDesk) {
      return;
    }

    tenantNode.expanded = !tenantNode.expanded;

    // fold/unfold "by hand" all the desks nodes, as do not use a tree struct anymore
    const elemIdx = this.flattenTreeStructureUnfiltered.findIndex(elem => elem.id === tenantNode.id);
    for (let i = elemIdx + 1 ; i < this.flattenTreeStructureUnfiltered.length ; ++i) {
      let node = this.flattenTreeStructureUnfiltered[i];
      if (node.isDesk) {
        node.expanded = tenantNode.expanded
      } else {
        break;
      }
    }
    this.updatePage();
  }


  getDefaultFilterRoutePath(): string {
    const targetState = this.filterService.getDefaultFilter()?.state ?? State.Pending;
    return targetState.toLowerCase();
  }

  getDeskCount(deskId: string): DeskCount {
    return this.deskCounts?.find(dc => dc.deskId === deskId);
  }


}
