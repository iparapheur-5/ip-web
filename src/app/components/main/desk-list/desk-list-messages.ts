/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { CommonMessages } from '../../../shared/common-messages';

export class DeskListMessages {

  static readonly PAGE_TITLE = `Bienvenue sur le ${CommonMessages.APP_NAME}`;
  static readonly SELECT_A_DESK_MESSAGE = 'Sélectionnez un bureau pour parcourir ses dossiers';
  static readonly DISPLAY_MODE_LIST = 'Vue tableau';

  static readonly DISABLED_DISPLAY_MODE_TOOLTIP = 'Aucun bureau favori défini dans le profil';



  static getThumbsOrFavoritesTitle = (favoriteDesksCount: number) => favoriteDesksCount > 0 ? 'Favoris' : 'Vue tuile';
}
