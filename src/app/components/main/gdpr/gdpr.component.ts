/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { RgpdData } from "@libriciel/ls-composants";
import { NotificationsService } from '../../../services/notifications.service';
import { CommonMessages } from '../../../shared/common-messages';
import { ServerInfoService } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-gdpr',
  templateUrl: './gdpr.component.html',
  styleUrls: ['./gdpr.component.scss']
})
export class GdprComponent implements OnInit {


  gdprData: RgpdData;


  // <editor-fold desc="LifeCycle">


  constructor(private serverInfoService: ServerInfoService,
              public notificationsService: NotificationsService) {}


  ngOnInit(): void {
    this.serverInfoService
      .getGdprProperties()
      .subscribe(
        value => this.gdprData = value as RgpdData,
        error => this.notificationsService.showErrorMessage(CommonMessages.CANNOT_LOAD_SERVER_PARAMETERS, error)
      );
  }


  // </editor-fold desc="LifeCycle">


}
