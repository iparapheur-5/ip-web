/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, Inject, OnInit } from '@angular/core';
import { IpngService } from '../../../../services/ipng.service';
import { ExternalState } from '../../../../models/external-state.enum';
import { Task } from '../../../../models/task';
import { Folder } from '../../../../models/folder/folder';
import { IpngParams } from '../../../../models/ipng/ipng-params';
import { Deskbox } from '../../../../models/ipng/deskbox';
import { IpngEntity } from '../../../../models/ipng/ipng-entity';
import { IpngTaskData } from '../../../../models/ipng/ipng-task-data';
import { SimpleActionPopupComponent } from '../../folder-view/action-popups/simple-action-popup/simple-action-popup.component';
import { Action } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-ipng-form',
  templateUrl: './ipng-form.component.html',
  styleUrls: ['./ipng-form.component.scss']
})
export class IpngFormComponent implements OnInit {

  public readonly externalStateEnum = ExternalState;

  @Input() publicAnnotation: string;
  @Input() privateAnnotation: string;

  @Output() created = new EventEmitter();

  folder: Folder;
  task: Task;

  chosenDeskbox: Deskbox;
  availableDeskboxes: Deskbox[];

  availableEntities: IpngEntity[];
  selectedEntity: IpngEntity;


  constructor(private ipngService: IpngService,
              @Inject(SimpleActionPopupComponent.injectableFoldersKey) public folders: Folder[],
              @Inject(SimpleActionPopupComponent.injectableDeskIdKey) public deskId: string,
              @Inject(SimpleActionPopupComponent.injectableTenantKey) public tenantId: string,
              @Inject(SimpleActionPopupComponent.injectablePerformedActionKey) public performedAction: Action) { }


  ngOnInit(): void {
    this.folder = this.folders[0];
    this.task = this.folder.stepList[0];
    this.requestIpngEntities();
  }


  updateAvailableDeskboxList(searchEvt: { term: string, items: any[] }) {
    this.availableDeskboxes = this.selectedEntity.deskboxProfiles || [];
  }


  updateAvailableEntityList(searchEvt: { term: string, items: any[] }) {
    this.requestIpngEntities();
  }


  requestIpngEntities() {
    this.ipngService.listEntities(this.tenantId).subscribe(entityList => {
      this.availableEntities = entityList;
    });
  }


  executeIpng() {
    let proof = new IpngTaskData();
    proof.receiverDeskboxId = this.chosenDeskbox.id
    const ipngParams: IpngParams = new IpngParams();
    ipngParams.ipngProof = proof;
    ipngParams.publicAnnotation = this.publicAnnotation;
    ipngParams.privateAnnotation = this.privateAnnotation;

    this.ipngService.executeIpng(this.tenantId, this.deskId, this.folder.id, this.task.id, ipngParams)
      .subscribe(() => this.created.emit());
  }


}
