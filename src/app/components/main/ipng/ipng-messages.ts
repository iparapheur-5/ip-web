/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class IpngMessages {

  static readonly SEND_PROOF = 'Preuve d\'envoi';
  static readonly RECEIPT_PROOF = 'Preuve de réception';
  static readonly RESPONSE_PROOF = 'Preuve de réponse';

  static ipngFullProofHeader(entityId: string, deskboxId: string): string {
    return `Dossier envoyé à l'entité ${entityId}, sur la Deskbox ${deskboxId}`;
  }


  static showBusinessKey(businessKey: string): string {
    return `Business key : ${businessKey}`;
  }

  static showDate(date): string {
    return `Date : ${date}`;
  }

  static showHashForStep(state: string, hash: string): string {
    return `hash du dossier ${state} : ${hash}`;
  }

  static showProofId(proofId: string): string {
    return `ID de preuve sur la blockchain :  : ${proofId}`;
  }



}
