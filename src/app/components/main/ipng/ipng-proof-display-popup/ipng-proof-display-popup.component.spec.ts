/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IpngProofDisplayPopupComponent } from './ipng-proof-display-popup.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Folder } from '../../../../models/folder/folder';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { ToastrModule } from 'ngx-toastr';
import { NotificationsService } from '../../../../services/notifications.service';


describe('IpngProofDisplayPopupComponent', () => {


  let component: IpngProofDisplayPopupComponent;
  let fixture: ComponentFixture<IpngProofDisplayPopupComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ToastrModule.forRoot()],
      providers: [
        HttpClient, HttpHandler, NgbActiveModal, FolderService, NotificationsService,
        {provide: IpngProofDisplayPopupComponent.injectableFolderKey, useValue: new Folder()},
        {provide: IpngProofDisplayPopupComponent.injectableTenantIdKey, useValue: 'tenant01'},
        {provide: IpngProofDisplayPopupComponent.injectableDeskIdKey, useValue: 'desk01'}
      ],
      declarations: [IpngProofDisplayPopupComponent]
    })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(IpngProofDisplayPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // FIXME without this we are not testing anything
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });


});
