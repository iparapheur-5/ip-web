/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { CommonMessages } from '../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { Folder } from '../../../../models/folder/folder';
import { IpngFolderProofs } from '../../../../models/ipng/ipng-folder-proofs';
import { IpngProofWrap } from '../../../../models/ipng/ipng-wrapped-proof';
import { IpngMessages } from '../ipng-messages';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';

@Component({
  selector: 'app-ipng-proof-display-popup',
  templateUrl: './ipng-proof-display-popup.component.html',
  styleUrls: ['./ipng-proof-display-popup.component.scss']
})
export class IpngProofDisplayPopupComponent implements OnInit {

  public static readonly injectableFolderKey: string = 'folder';
  public static readonly injectableDeskIdKey: string = 'deskId';
  public static readonly injectableTenantIdKey: string = 'tenantId';

  readonly secondaryActionEnum = SecondaryAction;
  readonly commonMessages = CommonMessages;
  readonly messages = IpngMessages;
  readonly commonIcons = CommonIcons;


  ipngFolderProofList: IpngFolderProofs[];


  constructor(public activeModal: NgbActiveModal,
              public folderService: FolderService,
              @Inject(IpngProofDisplayPopupComponent.injectableFolderKey) public folder: Folder,
              @Inject(IpngProofDisplayPopupComponent.injectableDeskIdKey) public deskId: string,
              @Inject(IpngProofDisplayPopupComponent.injectableTenantIdKey) public tenantId: string) { }


  ngOnInit(): void {
    console.log('folder : ', this.folder);

    this.folderService.getIpngProofForFolder(this.tenantId, this.folder.id).subscribe(
      folderProofList => {
        console.log('ipngProof : ', folderProofList);
        this.ipngFolderProofList = folderProofList;
      });
  }


  getCleanedHash(proofWrap: IpngProofWrap): string {
    const rawHash = proofWrap.folderExchange.hash;
    const startIdx = rawHash.lastIndexOf("---");
    return rawHash.substring(startIdx + 3);
  }


}
