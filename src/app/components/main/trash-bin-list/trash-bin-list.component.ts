/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit } from '@angular/core';
import { faCaretUp, faCaretDown, faDownload } from '@fortawesome/free-solid-svg-icons';
import { Folder } from '../../../models/folder/folder';
import { NotificationsService } from '../../../services/notifications.service';
import { compareById, isNotNullOrUndefined } from '../../../utils/string-utils';
import { ActivatedRoute } from '@angular/router';
import { CommonMessages } from '../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { GlobalPopupService } from '../../../shared/service/global-popup.service';
import { LegacyUserService } from '../../../services/ip-core/legacy-user.service';
import { AdminTenantService, TenantSortBy, TenantService, FolderSortBy, AdminTrashBinService, ArchiveViewColumn, UserPreferencesDto, TenantRepresentation } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';
import { TasksMessages } from '../tasks/tasks-messages';
import { FileService } from '../../../services/file.service';
import { CrudOperation } from '../../../services/crud-operation';

@Component({
  selector: 'app-trash-bin-list',
  templateUrl: './trash-bin-list.component.html',
  styleUrls: ['./trash-bin-list.component.scss']
})
export class TrashBinListComponent implements AfterViewInit {

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly downloadIcon = faDownload;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly sortByEnum = FolderSortBy;
  readonly compareByIdFn = compareById;
  readonly columnsEnum = ArchiveViewColumn;


  tenantList: Array<TenantRepresentation> = [];
  selectedTenant: TenantRepresentation;

  deskId: string;
  state: string;
  sortBy: FolderSortBy = FolderSortBy.FolderName;
  asc = true;

  folderList: Folder[] = [];
  page = 1;
  pageSizeIndex = 1;
  total = 0;

  userPreferences: UserPreferencesDto;
  loading: boolean = false;
  colspan: number;
  currentUserAdmin: boolean;


  // <editor-fold desc="LifeCycle">


  constructor(private adminTrashBinService: AdminTrashBinService,
              private fileService: FileService,
              public notificationsService: NotificationsService,
              public adminTenantService: AdminTenantService,
              public tenantService: TenantService,
              public legacyUserService: LegacyUserService,
              public route: ActivatedRoute,
              public globalPopupService: GlobalPopupService) {}


  ngAfterViewInit() {

    this.currentUserAdmin = this.legacyUserService.isCurrentUserAdmin();
    this.userPreferences = this.route.snapshot.data["userPreferences"];
    this.colspan = this.userPreferences.archiveViewColumnList.length;
    this.pageSizeIndex = this.pageSizes.findIndex(item => item === this.userPreferences.taskViewDefaultPageSize) | 1;

    let tenantRetriever$ = this.legacyUserService.isCurrentUserSuperAdmin()
      ? this.adminTenantService.listTenantsAsAdmin(0, 10000, [TenantSortBy.Name + ',ASC'])
      : this.tenantService.listTenantsForUser(0, 10000, [TenantSortBy.Name + ',ASC'], true);

    tenantRetriever$
      .pipe(catchError(this.notificationsService.handleHttpError('getTenants')))
      .subscribe(tenantsRetrieved => {
        this.tenantList = tenantsRetrieved.content;
        this.selectedTenant = this.tenantList.length > 0 ? this.tenantList[0] : undefined;
        this.refreshFolderList();
      });
  }


  // </editor-fold desc="LifeCycle">


  refreshFolderList() {

    if (!this.selectedTenant) {
      return;
    }

    this.loading = true;
    const direction = (this.asc ? ",ASC" : ",DESC");
    const sortBy = [
      this.sortBy + direction,
      FolderSortBy.FolderName + direction,
      FolderSortBy.FolderId + direction,
    ];

    this.adminTrashBinService
      .listTrashBinFolders(this.selectedTenant.id, this.page - 1, this.getPageSize(this.pageSizeIndex), sortBy)
      .pipe(catchError(this.notificationsService.handleHttpError('listTrashBinFolders')))
      .subscribe(
        foldersRetrieved => {
          this.folderList = foldersRetrieved.content.map(representation => Object.assign(new Folder(), representation));
          this.total = foldersRetrieved.totalElements;
        },
        error => this.notificationsService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FOLDERS, error)
      )
      .add(() => this.loading = false);
  }


  onDownloadButtonClicked(folder: Folder) {
    this.fileService
      .downloadFileWithObservable(
        this.adminTrashBinService
          .downloadTrashBinFolderZip(this.selectedTenant.id, folder.id, 'body', true, {httpHeaderAccept: 'application/octet-stream'})
          .pipe(catchError(this.notificationsService.handleHttpError('downloadTrashBinFolderZip'))),
        folder.name + '.zip'
      )
      .subscribe(
        () => { /* Nothing to do */ },
        error => this.notificationsService.showErrorMessage(TasksMessages.ERROR_DOWNLOADING_FOLDER, error)
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  mapColumnToSortBy(column: ArchiveViewColumn): FolderSortBy {
    switch (column) {
      case ArchiveViewColumn.Name:
        return FolderSortBy.FolderName;
      case ArchiveViewColumn.Id:
        return FolderSortBy.FolderId;
      case ArchiveViewColumn.CreationDate:
        return FolderSortBy.CreationDate;
      default:
        return null;
    }
  }


  // <editor-fold desc="UI callbacks">


  onDeleteButtonClicked(folder: Folder) {
    this.globalPopupService
      .showDeleteValidationPopup(CommonMessages.foldersValidationPopupLabel([folder]), CommonMessages.foldersValidationPopupTitle([folder]))
      .then(
        () => this.adminTrashBinService
          .deleteTrashBinFolder(this.selectedTenant.id, folder.id)
          .pipe(catchError(this.notificationsService.handleHttpError('deleteTrashBinFolder')))
          .subscribe(
            () => {
              this.notificationsService.showCrudMessage(CrudOperation.Delete, folder);
              this.refreshFolderList();
            },
            error => this.notificationsService.showCrudMessage(CrudOperation.Delete, folder, error.message, false)
          ),
        () => {/* Dismissed */}
      );
  }


  onRowOrderClicked(column: ArchiveViewColumn) {
    let row = this.mapColumnToSortBy(column);
    if (isNotNullOrUndefined(row)) {
      this.asc = (row === this.sortBy) ? !this.asc : true;
      this.sortBy = row;
      this.refreshFolderList();
    }
  }


  // </editor-fold desc="UI callbacks">


}
