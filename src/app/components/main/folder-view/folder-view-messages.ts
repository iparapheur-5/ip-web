/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { DatePipe } from '@angular/common';
import { WorkflowActor } from '@libriciel/ls-composants/workflows';
import { DeskRepresentation } from '@libriciel/iparapheur-standard';


export class FolderViewMessages {

  static readonly ADD_SIGNATURE_DOCKET = 'Ajouter le bordereau de signature';
  static readonly ANNEXES = 'Annexes';

  static readonly VARIABLE_DESK = 'Bureau variable';
  static readonly ANNOTATION_POPUP_CONTENT_LABEL = 'Contenu';

  static readonly ADD_DETACHED_SIGNATURE = 'Ajouter une signature détachée';
  static readonly DETACHED_SIGNATURE = 'Signatures détachées';
  static readonly DELETE_DOCUMENT = 'Supprimer le document';
  static readonly REPLACE_DOCUMENT = 'Remplacer le document';
  static readonly DOWNLOAD_ORIGINAL_DOCUMENT = 'Télécharger le document original';

  static readonly ADD_DOCUMENT_SUCCESS = 'Document ajouté avec succès.';
  static readonly ADD_DOCUMENT_ERROR = `Erreur lors de l'ajout du document`;
  static readonly ADD_DETACHED_SIGNATURE_SUCCESS = 'Signature détachée créée avec succès.';
  static readonly ADD_DETACHED_SIGNATURE_ERROR = `Erreur lors de l'ajout de la signature détachée au document.`;
  static readonly REPLACE_DOCUMENT_SUCCESS = 'Document remplacé avec succès.';
  static readonly REPLACE_DOCUMENT_ERROR = `Erreur lors du remplacement du document`;
  static readonly REMOVE_DETACHED_SIGNATURE_SUCCESS = 'Signature détachée supprimée avec succès.';
  static readonly REMOVE_DETACHED_SIGNATURE_ERROR = 'Impossible de supprimer la signature détachée.';
  static readonly DELETE_DOCUMENT_SUCCESS = 'Document supprimé avec succès.';
  static readonly DELETE_DOCUMENT_ERROR = 'Erreur lors de la suppression du document';

  static readonly DOWNLOAD_PRINT_DOC_SUCCESS = `Téléchargement du document d'impression terminé`;
  static readonly DOWNLOAD_PRINT_DOC_ERROR = `Ereur lors du téléchargement du document d'impression.`;
  static readonly CANNOT_DELETE_THE_LAST_DOCUMENT = `Impossible de supprimer le dernier document du dossier. Ajoutez-en un avant.`;

  static readonly ERROR_LOADING_WORKFLOW = 'Erreur lors du chargement du circuit en cours pour le dossier';
  static readonly AT = 'Le ';

  static readonly SIGNATURE_PLACEMENT_POPUP_CONTENT = 'Cet emplacement sera utilisé pour placer la prochaine signature ou cachet.';

  static readonly ANNOTATION_POPUP_TITLE_SIGNATURE_PLACEMENT = 'Position de la signature';
  static readonly ANNOTATION_POPUP_TITLE_ANNOTATION = 'Annotation';

  static readonly PES_DOCKET_REQUIRED = "Le document principal étant au format XML, seul le bordereau sera imprimé."

  static readonly FORCE_EXTERNAL_SIGNATURE = "Terminer l'action maintenant"

  static taskStatus = (stateString: string) => ` (Statut : ${stateString})`;

  static multipleValidatorsStatus = (deskCount: number, pending: number, current: boolean) => current
    ? `${deskCount - pending}/${deskCount} bureaux ont validé cette étape`
    : `${deskCount} bureaux sont valideurs sur cette étape`;

  static validatorLabel = (validator: WorkflowActor) => validator.id == 'i_Parapheur_internal_automatic_task'
    ? `Action automatique`
    : `Par ${validator.firstname} ${validator.lastname}`;

  static delegatedByLabel = (desk: DeskRepresentation) => `En l'absence du bureau ${desk.name}`;

  static validatorsLabel = (validators: WorkflowActor[]) => `Par ${validators.length} utilisateurs`;

  static dateLabel = (step: any) => `Le ${new DatePipe('fr-FR').transform(step.completedDate, 'medium')}`;

  static detachedSignatureDeleteValidationPopup = (name: string) => `Êtes-vous sûr de vouloir supprimer la signature détachée "${name}" ?`;

  static deleteDocumentValidationPopup = (name: string) => `Êtes-vous sûr de vouloir supprimer le document "${name}" ?`;

  static errorDownloadingFile = (isDetachedSignature: boolean, name: string) =>
    `Une erreur est survenue durant le téléchargement 
    ${isDetachedSignature ? 'de la signature détachée' : 'du document'} 
    "${name}".`;


}
