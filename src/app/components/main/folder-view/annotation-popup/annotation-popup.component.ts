/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../shared/common-messages';
import { StickyNote } from '../../../../models/annotation/sticky-note';
import { SignaturePlacement } from '../../../../models/annotation/signature-placement';
import { FolderViewMessages } from '../folder-view-messages';
import { User } from '../../../../models/auth/user';

@Component({
  selector: 'app-annotation-popup',
  templateUrl: './annotation-popup.component.html',
  styleUrls: ['./annotation-popup.component.scss']
})
export class AnnotationPopupComponent implements OnInit {

  public static readonly injectableAnnotationKey: string = 'annotation';
  public static readonly injectableTypeKey: string = 'type';
  public static readonly injectableUserKey: string = 'user';

  readonly messages = FolderViewMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly style = Style;
  readonly checkIcon = faCheck;

  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              @Inject(AnnotationPopupComponent.injectableAnnotationKey) public targetAnnotation: StickyNote | SignaturePlacement,
              @Inject(AnnotationPopupComponent.injectableTypeKey) public type: string,
              @Inject(AnnotationPopupComponent.injectableUserKey) public user: User) {
    console.log('AnnotationPopupComponent constructor : ' + JSON.stringify(targetAnnotation));
  }


  ngOnInit(): void {
    this.targetAnnotation instanceof StickyNote
      ? this.targetAnnotation.content = this.targetAnnotation.content || ""
      : this.targetAnnotation.signatureNumber = this.targetAnnotation.signatureNumber || 0;
  }


  // </editor-fold desc="LifeCycle">


  perform() {
    this.activeModal.close({value: 'success', annotation: this.targetAnnotation});
  }


  getTitle(): string {
    return this.type === 'signaturePlacementCreated' || this.type === 'signaturePlacementClick'
      ? FolderViewMessages.ANNOTATION_POPUP_TITLE_SIGNATURE_PLACEMENT
      : FolderViewMessages.ANNOTATION_POPUP_TITLE_ANNOTATION
  }


  isSignaturePlacement() {
    return this.targetAnnotation instanceof SignaturePlacement
  }


}
