/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { getOrDefault } from '../../../../../utils/string-utils';
import { Folder } from '../../../../../models/folder/folder';
import { WorkflowService } from '../../../../../services/ip-core/workflow.service';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { Task } from 'src/app/models/task';
import { ActionPopupMessages } from '../action-popup-messages';
import { NotificationsService } from '../../../../../services/notifications.service';
import { combineLatest, Observable } from 'rxjs';
import { TypologyService as StandardTypologyService, SignatureFormat, Action, SubtypeDto, TypeDto, PageTypeRepresentation, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { WorkflowDefinition } from '../../../../../models/workflow-definition';
import { WorkflowUtils } from '../../../../../shared/utils/workflow-utils';
import { DeskService } from '../../../../../services/ip-core/desk.service';
import { CustomMap } from '../../../../../shared/models/custom-types';
import { DeskDto, PageSubtypeRepresentation } from '@libriciel/iparapheur-provisioning';
import { TypologyService } from '../../../../../services/ip-core/typology.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-chain-popup',
  templateUrl: './chain-popup.component.html',
  styleUrls: ['./chain-popup.component.scss']
})
export class ChainPopupComponent {

  public static readonly INJECTABLE_FOLDER_KEY: string = 'folder';
  public static readonly INJECTABLE_DESK_KEY: string = 'desk';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = ActionPopupMessages;
  readonly getOrDefaultFn = getOrDefault;
  readonly actionEnum = Action;

  isTypologyCompatible = true;
  isProcessing = false;
  task: Task;
  selectedType: TypeDto;
  selectedSubtype: SubtypeDto;
  workflowDefinition: WorkflowDefinition;
  variableDesksIds: CustomMap = {};
  variableDeskStepCount: number = 0;

  retrieveTypesFn = (page: number, pageSize: number) => this.retrieveTypes(page, pageSize);
  retrieveSubtypesFn = (page: number, pageSize: number) => this.retrieveSubtypes(page, pageSize);

  // </editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              public workflowService: WorkflowService,
              public typologyService: TypologyService,
              public standardTypologyService: StandardTypologyService,
              public deskService: DeskService,
              public notificationService: NotificationsService,
              @Inject(ChainPopupComponent.INJECTABLE_FOLDER_KEY) public folder: Folder,
              @Inject(ChainPopupComponent.INJECTABLE_DESK_KEY) public desk: DeskDto,
              @Inject(ChainPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string) {
    this.task = this.folder.stepList.find(t => t.action == Action.Archive || t.action == Action.Chain);
  }


  // </editor-fold desc="LifeCycle">


  onTypeSelectionChanged(newType: TypeRepresentation) {
    this.workflowDefinition = null;
    this.selectedSubtype = null;


    this.standardTypologyService.getType(this.tenantId, newType.id)
      .subscribe(type => {
          this.selectedType = type;
          const newSignatureFormat = this.selectedType?.signatureFormat;
          const isSameFormat = this.folder.type.signatureFormat === newSignatureFormat;
          const isWildcardFormat = [SignatureFormat.Auto, SignatureFormat.XadesDetached, SignatureFormat.Pkcs7].includes(newSignatureFormat);
          this.isTypologyCompatible = isSameFormat || isWildcardFormat;
        },
        error => this.notificationService.showErrorMessage(`Erreur lors de la récupération du type ${newType.name}`, error.message));

  }


  onSubtypeSelectionChanged(partialSubtype: SubtypeDto) {
    this.workflowDefinition = null;
    this.selectedSubtype = null;
    this.variableDesksIds = {};
    this.variableDeskStepCount = 0;

    combineLatest([
      this.standardTypologyService.getSubtype(this.tenantId, this.selectedType.id, partialSubtype.id),
      this.workflowService.getWorkflowDefinitionByKey(this.tenantId, this.desk.id, partialSubtype.validationWorkflowId)
    ])
      .subscribe(
        ([subtype, workflowDef]) => {
          this.selectedSubtype = subtype;
          this.workflowDefinition = workflowDef;
          this.variableDeskStepCount = WorkflowUtils.countVariableDeskStep(workflowDef);
        },
        error => {
          this.notificationService.showErrorMessage(this.messages.CANNOT_RETRIEVE_SUBTYPE, error.message);
        }
      );
  }

  perform() {
    let newMetadata = new Map<string, string>();
    this.selectedSubtype.subtypeMetadataList
      .forEach(subtypeMetadata => newMetadata[subtypeMetadata.metadata.id] = this.folder.metadata[subtypeMetadata.metadata.id]);

    this.isProcessing = true;

    return this.workflowService
      .performChain(this.tenantId, this.folder, this.task, this.desk.id, this.selectedType.id, this.selectedSubtype.id, this.variableDesksIds, newMetadata)
      .subscribe(
        () => {
          this.notificationService.showSuccessMessage(ActionPopupMessages.actionFolderSuccessMessage(Action.Chain, this.folder));
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error => {
          this.notificationService.showErrorMessage(ActionPopupMessages.actionFolderErrorMessage(Action.Chain, this.folder), error.message);
        }
      )
      .add(() => this.isProcessing = false);
  }


  isFormValid(): boolean {
    return !!this.selectedSubtype
      && this.isTypologyCompatible
      && Object.values(this.variableDesksIds).filter(elem => !!elem).length === this.variableDeskStepCount;

  }

  retrieveTypes(page: number, pageSize: number): Observable<PageTypeRepresentation> {
    console.debug(`retrieve creation allowed types : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getCreationAllowedTypes(this.tenantId, this.desk.id)
      .pipe(map(page => {
        let result: PageTypeRepresentation = {
          content: page.data
        };
        return result;
      }));
  }

  retrieveSubtypes(page: number, pageSize: number): Observable<PageSubtypeRepresentation> {
    console.debug(`retrieve creation allowed subtypes : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getCreationAllowedSubtypes(this.tenantId, this.desk.id, this.selectedType?.id)
      .pipe(map(page => {
        let result: PageSubtypeRepresentation = {
          content: page.data
        };
        return result;
      }));
  }


}
