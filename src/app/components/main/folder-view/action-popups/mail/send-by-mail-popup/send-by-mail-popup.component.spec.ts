/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { SendByMailPopupComponent } from './send-by-mail-popup.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Folder } from '../../../../../../models/folder/folder';
import { SecondaryAction } from '../../../../../../shared/models/secondary-action.enum';


describe('SendByMailPopupComponent', () => {


  let component: SendByMailPopupComponent;
  let fixture: ComponentFixture<SendByMailPopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: SendByMailPopupComponent.INJECTABLE_CURRENT_ACTION_KEY, useValue: SecondaryAction.Mail},
          {provide: SendByMailPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [new Folder()]},
          {provide: SendByMailPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant_01'},
          {provide: SendByMailPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: 'desk_01'},
        ],
        declarations: [SendByMailPopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(SendByMailPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });


});
