/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { ActionPopupMessages } from '../../action-popup-messages';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FolderService } from '../../../../../../services/ip-core/folder.service';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { Task } from '../../../../../../models/task';
import { SecureMailService } from '../../../../../../services/ip-core/secure-mail.service';
import { SecureMailDocument } from '../../../../../../models/securemail/secure-mail-document';
import { faEye, faEyeSlash, faRedo } from '@fortawesome/free-solid-svg-icons';
import { CrudOperation } from '../../../../../../services/crud-operation';
import { Folder } from '../../../../../../models/folder/folder';

@Component({
  selector: 'app-secure-mail-status-popup',
  templateUrl: './secure-mail-status-popup.component.html',
  styleUrls: ['./secure-mail-status-popup.component.scss']
})
export class SecureMailStatusPopupComponent implements OnInit {

  public static readonly INJECTABLE_FOLDER_KEY = 'folder';
  public static readonly INJECTABLE_DESK_ID_KEY = 'deskId';
  public static readonly INJECTABLE_TENANT_KEY = 'tenant';
  public readonly eyeIcon = faEye;
  public readonly eyeSlashIcon = faEyeSlash;
  public readonly refreshIcon = faRedo;

  public readonly commonIcons = CommonIcons;
  public readonly messages = ActionPopupMessages;
  public readonly commonMessages = CommonMessages;

  currentStep: Task;
  pastellDocument: SecureMailDocument;
  isProcessing: boolean = false;


  constructor(public activeModal: NgbActiveModal,
              public folderService: FolderService,
              public notificationService: NotificationsService,
              public secureMailService: SecureMailService,
              @Inject(SecureMailStatusPopupComponent.INJECTABLE_FOLDER_KEY) public folder: Folder,
              @Inject(SecureMailStatusPopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string,
              @Inject(SecureMailStatusPopupComponent.INJECTABLE_TENANT_KEY) public tenantId: string) { }


  ngOnInit(): void {
    this.currentStep = this.folder.stepList[0];
    this.findDocument();
  }


  findDocument(): void {
    this.isProcessing = true;
    this.secureMailService.findDocument(this.tenantId, this.folder.subtype.secureMailServerId, this.folder.id)
      .subscribe(
        data => this.pastellDocument = data,
        error => this.notificationService.showCrudMessage(CrudOperation.Read, SecureMailDocument, error))
      .add(() => this.isProcessing = false);
  }


}
