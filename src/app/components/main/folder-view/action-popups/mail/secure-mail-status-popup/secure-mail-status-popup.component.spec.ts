/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureMailStatusPopupComponent } from './secure-mail-status-popup.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { KeycloakService } from 'keycloak-angular';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Folder } from '../../../../../../models/folder/folder';
import { ExternalStatePipe } from '../../../../../../utils/external-signature-state.pipe';
import { Task } from '../../../../../../models/task';

describe('SecureMailStatusPopupComponent', () => {
  let component: SecureMailStatusPopupComponent;
  let fixture: ComponentFixture<SecureMailStatusPopupComponent>;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, KeycloakService, NgbActiveModal,
          {
            provide: SecureMailStatusPopupComponent.INJECTABLE_FOLDER_KEY, useValue: {
              stepList: [new Task()],
              subtype: {}
            } as Folder
          },
          {provide: SecureMailStatusPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: 'deskId'},
          {provide: SecureMailStatusPopupComponent.INJECTABLE_TENANT_KEY, useValue: 'tenantId'}

        ],
        declarations: [ExternalStatePipe, SecureMailStatusPopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(SecureMailStatusPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
