/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FolderService } from '../../../../../services/ip-core/folder.service';
import { Folder } from '../../../../../models/folder/folder';
import { Document } from '../../../../../models/document';
import { FolderViewMessages } from '../../folder-view-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { IconSize, CommonIcons } from '@libriciel/ls-composants';
import { faPrint } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../services/notifications.service';
import { SignatureFormat, Action } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../../shared/models/secondary-action.enum';
import { FileService } from '../../../../../services/file.service';

@Component({
  selector: 'app-print-popup',
  templateUrl: './print-popup.component.html',
  styleUrls: ['./print-popup.component.scss']
})
export class PrintPopupComponent implements OnInit {


  public static readonly INJECTABLE_FOLDER_KEY: string = 'folder';
  public static readonly INJECTABLE_DESK_ID_KEY: string = 'deskId';
  public static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenantId';

  readonly iconSize = IconSize;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly actionEnum = Action;
  readonly secondaryActionEnum = SecondaryAction;
  readonly printIcon = faPrint;


  checkedAnnexes: string[] = [];
  includedAnnexes: Document[] = [];
  includeDocket = true;
  downloadInProgress = false;
  downloadUrl = '';
  isPes: boolean = false;


  // </editor-fold desc="LifeCycle">


  constructor(private fileService: FileService,
              public activeModal: NgbActiveModal,
              private folderService: FolderService,
              private notificationService: NotificationsService,
              @Inject(PrintPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(PrintPopupComponent.INJECTABLE_FOLDER_KEY) public folder: Folder,
              @Inject(PrintPopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string) {
  }


  ngOnInit(): void {
    this.refreshDownloadUrl();
    this.filterAnnexes();

    if (this.folder?.subtype?.annexeIncluded) {
      this.checkedAnnexes = this.folder.documentList.filter(document => !document.isMainDocument).map(document => document.id);
      this.refreshDownloadUrl();
    }

    this.isPes = this.folder.type?.signatureFormat === SignatureFormat.PesV2;
  }


  // </editor-fold desc="LifeCycle">


  onAnnexeCheckboxClicked(annexe: Document) {
    if (this.checkedAnnexes.includes(annexe.id)) {
      this.checkedAnnexes = this.checkedAnnexes.filter(doc => doc !== annexe.id);
    } else {
      this.checkedAnnexes.push(annexe.id);
    }
    this.refreshDownloadUrl();
  }

  onDocketCheckboxClicked() {
    this.refreshDownloadUrl();
  }


  filterAnnexes(): void {
    this.includedAnnexes = this.folder.documentList.filter(doc => !doc.isMainDocument)
  }


  refreshDownloadUrl() {
    this.downloadUrl = this.folderService.getPrintUrl(this.tenantId, this.deskId, this.folder, this.includeDocket, this.checkedAnnexes);
  }


  perform() {
    this.downloadInProgress = true;

    // TODO : This should not force the target file name,
    //  we shall parse the response header to get the real parameterized one.
    this.fileService
      .downloadFileWithUrl(this.downloadUrl, this.folder.name + '_impression.pdf')
      .subscribe(
        () => console.log(this.messages.DOWNLOAD_PRINT_DOC_SUCCESS),
        error => this.notificationService.showErrorMessage(this.messages.DOWNLOAD_PRINT_DOC_ERROR, error)
      )
      .add(() => this.downloadInProgress = false);
  }


}
