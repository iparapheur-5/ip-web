/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPopupComponent } from './print-popup.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FolderService } from '../../../../../services/ip-core/folder.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NotificationsService } from '../../../../../services/notifications.service';
import { ToastrModule } from 'ngx-toastr';
import { Folder } from '../../../../../models/folder/folder';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { LsComposantsModule } from '@libriciel/ls-composants';


describe('PrintPopupComponent', () => {

  let component: PrintPopupComponent;
  let fixture: ComponentFixture<PrintPopupComponent>;


  beforeEach(async () => {

    const tenant = {id: 'tenantId01'};

    const folder = new Folder();
    folder.id = 'folderId01';
    folder.documentList = [];

    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          NgbActiveModal, FolderService, HttpClient, HttpHandler, NotificationsService, LsComposantsModule,
          {provide: PrintPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: tenant.id},
          {provide: PrintPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folder},
          {provide: PrintPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: 'deskId01'},
        ],
        declarations: [PrintPopupComponent, ActionNamePipe]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(PrintPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
