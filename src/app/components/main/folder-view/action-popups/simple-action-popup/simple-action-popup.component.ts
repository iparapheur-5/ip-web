/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, AfterViewInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Folder } from '../../../../../models/folder/folder';
import { WorkflowService as LegacyWorkflowService } from '../../../../../services/ip-core/workflow.service';
import { FolderService } from '../../../../../services/ip-core/folder.service';
import { DeskService } from '../../../../../services/ip-core/desk.service';
import { Observable, combineLatest, concat } from 'rxjs';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons, Style, LibersignCertificate } from '@libriciel/ls-composants';
import { catchError, map, } from 'rxjs/operators';
import { ActionPopupMessages } from '../action-popup-messages';
import { FolderDataToSign } from '../../../../../models/signature/folder-data-to-sign';
import { SimpleTaskParams } from '../../../../../models/simple-task-params';
import { SignatureTaskParams } from '../../../../../models/signature-task-params';
import { DataToSignHolder } from '../../../../../models/signature/data-to-sign-holder';
import { Action, WorkflowService, State } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../../shared/models/secondary-action.enum';
import { CONFIG } from '../../../../../shared/config/config';
import { FortifyCertificateData } from '../../../../../models/fortify-certificate-data';
import { LiberSignCheckComponent, LiberExtensionSignStatus } from '../../../../settings/liber-sign-check/liber-sign-check.component';
import { DeviceDetectorService } from 'ngx-device-detector';
import { GLOBAL_SETTINGS } from '../../../../../shared/models/global-settings';
import { Task } from '../../../../../models/task';

type SubscriptionResult = {
  folderId: string,
  isSignature: boolean,
  error: Error
}

@Component({
  selector: 'app-simple-action-popup',
  templateUrl: './simple-action-popup.component.html',
  styleUrls: ['./simple-action-popup.component.scss']
})
export class SimpleActionPopupComponent implements AfterViewInit {


  // FIXME internal_workflow metadata should be parsed beforehand
  public static readonly injectableFoldersKey = 'folders';
  public static readonly injectableDeskIdKey = 'deskId';
  public static readonly injectablePerformedActionKey = 'performedAction';
  public static readonly injectableTenantKey = 'tenant';

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly styleEnum = Style;
  readonly actionEnum = Action;
  readonly actionsWithoutAnnotations: (Action | SecondaryAction)[] = [Action.Archive, Action.Bypass, Action.Undo, Action.Recycle];
  readonly actionsWithCustomSignatureField: (Action | SecondaryAction)[] = [Action.Signature, Action.Seal];

  publicAnnotation: string = "";
  privateAnnotation: string = "";
  showAnnotations: boolean;
  customSignatureField: string = "";
  showCustomSignatureField: boolean;
  action: Action | SecondaryAction;
  isProcessing = false;

  libersignUpdateUrl: string = CONFIG.LIBERSIGN_UPDATE_URL;
  libersignAppletUrl: string = '/applets/';

  // Signatures
  hasSignature = false;
  useLibersign: boolean;
  fortifyCertificate: FortifyCertificateData;
  libersignCertificate: LibersignCertificate;

  subscriptionResults: SubscriptionResult[] = [];

  foldersDataToSign: FolderDataToSign[] = [];
  dataToSign: string[] = [];

  // Progress bar
  numberOfTasks: number = 0;
  numberOfSuccessfulTasks: number = 0;
  numberOfFailedTasks: number = 0;
  successfulTasksProgressBarPercentage: number = 0;
  failedTasksProgressBarPercentage: number = 0;

  // metadata
  validationMetadataFormValid: boolean = false;
  rejectionMetadataFormValid: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(private deviceService: DeviceDetectorService,
              private folderService: FolderService,
              private deskService: DeskService,
              private legacyWorkflowService: LegacyWorkflowService,
              private workflowService: WorkflowService,
              private notificationsService: NotificationsService,
              public activeModal: NgbActiveModal,
              @Inject(SimpleActionPopupComponent.injectableFoldersKey) public folders: Folder[],
              @Inject(SimpleActionPopupComponent.injectableDeskIdKey) public deskId: string,
              @Inject(SimpleActionPopupComponent.injectableTenantKey) public tenantId: string,
              @Inject(SimpleActionPopupComponent.injectablePerformedActionKey) public performedAction: Action | SecondaryAction) {

    const isSignatureAction = performedAction === Action.Signature;
    const isStackedValidation = performedAction === SecondaryAction.StackedValidation;
    const doFolderContainsSignature = folders
      .map(folder => this.getCurrentTask(folder).action)
      .some(action => action == Action.Signature);

    this.hasSignature = isSignatureAction || (isStackedValidation && doFolderContainsSignature);
  }


  ngAfterViewInit(): void {
    this.action = this.performedAction;
    this.useLibersign = GLOBAL_SETTINGS.admin?.enableFortify === false || this.isLibersignUp();
    this.showAnnotations = !this.actionsWithoutAnnotations.includes(this.performedAction);
    this.showCustomSignatureField = GLOBAL_SETTINGS.admin.showCustomSignatureField && this.actionsWithCustomSignatureField.includes(this.performedAction);
    this.numberOfTasks = this.folders.length;
  }


  // </editor-fold desc="LifeCycle">


  onCertificateChosen(certificateData: FortifyCertificateData | LibersignCertificate): void {
    !!this.useLibersign
      ? this.libersignCertificate = certificateData as LibersignCertificate
      : this.fortifyCertificate = certificateData as FortifyCertificateData;

    this.performAll();
  }


  performAll(): void {
    let observable$: Observable<any> = (this.performedAction === SecondaryAction.StackedValidation)
      ? concat(
        combineLatest(this.getTaskRequests(Action.SecondOpinion)),
        combineLatest(this.getTaskRequests(Action.Visa)),
        combineLatest(this.getTaskRequests(Action.Seal)),
        combineLatest(this.getTaskRequests(Action.Signature))
      )
      : combineLatest(this.getTaskRequests(this.performedAction));

    this.isProcessing = true;

    observable$
      .subscribe(data => {
        if (!this.isThereSignatureSubscriptionResults())
          return;

        this.foldersDataToSign.push(...data);

        if (this.setNextSignature())
          this.showNotificationsAndResetIfNeeded();
      })
      .add(() => {
        if (!this.isThereSignatureSubscriptionResults() || this.areAllTasksOnError()) {
          this.isProcessing = false;
          this.showNotificationsAndResetIfNeeded();
        }

        if (!this.isThereSignatureSubscriptionResults() && this.isThereNoErrorsOnTasks())
          this.activeModal.close({value: CommonMessages.ACTION_RESULT_OK});
      });
  }


  onSignatureResult(signatures: string[]): void {
    console.log('onSignatureResult - signatures : ', signatures);

    const isCertPresent: boolean = this.useLibersign
      ? !!this.libersignCertificate
      : !!this.fortifyCertificate.certificate;

    if (!signatures || signatures.length === 0 || !isCertPresent || !this.dataToSign) {
      console.log('finalizeSignature  missing mandatory param, abort');
      return;
    }

    let wasLastSignature: boolean;
    const currentFolder: Folder = this.folders.filter(f => this.getCurrentTask(f).action === Action.Signature)[this.getCurrentSignatureIndex()];

    this.addRetrievedSignatures(signatures)
      .subscribe(() => {
          this.setNotificationsAndSubscriptionResult(currentFolder);
          wasLastSignature = this.setNextSignature('done');
          this.updateProgressBar(true);
        },
        response => {
          this.setNotificationsAndSubscriptionResult(currentFolder, response?.error || response);
          wasLastSignature = this.setNextSignature('failed');
          this.updateProgressBar(false);
        }
      )
      .add(() => {
        if (!wasLastSignature) return;

        this.isProcessing = false;
        this.showNotificationsAndResetIfNeeded();

        if (this.isThereNoErrorsOnTasks())
          this.activeModal.close({value: CommonMessages.ACTION_RESULT_OK});
      });
  }


  finalizeAction($event: any): void {
    console.log(`finalize ${this.performedAction.toString()} - task result : ${JSON.stringify($event)}`);
    this.activeModal.close({value: CommonMessages.ACTION_RESULT_OK});
  }


  isFormValid(): boolean {
    const isRejectionFormValid: boolean = this.performedAction === Action.Reject && this.publicAnnotation.length >= 3 && this.rejectionMetadataFormValid;
    const isValidationFormValid: boolean = this.performedAction !== Action.Reject && this.validationMetadataFormValid;
    return isRejectionFormValid || isValidationFormValid;
  }


  shouldDisplayStandardActionButton(): boolean {
    return !([Action.Signature, Action.ExternalSignature, Action.SecureMail, Action.Ipng] as (Action | SecondaryAction)[])
      .includes(this.performedAction);
  }


  isPublicAnnotationMandatory(): boolean {
    return this.performedAction === Action.Reject;
  }


  getTaskRequests(action: Action | SecondaryAction): Observable<any>[] {
    return this.folders
      .filter(folder => this.performedAction === SecondaryAction.StackedValidation ? this.getCurrentTask(folder).action === action : true)
      .map(folder => this.getSingleTaskRequest(folder)
        .pipe(map(data => {
          this.setNotificationsAndSubscriptionResult(folder);
          return data;
        }))
        .pipe(catchError(response => {
          // An empty observable is returned when an error is thrown
          // We do not want to show any notification here, we just update the progressbar with "setNotificationsAndSubscriptionResult"
          this.setNotificationsAndSubscriptionResult(folder, response?.error || response);
          return new Observable<any>(observer => observer.complete());
        }))
      );
  }


  getSingleTaskRequest(folder: Folder): Observable<any> {

    const currentTask = this.getCurrentTask(folder);
    const actionToExecute = this.performedAction === SecondaryAction.StackedValidation ? currentTask.action : this.performedAction;

    switch (actionToExecute) {

      case Action.Start:
        return this.legacyWorkflowService.startFolder(
          this.tenantId,
          this.deskId,
          folder.id,
          this.getSimpleTaskParams(folder)
        );

      case Action.Archive:
        return this.legacyWorkflowService.sendToTrashBin(this.tenantId, this.deskId, folder.id);

      case Action.Signature:
        return this.getSignatureDataRequest(folder);

      case Action.SecondOpinion :
        return this.workflowService
          .secondOpinion(this.tenantId, this.deskId, folder.id, currentTask.id, this.getSimpleTaskParams(folder))
          .pipe(catchError(this.notificationsService.handleHttpError('performSecondOpinion')));

      case Action.Visa :
        return this.workflowService
          .visa(this.tenantId, this.deskId, folder.id, currentTask.id, this.getSimpleTaskParams(folder))
          .pipe(catchError(this.notificationsService.handleHttpError('performVisa')));

      case Action.Seal :
        return this.workflowService
          .seal(this.tenantId, this.deskId, folder.id, currentTask.id, this.getSimpleTaskParams(folder))
          .pipe(catchError(this.notificationsService.handleHttpError('performSeal')));

      default:
        return this.legacyWorkflowService.performTask(this.tenantId, folder, currentTask, this.deskId, actionToExecute, this.getSimpleTaskParams(folder));
    }
  }


  getSignatureDataRequest(folder: Folder): Observable<FolderDataToSign> {
    const cert: string = this.useLibersign
      ? this.libersignCertificate?.PUBKEY
      : this.fortifyCertificate?.certBase64;

    const urlSafePublicKeyBase64 = cert
      .replace(new RegExp('\\+', 'g'), '-')
      .replace(new RegExp('/', 'g'), '_');

    console.log('prepareDataToSign for folder "' + folder.name + '", urlsafe pubkey : ', urlSafePublicKeyBase64);

    return this.folderService.getDataToSign(this.tenantId, this.deskId, folder.id, urlSafePublicKeyBase64);
  }


  addRetrievedSignatures(signatures: string[]): Observable<void> {

    console.log('addRetrievedSignatures - signatures : ', signatures);

    const currentSignatureFolderIndex = this.getCurrentSignatureIndex();
    const currentFolderDataToSign: FolderDataToSign = this.foldersDataToSign[currentSignatureFolderIndex];

    const currentFolder: Folder = this.folders.find(f => f.id === currentFolderDataToSign.folderId);
    let currentTask: Task = this.getCurrentTask(currentFolder);
    const dataToSignCount: number = currentFolderDataToSign.dataToSignHolderList
      .map(holder => holder.dataToSignList.length)
      .reduce((a, b) => a + b, 0); // just sum the length

    if (dataToSignCount !== signatures.length) {
      console.warn("the length of the list of prepared signature data differ from the length of the list of signature : " +
        "prepared : " + dataToSignCount + ", " +
        "signed : " + signatures.length);
      console.warn("we're still adding the signatures we can, but result might not be good");
    }

    currentTask.publicAnnotation = this.publicAnnotation;
    currentTask.privateAnnotation = this.privateAnnotation;

    // For some reasons, Libersign returns the public key with the signature.
    const cert: string = this.useLibersign
      ? this.libersignCertificate?.PUBKEY
      : this.fortifyCertificate?.certBase64;

    for (let i = 0 ; i < signatures.length ; i++) {
      signatures[i] = signatures[i].replace(';' + cert, '');
    }

    // the signatures should be in correct order, we redispatch them in each dataToSignHolder object
    let signatureIdx = 0;
    currentFolderDataToSign.dataToSignHolderList.forEach(holder => {
      for (let i = 0 ; (i < holder.dataToSignList.length) && (signatureIdx < signatures.length) ; ++i, ++signatureIdx) {
        holder.dataToSignList[i].signatureValue = signatures[signatureIdx];
      }
    });

    const signatureParams = this.getSignatureTaskParams(currentFolder, cert, currentFolderDataToSign.dataToSignHolderList);

    return this.workflowService.signature(this.tenantId, this.deskId, currentFolder.id, currentTask.id, signatureParams);
  }


  setNotificationsAndSubscriptionResult(folder: Folder, error?: Error): void {
    console.log('setNotificationsAndSubscriptionResult - got result for folder ', folder.id);

    const existingFolderIndex = this.subscriptionResults.findIndex(sub => sub.folderId === folder.id);

    if (existingFolderIndex !== -1) {
      this.subscriptionResults[existingFolderIndex].error = error;
      return;
    }
    let currentTask: Task = this.getCurrentTask(folder);
    let subscriptionResult: SubscriptionResult = {
      folderId: folder.id,
      isSignature: currentTask.action === Action.Signature && this.performedAction === Action.Signature,
      error: error
    };

    this.subscriptionResults.push(subscriptionResult);


    const isNotSignature: boolean = !(currentTask.action === Action.Signature && this.performedAction === Action.Signature);
    if (!!error) this.updateProgressBar(false);
    else if (isNotSignature) this.updateProgressBar(true);
  }


  refreshProgressBar(): void {
    this.successfulTasksProgressBarPercentage = Math.round((this.numberOfSuccessfulTasks / this.numberOfTasks) * 100)
    this.failedTasksProgressBarPercentage = Math.round((this.numberOfFailedTasks / this.numberOfTasks) * 100)
  }


  updateProgressBar(success: boolean): void {
    success ? this.numberOfSuccessfulTasks += 1 : this.numberOfFailedTasks += 1;
    this.refreshProgressBar();
  }


  getCurrentSignatureIndex(): number {
    return this.foldersDataToSign.map(data => data.state).findIndex(value => value === "current");
  }


  getNextSignatureIndex(): number {
    return this.foldersDataToSign.map(data => data.state).findIndex(value => value === "unsigned");
  }


  isThereSignatureSubscriptionResults(): boolean {
    return this.subscriptionResults.map(sub => sub.isSignature).filter(isSig => isSig === true).length > 0;
  }


  areAllTasksOnError(): boolean {
    return this.subscriptionResults.every(sr => !!sr.error);
  }


  isThereNoErrorsOnTasks(): boolean {
    return this.subscriptionResults.every(sr => sr.error === undefined);
  }


  /**
   *
   * @Return true if that was the last data to sign, false if that was not.
   *
   * @param state, the state to give to the current data
   */
  setNextSignature(state?: string): boolean {

    let currentDataToSignHolder: FolderDataToSign = this.foldersDataToSign[this.getCurrentSignatureIndex()];
    let nextDataToSignHolder: FolderDataToSign = this.foldersDataToSign[this.getNextSignatureIndex()];
    if (!!currentDataToSignHolder) {
      currentDataToSignHolder.state = state;
    }

    if (!!nextDataToSignHolder) {
      nextDataToSignHolder.state = 'current';
      let flattenedDataTosign = [];

      nextDataToSignHolder.dataToSignHolderList
        .map(holder => holder.dataToSignList.map(o => o.dataToSignBase64))
        .forEach(list => flattenedDataTosign = flattenedDataTosign.concat(list));

      this.dataToSign = flattenedDataTosign;
    }
    return !nextDataToSignHolder;
  }


  showNotificationsAndResetIfNeeded(): void {
    if (this.isThereNoErrorsOnTasks()) {
      this.notificationsService.showSuccessMessage(
        this.folders.length === 1
          ? ActionPopupMessages.actionFolderSuccessMessage(this.performedAction, this.folders[0])
          : ActionPopupMessages.ALL_TASKS_SUCCESSFUL
      );
      return;
    }

    const resultsOnError: SubscriptionResult[] = this.subscriptionResults
      .filter(sr => !!sr.error)

    this.notificationsService.showErrorMessage(ActionPopupMessages.parseMultipleActionErrorMessage(resultsOnError))

    this.reset();
  }


  reset(): void {
    this.performedAction = null;
    this.fortifyCertificate = null;
    this.libersignCertificate = null;

    setTimeout(() => {
      this.resetData();
      setTimeout(() => this.performedAction = this.action);
    }, 1500);
  }


  resetData(): void {
    const errorFolderIds: string[] = this.subscriptionResults
      .filter(res => !!res.error)
      .map(res => res.folderId)

    this.folders = this.folders.filter(folder => errorFolderIds.includes(folder.id))

    this.foldersDataToSign = [];
    this.subscriptionResults = [];
    this.dataToSign = [];

    this.numberOfFailedTasks = 0;

    this.refreshProgressBar();
  }


  getSignatureTaskParams(folder: Folder, certificateBase64: string, dataToSignHolderList: DataToSignHolder[]): SignatureTaskParams {

    let signatureTaskParams: SignatureTaskParams = new SignatureTaskParams();
    signatureTaskParams.publicAnnotation = this.publicAnnotation;
    signatureTaskParams.privateAnnotation = this.privateAnnotation;
    signatureTaskParams.metadata = folder.populatedMandatoryStepMetadata;
    signatureTaskParams.certificateBase64 = certificateBase64;
    signatureTaskParams.dataToSignHolderList = dataToSignHolderList;

    if (this.showCustomSignatureField) {
      signatureTaskParams.customSignatureField = this.customSignatureField;
    }

    return signatureTaskParams;
  }


  getSimpleTaskParams(folder: Folder): SimpleTaskParams {
    let simpleTaskParams: SimpleTaskParams = new SimpleTaskParams();
    simpleTaskParams.publicAnnotation = this.publicAnnotation;
    simpleTaskParams.privateAnnotation = this.privateAnnotation;
    simpleTaskParams.metadata = folder.populatedMandatoryStepMetadata;

    if (this.showCustomSignatureField) {
      simpleTaskParams.customSignatureField = this.customSignatureField;
    }

    return simpleTaskParams;
  }


  isLibersignUp(): boolean {
    const isUniversignCompatibleWithDevice: boolean = LiberSignCheckComponent.authorizedOss.includes(this.deviceService.getDeviceInfo().os)
      && LiberSignCheckComponent.authorizedBrowsers.includes(this.deviceService.getDeviceInfo().browser);

    if (!isUniversignCompatibleWithDevice) return false;

    const liberSign = window['LiberSign'];

    if (!liberSign) return false;

    const libersignExtensionVersionStatus: LiberExtensionSignStatus = LiberSignCheckComponent.guessLiberSignExtensionVersion(liberSign);

    return libersignExtensionVersionStatus === LiberExtensionSignStatus.ExtensionInstalledUpToDate;
  }


  getCurrentTask(folder: Folder): Task {
    console.log(this.folders)
    // For AND/OR tasks, the folder will contain two tasks, we select the one with the current desk.
    // For basic and delegated tasks (even AND/OR), the folder will contain only one task, so we use a fallback to CURRENT task.
    const currentTask: Task = folder.stepList.find(step => step.state === State.Current)
      || folder.stepList.find(step => step.state === State.Pending && step.desks.map(desk => desk.id).includes(this.deskId));

    return currentTask || folder.stepList[0];
  }

}
