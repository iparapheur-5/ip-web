/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { SimpleActionPopupComponent } from './simple-action-popup.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Action } from '@libriciel/iparapheur-standard';
import { Folder } from '../../../../../models/folder/folder';


describe('SimpleActionPopupComponent', () => {


  let component: SimpleActionPopupComponent;
  let fixture: ComponentFixture<SimpleActionPopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: SimpleActionPopupComponent.injectableDeskIdKey, useValue: 'desk01'},
          {provide: SimpleActionPopupComponent.injectablePerformedActionKey, useValue: Action.Signature},
          {provide: SimpleActionPopupComponent.injectableTenantKey, useValue: {id: 'tenant01', name: 'Tenant 01'}},
          {provide: SimpleActionPopupComponent.injectableFoldersKey, useValue: [new Folder()]},
        ],
        declarations: [SimpleActionPopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleActionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });


});
