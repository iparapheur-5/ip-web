/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class ExternalSignatureMessages {

  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_FIRSTNAME = 'Prénom';
  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_LASTNAME = 'Nom';
  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_MAIL_ADDRESS = 'Adresse email';
  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_PHONE = 'Numéro de téléphone portable';
  static readonly FORCE_EXTERNAL_SIGNATURE = `Finaliser la signature externe`;

}
