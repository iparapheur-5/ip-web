/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDesksToNotifyPopupComponent } from './add-desks-to-notify-popup.component';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { KeycloakService } from 'keycloak-angular';
import { Task } from '../../../../../models/task';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { ActionIconPipe } from '../../../../../shared/utils/action-icon.pipe';

describe('AddDesksToNotifyPopupComponent', () => {
  let component: AddDesksToNotifyPopupComponent;
  let fixture: ComponentFixture<AddDesksToNotifyPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ToastrModule.forRoot(), HttpClientTestingModule, NgbModule],
      providers: [
        NgbActiveModal,
        KeycloakService,
        {provide: AddDesksToNotifyPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant01'},
        {provide: AddDesksToNotifyPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: 'desk01'},
        {provide: AddDesksToNotifyPopupComponent.INJECTABLE_TASK_KEY, useValue: new Task()}
      ],
      declarations: [AddDesksToNotifyPopupComponent, ActionNamePipe, ActionIconPipe]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDesksToNotifyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
