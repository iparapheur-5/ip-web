/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { Task } from '../../../../../models/task';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../services/notifications.service';
import { LegacyUserService } from '../../../../../services/ip-core/legacy-user.service';
import { Observable, combineLatest } from 'rxjs';
import { ActionPopupMessages } from '../action-popup-messages';
import { isNullOrUndefined, isEmpty } from '../../../../../utils/string-utils';
import { tap } from 'rxjs/operators';
import { SecondaryAction } from '../../../../../shared/models/secondary-action.enum';
import { DeskService } from '@libriciel/iparapheur-standard';
import { DeskService as InternalDeskService, PageDeskRepresentation } from '@libriciel/iparapheur-internal';
import { DeskDto } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-add-desks-to-notify-popup',
  templateUrl: './add-desks-to-notify-popup.component.html',
  styleUrls: ['./add-desks-to-notify-popup.component.scss']
})
export class AddDesksToNotifyPopupComponent implements OnInit {

  static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  static readonly INJECTABLE_DESK_ID_KEY = 'deskId';
  static readonly INJECTABLE_TASK_KEY = 'task';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = ActionPopupMessages;
  readonly secondaryActionEnum = SecondaryAction;
  isProcessing = false;
  desksToNotify: DeskDto[] = [];
  _selectedDeskList: DeskDto[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              public legacyUserService: LegacyUserService,
              public deskService: DeskService,
              public internalDeskService: InternalDeskService,
              public notificationService: NotificationsService,
              @Inject(AddDesksToNotifyPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(AddDesksToNotifyPopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string,
              @Inject(AddDesksToNotifyPopupComponent.INJECTABLE_TASK_KEY) public task: Task) {
  }


  ngOnInit(): void {
    this.populateDesksToNotify();
  }


  // <editor-fold desc="LifeCycle">


  populateDesksToNotify() {
    if (isNullOrUndefined(this.tenantId)) {
      return;
    }

    let observableList: Observable<DeskDto>[] = [];
    this.task?.notifiedDeskIds?.forEach(desk => observableList.push(this.deskService.getDesk(this.tenantId, desk.id)));

    combineLatest(observableList)
      .subscribe(
        data => this.desksToNotify.push(...data),
        error => this.notificationService.showErrorMessage(`Erreur lors de la récupération du bureau.`, error.message)
      );
  }


  selectDesk(): void {
    if (this.desksToNotify.map(desk => desk.id).includes(this._selectedDeskList[0].id)) {
      this.notificationService.showErrorMessage(ActionPopupMessages.DESK_ALREADY_SELECTED_ERROR)
      this._selectedDeskList = [];
      return;
    }
    this.desksToNotify.push(this._selectedDeskList[0]);
    this._selectedDeskList = [];
  }


  retrieveDesksFn = (page: number, pageSize: number, searchTerm): Observable<PageDeskRepresentation> => {
    return this.internalDeskService.getAssociatedDesks(this.tenantId, this.deskId, page, pageSize, [], searchTerm);
  }


  isFormValid(): boolean {
    return this.desksToNotify.length > 0;
  }


  validate(): void {
    // FIXME update the task
    this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
  }


  removeDesk(deskId: string): void {
    this.desksToNotify.splice(this.desksToNotify.findIndex(desk => desk.id === deskId), 1);
  }


}