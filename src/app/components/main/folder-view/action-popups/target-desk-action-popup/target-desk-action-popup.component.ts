/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { CommonIcons, IconSize } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../../shared/common-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WorkflowService } from '../../../../../services/ip-core/workflow.service';
import { ActionPopupMessages } from '../action-popup-messages';
import { Folder } from '../../../../../models/folder/folder';
import { Observable, combineLatest } from 'rxjs';
import { NotificationsService } from '../../../../../services/notifications.service';
import { DeskSearchInputComponent } from '../../../../../shared/components/desk-search-input/desk-search-input.component';
import { SimpleTaskParams } from '../../../../../models/simple-task-params';
import { map, catchError } from 'rxjs/operators';
import { FolderUtils } from '../../../../../utils/folder-utils';
import { Action } from '@libriciel/iparapheur-standard';
import { DeskService } from '@libriciel/iparapheur-internal';
import { AdminDeskService, PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-target-desk-action-popup',
  templateUrl: './target-desk-action-popup.component.html',
  styleUrls: ['./target-desk-action-popup.component.scss']
})
export class TargetDeskActionPopupComponent implements AfterViewInit {

  public static readonly INJECTABLE_PERFORMED_ACTION_KEY = 'performedAction';
  public static readonly INJECTABLE_FOLDERS_KEY = 'folders';
  public static readonly INJECTABLE_DESK_ID_KEY = 'deskId';
  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_AS_ADMIN_KEY = 'asAdmin';


  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = ActionPopupMessages;
  readonly actionEnum = Action;
  readonly iconSize = IconSize;

  @ViewChild('deskSelector') deskSelector: DeskSearchInputComponent;

  isProcessing = false;
  targetDesks: DeskRepresentation[] = [];
  publicAnnotation: string = null;
  privateAnnotation: string = null;

  // Progress bar
  numberOfTasks: number = 0;
  numberOfSuccessfulTasks: number = 0;
  numberOfFailedTasks: number = 0;
  successfulTasksProgressBarPercentage: number = 0;
  failedTasksProgressBarPercentage: number = 0;

  subscriptionResults: {
    folderId: string,
    error: Error,
  }[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              public workflowService: WorkflowService,
              public deskService: DeskService,
              public adminDeskService: AdminDeskService,
              public notificationService: NotificationsService,
              @Inject(TargetDeskActionPopupComponent.INJECTABLE_PERFORMED_ACTION_KEY) public performedAction: Action,
              @Inject(TargetDeskActionPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(TargetDeskActionPopupComponent.INJECTABLE_FOLDERS_KEY) public folders: Folder[],
              @Inject(TargetDeskActionPopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string,
              @Inject(TargetDeskActionPopupComponent.INJECTABLE_AS_ADMIN_KEY) public asAdmin: boolean) {

    if (![Action.Transfer, Action.AskSecondOpinion].includes(performedAction)) {
      throw new Error('TargetDeskActionPopupComponent performedAction not relevant ' + performedAction);
    }
  }


  ngAfterViewInit(): void {


  }


  // </editor-fold desc="LifeCycle">


  public retrieveDesksFn = (page: number, pageSize: number, searchTerm: string): Observable<PageDeskRepresentation> => {
    return this.asAdmin
      ? this.adminDeskService.listDesks(this.tenantId, page, pageSize, [], searchTerm)
      : this.deskService.getAssociatedDesks(this.tenantId, this.deskId, page, pageSize, [], searchTerm);
  }


  perform(): void {

    if (this.targetDesks.length !== 1) {
      console.log('Something went wrong, aborting...');
      return;
    }

    let targetDesk = this.targetDesks[0];

    let simpleTaskParams: SimpleTaskParams = new SimpleTaskParams();
    simpleTaskParams.publicAnnotation = this.publicAnnotation;
    simpleTaskParams.privateAnnotation = this.privateAnnotation;
    simpleTaskParams.targetDeskId = targetDesk.id;

    this.isProcessing = true;

    combineLatest(this.getTaskRequests(simpleTaskParams))
      .subscribe()
      .add(() => {
        this.isProcessing = false;
        this.showNotificationsAndResetIfNeeded();

        if (this.isThereNoErrorsOnTasks()) {
          this.activeModal.close({value: CommonMessages.ACTION_RESULT_OK});
        }
      });
  }


  showNotificationsAndResetIfNeeded(): void {
    if (this.isThereNoErrorsOnTasks()) {
      const message = this.folders.length === 1
        ? ActionPopupMessages.actionFolderSuccessMessage(this.performedAction, this.folders[0])
        : ActionPopupMessages.ALL_TASKS_SUCCESSFUL;
      this.notificationService.showSuccessMessage(message);
    } else {
      const resultsOnError: { folderId: string, error: Error }[] = this.subscriptionResults
        .filter(sr => !!sr.error)

      this.notificationService.showErrorMessage(ActionPopupMessages.parseMultipleActionErrorMessage(resultsOnError))

      this.reset();
    }
  }


  isThereNoErrorsOnTasks(): boolean {
    return this.subscriptionResults.every(sr => sr.error === undefined);
  }


  reset(): void {
    setTimeout(() => {
      // We need to reset the ls-libersign component by refreshing the ngIf
      // setTimeout is needed or the component does not refresh

      this.numberOfFailedTasks = 0;
      this.refreshProgressBar();

      const errorFolderIds: string[] = this.subscriptionResults
        .filter(res => !!res.error)
        .map(res => res.folderId)

      this.folders = this.folders.filter(folder => errorFolderIds.includes(folder.id))
      this.subscriptionResults = [];
    }, 1500);
  }


  getTaskRequests(simpleTaskParams: SimpleTaskParams): Observable<any>[] {
    let observableList: Observable<any>[] = [];

    this.folders
      .forEach((folder) => {
        observableList.push(this.getSingleTaskRequest(folder, simpleTaskParams)
          .pipe(
            map(data => {
              this.setNotificationsAndSubscriptionResult(folder);
              return data;
            }),
            catchError(response => {
              this.setNotificationsAndSubscriptionResult(folder, response?.error || response);
              return new Observable<any>(observer => observer.complete());
            }))
        );
      });

    return observableList;
  }


  getSingleTaskRequest(folder: Folder, simpleTaskParams: SimpleTaskParams): Observable<any> {

    switch (this.performedAction) {

      case Action.Transfer:
        return this.workflowService.performTransfer(
          this.tenantId,
          folder,
          folder.stepList.filter(task => FolderUtils.isActive(task))[0],
          this.deskId,
          simpleTaskParams
        );

      case Action.AskSecondOpinion:
        return this.workflowService.performAskSecondOpinion(
          this.tenantId,
          folder,
          folder.stepList.filter(task => FolderUtils.isActive(task))[0],
          this.deskId,
          simpleTaskParams
        );
    }
  }


  updateProgressBar(success: boolean): void {
    success ? this.numberOfSuccessfulTasks += 1 : this.numberOfFailedTasks += 1;
    this.refreshProgressBar();
  }

  refreshProgressBar(): void {
    this.successfulTasksProgressBarPercentage = Math.round((this.numberOfSuccessfulTasks / this.numberOfTasks) * 100)
    this.failedTasksProgressBarPercentage = Math.round((this.numberOfFailedTasks / this.numberOfTasks) * 100)
  }


  setNotificationsAndSubscriptionResult(folder: Folder, error?: Error): void {
    const existingFolderIndex = this.subscriptionResults.findIndex(sub => sub.folderId === folder.id);

    if (existingFolderIndex !== -1) {
      this.subscriptionResults[existingFolderIndex].error = error;
      return;
    }

    this.subscriptionResults.push({
      folderId: folder.id,
      error: error
    });

    this.updateProgressBar(error === undefined);

  }


}
