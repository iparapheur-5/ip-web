/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Injector, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Folder } from '../../../../models/folder/folder';
import { SimpleActionPopupComponent } from '../action-popups/simple-action-popup/simple-action-popup.component';
import { ExternalState } from '../../../../models/external-state.enum';
import { ChainPopupComponent } from '../action-popups/chain-popup/chain-popup.component';
import { isNotNullOrUndefined } from '../../../../utils/string-utils';
import { SendByMailPopupComponent } from '../action-popups/mail/send-by-mail-popup/send-by-mail-popup.component';
import { PrintPopupComponent } from '../action-popups/print-popup/print-popup.component';
import { CommonMessages } from '../../../../shared/common-messages';
import { ExternalSignaturePopupComponent } from '../action-popups/external-signature-popup/external-signature-popup.component';
import { Task } from 'src/app/models/task';
import { FolderViewMessages } from '../folder-view-messages';
import { HistoryPopupComponent } from '../../history-popup/history-popup.component';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { FolderService as LegacyFolderService } from '../../../../services/ip-core/folder.service';
import { NotificationsService } from '../../../../services/notifications.service';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { SecureMailStatusPopupComponent } from '../action-popups/mail/secure-mail-status-popup/secure-mail-status-popup.component';
import { TargetDeskActionPopupComponent } from '../action-popups/target-desk-action-popup/target-desk-action-popup.component';
import { AddDesksToNotifyPopupComponent } from '../action-popups/add-desks-to-notify-popup/add-desks-to-notify-popup.component';
import { IpngProofDisplayPopupComponent } from '../../ipng/ipng-proof-display-popup/ipng-proof-display-popup.component';
import { Action, State, SignatureFormat, FolderService, ExternalSignatureService, ExternalSignatureProcedure } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { FolderUtils } from '../../../../utils/folder-utils';
import { FileService } from '../../../../services/file.service';
import { WebsocketService } from '../../../../services/websockets/websocket.service';
import { DeskDto } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-action-list',
  templateUrl: './action-list.component.html',
  styleUrls: ['./action-list.component.scss']
})
export class ActionListComponent implements OnInit {


  readonly actionEnum = Action;
  readonly secondaryActionEnum = SecondaryAction;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly infoIcon = faInfoCircle;


  @Input() folder: Folder;
  @Input() desk: DeskDto;
  @Input() tenantId: string;
  @Output() actualizeFolderEvent = new EventEmitter<void>();

  secondaryActionList: (Action | SecondaryAction) [] = [
    SecondaryAction.Print,
    SecondaryAction.Mail,
    SecondaryAction.AddDesksToNotify,
    Action.Transfer,
    Action.AskSecondOpinion,
    Action.Chain,
    SecondaryAction.HistoryTasks,
    SecondaryAction.Download,
    SecondaryAction.IpngShowProof
  ];
  alternativeActionList: SecondaryAction[] = [
    SecondaryAction.Print,
    SecondaryAction.Mail,
    SecondaryAction.HistoryTasks,
    SecondaryAction.Download,
    SecondaryAction.IpngShowProof
  ]

  stepList: Task[];
  externalSignatureProcedure: ExternalSignatureProcedure;
  isIpngProofAvailable: boolean = false;
  isMainActionEnabled: boolean = true;


  // <editor-fold desc="LifeCycle">


  constructor(private fileService: FileService,
              public modalService: NgbModal,
              private websocketService: WebsocketService,
              public globalPopupService: GlobalPopupService,
              public notificationsService: NotificationsService,
              public externalSignatureService: ExternalSignatureService,
              private legacyFolderService: LegacyFolderService,
              private folderService: FolderService,
              public router: Router) {}


  ngOnInit(): void {

    // Filtering currently performable tasks
    this.stepList = !!this.folder?.stepList
      ? this.folder.stepList
        ?.filter(task => task.state === State.Current)
        ?.filter(task => !task.date)
      : [];
    this.isIpngProofAvailable = this.areSomeIPNGProofAvailable();
    this.isMainActionEnabled = this.desk?.actionAllowed && !!this.folder?.type && !!this.folder.subtype;

    this.populateExternalSignatureDataIfNeeded();
  }


  // </editor-fold desc="LifeCycle">


  populateExternalSignatureDataIfNeeded(): void {
    let currentStep: Task = this.stepList[0];

    if (currentStep?.action !== Action.ExternalSignature || currentStep?.externalState === ExternalState.Form) return;

    this.externalSignatureService.getProcedureData(
      this.tenantId,
      this.desk.id,
      this.folder.subtype.externalSignatureConfig.id,
      currentStep.externalSignatureProcedureId
    ).subscribe(procedure => {
      this.externalSignatureProcedure = procedure
    });

  }


  areSomeIPNGProofAvailable(): boolean {
    if (!this.folder?.stepList) return false;

    const ipngTasks: Task[] = this.folder.stepList
      .filter(task => task.action === Action.Ipng)

    const validatedIpngTasks: Task[] = ipngTasks.filter(task => task.state === State.Validated);
    const validatedIpngStepIndexes = validatedIpngTasks.map(t => t.stepIndex);

    const unfinishedIpngTasks: Task[] = ipngTasks
      .filter(task => validatedIpngStepIndexes.includes(task.stepIndex))
      .filter(task => task.state != State.Validated)

    return validatedIpngTasks.length > 0 && validatedIpngTasks.length > unfinishedIpngTasks.length;
  }


  // <editor-fold desc="UI callbacks">


  isPaperSignable(step: Task): boolean {
    return this.desk?.actionAllowed
      && this.folder.type?.signatureFormat === SignatureFormat.Pades
      && (step.action === Action.Signature)
      && !this.folder.subtype.digitalSignatureMandatory;
  }


  isRejectable(step: Task): boolean {

    let deskActionAllowed: boolean = this.desk?.actionAllowed;
    let isSimpleAction: boolean = FolderUtils.SIMPLE_ACTIONS.includes(step.action);
    let isExternalAction: boolean = FolderUtils.EXTERNAL_ACTIONS.includes(step.action);
    let isInFormState: boolean = !this.externalActionWaitingStatus(step);

    return deskActionAllowed && (isSimpleAction || (isExternalAction && isInFormState));
  }


  /**
   * Only rejected folders steps can be chained.
   * Final steps on rejected folders are the ones asking for an {@link Delete} action.
   * @param step
   */
  isRecyclable(step: Task): boolean {
    return this.desk.archivingAllowed
      && (step.action === Action.Delete);
  }


  /**
   * Drafts and archivable folders can be deleted.
   * @param step
   */
  isDeletableAsSecondaryAction(step: Task): boolean {
    const isDraft = (step.action === Action.Start);
    // const isDeleteAllowed = this.desk.archivingAllowed;
    // const isDeleteAvailable = (step.action === Action.Archive);
    // return isDraft || (isDeleteAllowed && isDeleteAvailable);
    return isDraft;
  }


  /**
   * Checking step and desk permissions.
   * @param step
   */
  isArchivableAsSecondaryAction(step: Task): boolean {
    const isArchiveAllowed = this.desk.archivingAllowed;
    const isArchiveAvailable = (step.action === Action.Delete);
    return isArchiveAllowed && isArchiveAvailable;
  }


  shouldHideArchiveAction(step: Task): boolean {
    const isArchiveAvailable = (step.action === Action.Archive);
    const isArchiveAllowed = this.desk.archivingAllowed;

    return isArchiveAvailable && !isArchiveAllowed;
  }

  /**
   * Only "external" steps can be bypassed.
   * @param step
   */
  isBypassable(step: Task): boolean {
    return this.desk?.actionAllowed
      && FolderUtils.EXTERNAL_ACTIONS.includes(step.action);
  }


  isUndoable(step: Task): boolean {
    switch (step.action) {
      case Action.SecureMail:
      case Action.ExternalSignature:
        return this.externalActionWaitingStatus(step);
      default:
        return false;
    }
  }


  isSecondaryActionAvailable(action: Action | SecondaryAction, step: Task): boolean {
    switch (action) {

      // Both are a kind of transfer,
      // and are subject to the same permissions
      case Action.Transfer:
      case Action.AskSecondOpinion:
        return this.desk?.actionAllowed
          && FolderUtils.TRANSFERABLE_ACTIONS.includes(step.action);

      // Only "final" steps can be chained.
      // Final steps are the ones asking for an {@link Archive} action.
      case Action.Chain:
        return this.desk.chainAllowed && (step.action == Action.Archive);

      case SecondaryAction.IpngShowProof:
        return this.isIpngProofAvailable;

      // Every other action (print, history...) is allowed
      default:
        return true;
    }
  }


  isSignedExternalSignature(step: Task): boolean {
    return step.action === Action.ExternalSignature && step.externalState === ExternalState.Signed
  }


  externalActionWaitingStatus(task: Task) {
    const externalAction: boolean =
      task.action === Action.ExternalSignature
      || task.action === Action.Ipng
      || task.action === Action.SecureMail;

    return externalAction
      && isNotNullOrUndefined(task.externalState)
      && (task.externalState !== ExternalState.Form);
  }


  // </editor-fold desc="UI callbacks">


  openModal(task: Task, executedAction: Action | SecondaryAction) {

    let modalResult: Promise<any>;
    const folderToDo = Object.assign(new Folder(), this.folder);
    const modalSize = FolderUtils.getModalSize(executedAction, [folderToDo]);
    const taskToDo = Object.assign(new Task(), task);
    taskToDo.action = executedAction;
    folderToDo.stepList = [taskToDo];

    switch (executedAction) {

      case SecondaryAction.AddDesksToNotify:
        modalResult = this.modalService
          .open(AddDesksToNotifyPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: AddDesksToNotifyPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: AddDesksToNotifyPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                {provide: AddDesksToNotifyPopupComponent.INJECTABLE_TASK_KEY, useValue: this.folder.stepList.find(step => step.state === State.Current)}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case Action.Delete:
        modalResult = this.globalPopupService
          .showDeleteValidationPopup(CommonMessages.foldersValidationPopupLabel([folderToDo]), CommonMessages.foldersValidationPopupTitle([folderToDo]))
          .then(
            () => this.legacyFolderService.deleteFolder(this.tenantId, folderToDo.id).toPromise(),
            () => { /* Dismissed */ }
          );
        break;

      case Action.Chain:
        modalResult = this.modalService
          .open(ChainPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: ChainPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folderToDo},
                {provide: ChainPopupComponent.INJECTABLE_DESK_KEY, useValue: this.desk},
                {provide: ChainPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.Print:
        modalResult = this.modalService
          .open(PrintPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: PrintPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: PrintPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folderToDo},
                {provide: PrintPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.Mail:
      case Action.SecureMail: {
        if (executedAction === Action.SecureMail && this.externalActionWaitingStatus(this.stepList[0])) {
          modalResult = this.modalService
            .open(SecureMailStatusPopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: SecureMailStatusPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                  {provide: SecureMailStatusPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folderToDo},
                  {provide: SecureMailStatusPopupComponent.INJECTABLE_TENANT_KEY, useValue: this.tenantId}
                ]
              }),
              size: "xl"
            })
            .result;
        } else {
          modalResult = this.modalService
            .open(SendByMailPopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: SendByMailPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                  {provide: SendByMailPopupComponent.INJECTABLE_CURRENT_ACTION_KEY, useValue: executedAction},
                  {provide: SendByMailPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folderToDo]},
                  {provide: SendByMailPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId}
                ]
              }),
              size: modalSize
            })
            .result;
        }
        break;
      }
      case Action.ExternalSignature:
        modalResult = this.modalService
          .open(ExternalSignaturePopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: ExternalSignaturePopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folderToDo]},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: executedAction},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_TENANT_KEY, useValue: this.tenantId},
                {provide: ExternalSignaturePopupComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY, useValue: this.externalSignatureProcedure}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.HistoryTasks:
        modalResult = this.modalService
          .open(HistoryPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: HistoryPopupComponent.injectableFolderKey, useValue: folderToDo},
                {provide: HistoryPopupComponent.injectableDeskIdKey, useValue: this.desk.id},
                {provide: HistoryPopupComponent.injectableTenantIdKey, useValue: this.tenantId}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.IpngShowProof:
        modalResult = this.modalService
          .open(IpngProofDisplayPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: IpngProofDisplayPopupComponent.injectableFolderKey, useValue: folderToDo},
                {provide: IpngProofDisplayPopupComponent.injectableDeskIdKey, useValue: this.desk.id},
                {provide: IpngProofDisplayPopupComponent.injectableTenantIdKey, useValue: this.tenantId}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case Action.Transfer:
      case Action.AskSecondOpinion:
        modalResult = this.modalService
          .open(TargetDeskActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: TargetDeskActionPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: executedAction},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folderToDo]},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: false},
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.Download:
        this.fileService
          .downloadFileWithObservable(
            this.folderService.downloadFolderZip(this.tenantId, this.folder.id, 'body', true, {httpHeaderAccept: 'application/octet-stream'}),
            this.folder.name + '.zip'
          )
          .subscribe(
            () => console.log(this.messages.DOWNLOAD_PRINT_DOC_SUCCESS),
            error => this.notificationsService.showErrorMessage(this.messages.DOWNLOAD_PRINT_DOC_ERROR, error)
          );
        break;

      default:
        modalResult = this.modalService
          .open(SimpleActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: SimpleActionPopupComponent.injectableFoldersKey, useValue: [folderToDo]},
                {provide: SimpleActionPopupComponent.injectableDeskIdKey, useValue: this.desk.id},
                {provide: SimpleActionPopupComponent.injectablePerformedActionKey, useValue: executedAction},
                {provide: SimpleActionPopupComponent.injectableTenantKey, useValue: this.tenantId}
              ]
            }),
            size: modalSize
          })
          .result;
        break;
    }

    if (executedAction === SecondaryAction.Print || executedAction === SecondaryAction.Download) {
      return;
    }

    modalResult.then(
      result => {
        this.websocketService.notifyFolderAction(this.tenantId, this.desk.id);

        // Delete was dismissed
        if (result === undefined) {
          return;
        }
        // Delete was successful
        if (result === null) {
          this.notificationsService.showSuccessMessage(CommonMessages.getDeleteCreationMessage(folderToDo));
        }
        // On success, go up in parent desk
        if ((result === null) || (result === CommonMessages.ACTION_RESULT_OK) || (result.value === CommonMessages.ACTION_RESULT_OK)) {

          if (executedAction === Action.Recycle) {
            setTimeout(() => this.actualizeFolderEvent.emit());
            return;
          }

          this.router
            .navigate(['/desk/'])
            .then(() => { /* Not used */ });
        }
      },
      () => { /* Dismissed */ }
    );
  }


}
