/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewChild, Injector, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Folder } from '../../../../models/folder/folder';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AnnotationPopupComponent } from '../annotation-popup/annotation-popup.component';
import { Annotation } from '../../../../models/annotation/annotation';
import { Document } from '../../../../models/document';
import { DocumentService } from '../../../../services/ip-core/document.service';
import { isNotNullOrUndefined } from '../../../../utils/string-utils';
import { WorkflowService } from '../../../../services/ip-core/workflow.service';
import { Config } from '../../../../config';
import { SignaturePlacement } from 'src/app/models/annotation/signature-placement';
import { combineLatest, Observable, forkJoin, concat } from 'rxjs';
import { StickyNote } from '../../../../models/annotation/sticky-note';
import { PdfJsParameters } from '../../../../models/annotation/pdf-js-parameters';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { CrudOperation } from '../../../../services/crud-operation';
import { MetadataEvent } from '../metadata-list/metadata-list.component';
import { NotificationsService } from '../../../../services/notifications.service';
import { FolderViewMessages } from '../folder-view-messages';
import { IpStepInstance } from '../../../../models/workflows/ip-step-instance';
import { Metadata } from '../../../../models/metadata';
import { CommonMessages } from '../../../../shared/common-messages';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { User } from '../../../../models/auth/user';
import { MetadataFormComponent } from '../metadata-form/metadata-form.component';
import { SignatureFormat, Action, MetadataService, State, PdfSignaturePosition, FolderViewBlock, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { FolderUtils } from '../../../../utils/folder-utils';
import { map, catchError } from 'rxjs/operators';
import { Task } from '../../../../models/task';
import { CustomNumberMap } from '../../../../shared/models/custom-types';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { DeskDto } from '@libriciel/iparapheur-provisioning';
import { IpWorkflowInstance } from '../../../../models/workflows/ip-workflow-instance';


@Component({
  selector: 'app-folder-main-layout',
  templateUrl: './folder-main-layout.component.html',
  styleUrls: ['./folder-main-layout.component.scss']
})
export class FolderMainLayoutComponent implements OnInit {

  private isProcessing = false;
  private currentWorkflowIndex = 0;
  private apiVersion = Config.API_VERSION;
  private pdfJsParameters: PdfJsParameters = {
    annotationButtonsParameters: {
      hideAnnotation: false,
      hideSignaturePlacement: false,
      hideDraw: true,
      hideTextAnnotation: true
    },
    signaturePlacementAnnotations: [],
    annotationOptions: {
      width: Config.STAMP_WIDTH,
      height: Config.STAMP_HEIGHT,
      origin: 'BOTTOM_LEFT'
    }
  };

  readonly folderViewBlockEnum = FolderViewBlock;
  readonly messages = FolderViewMessages;
  readonly commonMessages = CommonMessages;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;

  @ViewChild('pdfJsViewer') pdfViewer;
  @ViewChild('pesViewerFrame') pesViewerFrame: ElementRef;

  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  workflowInstance: IpWorkflowInstance = {} as IpWorkflowInstance;
  currentDocument: Document;
  currentUser: User;
  folder: Folder;
  currentDesk: DeskDto;
  stepMetadata: Metadata[] = [];

  isMetadataSectionCollapsed = false;
  annotationsLoaded: boolean = false;
  tenantId: string;
  currentPdfSource: string;
  isPesViewerActive = false;
  countMissingMetadata: number;
  showStepMetadata: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(private workflowService: WorkflowService,
              private documentService: DocumentService,
              private legacyUserService: LegacyUserService,
              private metadataService: MetadataService,
              private folderService: FolderService,
              private notificationsService: NotificationsService,
              public modalService: NgbModal,
              public route: ActivatedRoute) { }


  ngOnInit(): void {
    this.userPreferences = this.route.snapshot.data["userPreferences"];

    combineLatest([
      this.route.params,
      this.route.data,
      this.legacyUserService.getCurrentUser()
    ])
      .subscribe(([params, data, currentUser]) => {
        this.tenantId = params['tenantId'];
        this.currentDesk = data.desk;
        this.folder = data.folder;
        this.currentUser = currentUser;
        this.initializeWithNewFolder();
      });
  }


  private initializeWithNewFolder(): void {
    this.workflowService
      .getWorkflowInstance(this.folder)
      .subscribe(workflowInstance => {
          this.workflowInstance = workflowInstance as IpWorkflowInstance;
          const currentStep: IpStepInstance = this.workflowInstance.steps
            .filter(step => step.state === State.Current)
            .map(s => s as IpStepInstance)
            .pop();
          this.currentWorkflowIndex = currentStep?.workflowIndex;

          this.parseMetadata();

          if (this.folder.documentList.length > 0) {
            this.onDocumentSelected(this.folder.documentList.filter(d => d.isMainDocument)[0]);
          }

          this.initAnnotationButtons();
        },
        error => {
          this.notificationsService.showErrorMessage(this.messages.ERROR_LOADING_WORKFLOW, error?.message)
        });
  }


  refreshFolder() {
    let currentFolderId = this.folder.id;
    this.folder = null;

    this.folderService.getFolder(this.tenantId, currentFolderId, this.currentDesk.id).subscribe(newFolder => {
      this.folder = newFolder;
      this.initializeWithNewFolder();
    });
  }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="Annotations">


  onAnnotationEvent(event: any): void {
    switch (event.type) {

      case 'pageChange':
      case 'annotationMode' :
      case 'signaturePlacementMode' :
        // Not used
        break;

      case 'annotationCreated' :
      case 'annotationClick' :
        const stickyNote = new StickyNote();
        this.fillAnnotation(stickyNote, event);
        stickyNote.content = event.value?.contentsObj?.str;
        this.onAnnotationSelected(stickyNote, event.type);
        break;

      case 'signaturePlacementCreated' :
      case 'signaturePlacementClick' :
        const signaturePlacement = new SignaturePlacement();
        this.fillAnnotation(signaturePlacement, event);
        signaturePlacement.signatureNumber = event.value.signatureNumber;
        this.onAnnotationSelected(signaturePlacement, event.type);
        break;

      default:
        console.log('Unknown annotation eventType:' + event.type);
    }
  }


  fillAnnotation(annotation: Annotation, event: any): void {
    annotation.id = event.value.identifierName;
    annotation.page = event.value.page;
    annotation.pageRotation = event.value.pageRotation;
    annotation.x = event.value.rect[0];
    annotation.y = event.value.rect[1];
    annotation.width = event.value.rect[2] - event.value.rect[0];
    annotation.height = event.value.rect[3] - event.value.rect[1];
  }


  private onAnnotationSelected(annotation: Annotation, type: string): void {
    this.modalService
      .open(AnnotationPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: AnnotationPopupComponent.injectableAnnotationKey, useValue: annotation},
              {provide: AnnotationPopupComponent.injectableTypeKey, useValue: type},
              {provide: AnnotationPopupComponent.injectableUserKey, useValue: this.currentUser}
            ]
          })
        }
      )
      .result
      .then(result => {
        switch (result.value) {
          case 'success' : {
            const isSignaturePlacement: boolean = result.annotation instanceof SignaturePlacement;

            let request$: Observable<void> = isSignaturePlacement
              ? this.documentService.createSignaturePlacement(this.tenantId, this.folder, this.currentDocument, result.annotation)
              : this.documentService.createAnnotation(this.tenantId, this.folder, this.currentDocument, result.annotation)

            request$.subscribe(() => {
              this.refreshViewer();
              console.log('Annotation saved : ' + result.annotation.id);
            });
            break;
          }
          case  'delete': {
            this.documentService
              .deleteAnnotation(this.tenantId, this.folder.id, this.currentDocument.id, result.annotation.id)
              .subscribe(() => {
                console.log('Annotation deleted : ' + result.annotation.id);
                this.refreshViewer();
              });
            break;
          }

          default: {
            if (type === "annotationCreated" || type === "signaturePlacementCreated") {
              this.refreshViewer();
            }
          }
        }
      }, () => {
        if (type === "annotationCreated" || type === "signaturePlacementCreated") {
          this.refreshViewer();
        }
      });
  }


  private refreshViewer(): void {
    this.annotationsLoaded = false;
    this.documentService
      .getSignaturePlacementAnnotations(this.tenantId, this.folder.id, this.currentDocument.id)
      .subscribe(
        annotations => {
          this.pdfJsParameters.signaturePlacementAnnotations = annotations || []
          this.currentDocument.signaturePlacementAnnotations = annotations || []
        },
        () => console.debug("Error retrieving signature placement annotations.")
      )
      .add(() => {
        this.setSignatureAnnotationParameters(this.currentDocument);
        this.pdfJsParametersToLocalStorage();
        this.annotationsLoaded = true;
        this.pdfViewer?.refresh();
      })
  }


  private initAnnotationButtons(): void {

    switch (this.folder.type?.signatureFormat ?? null) {

      case SignatureFormat.XadesDetached:
      case SignatureFormat.Pkcs7:
        this.pdfJsParameters.annotationButtonsParameters.hideAnnotation = true;
        this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = true;
        break;

      case SignatureFormat.Pades:
      case SignatureFormat.PesV2: // Useless case here, since we are going for the PES-Viewer, but we don't want to log the default case warning
        this.pdfJsParameters.annotationButtonsParameters.hideAnnotation = !this.folder.subtype?.annotationsAllowed;
        this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = false;
        break;

      case SignatureFormat.Auto: // It shall be evaluated server-side, and never appear here
      default:
        console.log('initAnnotationButtons: something went wrong on signature format evaluation, disabling everything annotation-like...');
        this.pdfJsParameters.annotationButtonsParameters.hideAnnotation = true;
        this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = true;
        break;
    }

    if (this.folder.stepList
      .filter(step => FolderUtils.CRYPTOGRAPHIC_ACTIONS.includes(step.action))
      .filter(step => !FolderUtils.isPassed(step))
      .length === 0) {
      this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = true;
    }
    this.pdfJsParametersToLocalStorage();
  }


  pdfJsParametersToLocalStorage(): void {
    localStorage.setItem("signaturePlacementAnnotations", JSON.stringify(this.pdfJsParameters.signaturePlacementAnnotations));
    localStorage.setItem("annotationButtonsParameters", JSON.stringify(this.pdfJsParameters.annotationButtonsParameters));
    localStorage.setItem("annotationOptions", JSON.stringify(this.pdfJsParameters.annotationOptions));
  }


  // </editor-fold desc="Annotations">


  // <editor-fold desc="UI callbacks">


  isInCreationWorkflow(): boolean {
    return this.currentWorkflowIndex === 1;
  }


  public isFolderStarted(): boolean {
    return this.folder.stepList
      .filter(t => t.action === Action.Start)
      .filter(t => t.state === State.Validated)
      .length > 0;
  }


  public onFolderUpdate(): void {
    this.folderService
      .getFolder(this.tenantId, this.folder.id)
      .subscribe(updatedFolder => {
        this.folder = updatedFolder;
        this.initializeWithNewFolder();
      })
  }


  public onMetadataChanged(metadataUpdateEvt: MetadataEvent): void {

    this.isProcessing = true;

    const requests: Observable<any>[] = metadataUpdateEvt.modifiedMetadataList
      .map(m => this.folderService.setMetadata(this.tenantId, this.folder, m.id, metadataUpdateEvt.fullMetadataMap[m.key]));

    console.log('onSaveButtonClicked - requests len : ', requests.length);
    // We cannot use combineLatest here because concurrent on the same folder update triggers a safety crash on workflow
    concat(...requests)
      .subscribe(
        () => {
          this.onFolderUpdate();
          this.notificationsService.showCrudMessage(CrudOperation.Update, this.folder)
        },
        error => this.notificationsService.showCrudMessage(CrudOperation.Update, this.folder, error.message, false)
      )
      .add(() => this.isProcessing = false);
  }


  setSignatureAnnotationParameters(document?: Document) {
    if(!document.isMainDocument) {
      this.pdfJsParameters.signaturePlacementAnnotations = [];
      return;
    }

    let documentToUse = document || this.currentDocument;

    const upcomingFilteredSteps: Array<Task> = this.folder.stepList
      .filter(step => step.state === State.Current || step.state === State.Upcoming || step.state === State.Pending)
      .filter(step => step.action === Action.Signature || step.action === Action.ExternalSignature || step.action === Action.Seal);

    if (upcomingFilteredSteps.length === 0) {
      this.pdfJsParameters.signaturePlacementAnnotations = [];
      return;
    }

    if (documentToUse.signaturePlacementAnnotations.length !== 0) {
      documentToUse.signaturePlacementAnnotations.forEach(annotation => annotation.y += this.pdfJsParameters.annotationOptions.height);
      this.pdfJsParameters.signaturePlacementAnnotations = documentToUse.signaturePlacementAnnotations;
      return;
    }

    this.pdfJsParameters.signaturePlacementAnnotations = [this.getDefaultSignaturePosition];

    switch (upcomingFilteredSteps[0].action) {
      case Action.Seal:
        this.setPdfJsParametersFromTagMap(documentToUse.sealTags, [Action.Seal]);
        break;
      case Action.Signature:
      case Action.ExternalSignature:
        this.setPdfJsParametersFromTagMap(documentToUse.signatureTags, [Action.Signature, Action.ExternalSignature]);
        break;
    }
  }


  setPdfJsParametersFromTagMap(tagList: CustomNumberMap<PdfSignaturePosition>, actions: (Action | SecondaryAction)[]) {
    const signatureIndex: number = this.folder.stepList
      .filter(step => actions.includes(step.action))
      .filter(step => step.state === State.Validated)
      .length + 1;

    const tagListSize: number = Object.keys(tagList).length;
    if (tagListSize === 0 || signatureIndex >= tagListSize) {
      this.pdfJsParameters.signaturePlacementAnnotations = [this.getDefaultSignaturePosition];
      return;
    }

    let result: SignaturePlacement[] = [];
    for (let key in tagList) {
      result.push({
        pageRotation: 0,
        signatureNumber: 0,
        rectangleOrigin: 'BOTTOM_LEFT',
        height: Config.STAMP_HEIGHT,
        width: Config.STAMP_WIDTH,
        page: tagList[key].page,
        x: tagList[key].x - (Config.STAMP_WIDTH / 2),
        y: tagList[key].y + (Config.STAMP_HEIGHT / 2)
      });
    }
    this.pdfJsParameters.signaturePlacementAnnotations = [result[signatureIndex]];
  }


  public onDocumentSelected(document: Document): void {
    this.currentDocument = document;
    this.unloadPesViewer();
    this.setSignatureAnnotationParameters(document);

    if (document.mediaType === 'text/xml') {
      // Special case on PES documents
      this.isPesViewerActive = true;
      this.loadPesViewerForCurrentDocument();

    } else {
      // It may be 'application/pdf', 'application/octet-stream', or any weird things like that...
      // The default case is a good match.
      this.isPesViewerActive = false;

      this.currentPdfSource = isNotNullOrUndefined(this.currentDocument.pdfVisualId)
        ? this.documentService.pdfVisualUrl(this.tenantId, this.folder.id, this.currentDocument)
        : this.documentService.documentUrl(this.tenantId, this.folder.id, this.currentDocument.id);

      // For some reason, we have to force it there too, before the refresh.
      // Modifying the currentPdfSource isn't enough.
      if (this.pdfViewer) {
        this.pdfViewer.pdfSrc = this.currentPdfSource;
      }
    }
    this.refreshViewer();
  }


  private loadPesViewerForCurrentDocument(): void {
    const url: string = '/api/' + this.apiVersion + '/tenant/' + this.tenantId + '/folder/' + this.folder.id + '/document/' + this.currentDocument.id + '/pes-viewer';

    this.documentService.loadPageFromUrl(url)
      .subscribe((response: HTMLDocument) => {
          if (this.pesViewerFrame) {
            this.pesViewerFrame.nativeElement.srcdoc = response.documentElement.innerHTML;
          }
        }
      );
  }


  private unloadPesViewer(): void {
    if (this.pesViewerFrame) {
      this.pesViewerFrame.nativeElement.srcdoc = '';
    }
  }


  private setParsedMetadata(metadataKey: string): Observable<Metadata>[] {
    let metadataObservables: Observable<Metadata>[] = [];
    let requestedMetadataId: string[] = [];
    this.folder
      .stepList
      .filter(step => isNotNullOrUndefined(step[metadataKey]))
      .forEach(step => {
        step[metadataKey]
          .filter(id => !requestedMetadataId.includes(id))
          .forEach(id => {
            this.removeStepMetadataInSubtype(id);
            requestedMetadataId.push(id);
            metadataObservables.push(
              this.metadataService
                .getMetadata(this.tenantId, id)
                .pipe(
                  map(metadataDto => Object.assign(new Metadata(), metadataDto)),
                  catchError(this.notificationsService.handleHttpError('getMetadata'))
                ));
          });
      });
    return metadataObservables;
  }


  private removeStepMetadataInSubtype(id: string): void {
    const duplicateSubtypeExists = this.folder
      .subtype
      .subtypeMetadataList
      .filter(metadata => metadata.metadata.id === id)
      .length > 0

    if (!duplicateSubtypeExists) return;

    const index = this.folder
      .subtype
      .subtypeMetadataList
      .findIndex(metadata => metadata.metadata.id === id);

    this.folder.subtype.subtypeMetadataList.splice(index, 1);
  }


  private parseMetadata(): void {
    forkJoin(
      [
        ...this.setParsedMetadata(MetadataFormComponent.MANDATORY_VALIDATION_KEY),
        ...this.setParsedMetadata(MetadataFormComponent.MANDATORY_REJECTION_KEY)
      ]
    ).subscribe(
      result => {
        let distinctMetadata: Metadata[] = [];
        result.forEach(meta => {
          if (!distinctMetadata.map(meta => meta.id).includes(meta.id)) {
            distinctMetadata.push(meta);
          }
        });
        this.stepMetadata = distinctMetadata
        this.countMissingMetadata = this.stepMetadata.filter(metadata => isNotNullOrUndefined(this.folder.metadata[metadata.key])).length
        this.setShowStepMetadata();
      },
      error => this.notificationsService.showCrudMessage(CrudOperation.Read, error));
  }


  showSubTypeMetadata(): boolean {
    return this.folder?.subtype?.subtypeMetadataList?.length > 0;
  }


  setShowStepMetadata(): void {
    this.showStepMetadata = this.stepMetadata?.length > 0
      && this.countMissingMetadata > 0;
  }


  // </editor-fold desc="UI callbacks">


  get getDefaultSignaturePosition(): SignaturePlacement {
    return {
      pageRotation: 0,
      signatureNumber: 0,
      rectangleOrigin: 'BOTTOM_LEFT',
      height: Config.STAMP_HEIGHT,
      width: Config.STAMP_WIDTH,
      page: this.folder.type.signaturePosition?.page,
      x: this.folder.type.signaturePosition?.x,
      y: this.folder.type.signaturePosition?.y + Config.STAMP_HEIGHT
    };
  }


}
