/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


import { FolderMainLayoutComponent } from './folder-main-layout.component';
import { ToastrModule } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';

describe('FolderMainLayoutComponent', () => {


  let component: FolderMainLayoutComponent;
  let fixture: ComponentFixture<FolderMainLayoutComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          NgbActiveModal, HttpClient, HttpHandler,
        ],
        declarations: [FolderMainLayoutComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(FolderMainLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  // FIXME : We still miss some injectable Dummy KeycloakService of some sort
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });


});
