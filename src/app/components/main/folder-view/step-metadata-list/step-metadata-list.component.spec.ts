/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepMetadataListComponent } from './step-metadata-list.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { KeycloakService } from 'keycloak-angular';
import { Folder } from '../../../../models/folder/folder';
import { Task } from '../../../../models/task';
import { MetadataFormComponent } from '../metadata-form/metadata-form.component';

describe('StepMetadataListComponent', () => {

  let component: StepMetadataListComponent;
  let fixture: ComponentFixture<StepMetadataListComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [HttpClient, HttpHandler, KeycloakService],
        declarations: [StepMetadataListComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(StepMetadataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component.checkIfFolderContainsMetadata(null, null, null)).toBeFalse();

    let folder: Folder = new Folder();
    expect(component.checkIfFolderContainsMetadata(folder, "nonExistingMetadataKey", "id")).toBeFalse();

    let task: Task = new Task();
    task[MetadataFormComponent.MANDATORY_VALIDATION_KEY] = ["id"]

    folder.stepList = [task]

    expect(component.checkIfFolderContainsMetadata(folder, MetadataFormComponent.MANDATORY_VALIDATION_KEY, "id")).toBeTrue();
    expect(component.checkIfFolderContainsMetadata(folder, "nonExistingMetadataKey", "id")).toBeFalse();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
