/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Metadata } from '../../../../models/metadata';
import { Folder } from '../../../../models/folder/folder';
import { MetadataType } from '../../../../models/metadata-type.enum';
import { MetadataFormComponent } from '../metadata-form/metadata-form.component';
import { Action } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';

type ValuedMetadata = {
  metadata: Metadata,
  value: any
};


@Component({
  selector: 'app-step-metadata-list',
  templateUrl: './step-metadata-list.component.html',
  styleUrls: ['./step-metadata-list.component.scss']
})
export class StepMetadataListComponent implements OnInit {

  @Input() performedAction?: Action | SecondaryAction;

  @Input() labelFontWeightNormal: boolean = false
  @Input() isFolderEditable: boolean = false;
  @Input() isMetadataRequired: boolean = false;

  @Input() folders: Folder[];
  @Input() metadataList: Metadata[];
  @Input() showValidationMetadata: boolean = true;
  @Input() showRejectionMetadata: boolean = true;

  @Output() valid = new EventEmitter<boolean>();

  public isProcessing = false;

  public valuedMetadataList: ValuedMetadata[] = [];


  ngOnInit(): void {
    if (this.showValidationMetadata) {
      this.populateMetadataList(MetadataFormComponent.MANDATORY_VALIDATION_KEY);
    }
    if (this.showRejectionMetadata) {
      this.populateMetadataList(MetadataFormComponent.MANDATORY_REJECTION_KEY);
    }
    this.setMetadataValues();
  }

  populateMetadataList(metadataName: string): void {
    this.folders?.forEach(folder => {
      folder
        .stepList
        .filter(step => !!step[metadataName])
        .forEach(step => {
          step[metadataName]
            .forEach(id => {
              const foundMetadata: Metadata = this.metadataList.find(metadata => metadata.id === id);
              const foundData: ValuedMetadata = this.valuedMetadataList.find(data => data.metadata.id === id);

              if (!foundData) {
                this.valuedMetadataList.push({
                  metadata: foundMetadata,
                  value: null
                });
              }

            });

        });
    });
  }

  setMetadataValues(): void {
    if (this.isFolderEditable) {
      return;
    }

    this.folders?.forEach(folder => {
      this.valuedMetadataList.forEach(data => {
        let currentMetadataValue: any = folder.metadata[data.metadata?.key];

        if (this.folders.length > 1 && !currentMetadataValue) return;

        data.value = currentMetadataValue;
      });
    });


  }

  onValueChanged(valuedMetadata: ValuedMetadata): void {
    if (this.performedAction === Action.Reject) {
      this.folders?.forEach(folder => {
        const folderContainsRejectionMetadata: boolean = this.checkIfFolderContainsMetadata(
          folder,
          MetadataFormComponent.MANDATORY_REJECTION_KEY,
          valuedMetadata.metadata.id
        );
        if (folderContainsRejectionMetadata) {
          folder.metadata[valuedMetadata.metadata.key] = valuedMetadata.value;
        }
      });
    } else {
      this.folders?.forEach(folder => {
        const folderContainsValidationMetadata: boolean = this.checkIfFolderContainsMetadata(
          folder,
          MetadataFormComponent.MANDATORY_VALIDATION_KEY,
          valuedMetadata.metadata.id
        );
        if (folderContainsValidationMetadata) {
          folder.metadata[valuedMetadata.metadata.key] = valuedMetadata.value;
        }
      });
    }

    this.valid.emit(this.checkFormValidity());
  }

  checkFormValidity(): boolean {
    let result: boolean = true;
    this.valuedMetadataList?.forEach(valuedMetadata => {
      switch (valuedMetadata.metadata.type) {
        case MetadataType.Text : {
          if (!valuedMetadata.value || valuedMetadata.value.length <= 0) {
            result = false;
          }
          break;
        }
        case MetadataType.Boolean : {
          if (valuedMetadata.value !== true && valuedMetadata.value !== false) {
            result = false;
          }
          break;
        }
        default : {
          if (!valuedMetadata.value) {
            result = false;
          }
          break;
        }

      }
    });

    return result;
  }

  checkIfFolderContainsMetadata(folder: Folder, metadataKey: string, metadataId: string): boolean {
    return !!folder?.stepList
      ?.find(step => step[metadataKey]?.includes(metadataId));
  }
}
