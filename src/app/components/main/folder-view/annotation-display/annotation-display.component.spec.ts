/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnotationDisplayComponent } from './annotation-display.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { KeycloakService } from 'keycloak-angular';

describe('AnnotationDisplayComponent', () => {
  let component: AnnotationDisplayComponent;
  let fixture: ComponentFixture<AnnotationDisplayComponent>;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [HttpClient, HttpHandler, KeycloakService],
        declarations: [AnnotationDisplayComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AnnotationDisplayComponent);
    component = fixture.componentInstance;
    component.folder = {
      documentList: [],
      draftCreationDate: new Date(),
      dueDate: new Date(),
      finalDesk: undefined,
      id: 'id',
      metadata: undefined,
      name: '',
      originDesk: {id: 'id', name: 'name'},
      populatedMandatoryStepMetadata: {},
      readByCurrentUser: false,
      selected: false,
      sentToArchivesDate: new Date(),
      state: 'PENDING',
      stepList: [],
      subtype: undefined,
      type: undefined,
      visibility: 'PUBLIC'
    }
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
