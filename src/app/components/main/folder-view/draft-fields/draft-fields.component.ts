/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Folder } from '../../../../models/folder/folder';
import { NotificationsService } from '../../../../services/notifications.service';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { CommonMessages } from '../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { CrudOperation } from '../../../../services/crud-operation';
import { FolderViewMessages } from '../folder-view-messages';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { Action, State, FolderViewBlock } from '@libriciel/iparapheur-standard';
import { DeskDto } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-draft-fields',
  templateUrl: './draft-fields.component.html',
  styleUrls: ['./draft-fields.component.scss']
})
export class DraftFieldsComponent implements OnInit {


  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly folderViewBlock = FolderViewBlock;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;

  @Input() modificationsEnabled: boolean = true;
  @Input() tenantId: string;
  @Input() desk: DeskDto;
  @Input() folder: Folder;
  @Output() folderChange: EventEmitter<Folder> = new EventEmitter<Folder>();

  saveProcessing = false;
  isCollapsed = false;
  isDraft = false;

  // <editor-fold desc="LifeCycle">


  constructor(private folderService: FolderService,
              private notificationsService: NotificationsService) {}


  ngOnInit(): void {
    this.isDraft = this.isInDraftState();
  }


  // </editor-fold desc="LifeCycle">


  isDateBeforeNow(date: Date): boolean {
    return (date != null) && (new Date(date).getTime() < Date.now());
  }


  isInDraftState(): boolean {
    return this.folder.stepList
      .filter(step => step.action == Action.Start)
      .filter(step => step.state == State.Current)
      .length >= 1;
  }


  // <editor-fold desc="UI callbacks">


  onSaveButtonClicked() {
    this.saveProcessing = true;

    this.folderService
      .editFolder(this.tenantId, this.folder)
      .subscribe(() => {
          this.folderChange.emit(this.folder);
          this.notificationsService.showCrudMessage(CrudOperation.Update, this.folder);
        },
        error => this.notificationsService.showCrudMessage(CrudOperation.Update, this.folder, error.message, false)
      )
      .add(() => this.saveProcessing = false);
  }


  // </editor-fold desc="UI callbacks">


}
