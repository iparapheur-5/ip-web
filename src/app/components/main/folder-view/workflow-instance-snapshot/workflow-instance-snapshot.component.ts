/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Injector } from '@angular/core';
import { StepInstance, WorkflowActor } from '@libriciel/ls-composants/workflows';
import { IpWorkflowInstance } from '../../../../models/workflows/ip-workflow-instance';
import { IpStepInstance } from '../../../../models/workflows/ip-step-instance';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../shared/common-messages';
import { FolderViewMessages } from '../folder-view-messages';
import { isNotNullOrUndefined } from '../../../../utils/string-utils';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { StepValidationMode } from '../../../../models/workflows/ip-step-model';
import { DeskListPopupComponent } from '../../../../shared/components/desk-list-popup/desk-list-popup.component';
import { StepToActionPipe } from '../../../../shared/utils/step-to-action.pipe';
import { FolderUtils } from '../../../../utils/folder-utils';
import { Action, State, FolderViewBlock, DeskRepresentation, UserPreferencesDto } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';

@Component({
  selector: 'app-workflow-instance-snapshot',
  templateUrl: './workflow-instance-snapshot.component.html',
  styleUrls: ['./workflow-instance-snapshot.component.scss']
})
export class WorkflowInstanceSnapshotComponent {

  readonly messages = FolderViewMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly stateEnum = State;
  readonly actionEnum = Action;
  readonly secondaryActionEnum = SecondaryAction;
  readonly maxDisplayedValidators = 8;
  readonly validationModeEnum = StepValidationMode;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;
  readonly folderUtils = FolderUtils;
  readonly folderViewBlockEnum = FolderViewBlock;


  @Input() userPreferences: UserPreferencesDto;

  internalWorkflowInstance: IpWorkflowInstance;
  steps: IpStepInstance[][] = [];
  isCollapsed = false;


  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal) {}


  @Input()
  get workflowInstance() {return this.internalWorkflowInstance}


  set workflowInstance(newWorkflowInstance: IpWorkflowInstance) {
    this.internalWorkflowInstance = newWorkflowInstance;
    this.steps = this.groupSteps();
    console.log(this.steps)
  }


  // </editor-fold desc="LifeCycle">


  isRejected(step: StepInstance): boolean {
    return step.state === State.Rejected;
  }


  areParallel(steps: StepInstance[]): boolean {

    let validatedCount = steps
      .filter(step => step.state === State.Validated)
      .length;

    let unPerformedCount = steps
      .filter(step => !FolderUtils.isPassed(step))
      .length;

    // Pending secondOpinions adds one task to the list.
    // We have to count these, and retract them
    let currentSecondOpinions = steps
      .filter(step => StepToActionPipe.compute(step.type) === Action.SecondOpinion)
      .length;

    return (validatedCount + unPerformedCount - currentSecondOpinions) > 1;
  }


  countPending(steps: StepInstance[]): number {
    return steps
      .filter(step => FolderUtils.isActive(step))
      .length;
  }


  flatMapStepValidators(steps: IpStepInstance[]): DeskRepresentation[] {
    let validators: DeskRepresentation[] = [];
    // TODO : Use a flatmap on es2019 compliance
    steps.map(s => s.validators.forEach(s => validators.push(s)));
    return validators;
  }


  showValidators(validators: WorkflowActor[] | DeskRepresentation[]) {
    this.modalService
      .open(
        DeskListPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: DeskListPopupComponent.INJECTABLE_DESK_LIST_KEY, useValue: validators},
              {provide: DeskListPopupComponent.INJECTABLE_USER_PREFERENCES, useValue: this.userPreferences},
            ]
          }),
          size: 'lg'
        }
      )
      .result
      .then(
        result => console.log(result),
        () => {/* dismissed */}
      );
  }


  /**
   * The creation workflow index is always 1.
   * The validation workflow is 2 or more (+1 for every chain action).
   */
  hasCreationWorkflow(): boolean {
    return this.steps.some(l => l.some(s => s.workflowIndex == 1));
  }


  /**
   * By convenience, we display the draft step in the creation workflow.
   * Similarly, we display the final step as part of the validation workflow.
   * @param step
   */
  isWorkflowFirstStep(step: IpStepInstance): boolean {
    const isFirstStep = step?.stepIndex === 0;
    const isCreationWorkflow = step?.workflowIndex == 0;
    const isFistValidationWorkflow = step?.workflowIndex == 2;
    return isFirstStep && (isCreationWorkflow || isFistValidationWorkflow);
  }


  private groupSteps(): IpStepInstance[][] {

    let result: IpStepInstance[][] = [];
    for (let step of this.workflowInstance.steps as IpStepInstance[]) {

      let lastExistingTask = result[result.length - 1]?.[0];
      let isSameWorkflow = isNotNullOrUndefined(step.workflowIndex) && (lastExistingTask?.workflowIndex === step.workflowIndex);
      let isSameIndex = isNotNullOrUndefined(step.stepIndex) && (lastExistingTask?.stepIndex === step.stepIndex);

      if (isSameWorkflow && isSameIndex) {
        result[result.length - 1].push(step);
      } else {
        result.push([step]);
      }
    }

    return result;
  }


}
