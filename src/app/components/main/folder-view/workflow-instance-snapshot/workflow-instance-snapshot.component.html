<!--
  ~ iparapheur Web
  ~ Copyright (C) 2019-2023 Libriciel SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<table aria-label="workflow steps" class="border table table-borderless table-sm shadow-sm bg-white table-fixed">
  <thead class="border">
  <tr>
    <th scope="col" class="icon-column text-left">
      <fa-icon
        class="pointer text-primary"
        [icon]="isCollapsed ? plusSquareIcon : minusSquareIcon"
        (click)="isCollapsed = !isCollapsed"
        [fixedWidth]="true">
      </fa-icon>
    </th>
    <th scope="col" class="icon-column">
      <span
        id="card-title"
        [innerText]="folderViewBlockEnum.Workflow | folderViewBlockName">
      </span>
    </th>
    <th scope="col"></th>
  </tr>
  </thead>
  <tbody [ngClass]="{'collapse': isCollapsed}">

  <ng-container *ngFor="let workflowSteps of steps">

    <!-- Label, if needed, on first steps -->
    <tr *ngIf="hasCreationWorkflow() && isWorkflowFirstStep(workflowSteps[0])">
      <td class="col-status-icon-fixed-size"></td>
      <td class="col-status-icon-fixed-size"></td>
      <td class="col align-baseline">
        <span
          class="text-secondary"
          [innerText]="workflowSteps[0]?.workflowIndex === 0 ? commonMessages.CREATION_WORKFLOW : commonMessages.VALIDATION_WORKFLOW">
        </span>
      </td>
    </tr>

    <!-- OR (and singles) tasks -->
    <!-- Special case : already performed AND tasks are flattened too -->
    <ng-container *ngIf="!areParallel(workflowSteps) || folderUtils.arePassed(workflowSteps)">

      <tr
        *ngFor="let step of workflowSteps"
        [ngClass]="{
          'font-weight-bold': folderUtils.isActive(step),
          'text-success': folderUtils.isPassed(step) && !isRejected(step),
          'text-danger': isRejected(step)
        }">
        <!-- Icons -->
        <td class="col-status-icon-fixed-size align-baseline text-right">
          <fa-icon
            *ngIf="folderUtils.isPassed(step) || folderUtils.isActive(step)"
            [icon]="step.state | stepStateIcon"
            [ngbTooltip]="step.state | stateName: false"
            [fixedWidth]="true">
          </fa-icon>
        </td>

        <td class="col-status-icon-fixed-size align-baseline pb-0">
          <fa-icon
            [icon]="step.type | actionIcon"
            [ngbTooltip]="step.type | actionName"
            [fixedWidth]="true">
          </fa-icon>
          <div
            class="border-left"
            style="margin-left: 0.563rem; margin-top: 0.4rem; height: 0{{ folderUtils.isPassed(step) ? 2.15 : 0 }}rem">
          </div>
        </td>

        <!-- Text -->
        <td class="col align-baseline">

          <!-- Tooltip content to display for Multi-validators viewing -->
          <ng-template #multiValidatorTooltip>
            <div class="validator-restricted-list">
              <div
                *ngFor="let validator of step.validators.slice(0, maxDisplayedValidators)"
                class="tooltip-validator overflow-hidden text-overflow-ellipsis">
                <span [innerText]="validator.name | namePipe: commonMessages.DELETED_DESK_LABEL"></span>
                <span *ngIf="step.validators.length > maxDisplayedValidators">…</span>
              </div>
            </div>
          </ng-template>

          <!-- Steps already treated (validated or rejected) -->
          <div *ngIf="folderUtils.isPassed(step)">
            <div
              class="overflow-hidden text-overflow-ellipsis"
              *ngIf="step.validators.length == 1"
              [innerText]="step.validators[0]?.name | namePipe: commonMessages.DELETED_DESK_LABEL">
            </div>

            <div
              class="pointer"
              *ngIf="step.validators.length > 1"
              [ngbTooltip]="multiValidatorTooltip"
              [openDelay]="300"
              tooltipClass="validator-tooltip"
              (click)="showValidators(step.validators)"
              [innerText]="messages.multipleValidatorsStatus(step.validators.length, 0, false)">
            </div>

            <div
              *ngIf="userPreferences.showAdminIds"
              class="text-secondary text-size-small text-monospace"
              [innerText]="commonMessages.ID + '\u00a0: ' + step.id">
            </div>
          </div>

          <!-- Steps that have not been treated yet -->
          <div *ngIf="!folderUtils.isPassed(step)">

            <div
              class="overflow-hidden text-overflow-ellipsis"
              *ngIf="step.validators.length == 1"
              [innerText]="step.validators[0]?.name | namePipe: commonMessages.DELETED_DESK_LABEL">
            </div>

            <div
              class="pointer"
              *ngIf="step.validators.length > 1"
              [ngbTooltip]="multiValidatorTooltip"
              [openDelay]="300"
              tooltipClass="validator-tooltip"
              (click)="showValidators(step.validators)"
              [innerText]="messages.multipleValidatorsStatus(step.validators.length, step.validators.length, folderUtils.isActive(step))">
            </div>

            <div
              *ngIf="step.validators.length > 1"
              class="small text-secondary"
              [innerText]="validationModeEnum.Or | validationModeName">
            </div>

          </div>

          <div
            *ngIf="folderUtils.isActive(step) && userPreferences.showAdminIds"
            class="text-secondary text-size-small text-monospace font-weight-normal"
            [innerText]="commonMessages.ID + '\u00a0: ' + step.id">
          </div>

          <div
            *ngIf="folderUtils.isPassed(step)"
            class="small text-secondary">
            <div
              class="overflow-hidden text-overflow-ellipsis"
              *ngIf="!!step.delegatedByDesk"
              [innerText]="messages.delegatedByLabel(step.delegatedByDesk)">
            </div>
            <div
              class="overflow-hidden text-overflow-ellipsis"
              *ngIf="step.validatedBy.length == 1"
              [innerText]="messages.validatorLabel(step.validatedBy[0])">
            </div>
            <div
              class="overflow-hidden text-overflow-ellipsis"
              *ngIf="step.validatedBy.length > 1"
              [innerText]="messages.validatorsLabel(step.validatedBy)">
            </div>
            <div [innerText]="messages.dateLabel(step)"></div>
          </div>
        </td>
      </tr>
    </ng-container>

    <!-- Upcoming/Current AND tasks -->
    <ng-container *ngIf="(!folderUtils.arePassed(workflowSteps)) && areParallel(workflowSteps)">

      <tr [ngClass]="{'font-weight-bold': folderUtils.areActive(workflowSteps)}">

        <!-- Icons -->
        <td class="col-status-icon-fixed-size align-baseline text-right">
          <fa-icon
            *ngIf="folderUtils.areActive(workflowSteps)"
            [icon]="stateEnum.Current | stepStateIcon"
            [ngbTooltip]="stateEnum.Current | stateName: false"
            [fixedWidth]="true">
          </fa-icon>
        </td>

        <td class="col-action-icon-fixed-size align-baseline pb-0 overflow-hidden">
          <fa-icon
            [icon]="workflowSteps[0]?.type | actionIcon"
            [ngbTooltip]="workflowSteps[0]?.type | actionName"
            [fixedWidth]="true">
          </fa-icon>
          <div
            class="border-left"
            style="margin-left: 0.563rem; margin-top: 0.4rem; height: 0.80rem">
          </div>
        </td>

        <!-- Text -->
        <td class="col align-baseline">
          <!-- Tooltip content to display for Multi-desks viewing -->
          <ng-template #multiDeskTooltip>
            <div class="validator-restricted-list">
              <div
                *ngFor="let desk of flatMapStepValidators(workflowSteps).slice(0, maxDisplayedValidators)"
                class="tooltip-validator overflow-hidden text-overflow-ellipsis"
                [innerText]="desk.name | namePipe: commonMessages.DELETED_DESK_LABEL">
              </div>
              <div *ngIf="workflowSteps.length > maxDisplayedValidators">…</div>
            </div>
          </ng-template>

          <div
            class="pointer"
            [ngbTooltip]="multiDeskTooltip"
            [openDelay]="300"
            tooltipClass="validator-tooltip"
            (click)="showValidators(flatMapStepValidators(workflowSteps))"
            [innerText]="messages.multipleValidatorsStatus(workflowSteps.length, countPending(workflowSteps), folderUtils.areActive(workflowSteps))">
          </div>

          <div
            class="small text-secondary"
            [innerText]="validationModeEnum.And | validationModeName">
          </div>
        </td>
      </tr>
    </ng-container>
  </ng-container>

  <!-- Final steps -->
  <tr>
    <td class="col-status-icon-fixed-size align-baseline text-right">
      <fa-icon
        *ngIf="workflowInstance.isOver"
        [icon]="stateEnum.Current | stepStateIcon"
        [ngbTooltip]="stateEnum.Current | stateName: false"
        [fixedWidth]="true">
      </fa-icon>
    </td>
    <td class="col-action-icon-fixed-size align-baseline">
      <fa-icon
        [icon]="secondaryActionEnum.End | actionIcon"
        [ngbTooltip]="secondaryActionEnum.End | actionName"
        [fixedWidth]="true">
      </fa-icon>
    </td>
    <td class="col align-baseline overflow-hidden text-overflow-ellipsis">
      <span [innerText]="workflowInstance.finalDesk?.name | namePipe: commonMessages.DELETED_DESK_LABEL"></span>
    </td>
  </tr>

  </tbody>
</table>
