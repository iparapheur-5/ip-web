<!--
  ~ iparapheur Web
  ~ Copyright (C) 2019-2023 Libriciel SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<table aria-label="document list" class="table table-sm table-striped table-borderless border bg-white shadow-sm">
  <thead class="border">
  <tr>
    <th scope="col" class="icon-column">
      <fa-icon
        class="pointer text-primary"
        [icon]="isCollapsed ? plusSquareIcon : minusSquareIcon"
        (click)="isCollapsed = !isCollapsed"
        [fixedWidth]="true">
      </fa-icon>
    </th>
    <th
      scope="colgroup"
      colspan="2"
      [innerText]="currentFolderViewBlock | folderViewBlockName">
    </th>
    <th scope="col" class="col-actions-fixed-size text-right">
      <label
        [ngClass]="{'opacity-0 cursor-default' : !isEditable || !canAddDocument}"
        for="addDocumentFileInput{{ displayMainDocuments ? 'main' : '' }}"
        class="p-0 m-0 text-primary pointer">
        <fa-icon [icon]="commonIcons.ADD_ICON"></fa-icon>
      </label>
      <input
        id="addDocumentFileInput{{ displayMainDocuments ? 'main' : '' }}"
        type="file"
        [disabled]="!isEditable || !canAddDocument"
        [accept]="selectableMediaTypes.join(' ')"
        (change)="addDocument($event)"
        hidden>
    </th>
  </tr>
  </thead>
  <tbody [ngClass]="isCollapsed === true ? 'collapse' : ''">
  <tr *ngFor="let document of getDocumentList()"
      class="clickable-row show-icons-on-hover"
      [ngClass]="{'table-primary' : document === currentDocument}"
      (click)="onRowClicked(document)">

    <td class="icon-column">
      <fa-icon
        class="text-primary"
        *ngIf="document.mediaType | documentIcon"
        [icon]="document.mediaType | documentIcon"
        [fixedWidth]="true">
      </fa-icon>
    </td>

    <td
      class="overflow-hidden text-truncate"
      [ngClass]="{
        'col-name-reduced': userPreferences?.showAdminIds,
        'col-name': !userPreferences?.showAdminIds
      }"
      [colSpan]="userPreferences?.showAdminIds ? '1' : '2'">
      <span [innerText]="document.name | namePipe"></span>
    </td>

    <td
      *ngIf="userPreferences?.showAdminIds ?? false"
      class="col-id-unsortable-fixed-size">
      <span
        class="text-monospace text-size-small"
        [innerText]="document.id">
      </span>
    </td>

    <td>
      <span
        ngbDropdown
        container="body"
        #myDrop="ngbDropdown">

        <button
          class="btn"
          type="button"
          id="dropdownManual"
          ngbDropdownAnchor
          (focus)="myDrop.open()">
          <fa-icon
            class="text-primary"
            [icon]="faEllipsisV">
          </fa-icon>
        </button>

        <div
          ngbDropdownMenu
          aria-labelledby="dropdownManual">

          <a
            ngbDropdownItem
            *ngIf="isEditable && (!displayMainDocuments || (getDocumentList().length > 1)) && canDeleteDocument(document)"
            class="pointer"
            (click)="deleteDocument($event, document)">
            <fa-icon
              class="mr-2 delete-icon-secondary"
              [title]="commonMessages.DELETE"
              [icon]="commonIcons.DELETE_ICON"
              [fixedWidth]="true">
            </fa-icon>
            <span [innerText]="messages.DELETE_DOCUMENT"></span>
          </a>

          <a
            *ngIf="isEditable && displayMainDocuments && (getDocumentList().length <= 1)"
            ngbDropdownItem
            class="pointer">
            <label
              class="my-0 py-0"
              for="replaceDocumentFileInput{{ document.id }}">
              <fa-icon
                class="mr-2 text-secondary"
                [title]="commonMessages.REPLACE"
                [icon]="faSyncAlt"
                [fixedWidth]="true">
              </fa-icon>
              <span [innerText]="messages.REPLACE_DOCUMENT"></span>
            </label>
            <input
              id="replaceDocumentFileInput{{ document.id }}"
              type="file"
              [accept]="selectableMediaTypes.join(' ')"
              (change)="replaceDocument(document, $event)"
              hidden>
          </a>

          <a
            (click)="downloadFile(document)"
            [download]="document.name"
            ngbDropdownItem
            class="pointer">
            <fa-icon
              class="mr-2 text-secondary"
              [icon]="faDownload"
              [fixedWidth]="true">
            </fa-icon>
            <span [innerText]="messages.DOWNLOAD_ORIGINAL_DOCUMENT"></span>
          </a>

          <ng-container *ngIf="displayMainDocuments && showDetachedSignatures">

            <!-- No need for a label in some cases -->
            <label
              class="mx-3 mt-2 font-weight-bold"
              *ngIf="(!isInValidationWorkflowFn(folder)) || (document.detachedSignatures.length > 0)"
              [innerText]="messages.DETACHED_SIGNATURE">
            </label>

            <ng-container *ngIf="isInValidationWorkflowFn(folder)">
              <!-- If the folder has be started, the default action on detached signatures is a regular download -->
              <a
                ngbDropdownItem
                *ngFor="let detachedSignature of document.detachedSignatures"
                (click)="downloadFile(document, detachedSignature)"
                [download]="detachedSignature.name"
                class="pointer">
                <fa-icon
                  class="mr-2 text-secondary"
                  [icon]="faDownload"
                  [fixedWidth]="true">
                </fa-icon>
                {{ detachedSignature?.name | namePipe }}
              </a>
            </ng-container>

            <ng-container *ngIf="!isInValidationWorkflowFn(folder)">

              <!-- If the folder is still a draft, the default action on detached signatures is a delete -->
              <a
                ngbDropdownItem
                *ngFor="let detachedSignature of document.detachedSignatures"
                (click)="deleteDetachedSignature($event, document.id, detachedSignature)"
                class="pointer">
                <fa-icon
                  class="mr-2 delete-icon-secondary"
                  [icon]="commonIcons.DELETE_ICON"
                  [fixedWidth]="true">
                </fa-icon>
                {{ detachedSignature?.name | namePipe }}
              </a>

              <!-- And if the folder is still a draft, we can still add a signature -->
              <a
                *ngIf="!isInValidationWorkflow"
                ngbDropdownItem
                class="pointer">
                <label
                  class="my-0 py-0"
                  for="addDetachedSignatureFileInput{{ document.id }}">
                  <fa-icon
                    class="mr-2 text-secondary"
                    [icon]="commonIcons.ADD_ICON"
                    [fixedWidth]="true">
                  </fa-icon>
                  <span [innerText]="messages.ADD_DETACHED_SIGNATURE"></span>
                </label>
                <input
                  id="addDetachedSignatureFileInput{{ document.id }}"
                  type="file"
                  [accept]="mimeTypesDetachedSignature"
                  (change)="addDetachedSignature($event, document)"
                  hidden>
              </a>

            </ng-container>
          </ng-container>

        </div>
      </span>
      <fa-icon
        [ngClass]="{'opacity-0 cursor-default' : !canBeOpenedInPdfViewer(document)}"
        class="text-primary pointer"
        [icon]="openDocInNewWindowIcon"
        (click)="openDocumentInNewWindow($event, document)"
        [fixedWidth]="true">
      </fa-icon>
      <fa-icon
        [ngClass]="{'opacity-0 cursor-default' : !canBeOpenedInPdfViewer(document)}"
        class="ml-2 text-primary pointer"
        [icon]="rightChevronIcon"
        [fixedWidth]="true">
      </fa-icon>
    </td>
  </tr>
  </tbody>
  <tbody
    app-list-loader
    [data]="getDocumentList()"
    [loading]="false"
    [colspan]="colspan"
    [emptyMessage]="commonMessages.EMPTY_DOCUMENT_LIST">
  </tbody>
</table>
