/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectorRef, AfterViewInit, OnInit } from '@angular/core';
import { Document } from '../../../../models/document';
import { faChevronRight, faExternalLinkAlt, faEllipsisV, faDownload, faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { faPlusSquare, faMinusSquare } from '@fortawesome/free-regular-svg-icons';
import { Folder } from '../../../../models/folder/folder';
import { DocumentService } from '../../../../services/ip-core/document.service';
import { NotificationsService } from '../../../../services/notifications.service';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../../shared/common-messages';
import { FolderViewMessages } from '../folder-view-messages';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { DetachedSignature } from '../../../../models/signature/detached-signature';
import { FolderUtils } from '../../../../utils/folder-utils';
import { MIME_TYPES_DETACHED_SIGNATURES } from '../../../../utils/string-utils';
import { TypologyUtils } from '../../../../utils/typology-utils';
import { FileService } from '../../../../services/file.service';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { FolderViewBlock, UserPreferencesDto, TypologyService } from '@libriciel/iparapheur-standard';
import { DeskDto } from '@libriciel/iparapheur-provisioning';


@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, AfterViewInit {

  readonly mimeTypesDetachedSignature = MIME_TYPES_DETACHED_SIGNATURES;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly faSyncAlt = faSyncAlt;
  readonly rightChevronIcon = faChevronRight;
  readonly openDocInNewWindowIcon = faExternalLinkAlt;
  readonly faEllipsisV = faEllipsisV;
  readonly faDownload = faDownload;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;
  readonly isInValidationWorkflowFn = FolderUtils.isInValidationWorkflow;
  readonly selectableMediaTypes: string[] = [
    'application/pdf', 'text/pdf',
    'application/vnd.oasis.opendocument.text', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  ];

  @Input() folder: Folder;
  @Input() currentDesk: DeskDto;
  @Input() currentDocument: Document;
  @Input() isEditable: boolean = false;
  @Input() currentFolderViewBlock: FolderViewBlock = FolderViewBlock.AnnexeDocuments;
  @Input() userPreferences: UserPreferencesDto;
  @Output() documentSelected = new EventEmitter<Document>();

  tenantId: string;
  isCollapsed: boolean;
  showDetachedSignatures: boolean;
  isInValidationWorkflow: boolean;
  canAddDocument: boolean = false;
  colspan: number;
  displayMainDocuments: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(private folderService: FolderService,
              private typologyService: TypologyService,
              private fileService: FileService,
              private legacyUserService: LegacyUserService,
              private globalPopupService: GlobalPopupService,
              private documentService: DocumentService,
              public route: ActivatedRoute,
              private notificationsService: NotificationsService,
              public cdr: ChangeDetectorRef) { }


  ngOnInit(): void {
    this.displayMainDocuments = this.currentFolderViewBlock === FolderViewBlock.MainDocuments;
    let isAnnexe: boolean = !this.displayMainDocuments;
    let isMultiMainDocument = this.folder?.subtype?.multiDocuments;
    let isMainActionEnabled = this.currentDesk.actionAllowed && !!this.folder?.type && !!this.folder.subtype
    this.canAddDocument = isMainActionEnabled && (isAnnexe || isMultiMainDocument);
  }


  // TODO : check if some of this can be done earlier, in the ngOnInit
  ngAfterViewInit(): void {
    this.tenantId = this.route.snapshot.paramMap.get('tenantId');
    this.colspan = 4 + (this.userPreferences.showAdminIds ? 1 : 0)
    this.setDetachedSignatureRights();
  }


  // </editor-fold desc="LifeCycle">


  setDetachedSignatureRights(): void {
    this.isInValidationWorkflow = FolderUtils.isInValidationWorkflow(this.folder);

    const isEnvelopedParsedSignatureFormat: boolean = TypologyUtils.isEnvelopedSignatureFormat(this.folder?.type?.signatureFormat);

    // If no detached signature were added before validation workflow, we hide the menu.
    if (this.isInValidationWorkflow && isEnvelopedParsedSignatureFormat) {
      this.showDetachedSignatures = false;
      return;
    }

    // FIXME fetch the unparsed signature format from getFolder
    // If not, the not parsed signature format might be "AUTO"
    this.typologyService.getType(this.tenantId, this.folder?.type?.id)
      .subscribe(newType => {
        const isEnvelopedSignatureFormat = TypologyUtils.isEnvelopedSignatureFormat(newType.signatureFormat);
        this.showDetachedSignatures = !isEnvelopedSignatureFormat;
      })
  }

  getDocumentList(): Document[] {
    return this.folder
      ? this.folder.documentList
        .filter(doc => this.displayMainDocuments === !!doc.isMainDocument)
        .sort((a: Document, b: Document): number => a.index - b.index)
      : [];
  }


  onRowClicked(document: Document) {
    if (this.currentDocument?.id !== document.id && this.canBeOpenedInPdfViewer(document)) {
      this.documentSelected.emit(document);
    }
  }


  canBeOpenedInPdfViewer(document: Document): boolean {
    return this.selectableMediaTypes.includes(document.mediaType);
  }


  addDocument(event: any) {
    for (const file of event.target.files) {
      this.documentService
        .addDocument(this.tenantId, this.folder, this.displayMainDocuments, file.name, file)
        .subscribe(
          () => {
            this.notificationsService.showSuccessMessage(FolderViewMessages.ADD_DOCUMENT_SUCCESS);
            this.refreshDocumentList();
          },
          error => this.notificationsService.showErrorMessage(FolderViewMessages.ADD_DOCUMENT_ERROR, error.message)
        );
    }
  }


  replaceDocument(document: Document, event: any) {
    for (const file of event.target.files) {
      this.documentService
        .updateDocument(this.tenantId, this.folder, document, file.name, file)
        .subscribe(
          () => {
            this.notificationsService.showSuccessMessage(FolderViewMessages.REPLACE_DOCUMENT_SUCCESS);
            // TODO : a soft refresh
            location.reload();
          },
          error => this.notificationsService.showErrorMessage(FolderViewMessages.REPLACE_DOCUMENT_ERROR, error.message)
        );
    }
  }


  addDetachedSignature(event: any, document: Document) {
    for (const file of event.target.files) {
      this.documentService
        .addDetachedSignature(this.tenantId, this.folder.id, document.id, file.name, file, this.currentDesk)
        .subscribe(
          () => {
            this.notificationsService.showSuccessMessage(FolderViewMessages.ADD_DETACHED_SIGNATURE_SUCCESS);
            this.refreshDocumentList();
          },
          error => this.notificationsService.showErrorMessage(FolderViewMessages.ADD_DETACHED_SIGNATURE_ERROR, error.message)
        );
    }
  }


  deleteDetachedSignature(event: any, documentId: string, detachedSignature: DetachedSignature) {
    event.stopPropagation();
    this.globalPopupService
      .showDeleteValidationPopup(FolderViewMessages.detachedSignatureDeleteValidationPopup(detachedSignature.name))
      .then(
        () => this.doDeleteDetachedSignature(documentId, detachedSignature.id),
        () => {/* Dismissed */}
      );
  }

  downloadFile(doc: Document, detachedSignature?: DetachedSignature): void {
    let documentName = detachedSignature ? detachedSignature.name : doc.name;

    let url: string = detachedSignature
      ? this.documentService.detachedSignatureUrl(this.tenantId, this.folder.id, doc.id, detachedSignature.id)
      : this.documentService.documentUrl(this.tenantId, this.folder.id, doc.id);

    this.fileService.downloadFileWithUrl(url, documentName)
      .subscribe(
        () => {/* nothing here */},
        error => this.notificationsService.showErrorMessage(
          FolderViewMessages.errorDownloadingFile(!!detachedSignature, documentName), error
        )
      );
  }


  doDeleteDetachedSignature(documentId: string, detachedSignatureId: string): void {
    this.documentService
      .deleteDetachedSignature(this.tenantId, this.folder.id, documentId, detachedSignatureId)
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(FolderViewMessages.REMOVE_DETACHED_SIGNATURE_SUCCESS);
          this.refreshDocumentList();
        },
        error => this.notificationsService.showErrorMessage(FolderViewMessages.REMOVE_DETACHED_SIGNATURE_ERROR, error.message)
      );
  }

  canDeleteDocument(document: Document): boolean {
    return !!document.deletable || !this.isInValidationWorkflow;
  }

  deleteDocument(event: any, document: Document) {
    event.stopPropagation();

    if (this.folder.documentList.length <= 1) {
      this.notificationsService.showErrorMessage(FolderViewMessages.CANNOT_DELETE_THE_LAST_DOCUMENT);
      return;
    }

    this.globalPopupService
      .showDeleteValidationPopup(FolderViewMessages.deleteDocumentValidationPopup(document.name))
      .then(
        () => this.doDeleteDocument(document),
        () => {/* Dismissed */}
      );
  }


  doDeleteDocument(document): void {
    this.documentService
      .deleteDocument(this.tenantId, this.folder, document)
      .subscribe(
        () => {
          this.notificationsService.showSuccessMessage(FolderViewMessages.DELETE_DOCUMENT_SUCCESS);
          this.refreshDocumentList();
        },
        error => this.notificationsService.showErrorMessage(FolderViewMessages.DELETE_DOCUMENT_ERROR, error.message)
      );
  }


  openDocumentInNewWindow(event: any, document: Document) {
    if (!this.canBeOpenedInPdfViewer(document)) return;

    this.documentService.getDocument(this.tenantId, this.folder.id, document.id)
      .subscribe(data => {
        let blob = new Blob([data], {type: document.mediaType});
        const url = window.URL.createObjectURL(blob);
        window.open(url, '_blank');
        URL.revokeObjectURL(url);
      });
  }


  refreshDocumentList() {
    this.folderService
      .getFolder(this.tenantId, this.folder.id)
      .subscribe(folderRetrieved => {
          this.folder.documentList = folderRetrieved.documentList;

          // Refreshing selection state, and swapping to the first doc,
          // if the previous selected one is now missing
          if (!this.folder.documentList.includes(this.currentDocument) && this.displayMainDocuments === true) {
            this.onRowClicked(this.folder.documentList[0]);
          }
          this.cdr.detectChanges();
        },
        error => this.notificationsService.showErrorMessage(FolderViewMessages.ERROR_LOADING_WORKFLOW, error)
      );
  }


}
