/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { HistoryPopupComponent } from './history-popup.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Folder } from '../../../models/folder/folder';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

describe('HistoryPopupComponent', () => {


  let component: HistoryPopupComponent;
  let fixture: ComponentFixture<HistoryPopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: HistoryPopupComponent.injectableFolderKey, useValue: new Folder()},
          {provide: HistoryPopupComponent.injectableTenantIdKey, useValue: 'tenant01'},
          {provide: HistoryPopupComponent.injectableDeskIdKey, useValue: {}}
        ],
        declarations: [HistoryPopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });


});
