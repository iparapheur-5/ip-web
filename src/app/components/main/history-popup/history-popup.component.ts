/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Task } from 'src/app/models/task';
import { Folder } from '../../../models/folder/folder';
import { FolderService } from '../../../services/ip-core/folder.service';
import { NotificationsService } from '../../../services/notifications.service';
import { CrudOperation } from '../../../services/crud-operation';
import { CommonMessages } from '../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-composants';
import { HistoryPopupMessages } from '../history-popup-messages';
import { ExternalState } from '../../../models/external-state.enum';
import { ActionPopupMessages } from '../folder-view/action-popups/action-popup-messages';
import { SecondaryAction } from '../../../shared/models/secondary-action.enum';
import { FolderUtils } from '../../../utils/folder-utils';

@Component({
  selector: 'app-history-popup',
  templateUrl: './history-popup.component.html',
  styleUrls: ['./history-popup.component.scss']
})
export class HistoryPopupComponent implements OnInit {

  public static readonly injectableFolderKey: string = 'folder';
  public static readonly injectableDeskIdKey: string = 'deskId';
  public static readonly injectableTenantIdKey: string = 'tenantId';

  readonly secondaryActionEnum = SecondaryAction;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly messages = HistoryPopupMessages;

  tasks: Task[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              private folderService: FolderService,
              private notificationsService: NotificationsService,
              @Inject(HistoryPopupComponent.injectableFolderKey) public folder: Folder,
              @Inject(HistoryPopupComponent.injectableDeskIdKey) public deskId: string,
              @Inject(HistoryPopupComponent.injectableTenantIdKey) public tenantId: string) { }


  ngOnInit(): void {
    this.folderService
      .getHistoryTasks(this.tenantId, this.deskId, this.folder)
      .subscribe(
        result => this.tasks = result,
        error => this.notificationsService.showCrudMessage(CrudOperation.Read, this.folder, error.message, false)
      );
  }


  getExternalTaskPrefix(task: Task): string {
    if (FolderUtils.EXTERNAL_ACTIONS.includes(task.action)) {
      return task.externalState === ExternalState.Active
        ? ActionPopupMessages.RESULT_PREFIX
        : ActionPopupMessages.SEND_PREFIX;
    }
    return '';
  }


  // </editor-fold desc="LifeCycle">


}
