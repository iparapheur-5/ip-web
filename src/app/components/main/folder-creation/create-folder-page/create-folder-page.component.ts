/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from '../../../../services/notifications.service';
import { WorkflowService as LegacyWorkflowService } from '../../../../services/ip-core/workflow.service';
import { faSyncAlt, faInfoCircle, faCaretDown, faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { NgModel, FormControl } from '@angular/forms';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { FolderCreationMessages } from '../folder-creation-messages';
import { CommonMessages } from '../../../../shared/common-messages';
import { isNotNullOrUndefined, isEmpty, MIME_TYPES_DETACHED_SIGNATURES, MIME_TYPES_PRINCIPAL_DEFAULT, MIME_TYPES_ANNEXES_DEFAULT, MIME_TYPES_PRINCIPAL_ACTES, MIME_TYPES_ANNEXES_ACTES, MIME_TYPES_PRINCIPAL_HELIOS, getFirstErrorMessage } from '../../../../utils/string-utils';
import { TypologyEntity } from '../../../../models/typology-entity';
import { Config } from '../../../../config';
import { Observable } from 'rxjs';
import { Folder } from '../../../../models/folder/folder';
import { tap, map, concatMap, catchError } from 'rxjs/operators';
import { FolderUtils } from '../../../../utils/folder-utils';
import { GLOBAL_SETTINGS } from '../../../../shared/models/global-settings';
import { GlobalSearchMessages } from '../../../header/global-search-bar/global-search-messages';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateAndStartFolderPopupComponent } from '../create-and-start-folder-popup/create-and-start-folder-popup.component';
import { TypologyService as StandardTypologyService, SignatureFormat, Action, SignatureProtocol, FolderVisibility, WorkflowService, DeskService, TypeDto, SubtypeDto, TypeRepresentation, PageTypeRepresentation } from '@libriciel/iparapheur-standard';
import { CreateFolderFileWrapper } from '../../../../models/folder/create-folder-file-wrapper';
import { CdkDropList } from '@angular/cdk/drag-drop';
import { WorkflowDefinition } from '../../../../models/workflow-definition';
import { WorkflowUtils } from '../../../../shared/utils/workflow-utils';
import { PaginatedResult } from '../../../../models/paginated-result';
import { CreateFolderRequest } from '../../../../models/create-folder-request';
import { SelectedDeskService } from '../../../../services/resolvers/desk/selected-desk.service';
import { CustomMap } from '../../../../shared/models/custom-types';
import { WebsocketService } from '../../../../services/websockets/websocket.service';
import { DeskDto, SubtypeRepresentation, PageSubtypeRepresentation } from '@libriciel/iparapheur-provisioning';
import { TypologyService } from '../../../../services/ip-core/typology.service';

@Component({
  selector: 'app-create-folder-page',
  templateUrl: './create-folder-page.component.html',
  styleUrls: ['./create-folder-page.component.scss']
})
export class CreateFolderPageComponent implements OnInit {

  readonly messages = FolderCreationMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly styles = Style;

  readonly signatureFormatEnum = SignatureFormat;
  readonly protocolEnum = SignatureProtocol;
  readonly actionEnum = Action;
  readonly faEllipsisV = faEllipsisV;
  readonly caretDownIcon = faCaretDown;
  readonly globalSettings = GLOBAL_SETTINGS;
  readonly infoCircleIcon = faInfoCircle;
  readonly faSyncAlt = faSyncAlt;
  readonly mimeTypesDetachedSignature = MIME_TYPES_DETACHED_SIGNATURES;

  readonly getMaxDocumentsCountFn = FolderUtils.getMaxDocumentsCount;
  readonly mainFileListId: string = 'mainFileListId';
  readonly annexeFileListId: string = 'annexeFileListId';

  @ViewChild('nameInput') nameInput: FormControl;
  @ViewChild('annexeFileInput') annexeFileInput: NgModel;
  @ViewChild('annexeFilesList') annexeFilesList: CdkDropList;
  @ViewChild('mainFileInput') mainFileInput: NgModel;
  @ViewChild('mainFilesList') mainFilesList: CdkDropList;

  acceptedMimeTypesPrincipal = MIME_TYPES_PRINCIPAL_DEFAULT;
  acceptedMimeTypesAnnexe = MIME_TYPES_ANNEXES_DEFAULT;

  tenantId: string;
  currentDesk: DeskDto;
  isProcessing: boolean = false;

  // FORM Documents
  selectedMainDocumentsFiles: CreateFolderFileWrapper[] = [];
  selectedAnnexesFiles: CreateFolderFileWrapper[] = [];

  // FORM Workflow
  selectedType: TypologyEntity;
  parsedSelectedType: TypeDto;
  selectedSubtype: SubtypeDto;
  selectedCreationWorkflowId = '';
  selectedValidationWorkflowId = '';
  creationWorkflowDefinition: WorkflowDefinition;
  validationWorkflowDefinition: WorkflowDefinition;
  variableDeskStepCount: number = 0;
  variableDesksIds: CustomMap = {};
  metadata: CustomMap = {};

  // FORM Information
  name: string;
  selectedVisibility: FolderVisibility = GLOBAL_SETTINGS.user.folderDefaultVisibility;
  limitDate: Date;

  retrieveTypesFn = (page: number, pageSize: number) => this.retrieveTypes(page, pageSize);
  retrieveSubtypesFn = (page: number, pageSize: number) => this.retrieveSubtypes(page, pageSize);


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public legacyWorkflowService: LegacyWorkflowService,
              public workflowService: WorkflowService,
              public typologyService: TypologyService,
              public standardTypologyService: StandardTypologyService,
              public deskService: DeskService,
              private selectedDeskService: SelectedDeskService,
              public route: ActivatedRoute,
              public modalService: NgbModal,
              public router: Router,
              private websocketService: WebsocketService) { }


  ngOnInit(): void {
    this.route.params.subscribe(params => this.tenantId = params['tenantId']);
    this.route.data.subscribe(data => this.currentDesk = data.desk);

    if (this.globalSettings.user.folderVisibility.length === 1) {
      this.selectedVisibility = this.globalSettings.user.folderVisibility[0];
    }
  }


  // </editor-fold desc="LifeCycle">


  onTypeSelectionChanged() {

    if (!this.selectedType) return;

    this.selectedSubtype = null;
    this.parsedSelectedType = null;
    this.selectedCreationWorkflowId = null;
    this.selectedValidationWorkflowId = null;
    this.creationWorkflowDefinition = null;
    this.validationWorkflowDefinition = null;

    this.standardTypologyService
      .getType(this.tenantId, this.selectedType.id)
      .subscribe(data => {

          this.parsedSelectedType = data;
          this.acceptedMimeTypesPrincipal = MIME_TYPES_PRINCIPAL_DEFAULT;
          this.acceptedMimeTypesAnnexe = MIME_TYPES_ANNEXES_DEFAULT;

          switch (this.parsedSelectedType.protocol) {
            case SignatureProtocol.Actes: {
              this.acceptedMimeTypesPrincipal = MIME_TYPES_PRINCIPAL_ACTES;
              this.acceptedMimeTypesAnnexe = MIME_TYPES_ANNEXES_ACTES;
              break;
            }
            case SignatureProtocol.Helios: {
              this.selectedAnnexesFiles = []
              this.annexeFileInput.reset()
              this.acceptedMimeTypesPrincipal = MIME_TYPES_PRINCIPAL_HELIOS;
              break;
            }
            default: {
              if (this.parsedSelectedType.signatureFormat === SignatureFormat.Pades) {
                this.acceptedMimeTypesPrincipal = MIME_TYPES_PRINCIPAL_ACTES;
              }
            }
          }

          this.removeExceedingFilesInMainDocumentListIfNeeded();
        },
        error => {
          this.notificationsService.showErrorMessage(FolderCreationMessages.GET_TYPE_INFO_ERROR, error.message);
        }
      );
  }


  disableMainFileInput(): boolean {
    return this.selectedMainDocumentsFiles.length >= FolderUtils.getMaxDocumentsCount(this.parsedSelectedType, this.selectedSubtype);
  }


  disableAnnexeFileInput(): boolean {
    return this.selectedMainDocumentsFiles.length + this.selectedAnnexesFiles.length >= Config.MAX_DOCUMENTS;
  }


  removeExceedingFilesInMainDocumentListIfNeeded() {
    const maxDocumentsSize = FolderUtils.getMaxDocumentsCount(this.parsedSelectedType, this.selectedSubtype);
    if (maxDocumentsSize < this.selectedMainDocumentsFiles.length) {
      const overwhelmingElements = this.selectedMainDocumentsFiles.slice(maxDocumentsSize, this.selectedMainDocumentsFiles.length);
      this.selectedMainDocumentsFiles = this.selectedMainDocumentsFiles.slice(0, maxDocumentsSize);
      overwhelmingElements.slice().reverse().forEach(file => this.selectedAnnexesFiles.unshift(file));
    }
  }


  onSubtypeSelectionChanged() {

    if (!this.selectedSubtype) return;

    this.variableDeskStepCount = 0;
    this.variableDesksIds = {};
    this.selectedCreationWorkflowId = this.selectedSubtype.creationWorkflowId;
    this.selectedValidationWorkflowId = this.selectedSubtype.validationWorkflowId;
    this.metadata = {};
    this.validationWorkflowDefinition = null;

    this.removeExceedingFilesInMainDocumentListIfNeeded();

    if (this.hasSelectionScript()) {
      this.updateValidationWorkflowFromSelectionScript();
      this.updateCreationWorkflowIfNeeded();
    } else {
      this.legacyWorkflowService
        .getWorkflowDefinitionByKey(this.tenantId, this.currentDesk.id, this.selectedSubtype.validationWorkflowId)
        .subscribe(
          result => {
            this.validationWorkflowDefinition = result;
            this.variableDeskStepCount = WorkflowUtils.countVariableDeskStep(result);
            this.updateCreationWorkflowIfNeeded();
          },
          error => this.notificationsService.showErrorMessage(FolderCreationMessages.GET_SUBTYPE_INFO_ERROR, error.message)
        );
    }
  }


  private updateValidationWorkflowFromSelectionScript() {

    const isEveryMandatoryMetadataSet = this.selectedSubtype.subtypeMetadataList
      .filter(subtypeMetadata => subtypeMetadata.mandatory)
      .every(subtypeMetadata => !!this.metadata[subtypeMetadata.metadata.key]);

    if (!isEveryMandatoryMetadataSet) {
      console.log('updateValidationWorkflowFromSelectionScript - Missing some mandatory metadata here, skipping...');
      return;
    }

    this.legacyWorkflowService
      .evaluateWorkflowSelectionScript(this.tenantId, this.currentDesk.id, this.selectedType.id, this.selectedSubtype.id, this.metadata)
      .subscribe(
        result => {
          this.validationWorkflowDefinition = result;
          this.variableDeskStepCount += WorkflowUtils.countVariableDeskStep(result);
        },
        error => {
          this.notificationsService.showErrorMessage(FolderCreationMessages.GET_SUBTYPE_INFO_ERROR, error.message);
        }
      );
  }


  private updateCreationWorkflowIfNeeded() {
    if (!!this.selectedSubtype.creationWorkflowId) {
      this.legacyWorkflowService
        .getWorkflowDefinitionByKey(this.tenantId, this.currentDesk.id, this.selectedSubtype.creationWorkflowId)
        .subscribe(
          result => {
            this.creationWorkflowDefinition = result;
            this.variableDeskStepCount += WorkflowUtils.countVariableDeskStep(result);
          },
          error => this.notificationsService.showErrorMessage(FolderCreationMessages.GET_SUBTYPE_INFO_ERROR, error.message)
        );
    } else {
      this.creationWorkflowDefinition = null;
    }
  }


  removeMainFile(index: number) {
    this.selectedMainDocumentsFiles.splice(index, 1);
    this.mainFileInput.reset();
  }


  removeAnnexeFile(index: number) {
    this.selectedAnnexesFiles.splice(index, 1);
    this.annexeFileInput.reset();
  }


  removeDetachedSignature(fileWrapperIndex: number, detachedSignatureIndex: number, input: NgModel) {
    this.selectedMainDocumentsFiles[fileWrapperIndex].detachedSignatures.splice(detachedSignatureIndex, 1);
    input.reset();
  }


  changeMainFile(event: Event) {
    let createFolderFileWrapper: CreateFolderFileWrapper = new CreateFolderFileWrapper();
    createFolderFileWrapper.file = (event.target as HTMLInputElement).files.item(0)

    this.selectedMainDocumentsFiles[0] = createFolderFileWrapper;
  }


  addDetachedSignature(event: Event, document: CreateFolderFileWrapper) {
    if (!document.detachedSignatures) {
      document.detachedSignatures = [];
    }

    let fileList: FileList = (event.target as HTMLInputElement).files

    for (let i = 0 ; i < fileList.length ; i++) {
      document.detachedSignatures.push(fileList.item(i));
    }
  }


  onFileSelected(event: Event) {
    this.addFilesToList((event.target as HTMLInputElement).files, true);
  }


  dragAndDropMainFiles(event: DragEvent) {
    this.addFilesToList(event.dataTransfer.files, true);
  }


  onAnnexeFileSelected(event: Event) {
    this.addFilesToList((event.target as HTMLInputElement).files, false);
  }


  dragAndDropAnnexeFiles(event: DragEvent) {
    this.addFilesToList(event.dataTransfer.files, false);
  }


  createDraftFolder() {
    if (!this.isValid()) return;

    if (this.isProcessing !== false) {
      console.log('Already pending');
      return;
    }

    this.isProcessing = true;
    this.createDraft$()
      .pipe(tap(folder => {
        if (!folder || !folder.id)
          throw new Error(this.messages.FOLDER_CREATION_ERROR);
      }))
      .subscribe(
        folder => {
          this.notificationsService.showSuccessMessage(this.messages.DRAFT_FOLDER_CREATED);
          this.router
            .navigate([`/tenant/${this.tenantId}/desk/${this.currentDesk.id}/folder/${folder.id}?${FolderUtils.AS_DESK_QUERY_PARAM_NAME}=${this.currentDesk.id}`])
            .then(
              () => {/* Not used */},
              () => this.notificationsService.showErrorMessage(GlobalSearchMessages.ERROR_NAVIGATING_TO_FOLDER)
            );
        },
        error => this.notificationsService.showErrorMessage(this.messages.FOLDER_CREATION_ERROR, error.message)
      )
      .add(() => this.isProcessing = false);
  }


  createAndStartFolder() {
    if (!this.isValid()) return;

    if (this.isProcessing !== false) {
      console.log('Already pending');
      return;
    }

    this.modalService.open(CreateAndStartFolderPopupComponent)
      .result
      .then(result => {
          this.isProcessing = true;
          this.createDraft$()
            .pipe(
              tap(folder => {
                if (!folder || !folder.id)
                  throw new Error(this.messages.FOLDER_CREATION_ERROR);
              }),
              map(folder => folder.id),
              concatMap(folderId => this.legacyWorkflowService.startFolder(this.tenantId, this.currentDesk.id, folderId, result.value)),
              tap(result => {
                if (!result || !result.id)
                  throw new Error(this.messages.FOLDER_START_ERROR);
              })
            )
            .subscribe(
              () => {
                this.notificationsService.showSuccessMessage(this.messages.FOLDER_CREATED_AND_STARTED);
                this.router
                  .navigate([`/tenant/${this.tenantId}/desk/${this.currentDesk.id}`])
                  .then(() => {/* Not used */});
              },
              error => this.notificationsService.showErrorMessage(this.messages.FOLDER_CREATION_ERROR, getFirstErrorMessage(error))
            )
            .add(() => this.isProcessing = false)
            .add(() => this.websocketService.notifyFolderAction(this.tenantId, this.currentDesk.id));
        },
        () => {/* Do nothing */});
  }


  subtypeIsInvalid(): boolean {
    return !this.selectedSubtype
      || (Object.keys(this.selectedSubtype).length === 0)
      || (!this.selectedValidationWorkflowId && isEmpty(this.selectedSubtype?.workflowSelectionScript));
  }


  isValid(): boolean {
    return (!this.subtypeIsInvalid() || this.hasSelectionScript())
      && this.selectedMainDocumentsFiles.length > 0
      && this.nameInput?.valid
      && Object.values(this.variableDesksIds).filter(elem => !!elem).length === this.variableDeskStepCount;
  }


  showSteps(): boolean {
    return !!this.validationWorkflowDefinition;
  }


  hasSelectionScript(): boolean {
    return !this.selectedSubtype?.validationWorkflowId
      && !!this.selectedSubtype?.workflowSelectionScript;
  }


  showCreationSteps(): boolean {
    return isNotNullOrUndefined(this.selectedSubtype?.creationWorkflowId)
      && isNotNullOrUndefined(this.creationWorkflowDefinition);
  }


  addFilesToList(fileList: FileList, mainDoc: boolean) {

    // Transforming FileList into a more convenient array

    let filesToAdd: File[] = [];
    for (let i = 0 ; i < fileList.length ; i++) {
      filesToAdd.push(fileList.item(i))
    }

    // Warning on already-existing file name

    const duplicatesCount = filesToAdd
      .filter(f => {
        const isInMainDocs = this.selectedMainDocumentsFiles.map(mainDocumentWrapper => mainDocumentWrapper.file.name).includes(f.name);
        const isInAnnexes = this.selectedAnnexesFiles.map(annexeWrapper => annexeWrapper.file.name).includes(f.name);
        return isInMainDocs || isInAnnexes;
      })
      .length;

    if (duplicatesCount > 0) {
      this.notificationsService.showErrorMessage(FolderCreationMessages.errorFileAlreadyExist(duplicatesCount));
    }

    // Adding

    filesToAdd
      .filter(file => !this.selectedMainDocumentsFiles.map(mainDocumentWrapper => mainDocumentWrapper.file.name).includes(file.name))
      .filter(file => !this.selectedAnnexesFiles.map(annexeWrapper => annexeWrapper.file.name).includes(file.name))
      .forEach(file => {
        let isEmptySlotAvailableInMainDoc = (FolderUtils.getMaxDocumentsCount(this.parsedSelectedType,
          this.selectedSubtype) - this.selectedMainDocumentsFiles.length) >= 1;
        let isEmptySlotAvailableInAnnexes = (Config.MAX_DOCUMENTS - this.selectedMainDocumentsFiles.length - this.selectedAnnexesFiles.length) >= 1;

        let fileWrapper: CreateFolderFileWrapper = new CreateFolderFileWrapper();
        fileWrapper.file = file;

        if (mainDoc && isEmptySlotAvailableInMainDoc) {
          this.selectedMainDocumentsFiles.push(fileWrapper);
        } else if (isEmptySlotAvailableInAnnexes) {
          this.selectedAnnexesFiles.push(fileWrapper);
        }
      });
  }


  availableWorkflows(): boolean {
    return !this.selectedType
      || (this.selectedType && !this.selectedSubtype)
      || isNotNullOrUndefined(this.selectedSubtype.validationWorkflowId);
  }


  drop(
    event: { previousContainer: CdkDropList, container: CdkDropList, previousIndex: number, currentIndex: number },
    destination: CreateFolderFileWrapper[]
  ): void {
    const previousIndex: number = event.previousIndex;
    const currentIndex: number = event.currentIndex;

    if (event.container.id === this.mainFileListId && this.selectedSubtype?.multiDocuments === false && this.selectedMainDocumentsFiles.length > 0) {
      return;
    }

    if (event.previousContainer.id === this.mainFileListId) {
      const item = this.selectedMainDocumentsFiles.splice(previousIndex, 1)[0];
      destination.splice(currentIndex, 0, item);
    } else if (event.previousContainer.id === this.annexeFileListId) {
      const item = this.selectedAnnexesFiles.splice(previousIndex, 1)[0];
      destination.splice(currentIndex, 0, item);
    }
  }

  metadataWasUpdated() {
    if (this.hasSelectionScript()) {
      this.validationWorkflowDefinition = null;
      this.updateValidationWorkflowFromSelectionScript();
    }
  }


  private createDraft$(): Observable<Folder> {

    let detachedSignatures: { [key: string]: Array<string> } = {};
    let detachedSignaturesFiles: File[] = [];

    const mainFiles = this.selectedMainDocumentsFiles;
    mainFiles
      .filter(mainFile => mainFile.detachedSignatures?.length > 0)
      .forEach(mainFile => {
        let detachedSignaturesIds: string[] = [];
        mainFile.detachedSignatures.forEach(detachedSignature => {
          detachedSignaturesFiles.push(detachedSignature);
          detachedSignaturesIds.push(detachedSignature.name);
        });
        detachedSignatures[mainFile.file?.name] = detachedSignaturesIds;
      });

    const createFolderRequest = new CreateFolderRequest();
    createFolderRequest.name = this.name;
    createFolderRequest.dueDate = this.limitDate;
    createFolderRequest.typeId = this.selectedType.id;
    createFolderRequest.subtypeId = this.selectedSubtype.id;
    createFolderRequest.metadata = this.metadata;
    createFolderRequest.variableDesksIds = this.variableDesksIds;
    createFolderRequest.detachedSignaturesMapping = detachedSignatures;
    createFolderRequest.visibility = this.selectedVisibility;

    return this.workflowService
      .createDraftFolder(
        this.tenantId,
        this.currentDesk.id,
        this.selectedMainDocumentsFiles.map(mainDoc => mainDoc.file),
        createFolderRequest,
        this.selectedAnnexesFiles.map(annexeDoc => annexeDoc.file),
        detachedSignaturesFiles,
      )
      .pipe(
        map(
          folderDto => {

            // Refresh the current desk
            setTimeout(() => this.deskService.getDesk(this.tenantId, this.currentDesk.id)
              .subscribe(desk => this.selectedDeskService.update(desk)), 1000);

            // Parse and return the current Folder
            return Object.assign(new Folder(), folderDto);
          }
        ),
        catchError(this.notificationsService.handleHttpError('createDraftFolder'))
      )
      .pipe(tap(() => this.websocketService.notifyFolderAction(this.tenantId, this.currentDesk.id)));
  }


  retrieveTypes(page: number, pageSize: number): Observable<PageTypeRepresentation> {
    console.debug(`retrieve creation allowed types : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getCreationAllowedTypes(this.tenantId, this.currentDesk.id)
      .pipe(map(page => {
        let result: PageTypeRepresentation = {
          content: page.data
        };
        return result;
      }));
  }


  retrieveSubtypes(page: number, pageSize: number): Observable<PageSubtypeRepresentation> {
    console.debug(`retrieve creation allowed subtypes : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getCreationAllowedSubtypes(this.tenantId, this.currentDesk.id, this.selectedType?.id)
      .pipe(map(page => {
        let result: PageSubtypeRepresentation = {
          content: page.data
        };
        return result;
      }));
  }


}
