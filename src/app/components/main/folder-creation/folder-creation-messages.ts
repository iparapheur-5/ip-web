/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { FolderUtils } from '../../../utils/folder-utils';
import { SubtypeDto, TypeDto } from '@libriciel/iparapheur-standard';

export class FolderCreationMessages {


  static readonly FORM_TITLE = `Création d'un dossier`;

  static readonly GET_TYPE_INFO_ERROR = 'impossible de récupérer les informations du Type.';
  static readonly GET_SUBTYPE_INFO_ERROR = 'impossible de récupérer les informations du Sous-type.';

  static readonly ANNEXE_DOCUMENT_SECTION_SUBTITLE = 'Documents annexes';
  static readonly ADD_ANNEXE_DOCUMENTS = 'Ajouter des annexes';

  static readonly GENERAL_INFORMATION_TITLE = 'Informations';
  static readonly GENERAL_INFORMATION_FORM_NAME = 'Nom';
  static readonly GENERAL_INFORMATION_FORM_ERROR_NO_NAME = 'Un nom de dossier doit être renseigné.';
  static readonly GENERAL_INFORMATION_FORM_ERROR_NAME_TOO_SHORT = 'Le nom doit contenir au moins 2 caractères.';
  static readonly GENERAL_INFORMATION_FORM_VISIBILITY = 'Visibilité';

  static readonly SAVE_AS_DRAFT = 'Enregistrer en tant que brouillon';
  static readonly DRAFT_FOLDER_CREATED = 'Le dossier a été créé avec succès en tant que brouillon.';
  static readonly FOLDER_CREATED_AND_STARTED = 'Le dossier a été créé avec succès et envoyé dans le circuit.';
  static readonly FOLDER_CREATION_ERROR = 'Erreur lors de la création du dossier';
  static readonly FOLDER_START_ERROR = `Erreur lors de l'envoi du dossier dans le circuit`;

  static readonly NO_WORKFLOW_FOR_SUBTYPE = `Le sous-type sélectionné n'a pas de circuit de validation ni de script de sélection associé.`;
  static readonly SELECTION_SCRIPT_MESSAGE = 'Circuit défini par un script de sélection.';

  static readonly ANNEXE_DOC_MAX_REACHED = 'Nombre de documents annexes max. atteint.';
  static readonly DOCUMENT_PDF_TRANSFORMATION_TOOLTIP = `Pour répondre au type PAdES, ce document sera transformé et remplacé par un PDF.\n
   Le rendu de la transformation peut-être variable. Un export en PDF depuis le logiciel d'origine offrira toujours un rendu plus stable.`;

  static readonly UNIQUE_VISIBILITY_WARNING_TOOLTIP = `L'administrateur n'a autorisé que cette visibilité.`;

  static readonly ADD_DETACHED_SIGNATURE = 'Ajouter une signature détachée';
  static readonly DETACHED_SIGNATURES = 'Signatures détachées';

  static errorFileAlreadyExist = (duplicatesCount: number) =>
    duplicatesCount == 1
      ? 'Un fichier du même nom existe déjà.'
      : `${duplicatesCount} fichiers de mêmes noms existent déjà.`;


  static mainDocumentsSectionSubtitle = (type?: TypeDto, subtype?: SubtypeDto) =>
    FolderUtils.getMaxDocumentsCount(type, subtype) == 1
      ? 'Document principal'
      : `Documents principaux (${FolderUtils.getMaxDocumentsCount(type, subtype)} maximum)`;


  static mainDocumentMaxReached = (type?: TypeDto, subtype?: SubtypeDto) =>
    FolderUtils.getMaxDocumentsCount(type, subtype) == 1
      ? 'Un document a déjà été sélectionné.'
      : 'Nombre de documents principaux max. atteint.';


  static addMainDocument = (type?: TypeDto, subtype?: SubtypeDto) =>
    FolderUtils.getMaxDocumentsCount(type, subtype) == 1
      ? 'Ajouter un document principal'
      : 'Ajouter des documents principaux';


}
