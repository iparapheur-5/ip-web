/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { CommonMessages } from '../../../../shared/common-messages';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleTaskParams } from '../../../../models/simple-task-params';
import { Action } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-create-and-start-folder-popup',
  templateUrl: './create-and-start-folder-popup.component.html',
  styleUrls: ['./create-and-start-folder-popup.component.scss']
})
export class CreateAndStartFolderPopupComponent {

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly styleEnum = Style;
  readonly actionEnum = Action;


  publicAnnotation: string = "";
  privateAnnotation: string = "";


  constructor(public activeModal: NgbActiveModal) { }


  perform(): void {
    let simpleTaskParams: SimpleTaskParams = new SimpleTaskParams();
    simpleTaskParams.publicAnnotation = this.publicAnnotation;
    simpleTaskParams.privateAnnotation = this.privateAnnotation;

    this.activeModal.close({
      value: simpleTaskParams
    });
  }


}
