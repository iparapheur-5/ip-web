/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskListComponent } from './components/main/tasks/task-list/task-list.component';
import { DeskListComponent } from './components/main/desk-list/desk-list.component';
import { FolderMainLayoutComponent } from './components/main/folder-view/folder-main-layout/folder-main-layout.component';
import { AdminTenantUserListComponent } from './components/admin/admin-sub-panels/tenant-users/admin-tenant-user-list/admin-tenant-user-list.component';
import { AboutComponent } from './components/settings/about/about.component';
import { AdminDeskListComponent } from './components/admin/admin-sub-panels/desk/admin-desk-list/admin-desk-list.component';
import { AdminMainLayoutComponent } from './components/admin/admin-main-layout/admin-main-layout.component';
import { AdminFolderListComponent } from './components/admin/admin-sub-panels/admin-folder-list/admin-folder-list.component';
import { WorkflowListComponent } from './components/admin/admin-sub-panels/workflow-list/workflow-list.component';
import { TypologyListComponent } from './components/admin/admin-sub-panels/typology/typology-list/typology-list.component';
import { WorkflowEditorComponent } from './components/admin/admin-sub-panels/workflow-editor/workflow-editor-main-layout/workflow-editor.component';
import { AdminTenantListComponent } from './components/admin/admin-sub-panels/tenants/admin-tenant-list/admin-tenant-list.component';
import { TrashBinListComponent } from './components/main/trash-bin-list/trash-bin-list.component';
import { ProfilePageComponent } from './components/main/users/profile-page/profile-page.component';
import { AdvancedAdminMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-main-layout/advanced-admin-main-layout.component';
import { ConnectorsMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-main-layout/connectors-main-layout.component';
import { AdvancedConfigMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/advanced-config-main-layout/advanced-config-main-layout.component';
import { ExternalSignatureConnectorsComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-sub-panels/external-signature/external-signature-connectors/external-signature-connectors.component';
import { AdminMetadataListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/metadata/admin-metadata-list/admin-metadata-list.component';
import { SealCertificatesListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/seal-certificates/seal-certificates-list/seal-certificates-list.component';
import { StatsComponent } from './components/main/stats/stats.component';
import { CreateFolderPageComponent } from './components/main/folder-creation/create-folder-page/create-folder-page.component';
import { SecureMailConnectorsComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/connectors/connectors-sub-panels/secure-mail/secure-mail-connectors/secure-mail-connectors.component';
import { IpngMainLayoutComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-main-layout/ipng-main-layout.component';
import { IpngHealthStateComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/ipng-health-state/ipng-health-state.component';
import { IpngTypologyComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/typology/ipng-typology/ipng-typology.component';
import { TemplatesListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/templates/templates-list/templates-list.component';
import { ServerDataMainLayoutComponent } from './components/admin/admin-sub-panels/server-data/server-data-main-layout/server-data-main-layout.component';
import { LayersListComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/layers/layers-list/layers-list.component';
import { BreadcrumbNames } from './shared/components/breadcrumbs/breadcrumb-names';
import { AdminAllUsersComponent } from './components/admin/admin-sub-panels/all-users/all-users-list/admin-all-users.component';
import { WorkflowDefinitionResolver } from './services/resolvers/workflow-definition.resolver';
import { DeskResolver } from './services/resolvers/desk/desk.resolver';
import { FolderResolver } from './services/resolvers/folder.resolver';
import { IpngDeskboxesComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/deskboxes/ipng-deskboxes/ipng-deskboxes.component';
import { IpngMetadataComponent } from './components/admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/ipng/ipng-sub-panels/metadata/ipng-metadata/ipng-metadata.component';
import { UserPreferencesResolver } from './services/resolvers/user-preferences.resolver';
import { DeskActuatorService } from './services/resolvers/desk/desk-actuator.service';
import { GdprComponent } from "./components/main/gdpr/gdpr.component";
import { AdminAbsencesMenuComponent } from './components/admin/admin-sub-panels/absences/admin-absences-menu/admin-absences-menu.component';
import { StateNamePipe } from './shared/utils/state-name.pipe';
import { TenantResolver } from './services/resolvers/tenant.resolver';
import { State } from '@libriciel/iparapheur-standard';
import { CurrentUserResolver } from './services/resolvers/current-user.resolver';

const routes: Routes = [
  {
    path: '',
    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_HOME},
    resolve: {
      userPreferences: UserPreferencesResolver,
      currentUser: CurrentUserResolver
    },
    children: [
      {path: '', redirectTo: 'desk', pathMatch: 'full'},
      {path: 'about', data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ABOUT}, component: AboutComponent},
      {path: 'trash-bin', data: {breadcrumb: BreadcrumbNames.BREADCRUMB_TRASH_BIN}, component: TrashBinListComponent},
      {path: 'desk', data: {breadcrumb: BreadcrumbNames.BREADCRUMB_DESK}, component: DeskListComponent},
      {path: 'profile', data: {breadcrumb: BreadcrumbNames.BREADCRUMB_PROFILE}, component: ProfilePageComponent},
      {path: 'gdpr', data: {breadcrumb: BreadcrumbNames.BREADCRUMB_DATA_PRIVACY}, component: GdprComponent},
      {path: 'stats', data: {breadcrumb: BreadcrumbNames.BREADCRUMB_STATS}, component: StatsComponent},
      {
        path: 'tenant/:tenantId',
        data: {breadcrumb: BreadcrumbNames.BREADCRUMB_TENANT_ID},
        resolve: {tenant: TenantResolver},
        children: [
          {
            path: 'desk/:deskId',
            canDeactivate: [DeskActuatorService],
            resolve: {desk: DeskResolver},
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_DESK_ID},
            children: [
              {path: '', redirectTo: State.Pending.toLowerCase(), pathMatch: 'full'},
              {path: State.Late.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Late, true)}, component: TaskListComponent},
              {path: State.Pending.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Current, true)}, component: TaskListComponent},
              {path: State.Rejected.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Rejected, true)}, component: TaskListComponent},
              {path: State.Finished.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Finished, true)}, component: TaskListComponent},
              {path: State.Delegated.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Delegated, true)}, component: TaskListComponent},
              {path: State.Retrievable.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Retrievable, true)}, component: TaskListComponent},
              {path: State.Downstream.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Downstream, true)}, component: TaskListComponent},
              {path: State.Draft.toLowerCase(), data: {breadcrumb: StateNamePipe.compute(State.Draft, true)}, component: TaskListComponent},
              {path: State.Draft.toLowerCase() + '/create', data: {breadcrumb: BreadcrumbNames.BREADCRUMB_DRAFT_CREATE}, component: CreateFolderPageComponent},
              {
                path: 'folder/:folderId',
                data: {breadcrumb: BreadcrumbNames.BREADCRUMB_FOLDER_ID},
                component: FolderMainLayoutComponent,
                resolve: {folder: FolderResolver},
              },
            ]
          }
        ]
      }
    ]
  },
  {
    path: 'admin',
    component: AdminMainLayoutComponent,
    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION},
    resolve: {userPreferences: UserPreferencesResolver},
    children: [
      {
        path: '', redirectTo: 'server', pathMatch: 'full'
      },
      {
        path: 'server',
        data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_SERVER},
        component: ServerDataMainLayoutComponent
      },
      {
        path: 'tenants',
        data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_TENANTS},
        component: AdminTenantListComponent
      },
      {
        path: 'all-users',
        data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_ALL_USERS},
        component: AdminAllUsersComponent
      },
      {
        path: ':tenantId',
        resolve: {tenant: TenantResolver},
        data: {breadcrumb: BreadcrumbNames.BREADCRUMB_TENANT_ID},
        children: [
          {
            path: 'tenant-users',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_TENANT_USERS},
            component: AdminTenantUserListComponent
          },
          {
            path: 'desks',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_DESKS},
            component: AdminDeskListComponent
          },
          {
            path: 'absences',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_ABSENCES, asAdmin: true},
            component: AdminAbsencesMenuComponent
          },
          {
            path: 'typology',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_TYPOLOGY},
            component: TypologyListComponent
          },
          {
            path: 'workflows',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_WORKFLOWS_DEFINITIONS},
            component: WorkflowListComponent
          },
          {
            path: 'workflows/definitions',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_WORKFLOWS_DEFINITIONS},
            component: WorkflowListComponent
          },
          {
            path: 'workflows/new',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_WORKFLOWS_EDITOR_NEW},
            component: WorkflowEditorComponent
          },
          {
            path: 'workflows/definitions/:workflowId/clone',
            component: WorkflowEditorComponent,
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_WORKFLOWS_EDITOR},
            resolve: {workflowDefinition: WorkflowDefinitionResolver}
          },
          {
            path: 'workflows/definitions/:workflowId',
            component: WorkflowEditorComponent,
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_WORKFLOWS_EDITOR},
            resolve: {workflowDefinition: WorkflowDefinitionResolver}
          },
          {
            path: 'folders',
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADMINISTRATION_FOLDERS},
            component: AdminFolderListComponent
          },
          {
            path: 'advanced',
            component: AdvancedAdminMainLayoutComponent,
            data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADVANCED_ADMIN},
            children: [
              {path: '', redirectTo: 'connectors', pathMatch: 'full'},
              {
                path: 'connectors',
                component: ConnectorsMainLayoutComponent,
                data: {breadcrumb: BreadcrumbNames.BREADCRUMB_CONNECTORS},
                children: [
                  {path: '', redirectTo: 'external-signature', pathMatch: 'full'},
                  {
                    path: 'external-signature',
                    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_CONNECTORS_EXTERNAL_SIGNATURE},
                    component: ExternalSignatureConnectorsComponent
                  },
                  {
                    path: 'secure-mail',
                    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_CONNECTORS_SECURE_MAIL},
                    component: SecureMailConnectorsComponent
                  },
                ]
              },
              {
                path: 'advanced-config',
                data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADVANCED_ADMIN},
                component: AdvancedConfigMainLayoutComponent,
                children: [
                  {path: '', redirectTo: 'templates', pathMatch: 'full'},
                  {
                    path: 'templates',
                    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADVANCED_ADMIN_TEMPLATES},
                    component: TemplatesListComponent
                  },
                  {
                    path: 'metadata',
                    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADVANCED_ADMIN_METADATA},
                    component: AdminMetadataListComponent
                  },
                  {
                    path: 'layers',
                    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADVANCED_ADMIN_LAYERS},
                    component: LayersListComponent
                  },
                  {
                    path: 'seal-certificates',
                    data: {breadcrumb: BreadcrumbNames.BREADCRUMB_ADVANCED_ADMIN_SEAL_CERTIFICATES},
                    component: SealCertificatesListComponent
                  }
                ]
              },
              {
                path: 'ipng-config',
                component: IpngMainLayoutComponent,
                children: [
                  {path: '', redirectTo: 'health-state', pathMatch: 'full'},
                  {path: 'health-state', component: IpngHealthStateComponent},
                  {path: ':entityId/deskboxes', component: IpngDeskboxesComponent},
                  {path: ':entityId/outgoing-typology', data: {forIncomingTypes: false}, component: IpngTypologyComponent},
                  {path: ':entityId/incoming-typology', data: {forIncomingTypes: true}, component: IpngTypologyComponent},
                  {path: ':entityId/metadata', component: IpngMetadataComponent}
                ]
              }
            ]
          }
        ]
      }

    ]
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})


export class AppRoutingModule {
}
