/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class Config {

  static readonly POINTS_PER_INCH = 72;
  static readonly POINTS_PER_MM = 1 / (10 * 2.54) * Config.POINTS_PER_INCH;

  static readonly STAMP_WIDTH = 200;
  static readonly STAMP_HEIGHT = 70;

  // FIXME : Link those two bad boys to the upcoming web params.
  static readonly MAX_DOCUMENTS = 30;

  static readonly API_VERSION = 'v1';

}
