/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationPopupComponent } from '../components/validation-popup/validation-popup.component';

@Injectable({
  providedIn: 'root'
})
export class GlobalPopupService {


  constructor(public modalService: NgbModal) {}


  showDeleteValidationPopup(message: string, title?: string): Promise<any> {
    return this.showAlertValidationPopup('delete', message, title);
  }


  showUnlinkValidationPopup(message: string, title?: string): Promise<any> {
    return this.showAlertValidationPopup('unlink', message, title);
  }


  showAlertValidationPopup(icon: 'delete' | 'unlink', message: string, title?: string): Promise<any> {
    return this.modalService
      .open(
        ValidationPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: ValidationPopupComponent.injectableIconKey, useValue: icon},
              {provide: ValidationPopupComponent.injectableMessageKey, useValue: message},
              {provide: ValidationPopupComponent.injectableTitleKey, useValue: title}
            ]
          }),
          size: 'lg'
        }
      )
      .result
  }


}
