/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { DeskSearchInputComponent } from './components/desk-search-input/desk-search-input.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { TypeSelectorComponent } from './components/type-selector/type-selector.component';
import { SubtypeSelectorComponent } from './components/subtype-selector/subtype-selector.component';
import { NamePipe } from './utils/name.pipe';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { WorkflowStepsComponent } from './components/workflow-steps/workflow-steps.component';
import { InputChipsComponent } from './components/input-chips/input-chips.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToggleButtonComponent } from './components/toggle-button/toggle-button.component';
import { DoubleListComponent } from './components/double-list/double-list.component';
import { ValidationPopupComponent } from './components/validation-popup/validation-popup.component';
import { FolderFilterPanelComponent } from './components/folder-filter-panel/folder-filter-panel.component';
import { ActionIconPipe } from './utils/action-icon.pipe';
import { ActionNamePipe } from './utils/action-name.pipe';
import { ClickStopPropagationDirective } from '../directives/click-stop-propagation.directive';
import { StepToActionPipe } from './utils/step-to-action.pipe';
import { StateNamePipe } from './utils/state-name.pipe';
import { ValidationModeNamePipe } from './utils/validation-mode-name.pipe';
import { ValidationModeIconPipe } from './utils/validation-mode-icon.pipe';
import { DeskListPopupComponent } from './components/desk-list-popup/desk-list-popup.component';
import { AuthImagePipe } from './utils/auth-image.pipe';
import { GenericDeskNamePipe } from './utils/generic-desk-name.pipe';
import { ListLoaderComponent } from './components/list-loader/list-loader.component';
import { AutofocusDirective } from './directives/autofocus.directive';

@NgModule({
  declarations: [
    DeskSearchInputComponent,
    SearchBarComponent,
    TypeSelectorComponent,
    SubtypeSelectorComponent,
    WorkflowStepsComponent,
    NamePipe,
    InputChipsComponent,
    BreadcrumbsComponent,
    ToggleButtonComponent,
    DoubleListComponent,
    ValidationPopupComponent,
    FolderFilterPanelComponent,
    ActionIconPipe,
    ActionNamePipe,
    StepToActionPipe,
    ClickStopPropagationDirective,
    StateNamePipe,
    ValidationModeNamePipe,
    ValidationModeIconPipe,
    DeskListPopupComponent,
    AuthImagePipe,
    GenericDeskNamePipe,
    ListLoaderComponent,
    AutofocusDirective
  ],
  exports: [
    ActionIconPipe,
    ActionNamePipe,
    StepToActionPipe,
    DeskSearchInputComponent,
    SearchBarComponent,
    TypeSelectorComponent,
    SubtypeSelectorComponent,
    WorkflowStepsComponent,
    NamePipe,
    InputChipsComponent,
    BreadcrumbsComponent,
    ToggleButtonComponent,
    DoubleListComponent,
    FolderFilterPanelComponent,
    ClickStopPropagationDirective,
    StateNamePipe,
    ValidationModeNamePipe,
    ValidationModeIconPipe,
    AuthImagePipe,
    GenericDeskNamePipe,
    ListLoaderComponent,
    AutofocusDirective
  ],
  imports: [
    CommonModule,
    LsComposantsModule,
    NgSelectModule,
    FormsModule,
    FontAwesomeModule,
    RouterModule,
    NgbModule
  ]
})
export class SharedModule {}
