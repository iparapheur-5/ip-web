/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { isNotNullOrUndefined, emailRfc5322Regex, isRfc5322ValidMail } from '../../../utils/string-utils';

@Component({
  selector: 'app-input-chips',
  templateUrl: './input-chips.component.html',
  styleUrls: ['./input-chips.component.scss']
})
export class InputChipsComponent {

  readonly rfc5322RegexPattern = emailRfc5322Regex;

  @Input() placeholder: string = '';
  @Input() inputType: 'email' | 'search' | 'text' | 'url';
  @Output() valuesChange = new EventEmitter<Array<string>>();

  inputFocused: boolean = false;
  storedElementList: Array<string> = [];
  currentElementInput: string = '';


  addInputToStoredElementList() {

    if (this.currentElementInput === '') {
      return;
    }

    if ((this.inputType === 'email') && !isRfc5322ValidMail(this.currentElementInput)) {
      return;
    }

    this.storedElementList.push(this.currentElementInput);
    this.currentElementInput = '';
    this.onValuesChanged();
  }


  onDeleteClick(element: string) {
    this.storedElementList = this.storedElementList.filter(o => o !== element);
    this.onValuesChanged();
  }


  onBackspaceKeyUp() {
    if (this.currentElementInput.length === 0) {
      this.storedElementList.pop();
    }
  }


  onValuesChanged() {

    let result = new Array<string>()
      .concat(this.storedElementList)
      .concat(this.currentElementInput)
      .filter(o => isNotNullOrUndefined(o))
      .filter(o => o !== '');

    this.valuesChange.emit(result)
  }

  onBlur() {
    this.inputFocused = false;
    this.addInputToStoredElementList();
  }

}
