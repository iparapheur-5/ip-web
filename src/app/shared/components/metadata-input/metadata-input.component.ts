/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CommonMessages } from '../../common-messages';
import { MetadataType } from '@libriciel/iparapheur-standard';


@Component({
  selector: 'app-metadata-input',
  templateUrl: './metadata-input.component.html',
  styleUrls: ['./metadata-input.component.scss']
})
export class MetadataInputComponent implements OnInit {

  @Input() value?: string;
  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() type: MetadataType;
  @Input() restrictedValues: string[] = [];
  @Input() defaultValue: string;
  @Input() editable: boolean;
  @Input() mandatory: boolean;

  public readonly commonMessages = CommonMessages;
  public readonly metadataTypeEnum = MetadataType;


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    if (!!this.defaultValue && !this.value) {
      this.value = this.defaultValue;
      // FIXME we should set the default value in CORE
      this.valueChange.emit(this.value);
    }
  }


  // </editor-fold desc="LifeCycle">


  public onValueChange(): void {
    this.valueChange.emit(this.value);
  }


  public getMetadataInputType(): string {
    switch (this.type) {
      case MetadataType.Date: { return 'date'; }
      case MetadataType.Integer: { return 'number'; }
      case MetadataType.Float: { return 'number'; }
      case MetadataType.Url: { return 'url'; }
      case MetadataType.Boolean: { return 'boolean'; }
      default : { return 'text'; }
    }
  }


  public getMetadataInputStep(): number {
    return (this.type === MetadataType.Float) ? 0.01 : null;
  }


  public hasSomeRestrictedValues(): boolean {
    return (!!this.restrictedValues) && (this.restrictedValues.length > 0);
  }


  public asBoolean(value: string | boolean): boolean {

    if (typeof value == 'boolean') {
      return value;
    }

    return Boolean(value)
      ? (value === 'true')
      : null;
  }


}
