/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, ViewChild, Input, OnChanges } from '@angular/core';
import { PdfJsParameters } from '../../../models/annotation/pdf-js-parameters';
import { Observable } from 'rxjs';
import { Config } from '../../../config';

@Component({
  selector: 'app-pdf-example',
  templateUrl: './pdf-example.component.html',
  styleUrls: ['./pdf-example.component.scss']
})
export class PdfExampleComponent implements OnChanges {


  @ViewChild('pdfJsViewer') pdfViewer;

  @Input() lastModificationDate: string;
  @Input() pdfSupplier: () => Observable<any>;

  pdfJsParameters: PdfJsParameters = {
    annotationButtonsParameters: {
      hideAnnotation: true,
      hideSignaturePlacement: true,
      hideDraw: true,
      hideTextAnnotation: true
    },
    signaturePlacementAnnotations: [],
    annotationOptions: {
      width: Config.STAMP_WIDTH,
      height: Config.STAMP_HEIGHT,
      origin: 'BOTTOM_LEFT'
    }
  };


  // <editor-fold desc="LifeCycle">


  ngOnChanges() {
    this.pdfSupplier()
      .subscribe(result => {
        if (!!this.pdfViewer) {
          this.pdfJsParametersToLocalStorage();
          this.pdfViewer.pdfSrc = result;
          this.pdfViewer.refresh();
        }
      });
  }


  pdfJsParametersToLocalStorage(): void {
    localStorage.setItem("signaturePlacementAnnotations", JSON.stringify(this.pdfJsParameters.signaturePlacementAnnotations));
    localStorage.setItem("annotationButtonsParameters", JSON.stringify(this.pdfJsParameters.annotationButtonsParameters));
  }


  // </editor-fold desc="LifeCycle">


}
