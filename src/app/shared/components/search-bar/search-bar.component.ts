/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild, ElementRef, Output, EventEmitter, AfterViewInit, OnDestroy } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CommonMessages } from '../../common-messages';
import { CommonIcons } from '@libriciel/ls-composants';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements AfterViewInit, OnDestroy {


  readonly commonIcons = CommonIcons;

  @Input() placeholder = CommonMessages.SEARCH;
  @Output() debouncedKeyup = new EventEmitter<string>();

  @ViewChild('searchInput') searchInput: ElementRef;
  searchInputSubscription: Subscription;


  // <editor-fold desc="LifeCycle">


  ngAfterViewInit(): void {
    this.searchInputSubscription = fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(350),
        distinctUntilChanged()
      )
      .subscribe(value => this.emitEvent(value));
  }


  ngOnDestroy(): void {
    this.searchInputSubscription.unsubscribe();
  }


  // </editor-fold desc="LifeCycle">


  btnClicked() {
    this.emitEvent(this.searchInput.nativeElement.value);
  }


  private emitEvent(value: string) {
    this.debouncedKeyup.emit(value?.trim());
  }


}
