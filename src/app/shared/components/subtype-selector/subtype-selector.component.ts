/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { compareById } from '../../../utils/string-utils';
import { CommonMessages } from '../../common-messages';
import { NotificationsService } from '../../../services/notifications.service';
import { Observable } from 'rxjs';
import { NgChanges } from '../../models/custom-types';
import { TypologyService, TypeRepresentation, SubtypeDto } from '@libriciel/iparapheur-standard';
import { PageSubtypeRepresentation, SubtypeRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-subtype-selector',
  templateUrl: './subtype-selector.component.html',
  styleUrls: ['./subtype-selector.component.scss']
})
export class SubtypeSelectorComponent implements OnChanges {

  readonly commonMessages = CommonMessages;
  readonly compareByIdFn = compareById;


  @Input() tenantId: string;
  @Input() type: TypeRepresentation;
  @Input() subtype: SubtypeRepresentation;
  @Input() retrieveSubtypesFunction: (page: number, pageSize: number) => Observable<PageSubtypeRepresentation>;

  @Input() mandatory: boolean = false;
  @Input() disabled: boolean = false;
  @Input() placeholder = CommonMessages.NO_VALUE;

  @Output() subtypeChange: EventEmitter<SubtypeDto> = new EventEmitter<SubtypeDto>();

  subtypeList: SubtypeRepresentation[];


  // <editor-fold desc="LifeCycle">


  constructor(private notificationsService: NotificationsService,
              private typologyService: TypologyService) { }

  ngOnChanges(changes: NgChanges<SubtypeSelectorComponent>): void {
    if (changes.type?.currentValue !== changes.type?.previousValue) {
      this.subtype = null;
      this.refreshSubtypes();
    }
  }


  // </editor-fold desc="LifeCycle">


  onSubtypeSelectionChanged($event: SubtypeRepresentation): void {
    if (!$event) {
      this.subtypeChange.emit(null);
      return;
    }

    this.typologyService
      .getSubtype(this.tenantId, this.type?.id, $event.id)
      .subscribe(subtypeDto => {
          this.subtype = subtypeDto as SubtypeRepresentation;
          this.subtypeChange.emit(subtypeDto)
        },
        () => this.subtypeChange.emit(this.subtype)
      );
  }


  refreshSubtypes(): void {

    if (!this.type?.id) {
      this.subtypeList = [];
      return;
    }

    this.retrieveSubtypesFunction(0, 999)
      .subscribe(
        result => {
          this.subtypeList = result.content;

          // Special case : Auto-select subtype if there is only one available
          if (this.mandatory && result.content.length === 1) {
            this.subtype = result.content[0];
            this.onSubtypeSelectionChanged(this.subtype);
          }
        },
        error => this.notificationsService.showErrorMessage(CommonMessages.ERROR_RETRIEVING_SUBTYPES, error.message)
      );
  }


}
