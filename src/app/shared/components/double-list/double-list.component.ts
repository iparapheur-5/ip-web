/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../common-messages';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../../../services/notifications.service';

@Component({
  selector: 'app-double-list',
  templateUrl: './double-list.component.html',
  styleUrls: ['./double-list.component.scss']
})
export class DoubleListComponent<T extends { id?: string }> implements OnInit {


  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly chevronLeftIcon = faChevronLeft;
  readonly chevronRightIcon = faChevronRight;
  readonly pageSize = 10;

  @Input() leftColumnHeader: string;
  @Input() rightColumnHeader: string;
  @Input() tintColor: 'blue' | 'green';
  @Input() retrieveCandidateElementsFn: (page: number, pageSize: number) => Observable<{ totalElements?: number; content?: Array<T> }>;
  @Input() stringifyElementFn: (element: T) => string;
  @Input() selectedElements: T[] = [];
  @Input() unmodifiableElements: T[] = [];

  candidateElements: T[] = [];
  page = 1;
  total = 0;

  // <editor-fold desc="LifeCycle">


  constructor(private notificationsService: NotificationsService) { }


  ngOnInit(): void {
    this.requestElements(true);
  }


  // </editor-fold desc="LifeCycle">


  filteredSelectedList(): T[] {
    return this.selectedElements.filter(e => this.unmodifiableElements.indexOf(e) === -1);
  }


  requestElements(newRequest: boolean) {

    if (newRequest) {
      this.page = 1;
    }

    this.retrieveCandidateElementsFn(this.page - 1, this.pageSize)
      .pipe(catchError(this.notificationsService.handleHttpError('retrieveCandidateElementsFn')))
      .subscribe(paginatedResult => {
        this.candidateElements = paginatedResult.content;
        this.total = paginatedResult.totalElements;
      });
  }


  getPaginationCount() {
    return (this.page > 0 ? this.page : '-') + '/' + Math.ceil(this.total / this.pageSize);
  }


  select(element: T) {
    if (this.selectedElements.find(e => e.id === element.id) === undefined) {
      this.selectedElements.push(element);
    }
  }


  removeSelection(element: T) {
    this.selectedElements.forEach((e, index) => {
      if (e.id === element.id) {
        this.selectedElements.splice(index, 1);
      }
    });
  }


  isSelected(element: T): boolean {
    return this.selectedElements.find(e => e.id === element.id) !== undefined;
  }


  isUnmodifiableElement(element: T): boolean {
    return this.unmodifiableElements.find(e => e.id === element.id) !== undefined;
  }


  previous() {
    if (this.hasPrevious()) {
      this.page--;
      this.requestElements(false);
    }
  }


  next() {
    if (this.hasNext()) {
      this.page++;
      this.requestElements(false);
    }
  }


  hasPrevious() {
    return this.page > 1;
  }


  hasNext() {
    return this.page < Math.ceil(this.total / this.pageSize);
  }


}
