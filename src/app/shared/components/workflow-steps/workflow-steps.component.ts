/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { IpWorkflowModel } from '../../../models/workflows/ip-workflow-model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WorkflowInstanceSnapshotComponent } from '../../../components/main/folder-view/workflow-instance-snapshot/workflow-instance-snapshot.component';
import { IpStepModel, StepType } from '../../../models/workflows/ip-step-model';
import { GENERIC_DESK_IDS, GENERIC_DESK_NAMES } from '../../../models/workflows/ip-workflow-actor';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { StepMessages } from './step-messages';
import { CommonMessages } from '../../common-messages';
import { tap } from 'rxjs/operators';
import { CommonIcons } from '@libriciel/ls-composants';
import { WorkflowActor, StepModel } from '@libriciel/ls-composants/workflows';
import { DataConversionService } from '../../../services/data-conversion.service';
import { WorkflowDefinition } from '../../../models/workflow-definition';
import { CustomMap } from '../../models/custom-types';
import { DeskService, PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-internal';
import { isEmpty } from '../../../utils/string-utils';


@Component({
  selector: 'app-workflow-steps',
  templateUrl: './workflow-steps.component.html',
  styleUrls: ['./workflow-steps.component.scss']
})
export class WorkflowStepsComponent extends WorkflowInstanceSnapshotComponent implements OnInit {

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly stepMessages = StepMessages;

  @Input() deskId: string;
  @Input() tenantId: string;
  @Input() allowVariableDeskEdition?: boolean = true;
  @Input() variableDesksIds: CustomMap;
  // for validation workflows, when there is a creation workflow before
  @Input() stepOffset: number = 0;
  @Input() showStart: boolean = true;
  @Input() showEnd: boolean = true;
  @Input() workflowDefinition: WorkflowDefinition;
  @Input() originDesk: DeskRepresentation;

  desks: DeskRepresentation[][] = [];
  definition: IpWorkflowModel;
  definitionSteps: IpStepModel[] = [];

  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal,
              public deskService: DeskService,
              public dataConversionService: DataConversionService,
              public route: ActivatedRoute) {
    super(modalService);
  }


  ngOnInit(): void {
    this.updateDefinition();
  }


  // </editor-fold desc="LifeCycle">


  updateDefinition() {

    if (!this.workflowDefinition) {
      return;
    }

    this.definition = this.dataConversionService.createWorkflowModelFromBackendData(this.workflowDefinition);
    this.definitionSteps.push(...this.definition.steps as IpStepModel[])
    if (this.showEnd) {
      const finalStep = new IpStepModel();
      finalStep.name = 'final';
      finalStep.type = StepType.End;
      finalStep.validators = [this.workflowDefinition.finalDesk] as any;
      this.definitionSteps.push(finalStep);
    }

    this.definitionSteps.forEach(() => {
      this.desks.push([]);
    });

    for (let i = 0 ; i < this.stepOffset ; i++) {
      this.desks.push([]);
    }
  }


  getIncrement(): number {
    return this.stepOffset + 2 - (this.showStart ? 0 : 1);
  }


  isVariableDesksStep(step: StepModel): boolean {
    return step.validators.map(validator => validator.id).includes(GENERIC_DESK_IDS.VARIABLE_DESK_ID);
  }


  retrieveAssociatedDesksFn = (page: number, pageSize: number, searchTerm): Observable<PageDeskRepresentation> => {
    searchTerm = !!searchTerm ? searchTerm.toLowerCase() : searchTerm;
    return this.deskService.getAssociatedDesks(this.tenantId, this.deskId, page, pageSize, [], searchTerm)
      .pipe(
        tap(deskPage => {
          if (page === 0) {
            const currentDesk: DeskRepresentation = {id: this.deskId, name: GENERIC_DESK_NAMES.EMITTER};
            deskPage.content.unshift(currentDesk);
          }
        }),
        tap(deskPage => deskPage.content = isEmpty(searchTerm)
          ? deskPage.content
          : deskPage.content.filter(desk => desk.name.toLowerCase().includes(searchTerm))
        )
      );
  }


  setVariableDesks(index: number, deskList: (DeskRepresentation | WorkflowActor)[]): void {
    if (deskList.length > 0) {
      this.variableDesksIds[index] = deskList[0]?.id as any;
    }
  }


}
