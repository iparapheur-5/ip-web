/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, AfterViewInit, ElementRef, ViewChild, Output, EventEmitter, OnChanges } from '@angular/core';
/// <reference path="../../../../../node_modules/monaco-editor/monaco.d.ts" />
let loadedMonaco = false;
let loadPromise: Promise<void>;


@Component({
  selector: 'app-monaco-editor',
  templateUrl: './monaco-editor.component.html',
  styleUrls: ['./monaco-editor.component.scss'],
})
export class MonacoEditorComponent implements AfterViewInit, OnChanges {


  @ViewChild('editorContainer') editorContainer: ElementRef;


  @Input() code = '';
  @Input() language;
  @Output() codeChange = new EventEmitter<string>();


  // @ts-ignore
  codeEditorInstance: monaco.editor.IStandaloneCodeEditor;


  // <editor-fold desc="LifeCycle">


  // supports two-way binding
  ngOnChanges() {
    if (this.codeEditorInstance) {
      this.codeEditorInstance.setValue(this.code);
    }
  }


  ngAfterViewInit() {
    if (loadedMonaco) {
      // Wait until monaco editor is available
      loadPromise.then(() => this.initMonaco());
    } else {
      loadedMonaco = true;
      loadPromise = new Promise<void>((resolve: any) => {
        if (typeof ((window as any).monaco) === 'object') {
          resolve();
          return;
        }
        const onAmdLoader: any = () => {
          // Load monaco
          (window as any).require.config({paths: {vs: 'assets/monaco/vs'}});

          (window as any).require(['vs/editor/editor.main'], () => {
            this.initMonaco();
            resolve();
          });
        };

        // Load AMD loader if necessary
        if (!(window as any).require) {
          const loaderScript: HTMLScriptElement = document.createElement('script');
          loaderScript.type = 'text/javascript';
          loaderScript.src = 'assets/monaco/vs/loader.js';
          loaderScript.addEventListener('load', onAmdLoader);
          document.body.appendChild(loaderScript);
        } else {
          onAmdLoader();
        }
      });
    }
  }


  // <editor-fold desc="LifeCycle">


  initMonaco(): void {
    // @ts-ignore
    this.codeEditorInstance = monaco.editor
      .create(this.editorContainer.nativeElement, {
        value: this.code,
        language: this.language,
        theme: 'vs-dark'
      });

    // To support two-way binding of the code
    this.codeEditorInstance
      .getModel()
      .onDidChangeContent(_ => {
        this.codeChange.emit(this.codeEditorInstance.getValue());
      });
  }


}
