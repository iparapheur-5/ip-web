/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class FilterPanelMessages {


  static readonly FILTER_FROM_LABEL = 'Créé à partir du';
  static readonly FILTER_TO_LABEL = `Jusqu'à`;
  static readonly FILTER_SAVE_FILTER_LABEL = `Enregistrement d'un filtre`;
  static readonly FILTER_SAVE_ERROR = 'Erreur lors de la création du filtre'
  static readonly FILTER_SAVE = 'Enregistrer le filtre'
  static readonly NEW_FILTER = 'Enregistrer un nouveau filtre';
  static readonly FILTER_DELETE_ERROR = 'Erreur lors de la suppression du filtre.';
  static readonly FILTER_BUTTON_LABEL = 'Filtrer';


  static getFilterSaveSuccessMessage(data: string) { return `Le filtre "${data}" a été créé avec succès.`; }

  static deleteFilter(filterName: string) { return `Supprimer le filtre ${filterName}`; }

  static saveFilter(filterName: string) { return `Enregistrer le filtre ${filterName}`; }

  static getFilterDeleteSuccessMessage(filterName: string) { return `Le filtre "${filterName}" a été supprimé avec succès.`; }


}
