/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, Injector, HostBinding } from '@angular/core';
import { CommonMessages } from '../../common-messages';
import { FolderFilter } from '../../../models/folder/folder-filter';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SaveFilterPopupComponent } from './save-filter-popup/save-filter-popup.component';
import { FilterPanelMessages } from './filter-panel-messages';
import { FilterPanelIcons } from './filter-panel-icons';
import { DatePipe } from '@angular/common';
import { DeskService } from '../../../services/ip-core/desk.service';
import { LegacyUserService } from '../../../services/ip-core/legacy-user.service';
import { NotificationsService } from '../../../services/notifications.service';
import { Observable } from 'rxjs';
import { State, MetadataService, FolderFilterDto, TypeDto, SubtypeDto, TypologyService, PageTypeRepresentation } from '@libriciel/iparapheur-standard';
import { PageSubtypeRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-folder-filter-panel',
  templateUrl: './folder-filter-panel.component.html',
  styleUrls: ['./folder-filter-panel.component.scss']
})
export class FolderFilterPanelComponent {
  @HostBinding('class.active')
  @Input() active = false;
  @Output() closeFiltersEvent = new EventEmitter();
  @Output() updateFilterEvent = new EventEmitter<{ folderFilter: FolderFilterDto, reloadFilters: boolean, changeOriginalFilter: boolean }>();

  private _originalFilter: FolderFilterDto;
  get originalFilter(): FolderFilterDto {
    return this._originalFilter;
  }

  @Input()
  set originalFilter(value: FolderFilterDto) {
    this._originalFilter = value;
    this.checkModifiedFilter();
  }

  get filter(): FolderFilterDto {
    return this._filter;
  }

  @Input()
  set filter(value: FolderFilterDto) {
    this.loading = true;
    this._filter = Object.assign(new FolderFilter(), value);
    this.createdAt = value?.from ? this.datePipe.transform(new Date(this._filter.from), 'yyyy-MM-dd') : null;
    this.limitDate = value?.to ? this.datePipe.transform(new Date(this._filter.to), 'yyyy-MM-dd') : null;
    this.title = value?.searchData;
    this.state = value?.state;

    console.log('SET filter  : ', value);

    this.type = value?.type as TypeDto;
    setTimeout(() => this.subtype = value?.subtype as SubtypeDto, 0);

    this.loading = false;

  }

  public readonly commonMessages = CommonMessages;
  public readonly messages = FilterPanelMessages;
  public readonly commonIcons = CommonIcons;
  public readonly icons = FilterPanelIcons;
  public readonly style = Style;
  public readonly filterableStates: State[] = [
    State.Draft,
    State.Finished,
    State.Late,
    State.Pending,
    State.Delegated,
    State.Rejected,
    State.Retrievable,
    State.Downstream,
  ];

  public type: TypeDto;
  public loading = false;
  public subtype: SubtypeDto;
  public state: State;
  public tenantId: string;
  public deskId: string;
  public createdAt: string;
  public limitDate: string;
  public title: string;
  public modifiedFilter = false;
  private _filter: FolderFilterDto;
  private isProcessing = false;

  retrieveTypesFn = (page: number, pageSize: number) => this.retrieveTypes(page, pageSize);
  retrieveSubtypesFn = (page: number, pageSize: number) => this.retrieveSubtypes(page, pageSize);


  constructor(private route: ActivatedRoute,
              private metadataService: MetadataService,
              private deskService: DeskService,
              private typologyService: TypologyService,
              public modalService: NgbModal,
              private datePipe: DatePipe,
              private legacyUserService: LegacyUserService,
              private notificationsService: NotificationsService) {
    if ((!this.tenantId || !this.deskId) && this.route.params) {
      this.route.params.subscribe(params => {
        this.tenantId = params['tenantId'];
        this.deskId = params['deskId'];
      })
    }
  }


  public typeSelectionChanged(newType: any): void {
    this.type = newType;
    if (!newType) {
      this.subtype = undefined;
    }
    this.checkModifiedFilter();
  }

  public subtypeSelectionChanged(newSubtype: SubtypeDto): void {
    this.subtype = newSubtype;
    this.checkModifiedFilter();
  }


  public createdAtDateChanged(): void {
    this.checkModifiedFilter();
  }

  public limitDateChanged(): void {
    this.checkModifiedFilter();
  }

  saveOrUpdateFilter(newFilter: boolean = false): void {
    const value = this.getFilter(newFilter);
    this.modalService.open(
      SaveFilterPopupComponent, {
        injector: Injector.create({
          providers: [
            {
              provide: SaveFilterPopupComponent.INJECTABLE_FILTER_KEY,
              useValue: value
            },
          ]
        }),
        size: 'lg'
      }
    ).result
      .then(
        result => {
          this.applyFilter(result, true, true);
          this.checkModifiedFilter();
        },
        () => {/* dismissed */}
      );
  }

  public applyFilter(folderFilter: FolderFilterDto, reloadFilters, changeOriginalFilter): void {
    this.updateFilterEvent.emit({folderFilter: folderFilter, reloadFilters: reloadFilters, changeOriginalFilter: changeOriginalFilter});
  }

  // FIXME this method is not really nice, the generated Filter serves in different contexts (hence the duplicate type/typeid...)
  // The whole dynamics probably need to be refactored to avoid this
  getFilter(newFilter: boolean = false): FolderFilterDto {
    const folderFilter = !this.filter || newFilter ? {} as FolderFilterDto : this.filter;
    folderFilter.from = this.createdAt ? this.datePipe.transform(new Date(this.createdAt), 'yyyy-MM-dd') as unknown as Date : null;
    folderFilter.to = this.limitDate ? this.datePipe.transform(new Date(this.limitDate), 'yyyy-MM-dd') as unknown as Date : null;
    folderFilter.typeId = this.type?.id;
    folderFilter.subtypeId = this.subtype?.id;
    folderFilter.type = this.type;
    folderFilter.subtype = this.subtype;
    folderFilter.searchData = this.title;
    folderFilter.state = this.state;

    return folderFilter;
  }

  deleteCurrentFilter() {
    this.isProcessing = true;
    this.legacyUserService.deleteFilter(this.filter as FolderFilter)
      .subscribe(
        () => {
          this.applyFilter(null, true, true);
          this.notificationsService.showSuccessMessage(this.messages.getFilterDeleteSuccessMessage(this.filter.filterName));
        },
        error => this.notificationsService.showErrorMessage(this.messages.FILTER_DELETE_ERROR, error))
      .add(() => this.isProcessing = false);
  }

  resetFilter() {
    this.applyFilter(Object.assign(new FolderFilter(), this.originalFilter), false, false);
    this.checkModifiedFilter();
  }

  checkModifiedFilter() {
    this.modifiedFilter = !FolderFilter.equals(this.getFilter(), this.originalFilter);
  }


  retrieveTypes(page: number, pageSize: number): Observable<PageTypeRepresentation> {
    console.debug(`retrieve types : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getTypes(this.tenantId, this.deskId);
  }

  retrieveSubtypes(page: number, pageSize: number): Observable<PageSubtypeRepresentation> {
    console.debug(`retrieve subtypes : page: ${page}, pageSize: ${pageSize}`);
    return this.typologyService.getSubtypes(this.tenantId, this.deskId, this.type?.id);
  }

}
