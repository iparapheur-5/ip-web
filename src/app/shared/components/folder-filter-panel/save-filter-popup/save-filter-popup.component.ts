/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-composants';
import { CommonMessages } from '../../../common-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FolderFilter } from '../../../../models/folder/folder-filter';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { NotificationsService } from '../../../../services/notifications.service';
import { FilterPanelMessages } from '../filter-panel-messages';

@Component({
  selector: 'app-save-filter-popup',
  templateUrl: './save-filter-popup.component.html',
  styleUrls: ['./save-filter-popup.component.scss']
})
export class SaveFilterPopupComponent {

  public static readonly INJECTABLE_FILTER_KEY = 'filter';
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = FilterPanelMessages;

  isProcessing: boolean = false;
  name: string = this.folderFilter.filterName || '';


  constructor(public activeModal: NgbActiveModal,
              private legacyUserService: LegacyUserService,
              private notificationsService: NotificationsService,
              @Inject(SaveFilterPopupComponent.INJECTABLE_FILTER_KEY) public folderFilter: FolderFilter) { }


  // TODO: Use a FormGroup & add a max to 255
  isValid(): boolean {
    return this.name.length > 3;
  }


  save(): void {

    if (this.isProcessing === true) {
      return;
    }

    this.isProcessing = true;
    this.folderFilter.filterName = this.name;
    this.legacyUserService
      .createOrUpdateFilter(this.folderFilter)
      .subscribe(
        (folderFilter: FolderFilter) => {
          this.notificationsService.showSuccessMessage(this.messages.getFilterSaveSuccessMessage(this.folderFilter.filterName));
          this.activeModal.close(folderFilter);
        },
        error => this.notificationsService.showErrorMessage(this.messages.FILTER_SAVE_ERROR, error.message)
      )
      .add(() => this.isProcessing = false);
  }


}
