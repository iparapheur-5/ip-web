/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderFilterPanelComponent } from './folder-filter-panel.component';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToastrModule } from 'ngx-toastr';
import { NgbNavModule, NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { TypeSelectorComponent } from '../type-selector/type-selector.component';
import { SubtypeSelectorComponent } from '../subtype-selector/subtype-selector.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { StateNamePipe } from '../../utils/state-name.pipe';

describe('FolderFilterPanelComponent', () => {

  let component: FolderFilterPanelComponent;
  let fixture: ComponentFixture<FolderFilterPanelComponent>;


  beforeEach(async () =>
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot(), RouterTestingModule, HttpClientTestingModule, NgbNavModule, LsComposantsModule, NgSelectModule, FormsModule, NgbModule],
        declarations: [FolderFilterPanelComponent, FaIconComponent, TypeSelectorComponent, SubtypeSelectorComponent, StateNamePipe],
        providers: [
          NgbActiveModal, DatePipe,
          {provide: ActivatedRoute, useValue: {data: of({result: 'test'})}}
        ]
      })
      .compileComponents()
  );


  beforeEach(() => {
    fixture = TestBed.createComponent(FolderFilterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  // it('should create', () => expect(component).toBeTruthy());


});
