/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeskListPopupComponent } from './desk-list-popup.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NamePipe } from '../../utils/name.pipe';
import { UserPreferencesDto } from '@libriciel/iparapheur-standard';


describe('DeskListPopupComponent', () => {


  let component: DeskListPopupComponent;
  let fixture: ComponentFixture<DeskListPopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        providers: [
          NgbActiveModal,
          {provide: DeskListPopupComponent.INJECTABLE_USER_PREFERENCES, useValue: {} as UserPreferencesDto},
          {provide: DeskListPopupComponent.INJECTABLE_DESK_LIST_KEY, useValue: [{id: '01', shortname: 'Desk 01'}, {id: '02', shortname: 'Desk 02'}]},
        ],
        declarations: [DeskListPopupComponent, NamePipe]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(DeskListPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
