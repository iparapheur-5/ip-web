/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { CommonIcons, Style } from '@libriciel/ls-composants';
import { CommonMessages } from '../../common-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { faUnlink } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-validation-popup',
  templateUrl: './validation-popup.component.html',
  styleUrls: ['./validation-popup.component.scss']
})
export class ValidationPopupComponent {


  static readonly injectableTitleKey = 'title';
  static readonly injectableMessageKey = 'message';
  static readonly injectableIconKey = 'icon';


  readonly commonIcons = CommonIcons;
  readonly unlinkIcon = faUnlink;
  readonly commonMessages = CommonMessages;
  readonly styles = Style;


  constructor(public activeModal: NgbActiveModal,
              @Inject(ValidationPopupComponent.injectableIconKey) public icon: 'delete' | 'unlink',
              @Inject(ValidationPopupComponent.injectableMessageKey) public message: string,
              @Inject(ValidationPopupComponent.injectableTitleKey) public title?: string) { }


}
