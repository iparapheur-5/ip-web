/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { WorkflowActor } from '@libriciel/ls-composants/workflows/shared/model/workflows/workflow-actor';
import { Observable } from 'rxjs';
import { CommonMessages } from '../../common-messages';
import { compareById } from '../../../utils/string-utils';
import { GenericWorkflowActors } from '../../../models/workflows/ip-workflow-actor';
import { PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-desk-search-input',
  templateUrl: './desk-search-input.component.html',
  styleUrls: ['./desk-search-input.component.scss']
})
export class DeskSearchInputComponent implements OnInit {

  readonly commonMessages = CommonMessages;
  readonly genericWorkflowActors = GenericWorkflowActors;
  readonly pageSize = 10;
  readonly compareByIdFn = compareById;

  @Input() desks: Array<DeskRepresentation | WorkflowActor>;
  @Input() clearable: boolean;
  @Input() multiple: boolean;
  @Input() retrieveDeskFunction: (page: number, pageSize: number, searchTerm: string) => Observable<PageDeskRepresentation>;
  @Input() genericValidatorsAvailable = false;
  @Input() genericEmitterOnly = false;
  @Input() disabled = false;
  @Output() desksChange = new EventEmitter<Array<DeskRepresentation | WorkflowActor>>();

  currentSearchTerm: string;
  currentPage: number = 0;
  availableValidators: Array<DeskRepresentation | WorkflowActor> = [];
  totalValidators: number;


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    this.updateAvailableDesksList(false);
  }


  // </editor-fold desc="LifeCycle">


  onDeskSelectionChanged(event: any) {
    // Wrapping single elements in an array, to ease factorization
    this.desks = this.multiple ? event : [event];
    this.desksChange.emit(this.desks);
  }


  updateDesksWithNewSearchTerm(searchEvt: { term: string, items: any[] }) {
    this.currentPage = 0;
    this.currentSearchTerm = searchEvt.term;
    this.updateAvailableDesksList(false);
  }


  loadNextPageOfDesks() {
    if (this.totalValidators > this.availableValidators.length) {
      this.currentPage += 1;
      this.updateAvailableDesksList(true);
    }
  }


  updateAvailableDesksList(appendToCurrentList: boolean) {
    this.retrieveDeskFunction(this.currentPage, this.pageSize, this.currentSearchTerm)
      .subscribe(paginatedDeskList => {
        this.totalValidators = paginatedDeskList.totalElements;
        this.availableValidators = (appendToCurrentList ? this.availableValidators : []).concat(paginatedDeskList.content)
      });
  }

  filterDesks = (term: string, item: DeskRepresentation) => item.name.includes(term);


}
