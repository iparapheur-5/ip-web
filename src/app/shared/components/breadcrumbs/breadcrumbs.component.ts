/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonMessages } from '../../common-messages';
import { BreadcrumbNames } from './breadcrumb-names';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit, OnChanges {


  @Input() forceReload?: string;

  breadcrumbList: { name: string, routerLink: string }[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(private route: ActivatedRoute) {}


  ngOnChanges(): void {
    this.loadBreadcrumbs();
  }


  ngOnInit(): void {
    this.loadBreadcrumbs();
  }


  // </editor-fold desc="LifeCycle">


  public isActive(breadcrumb: { name: string, routerLink: string }): boolean {
    return this.breadcrumbList.indexOf(breadcrumb) === this.breadcrumbList.length - 1;
  }


  private loadBreadcrumbs(): void {
    this.route.data.subscribe(data => {
      this.breadcrumbList = [];
      this.breadcrumbList.unshift({
        name: data.breadcrumb,
        routerLink: '/' + this.route.snapshot.url.join('/')
      })
      this.loadParentData(this.route.parent);
    })
  }


  private loadParentData(currentRoute: ActivatedRoute | null): ActivatedRoute | null {
    if (currentRoute !== null) {
      currentRoute.data.subscribe(data => {
        if (data.breadcrumb) {
          this.loadBreadcrumb(currentRoute, data.breadcrumb);
          if (currentRoute.parent !== null && data.breadcrumb !== BreadcrumbNames.BREADCRUMB_HOME) {
            this.loadParentData(currentRoute.parent);
          }
        }
      });
    }
    this.loadIdNames();
    return null;
  }


  private loadBreadcrumb(currentRoute: ActivatedRoute, breadcrumb: string): void {
    this.breadcrumbList.unshift({
      name: breadcrumb,
      routerLink: '/' + currentRoute.snapshot.url.join('/')
    });
  }


  private loadIdNames(): void {

    this.breadcrumbList.forEach(breadcrumb => {
      switch (breadcrumb.name) {

        case BreadcrumbNames.BREADCRUMB_TENANT_ID:
          breadcrumb.routerLink = null;
          this.route.data
            .subscribe(
              data => breadcrumb.name = data.tenant?.name || CommonMessages.UNREACHABLE_TENANT,
              () => breadcrumb.name = CommonMessages.UNREACHABLE_TENANT
            );
          break;

        case BreadcrumbNames.BREADCRUMB_DESK_ID:
          breadcrumb.routerLink = null;
          this.route.data
            .subscribe(
              data => breadcrumb.name = data.desk?.name || CommonMessages.UNREACHABLE_DESK,
              () => breadcrumb.name = CommonMessages.UNREACHABLE_DESK
            );
          break;

        case BreadcrumbNames.BREADCRUMB_FOLDER_ID:
          breadcrumb.routerLink = null;
          this.route.data
            .subscribe(
              data => breadcrumb.name = data.folder?.name || CommonMessages.UNREACHABLE_FOLDER,
              () => breadcrumb.name = CommonMessages.UNREACHABLE_FOLDER
            );
          break;

      }
    });
  }


}
