/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class BreadcrumbNames {
  static readonly BREADCRUMB_HOME = 'Accueil';
  static readonly BREADCRUMB_ABOUT = 'À propos';
  static readonly BREADCRUMB_TRASH_BIN = 'Corbeille';
  static readonly BREADCRUMB_DATA_PRIVACY = 'RGPD';
  static readonly BREADCRUMB_DESK = 'Bureaux';
  static readonly BREADCRUMB_PROFILE = 'Profil';
  static readonly BREADCRUMB_STATS = 'Statistiques';
  static readonly BREADCRUMB_DESK_ID = ':deskId';
  static readonly BREADCRUMB_TENANT_ID = ':tenantId';
  static readonly BREADCRUMB_DRAFT_CREATE = `Création d'un dossier`;
  static readonly BREADCRUMB_FOLDER_ID = ':folderId';
  static readonly BREADCRUMB_CONNECTORS = 'Connecteurs';
  static readonly BREADCRUMB_CONNECTORS_EXTERNAL_SIGNATURE = 'Signature externe';
  static readonly BREADCRUMB_CONNECTORS_SECURE_MAIL = 'Mail sécurisé';
  static readonly BREADCRUMB_ADVANCED_ADMIN = 'Administration avancée';
  static readonly BREADCRUMB_ADVANCED_ADMIN_TEMPLATES = 'Templates';
  static readonly BREADCRUMB_ADVANCED_ADMIN_METADATA = 'Métadonnées';
  static readonly BREADCRUMB_ADVANCED_ADMIN_LAYERS = `Calques d'impression des dossiers`;
  static readonly BREADCRUMB_ADVANCED_ADMIN_SEAL_CERTIFICATES = 'Certificats de cachets serveurs';
  static readonly BREADCRUMB_ADMINISTRATION = 'Administration';
  static readonly BREADCRUMB_ADMINISTRATION_SERVER = 'Informations serveur';
  static readonly BREADCRUMB_ADMINISTRATION_TENANTS = 'Entités';
  static readonly BREADCRUMB_ADMINISTRATION_ALL_USERS = 'Tous les utilisateurs';
  static readonly BREADCRUMB_ADMINISTRATION_TENANT_USERS = 'Utilisateurs';
  static readonly BREADCRUMB_ADMINISTRATION_DESKS = 'Bureaux';
  static readonly BREADCRUMB_ADMINISTRATION_ABSENCES = 'Absences';
  static readonly BREADCRUMB_ADMINISTRATION_TYPOLOGY = 'Typologie des dossiers';
  static readonly BREADCRUMB_ADMINISTRATION_WORKFLOWS_DEFINITIONS = 'Circuits';
  static readonly BREADCRUMB_ADMINISTRATION_WORKFLOWS_EDITOR = 'Éditeur de circuits';
  static readonly BREADCRUMB_ADMINISTRATION_WORKFLOWS_EDITOR_NEW = 'Nouveau circuit';
  static readonly BREADCRUMB_ADMINISTRATION_FOLDERS = 'Dossiers';
}
