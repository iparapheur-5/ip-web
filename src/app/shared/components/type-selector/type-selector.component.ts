/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { compareById } from '../../../utils/string-utils';
import { CommonMessages } from '../../common-messages';
import { NotificationsService } from '../../../services/notifications.service';
import { Observable } from 'rxjs';
import { TypeRepresentation, PageTypeRepresentation } from '@libriciel/iparapheur-standard';

@Component({
  selector: 'app-type-selector',
  templateUrl: './type-selector.component.html',
  styleUrls: ['./type-selector.component.scss']
})
export class TypeSelectorComponent implements OnInit {


  readonly commonMessages = CommonMessages;
  readonly compareByIdFn = compareById;

  @Input() mandatory = false;
  @Input() disabled = false;
  @Input() placeholder = CommonMessages.NO_VALUE;
  @Input() type: TypeRepresentation;
  @Input() retrieveTypesFunction: (page: number, pageSize: number) => Observable<PageTypeRepresentation>;

  @Output() typeChange: EventEmitter<TypeRepresentation> = new EventEmitter<TypeRepresentation>();

  typeList: TypeRepresentation[];


  // <editor-fold desc="LifeCycle">


  constructor(private notificationsService: NotificationsService) { }


  ngOnInit() {
    this.refreshTypeList();
  }


  // </editor-fold desc="LifeCycle">


  refreshTypeList() {
    if (!this.retrieveTypesFunction) {
      console.error('Missing mandatory parameter retrieveTypeFunction');
      return;
    }

    const getTypes$ = this.retrieveTypesFunction(0, 999);

    getTypes$.subscribe(
      result => {
        this.typeList = result.content;

        const hasSingleResult = (result.content.length === 1);
        const hasEmptyOrBrokenSelection = (!this.type || !compareById(this.type, result.content[0]));
        if (this.mandatory && hasSingleResult && hasEmptyOrBrokenSelection) {
          this.type = result.content[0];
          this.onTypeSelectionChanged();
        }
      },
      error => this.notificationsService.showErrorMessage(CommonMessages.ERROR_RETRIEVING_TYPES, error.message)
    );
  }


  onTypeSelectionChanged() {
    this.typeChange.emit(this.type);
  }


}
