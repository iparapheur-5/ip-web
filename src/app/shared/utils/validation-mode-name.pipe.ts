/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { StepValidationMode } from '../../models/workflows/ip-step-model';

@Pipe({
  name: 'validationModeName'
})
export class ValidationModeNamePipe implements PipeTransform {


  public static compute(state: any, short: boolean): string {
    switch (state) {
      case StepValidationMode.Simple:
      case 'Simple': { return short ? 'Simple' : 'Étape simple'; }
      case StepValidationMode.And:
      case 'And': { return short ? 'Collaborative (ET)' : 'Étape collaborative (ET)'; }
      case StepValidationMode.Or:
      case 'Or': { return short ? 'Concurrente (OU)' : 'Étape concurrente (OU)'; }
      default: { return null; }
    }
  }


  transform(value: any, short?: boolean): string {
    return ValidationModeNamePipe.compute(value, short ?? false);
  }


}
