/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { NamePipe } from './name.pipe';


describe('NamePipe', () => {


  it('create an instance', () => {
    const pipe = new NamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return given string on valid input', () => {
    expect(NamePipe.compute('test')).toBe('test');
    expect(NamePipe.compute('1')).toBe('1');
  });


  it('return the default value on invalid input', () => {
    expect(NamePipe.compute('')).toBe(NamePipe.DEFAULT_VALUE);
    expect(NamePipe.compute(undefined)).toBe(NamePipe.DEFAULT_VALUE);
    expect(NamePipe.compute(null)).toBe(NamePipe.DEFAULT_VALUE);
  });


});
