/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { SecondaryAction } from '../models/secondary-action.enum';
import { Action } from '@libriciel/iparapheur-standard';

@Pipe({
  name: 'actionName'
})
export class ActionNamePipe implements PipeTransform {


  public static readonly UNKNOWN_VALUE = 'Action inconnue';


  public static compute(value: Action | SecondaryAction, current?: boolean, indirect?: boolean, fromHistoricState?: boolean) {
    switch (value) {

      case Action.Archive: { return fromHistoricState ? 'Terminé' : indirect ? 'À envoyer dans la corbeille' : 'Envoyer dans la corbeille'; }
      case Action.AskSecondOpinion: { return `Demande d'avis complémentaire`; }
      case Action.Bypass: { return `Contourner l'étape`; }
      case Action.Chain: { return 'Enchaîner un circuit'; }
      case Action.Create: { return 'Créer'; }
      case Action.Delete: { return fromHistoricState ? 'Rejeté' : indirect ? 'À supprimer' : 'Supprimer'; }
      case Action.ExternalSignature: { return indirect ? 'À envoyer en signature externe' : current ? 'Demander la signature externe' : 'Signature externe'; }
      case Action.Ipng: { return 'IPNG'; }
      case Action.IpngReturn: { return 'Retour IPNG'; }
      case Action.SecureMail: { return indirect ? 'À envoyer par mail sécurisé' : 'Mail sécurisé'; }
      case Action.PaperSignature: { return indirect ? 'À signer en papier' : 'Signature papier'; }
      case Action.Recycle: { return 'Recycler'; }
      case Action.Read: { return 'Lecture'; }
      case Action.Reject: { return 'Rejet'; }
      case Action.Seal: { return indirect ? 'À cacheter' : 'Cachet'; }
      case Action.SecondOpinion: { return indirect ? 'À valider en complément' : 'Avis complémentaire'; }
      case Action.Signature: { return indirect ? 'À signer' : 'Signature'; }
      case Action.Start: { return indirect ? 'À envoyer dans le circuit' : 'Envoyer dans le circuit' }
      case Action.Transfer : { return current ? 'Transférer le dossier' : `Transfert de dossier`; }
      case Action.Undo: { return 'Annulation'; }
      case Action.Update: { return 'Modification'; }
      case Action.Visa: { return indirect ? 'À viser' : 'Visa'; }

      case SecondaryAction.HistoryTasks: { return 'Journal des évènements'; }
      case SecondaryAction.IpngShowProof: { return 'Preuves de transfert IPNG'; }
      case SecondaryAction.Mail: { return current ? 'Envoyer par mail' : 'Envoi par mail'; }
      case SecondaryAction.Print: { return `Générer le PDF d'impression`; }
      case SecondaryAction.AddDesksToNotify: { return 'Ajouter des bureaux à notifier'; }
      case SecondaryAction.End: { return 'Fin de circuit'; }
      case SecondaryAction.Download: { return 'Télécharger le dossier'; }
      case SecondaryAction.StackedValidation: { return 'Valider'; }

      default: {
        console.log("Action inconnue : " + value)
        return ActionNamePipe.UNKNOWN_VALUE;
      }
    }
  }


  transform(value: Action | SecondaryAction, current?: boolean, indirect?: boolean, fromHistoricState?: boolean): any {
    return ActionNamePipe.compute(value, current ?? false, indirect ?? false, fromHistoricState ?? false);
  }


}
