/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { StepType } from '../../models/workflows/ip-step-model';
import { Action } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../models/secondary-action.enum';

@Pipe({
  name: 'stepToAction'
})
export class StepToActionPipe implements PipeTransform {


  public static compute(state: StepType): Action | SecondaryAction {
    switch (state) {
      case StepType.End: { return SecondaryAction.End; }
      case StepType.ExternalSignature: { return Action.ExternalSignature; }
      case StepType.Ipng: { return Action.Ipng; }
      case StepType.Seal: { return Action.Seal; }
      case StepType.SecondOpinion: { return Action.SecondOpinion; }
      case StepType.SecureMail: { return Action.SecureMail; }
      case StepType.Signature: { return Action.Signature; }
      case StepType.Start: { return Action.Start; }
      case StepType.Visa: { return Action.Visa; }
      default: { return null; }
    }
  }


  transform(value: StepType): Action | SecondaryAction {
    return StepToActionPipe.compute(value);
  }


}
