/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ActionNamePipe } from './action-name.pipe';
import { Action } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../models/secondary-action.enum';


describe('ActionNamePipe', () => {


  it('create an instance', () => {
    const pipe = new ActionNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input on Actions', () => {
    Object
      .keys(Action)
      .map(k => Action[k])
      .filter(a => a != Action.Unknown)
      .forEach(a => {
        expect(ActionNamePipe.compute(a)).toBeTruthy();
        expect(ActionNamePipe.compute(a)).not.toBe(ActionNamePipe.UNKNOWN_VALUE);
      });
  });


  it('return not null on valid input on SecondaryActions', () => {
    Object
      .keys(SecondaryAction)
      .map(k => SecondaryAction[k])
      .forEach(a => {
        expect(ActionNamePipe.compute(a)).toBeTruthy();
        expect(ActionNamePipe.compute(a)).not.toBe(ActionNamePipe.UNKNOWN_VALUE);
      });
  });


  it('return default value on invalid input', () => {
    expect(ActionNamePipe.compute(undefined)).toBe(ActionNamePipe.UNKNOWN_VALUE);
    expect(ActionNamePipe.compute(null)).toBe(ActionNamePipe.UNKNOWN_VALUE);
  });


});
