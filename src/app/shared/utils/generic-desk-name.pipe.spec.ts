/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { GenericDeskNamePipe } from './generic-desk-name.pipe';
import { GENERIC_DESK_IDS, GENERIC_DESK_NAMES } from '../../models/workflows/ip-workflow-actor';

describe('GenericDeskNamePipe', () => {


  it('create an instance', () => {
    const pipe = new GenericDeskNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return a template string on generic desk input', () => {
    const dummyEmitterDesk = {id: GENERIC_DESK_IDS.EMITTER_ID, name: 'should not appear', shortName: 'should not appear', ownerIds: []};
    expect(GenericDeskNamePipe.compute(dummyEmitterDesk)).toBe(GENERIC_DESK_NAMES.EMITTER);

    const dummyVariableDesk = {id: GENERIC_DESK_IDS.VARIABLE_DESK_ID, name: 'should not appear', shortName: 'should not appear', ownerIds: []};
    expect(GenericDeskNamePipe.compute(dummyVariableDesk)).toBe(GENERIC_DESK_NAMES.VARIABLE_DESK);

    const dummyBossOfDesk = {id: GENERIC_DESK_IDS.BOSS_OF_ID, name: 'should not appear', shortName: 'should not appear', ownerIds: []};
    expect(GenericDeskNamePipe.compute(dummyBossOfDesk)).toBe(GENERIC_DESK_NAMES.BOSS_OF);
  });


  it('return given desk name on regular input', () => {
    expect(GenericDeskNamePipe.compute({id: 'desk01', shortName: 'Desk 01', name: 'Desk 01', ownerIds: []})).toBe('Desk 01');
    expect(GenericDeskNamePipe.compute({id: 'desk01', shortName: '', name: '', ownerIds: []})).toBe('');
    expect(GenericDeskNamePipe.compute({id: 'desk01', shortName: undefined, name: undefined, ownerIds: null})).toBe(undefined);
    expect(GenericDeskNamePipe.compute({id: 'desk01', shortName: null, name: null, ownerIds: null})).toBe(null);
  });


  it('return undefined on invalid input', () => {
    expect(GenericDeskNamePipe.compute(undefined)).toBe(undefined);
    expect(GenericDeskNamePipe.compute(null)).toBe(undefined);
  });


});
