/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { WorkflowDefinition } from '../../models/workflow-definition';
import { GENERIC_DESK_IDS } from '../../models/workflows/ip-workflow-actor';

export class WorkflowUtils {
  static countVariableDeskStep(workflowModel: WorkflowDefinition): number {
    let variableDesksInStepsCount = 0;
    workflowModel.steps
      .map(step => step.validatingDesks)
      .map(validators => validators?.filter(v => v.id === GENERIC_DESK_IDS.VARIABLE_DESK_ID).length)
      .forEach(count => variableDesksInStepsCount += count);
    return (workflowModel.finalDesk.id === GENERIC_DESK_IDS.VARIABLE_DESK_ID ? 1 : 0) + variableDesksInStepsCount;
  }
}
