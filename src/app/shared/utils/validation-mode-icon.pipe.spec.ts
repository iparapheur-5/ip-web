/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ValidationModeIconPipe } from './validation-mode-icon.pipe';
import { StepValidationMode } from '../../models/workflows/ip-step-model';


describe('ValidationModeIconPipe', () => {


  it('create an instance', () => {
    const pipe = new ValidationModeIconPipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {

    // Proper type
    Object
      .keys(StepValidationMode)
      .map(k => StepValidationMode[k])
      .forEach(e => expect(ValidationModeIconPipe.compute(e)).toBeTruthy());

    // String key
    Object
      .keys(StepValidationMode)
      .map(k => StepValidationMode[k])
      .forEach(e => expect(ValidationModeIconPipe.compute(e)).toBeTruthy());
  });


  it('return null on invalid input', () => {
    expect(ValidationModeIconPipe.compute('not_a_valid_input')).toBeNull();
    expect(ValidationModeIconPipe.compute(undefined)).toBeNull();
    expect(ValidationModeIconPipe.compute(null)).toBeNull();
  });


});
