/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { StepToActionPipe } from './step-to-action.pipe';
import { StepType } from '../../models/workflows/ip-step-model';

describe('StepToActionPipe', () => {


  it('create an instance', () => {
    const pipe = new StepToActionPipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(StepType)
      .map(k => StepType[k])
      .forEach(e => expect(StepToActionPipe.compute(e)).toBeTruthy());
  });


  it('return null on invalid input', () => {
    expect(StepToActionPipe.compute(undefined)).toBeNull();
    expect(StepToActionPipe.compute(null)).toBeNull();
  });


});
