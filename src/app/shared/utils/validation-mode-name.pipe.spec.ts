/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ValidationModeNamePipe } from './validation-mode-name.pipe';
import { StepValidationMode } from '../../models/workflows/ip-step-model';

describe('ValidationModeNamePipe', () => {


  it('create an instance', () => {
    const pipe = new ValidationModeNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {

    // Proper type
    Object
      .keys(StepValidationMode)
      .map(k => StepValidationMode[k])
      .forEach(e => {
        expect(ValidationModeNamePipe.compute(e, true)).toBeTruthy();
        expect(ValidationModeNamePipe.compute(e, false)).toBeTruthy();
      });

    // String key
    Object
      .keys(StepValidationMode)
      .forEach(e => {
        expect(ValidationModeNamePipe.compute(e, true)).toBeTruthy();
        expect(ValidationModeNamePipe.compute(e, false)).toBeTruthy();
      });
  });


  it('return null on invalid input', () => {
    expect(ValidationModeNamePipe.compute('not_a_valid_input', true)).toBeNull();
    expect(ValidationModeNamePipe.compute('not_a_valid_input', false)).toBeNull();
    expect(ValidationModeNamePipe.compute(undefined, false)).toBeNull();
    expect(ValidationModeNamePipe.compute(undefined, false)).toBeNull();
    expect(ValidationModeNamePipe.compute(null, true)).toBeNull();
    expect(ValidationModeNamePipe.compute(null, false)).toBeNull();
  });


});
