/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { Action } from '@libriciel/iparapheur-standard';
import { faReply, faTimes, faEnvelope, faTrash, faRecycle, faRoad, faPrint, faShare, IconDefinition, faFlagCheckered, faPaperPlane, faDesktop, faEye, faRetweet, faDownload, faCheck } from '@fortawesome/free-solid-svg-icons';
import { faFile, faFlag, faListAlt } from '@fortawesome/free-regular-svg-icons';
import { lsExternalSignature, lsSignature, lsSeal, lsIpng, lsVisa, lsSecondOpinion } from '../models/icons';
import { CommonIcons } from '@libriciel/ls-composants';
import { SecondaryAction } from '../models/secondary-action.enum';


@Pipe({
  name: 'actionIcon'
})
export class ActionIconPipe implements PipeTransform {


  /**
   * @deprecated We should use the other one, to unify resources
   */
  public static computeToString(type: Action | SecondaryAction) {
    switch (type) {

      case Action.Archive: { return 'fa fa-trash'; }
      // TODO case Action.Bypass: { return fa;
      case Action.Chain: { return 'fa fa-road'; }
      case Action.Delete: { return 'fa fa-trash'; }
      case Action.ExternalSignature: { return 'fa ls-icon-signature-externe'; }
      case Action.Ipng: { return 'fa ls-icon-ipng'; }
      // TODO case Action.IpngReturn: { return 'fa ls-icon-ipng'; }
      case Action.SecureMail: { return 'fa fa-envelope'; }
      case Action.PaperSignature: { return 'fa fa-file-o'; } // ls-signature-o
      case Action.Read: { return 'fa fa-eye'; }
      case Action.Recycle: { return 'fa fa-recycle'; }
      case Action.Reject: { return 'fa fa-times'; }
      case Action.Seal: { return 'fa ls-cachet-o'; } // FIXME : that does not work in IP5-beta-14
      case Action.AskSecondOpinion: { return 'fa fa-retweet'; }
      case Action.SecondOpinion: { return 'fa fa-retweet'; }
      case Action.Signature: { return 'fa ls-icon-declare-signe'; } // ls-signature-o
      case Action.Start: { return 'fa fa-flag-o'; } // ls-signature-o
      case Action.Transfer: { return 'fa fa-share'; }
      case Action.Undo: { return 'fa fa-reply'; }
      case Action.Visa: { return 'fa fa-check-square-o'; }

      case SecondaryAction.End: { return 'fa fa-flag-checkered'; }
      case SecondaryAction.HistoryTasks: { return 'fa fa-list-alt'; }
      case SecondaryAction.Mail: { return 'fa fa-paper-plane'; }
      case SecondaryAction.Print: { return 'fa fa-print'; }
      case SecondaryAction.StackedValidation: { return 'fa fa-check'; }

      default: { return 'fa fa-check-square-o'; }
    }
  }


  public static computeFontAwesomeResource(type: Action | SecondaryAction, fromHistoricStateTaskList: boolean) {

    switch (type) {

      case Action.Archive: { return fromHistoricStateTaskList ? faFlagCheckered : CommonIcons.DELETE_ICON; }
      // TODO case Action.Bypass: { return fa;
      case Action.Chain: { return faRoad; }
      case Action.Delete: { return fromHistoricStateTaskList ? faTimes : faTrash; }
      case Action.ExternalSignature: { return lsExternalSignature; }
      case Action.Ipng: { return lsIpng; }
      // TODO case Action.IpngReturn: { return lsIpng;
      case Action.SecureMail: { return faEnvelope; }
      case Action.PaperSignature : { return faFile; }
      case Action.Read: { return faEye; }
      case Action.Recycle: { return faRecycle; }
      case Action.Reject: { return faTimes; }
      case Action.Seal: { return lsSeal; }
      case Action.Signature: { return lsSignature; }
      case Action.Start: { return faFlag; }
      case Action.Transfer: { return faShare; }
      case Action.Undo: { return faReply; }
      case Action.AskSecondOpinion: { return faRetweet }
      case Action.SecondOpinion: { return lsSecondOpinion }
      case Action.Visa: { return lsVisa; }

      case SecondaryAction.End: { return faFlagCheckered; }
      case SecondaryAction.HistoryTasks: { return faListAlt; }
      case SecondaryAction.IpngShowProof: { return faListAlt; }
      case SecondaryAction.Mail: { return faPaperPlane; }
      case SecondaryAction.Print: { return faPrint; }
      case SecondaryAction.AddDesksToNotify: { return faDesktop; }
      case SecondaryAction.Download: { return faDownload; }
      case SecondaryAction.StackedValidation: { return faCheck; }

      default: { return lsVisa; }
    }
  }


  transform(value: any, fromHistoricStateTaskList?: boolean): IconDefinition {
    return ActionIconPipe.computeFontAwesomeResource(value, fromHistoricStateTaskList ?? false);
  }


}
