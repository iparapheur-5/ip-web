/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ActionIconPipe } from './action-icon.pipe';
import { Action } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../models/secondary-action.enum';
import { lsVisa } from '../models/icons';

describe('ActionIconPipe', () => {


  it('create an instance', () => {
    const pipe = new ActionIconPipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input on Actions', () => {
    Object
      .keys(Action)
      .map(k => Action[k])
      .filter(a => a != Action.Unknown)
      .forEach(a => {
        expect(ActionIconPipe.computeFontAwesomeResource(a, true)).toBeTruthy();
        expect(ActionIconPipe.computeFontAwesomeResource(a, false)).toBeTruthy();
      });
  });


  it('return not null on valid input on SecondaryActions', () => {
    Object
      .keys(SecondaryAction)
      .map(k => SecondaryAction[k])
      .forEach(a => {
        expect(ActionIconPipe.computeFontAwesomeResource(a, true)).toBeTruthy();
        expect(ActionIconPipe.computeFontAwesomeResource(a, false)).toBeTruthy();
      });
  });


  it('return default value on invalid input', () => {
    expect(ActionIconPipe.computeFontAwesomeResource(Action.Unknown, true)).toBe(lsVisa);
    expect(ActionIconPipe.computeFontAwesomeResource(Action.Unknown, false)).toBe(lsVisa);
    expect(ActionIconPipe.computeFontAwesomeResource(undefined, true)).toBe(lsVisa);
    expect(ActionIconPipe.computeFontAwesomeResource(undefined, false)).toBe(lsVisa);
    expect(ActionIconPipe.computeFontAwesomeResource(null, true)).toBe(lsVisa);
    expect(ActionIconPipe.computeFontAwesomeResource(null, false)).toBe(lsVisa);
  });


});
