/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { HasName } from '../models/has-name';
import { Folder } from '../models/folder/folder';
import { SealCertificate } from '../models/seal-certificate';
import { Layer } from '../models/pdf-stamp/layer';
import { Metadata } from '../models/metadata';
import { CrudOperation } from '../services/crud-operation';
import { LoggableUser } from '../models/auth/loggable-user';
import { LoggablePassword } from '../models/auth/loggable-password';
import { LoggableSignatureImage } from '../models/auth/loggable-signature-image';
import { LoggableSubtypeLayer } from '../models/pdf-stamp/loggable-subtype-layer';
import '@angular/localize/init';
import { I18nPluralPipe, NgLocaleLocalization } from '@angular/common';
import { IpWorkflowModel } from '../models/workflows/ip-workflow-model';
import { WorkflowDefinition } from '../models/workflow-definition';
import { SecureMailServer } from '../models/securemail/secure-mail-server';

export class CommonMessages {

  static readonly APP_NAME = 'iparapheur';

  static readonly NOT_FOUND = 'Aucun élément trouvé';
  static readonly NO_DESK_FOUND = 'Aucun bureau trouvé';
  static readonly ERROR_MESSAGE_TITLE = 'Erreur';
  static readonly GLOBAL_ERROR_TITLE = `Message d'erreur`
  static readonly GLOBAL_ERROR = `Une erreur critique empêchant le bon fonctionnement de l'application est survenue`;
  static readonly NO_ELEMENT = `(Pas d'élément)`;
  static readonly ALL_ELEMENTS = '(Tous)';

  static readonly ACTION_RESULT_OK = 'OK';
  static readonly ACTION_RESULT_CANCEL = 'cancel';

  static readonly TAB_EXAMPLE = 'Prévisualisation';

  static readonly CLEAR = 'Effacer';
  static readonly FOLD = 'Plier';
  static readonly FOLD_ALL = 'Tout plier';
  static readonly EXPAND = 'Déplier';
  static readonly EXPAND_ALL = 'Tout déplier';
  static readonly CLOSE = 'Fermer';
  static readonly CANCEL = 'Annuler';
  static readonly EDIT = 'Modifier';
  static readonly REPLACE = 'Remplacer';
  static readonly DELETE = 'Supprimer';
  static readonly SEARCH = 'Rechercher';
  static readonly VALIDATE = 'Valider';
  static readonly ADD = 'Ajouter';
  static readonly REMOVE = 'Retirer';
  static readonly LINK = 'Associer';
  static readonly UNLINK = 'Dissocier';
  static readonly DOWNLOAD = 'Télécharger';
  static readonly RESET = 'Remise à zéro';
  static readonly KEY = 'Clé';

  static readonly DESK_LIST = 'Liste des bureaux';
  static readonly SEARCH_USER = 'Rechercher un utilisateur';
  static readonly SEARCH_DESK = 'Rechercher un bureau';
  static readonly SEARCH_TENANT = 'Rechercher une entité';

  static readonly TENANT = 'Entité';
  static readonly TENANTS = 'Entités';
  static readonly TENANT_LABEL = 'Entité\u00a0:';
  static readonly FOLDER_NAME = 'Dossier';
  static readonly FOLDER_NAME_PLURAL = 'Dossiers';
  static readonly TRASH_BIN_NAME = 'Dossier dans la corbeille';
  static readonly TRASH_BIN_NAME_PLURAL = 'Dossiers dans la corbeille';
  static readonly USER = 'Utilisateur';
  static readonly USERS = 'Utilisateurs';
  static readonly DESK = 'Bureau';
  static readonly DESKS = 'Bureaux';
  static readonly STEP_DESK_LIST = `Liste des bureaux de l'étape`;
  static readonly TYPE = 'Type';
  static readonly SUBTYPE = 'Sous-type';
  static readonly TYPE_AND_SUBTYPE_NAME = 'Type';
  static readonly TYPES_AND_SUBTYPES_NAME_PLURAL = 'Types et sous-types';
  static readonly SEAL_CERTIFICATE_NAME = 'Certificat de cachet serveur';
  static readonly SEAL_CERTIFICATE_NAME_PLURAL = 'Certificats de cachet serveur';
  static readonly WORKFLOW_NAME = 'Circuit';
  static readonly WORKFLOW_NAME_PLURAL = 'Circuits';
  static readonly PAGINATION_SIZE_LABEL = 'Afficher\u00a0:';
  static readonly ACTION = 'Action';
  static readonly ACTIONS = 'Actions';
  static readonly MAIL = 'E-mail';
  static readonly LAST_NAME = 'Nom';
  static readonly FIRST_NAME = 'Prénom';
  static readonly USERNAME = `Identifiant`;
  static readonly NAME = 'Nom';
  static readonly ID = 'Id';
  static readonly INFOS = 'Infos';
  static readonly PASSWORD = 'Mot de passe';
  static readonly FOLDER_TRAY = 'Banettes';

  static readonly ASSOCIATE_DESKS = 'Associer des bureaux';
  static readonly ASSOCIATED_DESKS = 'Bureaux associés';

  static readonly EXTERNAL_SIGNATURE_CONFIG_NAME = 'Configuration de signature externe';
  static readonly EXTERNAL_SIGNATURE_CONFIG_NAME_PLURAL = 'Configurations de signature externe';
  static readonly LAYER_NAME = 'Calque';
  static readonly LAYER_NAME_PLURAL = 'Calques';
  static readonly METADATA_NAME = 'Métadonnée';
  static readonly METADATA_NAME_PLURAL = 'Métadonnées';
  static readonly SECURE_MAIL_SERVER_NAME = 'Serveur de mails sécurisés';
  static readonly SECURE_MAIL_SERVER_NAME_PLURAL = 'Serveurs de mails sécurisés';
  static readonly TEMPLATE_NAME = 'Modèle';
  static readonly TEMPLATE_NAME_PLURAL = 'Modèles';
  static readonly DESCRIPTION = 'Description';
  static readonly STATE = 'État';
  static readonly NO_VALUE = '(Aucun)';
  static readonly UNREACHABLE_DESK = '(Bureau inaccessible)'
  static readonly UNREACHABLE_TENANT = '(Entité inaccessible)'
  static readonly UNREACHABLE_FOLDER = '(Dossier inaccessible)'
  static readonly DUE_DATE = 'Date limite';
  static readonly TITLE = 'Titre';

  static readonly CREATE_FOLDER = 'Créer un dossier';
  static readonly CREATION_WORKFLOW = 'Circuit de création';
  static readonly VALIDATION_WORKFLOW = 'Circuit de validation';
  static readonly WORKFLOW_STEPS_DEFINITION = 'Aperçu du circuit';

  static readonly CANNOT_LOAD_SERVER_PARAMETERS = 'Impossible de lire les paramètres du serveur';

  static readonly PUBLIC_ANNOTATION = 'Annotation publique';
  static readonly PRIVATE_ANNOTATION = 'Annotation privée';
  static readonly CUSTOM_SIGNATURE_FIELD = 'Texte personnalisé dans le visuel de signature';

  static readonly FILTER_BUTTON_LABEL = 'Filtrer';
  static readonly FILTER_FIELD_PH = 'Filtres personnalisés';

  static readonly ERROR_RETRIEVING_TYPES = 'Erreur à la récupération des types';
  static readonly ERROR_RETRIEVING_SUBTYPES = 'Erreur à la récupération des sous-types';
  static readonly CANNOT_OPEN_FOLDER = `Impossible d'ouvrir ce dossier`;
  static readonly INSUFFICIENT_RIGHTS = `Droits insuffisants`;

  static readonly WARNING_CHANGES_ARE_INSTANTANEOUS = `Attention, les modifications sont instantanées.`;
  static readonly SUPERVISORS_INFO = `Les superviseurs sont des acteurs en lecture seule sur un bureau.`;
  static readonly DELEGATION_MANAGERS_INFO = `Les gestionnaires d'absences peuvent déclarer une absence dans leurs paramètres pour les bureaux associés. Ces bureaux gérés s'ajoutent à leurs propres bureaux.`;

  static readonly DELETED_DESK_LABEL = '(Bureau supprimé)';

  static readonly EMPTY_DESK_LIST = 'Aucun bureau à afficher';
  static readonly EMPTY_FOLDER_LIST = 'Aucun dossier à afficher';
  static readonly EMPTY_USER_LIST = 'Aucun utilisateur à afficher';
  static readonly EMPTY_ABSENCE_LIST = 'Aucune absence à afficher';
  static readonly EMPTY_SEAL_CERTIFICATE_LIST = 'Aucun certificat de cachet serveur à afficher';
  static readonly EMPTY_METADATA_LIST = 'Aucune métadonnée à afficher';
  static readonly EMPTY_LAYER_LIST = 'Aucun calque à afficher';
  static readonly EMPTY_TENANT_LIST = 'Aucune entité à afficher';
  static readonly EMPTY_TYPOLOGY_LIST = 'Aucun type à afficher';
  static readonly EMPTY_WORKFLOW_LIST = 'Aucun circuit à afficher';
  static readonly EMPTY_CONNECTOR_LIST = 'Aucun connecteur à afficher';
  static readonly EMPTY_DOCUMENT_LIST = 'Aucun document à afficher';

  static readonly ADMIN_RIGHTS_REQUIRED_FOR_ACTION = 'Vous devez être administrateur pour effectuer cette action';

  static readonly pluralizationMap = new Map([
    [CommonMessages.FOLDER_NAME, {'=1': CommonMessages.FOLDER_NAME, 'other': CommonMessages.FOLDER_NAME_PLURAL}],
    [
      CommonMessages.SEAL_CERTIFICATE_NAME, {
      '=1': CommonMessages.SEAL_CERTIFICATE_NAME,
      'other': CommonMessages.SEAL_CERTIFICATE_NAME_PLURAL
    }
    ],
    [
      CommonMessages.SECURE_MAIL_SERVER_NAME, {
      '=1': CommonMessages.SECURE_MAIL_SERVER_NAME,
      'other': CommonMessages.SECURE_MAIL_SERVER_NAME_PLURAL
    }
    ],
    [
      CommonMessages.EXTERNAL_SIGNATURE_CONFIG_NAME, {
      '=1': CommonMessages.EXTERNAL_SIGNATURE_CONFIG_NAME,
      'other': CommonMessages.EXTERNAL_SIGNATURE_CONFIG_NAME_PLURAL
    }
    ],
    [
      CommonMessages.TYPE_AND_SUBTYPE_NAME, {
      '=1': CommonMessages.TYPE_AND_SUBTYPE_NAME,
      'other': CommonMessages.TYPES_AND_SUBTYPES_NAME_PLURAL
    }
    ],
    [CommonMessages.TRASH_BIN_NAME, {'=1': CommonMessages.TRASH_BIN_NAME, 'other': CommonMessages.TRASH_BIN_NAME_PLURAL}],
    [CommonMessages.LAYER_NAME, {'=1': CommonMessages.LAYER_NAME, 'other': CommonMessages.LAYER_NAME_PLURAL}],
    [CommonMessages.WORKFLOW_NAME, {'=1': CommonMessages.WORKFLOW_NAME, 'other': CommonMessages.WORKFLOW_NAME_PLURAL}],
    [CommonMessages.DESK, {'=1': CommonMessages.DESK, 'other': CommonMessages.DESKS}],
    [CommonMessages.METADATA_NAME, {'=1': CommonMessages.METADATA_NAME, 'other': CommonMessages.METADATA_NAME_PLURAL}],
    [CommonMessages.USER, {'=1': CommonMessages.USER, 'other': CommonMessages.USERS}],
    [CommonMessages.TEMPLATE_NAME, {'=1': CommonMessages.TEMPLATE_NAME, 'other': CommonMessages.TEMPLATE_NAME_PLURAL}],
    [CommonMessages.TENANT, {'=1': CommonMessages.TENANT, 'other': CommonMessages.TENANTS}],
  ]);


  static getDeskNumberLabel = (count: number) => (count <= 1) ? `${count} bureau` : `${count} bureaux`;


  static getMinDescriptionLabelInfo(min: number) {
    return `min. ${min} ${(min == 1) ? 'caractère' : 'caractères'}`;
  }


  static foldersValidationPopupLabel(folders: Folder[]) {
    return (folders.length == 1)
      ? `Êtes-vous sûr de vouloir supprimer le dossier "${folders[0].name}" ?`
      : `Êtes-vous sûr de vouloir supprimer ces "${folders.length}" dossiers ?`;
  }


  static foldersValidationPopupTitle(folders: Folder[]) {
    return (folders.length == 1)
      ? `Suppression du dossier "${folders[0].name}".`
      : `Suppression de "${folders.length} dossiers".`;
  }


  static getSuccessCreationMessage(entity: HasName): string {
    switch (entity.constructor) {
      case Folder:
        return `Le dossier ${entity.name} a été créé avec succès`;
      case SecureMailServer:
        return `La configuration ${entity.name} a été créée avec succès`;
      case SealCertificate:
        return `Le certificat de cachet serveur ${entity.name} a été créé avec succès`;
      case Layer:
        return `Le calque ${entity.name} a été créé avec succès`;
      case Metadata:
        return `La métadonnée ${entity.name} a été créée avec succès`;
      case IpWorkflowModel:
      case WorkflowDefinition:
        return `Le circuit ${entity.name} a été créé avec succès`;
      case LoggableUser:
        return `L'utilisateur ${entity.name} a été créé avec succès`;
      case LoggablePassword:
        return `Le mot de passe a été créé avec succès`;
      case LoggableSignatureImage:
        return `L'image de signature a été créée avec succès`;
      case LoggableSubtypeLayer:
        return `L'association du sous-type et du calque a été créée avec succès`;
    }

    return '';
  }


  static getPluralized(name: string, count: number) {
    const map = CommonMessages.pluralizationMap.get(name);
    return map ? new I18nPluralPipe(new NgLocaleLocalization('fr')).transform(count, map) : name;
  }


  static getUpdateCreationMessage(entity: HasName): string {
    switch (entity.constructor) {
      case Folder:
        return `Le dossier ${entity.name} a été modifié avec succès`;
      case SecureMailServer:
        return `La configuration ${entity.name} a été modifiée avec succès`;
      case SealCertificate:
        return `Le certificat de cachet serveur ${entity.name} a été modifié avec succès`;
      case Layer:
        return `Le calque ${entity.name} a été modifié avec succès`;
      case Metadata:
        return `La métadonnée ${entity.name} a été modifiée avec succès`;
      case IpWorkflowModel:
      case WorkflowDefinition:
        return `Le circuit ${entity.name} a été modifié avec succès`;
      case LoggableUser:
        return `L'utilisateur ${entity.name} a été modifié avec succès`;
      case LoggablePassword:
        return `Le mot de passe a été modifié avec succès`;
      case LoggableSignatureImage:
        return `L'image de signature a été modifiée avec succès`;
      case LoggableSubtypeLayer:
        return `L'association du sous-type et du calque a été modifiée avec succès`;
    }

    return '';
  }


  static getDeleteCreationMessage(entity: HasName): string {
    switch (entity.constructor) {
      case Folder:
        return `Le dossier ${entity.name} a été supprimé avec succès`;
      case SecureMailServer:
        return `La configuration ${entity.name} a été supprimée avec succès`;
      case SealCertificate:
        return `Le certificat de cachet serveur ${entity.name} a été supprimé avec succès`;
      case Layer:
        return `Le calque ${entity.name} a été supprimé avec succès`;
      case Metadata:
        return `La métadonnée ${entity.name} a été supprimée avec succès`;
      case IpWorkflowModel:
      case WorkflowDefinition:
        return `Le circuit ${entity.name} a été modifié avec succès`;
      case LoggableUser:
        return `L'utilisateur ${entity.name} a été supprimé avec succès`;
      case LoggablePassword:
        return `Le mot de passe a été supprimé avec succès`;
      case LoggableSignatureImage:
        return `L'image de signature a été supprimée avec succès`;
      case LoggableSubtypeLayer:
        return `L'association du sous-type et du calque a été supprimée avec succès`;
    }

    return '';
  }


  static getMultipleDeleteCreationMessage(folders: Folder[]): string {
    return (folders.length === 1)
      ? this.getDeleteCreationMessage(folders[0])
      : `Les ${folders} dossiers ont été supprimés avec succès`;
  }


  static getEntityNameWithPreposition(entity: HasName): string {
    switch (entity.constructor) {
      case Folder:
        return 'du dossier';
      case SealCertificate:
        return 'du certificat de cachet serveur';
      case Layer:
        return 'du calque';
      case Metadata:
        return 'de la métadonnée';
      case LoggableUser:
        return `de l'utilisateur`;
      case LoggablePassword:
        return `du mot de passe`;
      case LoggableSignatureImage:
        return `de l'image de signature`;
      case IpWorkflowModel:
      case WorkflowDefinition:
        return `du circuit`;
      case LoggableSubtypeLayer:
        return `de l'association du sous-type et du calque`;
    }

    return '';
  }


  static getCrudSuccessMessage(operation: CrudOperation, entity: HasName): string {
    switch (operation) {
      case CrudOperation.Create:
        return this.getSuccessCreationMessage(entity);
      case CrudOperation.Update:
        return this.getUpdateCreationMessage(entity);
      case CrudOperation.Delete:
        return this.getDeleteCreationMessage(entity);
    }
  }


  static getCrudErrorMessage(operation: CrudOperation, entity: HasName): string {
    const entityName = this.getEntityNameWithPreposition(entity);
    switch (operation) {
      case CrudOperation.Create:
        return `Erreur lors de la création ${entityName} ${entity.name}`;
      case CrudOperation.Read:
        return `Erreur lors de la récupération ${entityName} ${entity.name}`;
      case CrudOperation.Update:
        return `Erreur lors de la modification ${entityName} ${entity.name}`;
      case CrudOperation.Delete:
        return `Erreur lors de la suppression ${entityName} ${entity.name}`;
    }
  }


}
