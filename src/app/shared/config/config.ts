/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export let CONFIG: {
  BASE_API_URL: string;
  APPLICATION_URL: string,
  LIBERSIGN_UPDATE_URL: string,
  VERSION: string,
  KEYCLOAK_URL: string,
  KEYCLOAK_REALM: string,
  KEYCLOAK_CLIENT_ID: string
} = {
  BASE_API_URL: null,
  APPLICATION_URL: null,
  LIBERSIGN_UPDATE_URL: null,
  VERSION: null,
  KEYCLOAK_URL: null,
  KEYCLOAK_REALM: null,
  KEYCLOAK_CLIENT_ID: null
};
