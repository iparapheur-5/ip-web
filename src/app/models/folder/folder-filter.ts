/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { HasId } from '../has-id';
import { State, FolderFilterDto } from '@libriciel/iparapheur-standard';

export class FolderFilter implements FolderFilterDto, HasId {

  id: string;
  userId: string;
  filterName: string;
  typeId: string;
  subtypeId: string;
  from?: Date;
  to?: Date;
  searchData: string;
  state: State;


  static getFolderFilterForState(state: State) : FolderFilterDto {
    const folderFilter = new FolderFilter();
    folderFilter.state = state;
    return folderFilter;
  }


  static equals(filter1: FolderFilterDto, filter2: FolderFilterDto) {
    if (!filter1 && !filter2) {
      return true;
    }
    if (!filter1 && filter2 || filter1 && !filter2) {
      return false;
    }
    return (!filter1.id && !filter2.id || filter1.id === filter2.id) &&
      (!filter1.userId && !filter2.userId || filter1.userId === filter2.userId) &&
      (!filter1.typeId && !filter2.typeId || filter1.typeId === filter2.typeId) &&
      (!filter1.subtypeId && !filter2.subtypeId || filter1.subtypeId === filter2.subtypeId) &&
      (!filter1.from && !filter2.from || filter1.from === filter2.from) &&
      (!filter1.to && !filter2.to || filter1.to === filter2.to) &&
      (!filter1.searchData && !filter2.searchData || filter1.searchData === filter2.searchData) &&
      (!filter1.state && !filter2.state || filter1.state === filter2.state);
  }


}
