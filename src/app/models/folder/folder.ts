/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Task } from '../task';
import { Document } from '../document';
import { HasId } from '../has-id';
import { HasName } from '../has-name';
import { State, FolderVisibility, DeskRepresentation, TypeDto, SubtypeDto } from '@libriciel/iparapheur-standard';


export class Folder implements HasId, HasName {

  id: string;
  name: string;
  stepList: Array<Task>;
  documentList: Array<Document>;
  metadata: Map<string, string>;
  state: State;
  originDesk: DeskRepresentation;
  finalDesk: DeskRepresentation;

  dueDate: Date;
  draftCreationDate: Date;
  sentToArchivesDate: Date;

  type: TypeDto;
  subtype: SubtypeDto;
  readByCurrentUser: boolean;
  selected: boolean = false;
  populatedMandatoryStepMetadata: { [p: string]: string } = {};
  visibility: FolderVisibility;

}
