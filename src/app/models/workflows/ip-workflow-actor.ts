/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { WorkflowActor } from '@libriciel/ls-composants/workflows';
import { User } from '../auth/user';
import { DeskDto } from '@libriciel/iparapheur-provisioning';

export class IpWorkflowActor extends WorkflowActor {


  name: string;


  constructor(data?: DeskDto | User | { id: string, name: string }) {
    super();

    if (data) {
      this.id = data.id;

      if (data instanceof User) {
        this.name = data.userName;
        this.firstname = data.firstName;
        this.lastname = data.lastName;
      } else {
        this.name = data.name;
      }
    }
  }


}


export const GENERIC_DESK_IDS = {
  EMITTER_ID: '##EMITTER##',
  VARIABLE_DESK_ID: '##VARIABLE_DESK##',
  BOSS_OF_ID: '##BOSS_OF##'
};

export const GENERIC_DESK_NAMES = {
  EMITTER: 'Émetteur',
  VARIABLE_DESK: 'Bureau Variable',
  BOSS_OF: 'Chef de'
};


export const GenericWorkflowActors = {
  EMITTER: new IpWorkflowActor({id: GENERIC_DESK_IDS.EMITTER_ID, name: GENERIC_DESK_NAMES.EMITTER}),
  VARIABLE_DESK: new IpWorkflowActor({id: GENERIC_DESK_IDS.VARIABLE_DESK_ID, name: GENERIC_DESK_NAMES.VARIABLE_DESK}),
  BOSS_OF: new IpWorkflowActor({id: GENERIC_DESK_IDS.BOSS_OF_ID, name: GENERIC_DESK_NAMES.BOSS_OF})
};


export const GENERIC_DESK_MAP: ReadonlyMap<string, IpWorkflowActor> = new Map([
  [GENERIC_DESK_IDS.EMITTER_ID, GenericWorkflowActors.EMITTER],
  [GENERIC_DESK_IDS.VARIABLE_DESK_ID, GenericWorkflowActors.VARIABLE_DESK],
  [GENERIC_DESK_IDS.BOSS_OF_ID, GenericWorkflowActors.BOSS_OF]
]);
