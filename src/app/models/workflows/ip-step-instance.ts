/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { StepInstance } from '@libriciel/ls-composants/workflows';
import { WorkflowActor } from '@libriciel/ls-composants/workflows/shared/model/workflows/workflow-actor';
import { StepType } from './ip-step-model';
import { State, DeskRepresentation } from '@libriciel/iparapheur-standard';

export class IpStepInstance extends StepInstance {


  completedDate: Date;
  state: State;
  delegatedByDesk: DeskRepresentation;
  workflowIndex: number;
  stepIndex: number;


  constructor(data?: {
    id: string;
    name: string;
    type: StepType;
    validators: WorkflowActor[];
    delegatedByDesk: DeskRepresentation;
    state: State;
    actedUponBy?: WorkflowActor;
    comment?: string;
    actionId?: string;
    workflowIndex?: number;
    stepIndex?: number;
  }) {
    super(data);

    // TODO this should be fixed in the shared lib
    this.type = data.type;
    this.delegatedByDesk = data.delegatedByDesk;
    this.workflowIndex = data.workflowIndex;
    this.stepIndex = data.stepIndex;
  }
}
