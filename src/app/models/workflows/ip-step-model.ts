/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { StepModel } from '@libriciel/ls-composants/workflows';
import { Metadata } from '../metadata';
import { ActionNamePipe } from '../../shared/utils/action-name.pipe';
import { StepToActionPipe } from '../../shared/utils/step-to-action.pipe';
import { DeskRepresentation } from '@libriciel/iparapheur-standard';


export enum StepType {
  Start = 'START',
  Visa = 'VISA',
  Signature = 'SIGNATURE',
  ExternalSignature = 'EXTERNAL_SIGNATURE',
  Ipng = 'IPNG',
  SecureMail = 'SECURE_MAIL',
  Seal = 'SEAL',
  SecondOpinion = 'SECOND_OPINION',
  End = 'END',
}


export enum StepValidationMode {
  Simple = 'SIMPLE',
  Or = 'OR',
  And = 'AND'
}


export const STEP_TYPE_LIST: StepType[] = Object.keys(StepType)
  .map(key => StepType[key])
  .filter(step => step !== StepType.Start)
  .filter(step => step !== StepType.SecondOpinion)
  .filter(step => step !== StepType.End);


export class IpStepModel extends StepModel {


  type: StepType;
  validationMode: StepValidationMode = StepValidationMode.Simple;
  notifiedDesks: DeskRepresentation[] = [];
  mandatoryValidationMetadata: Metadata[] = [];
  mandatoryRejectionMetadata: Metadata[] = [];


  constructor(data?: IpStepModel) {
    super();
    if (data) {
      let action = StepToActionPipe.compute(data.type);
      this.name = ActionNamePipe.compute(action);
      this.type = data.type;
      this.validationMode = data.validationMode || StepValidationMode.Simple;
      this.validators = !!data.validators ? [...data.validators] : [];
      this.notifiedDesks = data.notifiedDesks;
      this.mandatoryValidationMetadata = data.mandatoryValidationMetadata;
      this.mandatoryRejectionMetadata = data.mandatoryRejectionMetadata;

      if (this.validators?.length > 1 && data.validationMode) {
        this.validationMode = (data.validationMode === StepValidationMode.And) ? StepValidationMode.And : StepValidationMode.Or;
      }
    }
  }

  clone(): IpStepModel {
    // Strange case : a simple Object.assign keeps reference.
    // Quick fix : go for the JSON trick.
    return Object.assign(new IpStepModel(), JSON.parse(JSON.stringify(this)));
  }


}
