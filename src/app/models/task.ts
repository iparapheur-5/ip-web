/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ExternalState } from './external-state.enum';
import { User } from './auth/user';
import { Action, State, DeskRepresentation } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../shared/models/secondary-action.enum';


export class Task {

  id: string;
  action: Action | SecondaryAction;
  performedAction: Action | SecondaryAction;
  beginDate: Date;
  state: State;
  externalState: ExternalState;
  date: Date;
  desks: Array<DeskRepresentation>;
  delegatedByDesk: DeskRepresentation;
  user: User;
  publicAnnotation: string;
  privateAnnotation: string;
  externalSignatureProcedureId: string;
  metadata: any;
  workflowIndex: number;
  stepIndex: number;
  mandatoryValidationMetadata: Array<string>;
  mandatoryRejectionMetadata: Array<string>;
  notifiedDeskIds: Array<DeskRepresentation>;

}
