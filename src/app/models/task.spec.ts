/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Task } from './task';
import { Action, State } from '@libriciel/iparapheur-standard';


describe('Task', () => {


  it('should create an instance', () => {
    expect(new Task()).toBeTruthy();
  });


  it('should deserialize a full string', () => {

    const toParse: string = '' +
      '{' +
      '  "id":"ffff-ffff-ffff",' +
      '  "metadata":{' +
      '    "workflow_internal_origin_group_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '    "workflow_internal_final_group_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '    "workflow_internal_steps":"[]",' +
      '    "i_Parapheur_internal_emitter_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '    "workflow_internal_validation_start_date":"2021-05-28T09:41:09.954+0000",' +
      '    "workflow_internal_validation_workflow_id":"simple_workflow_38738",' +
      '    "workflow_internal_workflow_index":"0"' +
      '  },' +
      '  "action":"START",' +
      '  "state":"VALIDATED",' +
      '  "desks":[' +
      '    {' +
      '      "id":"dddd-dddd-dddd",' +
      '      "name":"Bureau de truc"' +
      '    }' +
      '  ],' +
      '  "user":{' +
      '    "id":"uuuu-uuuu-uuuu",' +
      '    "userName":"user",' +
      '    "firstName":"Marty",' +
      '    "lastName":"McFly"' +
      '  },' +
      '  "beginDate":"2021-05-28T19:40:46.389+00:00",' +
      '  "date":"2021-05-28T19:41:09.978+00:00"' +
      '}';

    const task: Task = JSON.parse(toParse);

    expect(task).toBeTruthy();
    expect(task.id).toBe('ffff-ffff-ffff');
    expect(task.action).toBe(Action.Start);
    expect(task.state).toBe(State.Validated);
    expect(task.desks).toBeTruthy();
    expect(task.desks.length).toBe(1);
    expect(task.desks[0].id).toBe('dddd-dddd-dddd');
    expect(task.desks[0].name).toBe('Bureau de truc');
    expect(task.user).toBeTruthy();
    expect(task.user.id).toBe('uuuu-uuuu-uuuu');
    expect(task.user.userName).toBe('user');
    expect(task.user.firstName).toBe('Marty');
    expect(task.user.lastName).toBe('McFly');
    expect(task.beginDate).toBeTruthy();
    expect(new Date(task.beginDate).getTime()).toBe(1622230846389);
    expect(task.date).toBeTruthy();
    expect(new Date(task.date).getTime()).toBe(1622230869978);
  });


});
