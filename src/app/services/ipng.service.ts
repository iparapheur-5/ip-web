/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { NotificationsService } from './notifications.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Deskbox, DeskboxAssociation } from '../models/ipng/deskbox';
import { IpngMetadata, MetadataAssociation } from '../models/ipng/ipng-metadata';
import { IpngParams } from '../models/ipng/ipng-params';
import { IpngEntity } from '../models/ipng/ipng-entity';
import { catchError, map } from 'rxjs/operators';
import { CONFIG } from '../shared/config/config';
import { IpngTypology, TypeAssociation } from '../models/ipng/ipng-typology';


@Injectable({
  providedIn: 'root'
})
export class IpngService {


  constructor(private notificationService: NotificationsService, public http: HttpClient) {
  }

  createDeskbox(tenantId: string, deskbox: Deskbox): Observable<Deskbox> {
    return this.http
      .post<Deskbox>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/deskboxProfile`, deskbox)
      .pipe(catchError(this.notificationService.handleHttpError('createDeskbox')));
  }

  updateDeskbox(tenantId: string, deskbox: Deskbox): Observable<Deskbox> {
    return this.http
      .put<Deskbox>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/deskboxProfile`, deskbox)
      .pipe(catchError(this.notificationService.handleHttpError('updatedeskbox')));
  }

  listEntities(tenantId: string): Observable<IpngEntity[]> {
    // TODO tenant is not actually used, maybe get rid of the parameter and pass a dummy tenantId
    return this.http
      .get<IpngEntity[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/entity`)
      .pipe(
        catchError(this.notificationService.handleHttpError('listEntities'))
      );
  }


  getEntityDeskboxList(tenantId: string, ipngEntityId: string): Observable<Deskbox[]> {
    return this.http
      .get<IpngEntity>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/entity/${ipngEntityId}`)
      .pipe(
        map(entity => entity.deskboxProfiles),
        catchError(this.notificationService.handleHttpError('getEntityDeskboxList'))
      );
  }

  getDeskbox(tenantId: string, deskboxProfileId: string): Observable<Deskbox> {
    return this.http
      .get<Deskbox>(`${CONFIG.BASE_API_URL}/deskboxProfile/${deskboxProfileId}`)
      .pipe(catchError(this.notificationService.handleHttpError('getDeskbox')));
  }


  getLatestMetadataList(tenantId: string): Observable<IpngMetadata[]> {
    return this.http
      .get<IpngMetadata[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/metadata/latest`)
      .pipe(catchError(this.notificationService.handleHttpError('getLatestMetadataList')));
  }


  getLatestTypologyList(tenantId: string): Observable<IpngTypology> {
    return this.http
      .get<IpngTypology>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/typology/latest`)
      .pipe(
        catchError(this.notificationService.handleHttpError('getLatestTypologyList'))
      );
  }

  /**
   *
   *  Mappings
   *
   */

  getEntitiesManagedByTenant(tenantId: string): Observable<IpngEntity[]> {
    return this.http
      .get<IpngEntity[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity`)
      .pipe(
        catchError(this.notificationService.handleHttpError('getEntitiesManagedByTenant'))
      );
  }


  // <editor-fold desc="Deskboxes">

  linkDeskAndDeskbox(tenantId: string, ipngEntityId: string, deskId: string, deskboxId: string): Observable<void> {
    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/linked-deskboxes/desk/${deskId}/deskbox/${deskboxId}`, {})
      .pipe(
        catchError(this.notificationService.handleHttpError('linkDeskAndDeskbox'))
      );
  }

  unlinkDeskAndDeskbox(tenantId: string, ipngEntityId: string, deskId: string, deskboxId: string): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/linked-deskboxes/desk/${deskId}/deskbox/${deskboxId}`)
      .pipe(
        catchError(this.notificationService.handleHttpError('unlinkDeskAndDeskbox'))
      );
  }

  getDeskboxesMapping(tenantId: string, ipngEntityId: string): Observable<DeskboxAssociation[]> {
    return this.http
      .get<DeskboxAssociation[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/linked-deskboxes`)
      .pipe(
        catchError(this.notificationService.handleHttpError('getDeskboxesMapping'))
      );
  }

  // </editor-fold desc="Deskboxes">


  // <editor-fold desc="Types">

  addOutgoingTypeMapping(tenantId: string, ipngEntityId: string, data: TypeAssociation[]): Observable<void> {
    return this.http
      .post<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/outgoing-types`, data)
      .pipe(
        catchError(this.notificationService.handleHttpError('addOutgoingTypeMapping'))
      );
  }

  getOutgoingTypeMapping(tenantId: string, ipngEntityId: string): Observable<TypeAssociation[]> {
    return this.http
      .get<TypeAssociation[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/outgoing-types`)
      .pipe(
        catchError(this.notificationService.handleHttpError('getOutgoingTypeMapping'))
      );
  }

  dissociateOutgoingTypes(tenantId: string, ipngEntityId: string, subetypeId: string): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/outgoing-types/subtype/${subetypeId}`)
      .pipe(
        catchError(this.notificationService.handleHttpError('dissociateOutgoingTypes'))
      );
  }

  //

  addIncomingTypeMapping(tenantId: string, ipngEntityId: string, data: TypeAssociation[]): Observable<void> {
    return this.http
      .post<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/incoming-types`, data)
      .pipe(
        catchError(this.notificationService.handleHttpError('addIncomingTypeMapping'))
      );
  }

  getIncomingTypeMapping(tenantId: string, ipngEntityId: string): Observable<TypeAssociation[]> {
    return this.http
      .get<TypeAssociation[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/incoming-types`)
      .pipe(
        catchError(this.notificationService.handleHttpError('getIncomingTypeMapping'))
      );
  }

  dissociateIncomingTypes(tenantId: string, ipngEntityId: string, ipngTypeId: string): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/incoming-types/ipngType/${ipngTypeId}`)
      .pipe(
        catchError(this.notificationService.handleHttpError('dissociateIncomingTypes'))
      );
  }

  // </editor-fold desc="Types">

  // <editor-fold desc="Metadata">

  addMetadataMapping(tenantId: string, ipngEntityId: string, data: MetadataAssociation[]): Observable<void> {
    return this.http
      .post<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/metadata`, data)
      .pipe(
        catchError(this.notificationService.handleHttpError('addMetadataMapping'))
      );
  }

  getMetadataMapping(tenantId: string, ipngEntityId: string): Observable<MetadataAssociation[]> {
    return this.http
      .get<MetadataAssociation[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/metadata`)
      .pipe(
        catchError(this.notificationService.handleHttpError('getMetadataMapping'))
      );
  }

  dissociateMetadata(tenantId: string, ipngEntityId: string, tenantMetadataKey: string): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/ipng/linked-entity/${ipngEntityId}/metadata/${tenantMetadataKey}`)
      .pipe(
        catchError(this.notificationService.handleHttpError('dissociateMetadata'))
      );
  }

  // </editor-fold desc="Metadata">

  // </editor-fold desc="Mappings">

  /*
   * Other
   */

  getHealthStates(tenantId: string): Observable<any[]> {
    return this.http
      .get<any[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/health_state`)
      .pipe(catchError(this.notificationService.handleHttpError('getHealthState')));
  }

  executeIpng(tenantId: string, deskId: string, folderId: string, taskId: string, ipngParams: IpngParams): Observable<any> {
    return this.http
      .put(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folderId}/task/${taskId}/ipng`, ipngParams)
      .pipe(catchError(this.notificationService.handleHttpError('executeIpng')));
  }


}
