/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { RxStomp } from '@stomp/rx-stomp';
import { Observable, from, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DeskRepresentation, DeskCount } from '@libriciel/iparapheur-standard';
import { CONFIG } from '../../shared/config/config';
import { websocketConfig } from './websocket-config';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService extends RxStomp {

  applicationHost: string;
  applicationUrl: string;

  constructor(private keycloakService: KeycloakService) {
    super();

    this.applicationHost = CONFIG?.APPLICATION_URL?.startsWith('https')
      ? 'wss'
      : 'ws';

    this.applicationUrl = this.applicationHost === 'wss'
      ? CONFIG?.APPLICATION_URL?.substring(8)
      : CONFIG?.APPLICATION_URL?.substring(7);

    websocketConfig.brokerURL = `${this.applicationHost}://${this.applicationUrl}/api/websocket`;

  }

  watchForDeskCountChanges(deskList: DeskRepresentation[]): Observable<Observable<DeskCount>[]> {
    return from(this.connect())
      .pipe(map(() => {
        let result: Observable<DeskCount>[] = [];
        deskList
          .filter(desk => !!desk)
          .forEach(desk => {
            result.push(this.watch({destination: `/websocket/tenant/${desk.tenantId}/desk/${desk.id}/folder/watch`})
              .pipe(map(message => JSON.parse(message.body) as DeskCount))
            );
          });
        return result;
      }));
  }

  notifyFolderAction(tenantId: string, deskId: string): void {
    this.connect().subscribe(() => this.publish({destination: `/websocket/tenant/${tenantId}/desk/${deskId}/folder/notify`}));
  }

  connect(): Observable<any> {
    if (this.active) return of("empty_token");

    return from(this.keycloakService.getToken())
      .pipe(tap(token => {
        websocketConfig.connectHeaders = {
          "Authorization": token
        }
        this.configure(websocketConfig);
        this.activate();
      }));
  }
}
