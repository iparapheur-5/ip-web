/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DeskDto } from '@libriciel/iparapheur-standard';


/**
 * This service allows to share the current user between components.
 *
 * Note that the deskId is often available through the route data,
 * this is mainly for the components that are not in the router-outlet.
 */
@Injectable({
  providedIn: 'root'
})
export class SelectedDeskService {


  selectedDeskBehaviourSubject = new BehaviorSubject<DeskDto>(null);
  selectedDesk: Observable<DeskDto> = this.selectedDeskBehaviourSubject.asObservable();


  update(newDesk: DeskDto) {
    this.selectedDeskBehaviourSubject.next(newDesk);
  }

}
