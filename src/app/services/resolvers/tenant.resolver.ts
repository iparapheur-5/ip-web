/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CommonMessages } from '../../shared/common-messages';
import { TenantService, TenantRepresentation } from '@libriciel/iparapheur-standard';
import { NotificationsService } from '../notifications.service';


@Injectable({
  providedIn: 'root'
})
export class TenantResolver implements Resolve<TenantRepresentation> {


  constructor(private tenantService: TenantService,
              private notificationsService: NotificationsService) {}


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TenantRepresentation> {

    const tenantId = route.paramMap.get('tenantId');

    if (!tenantId) {
      return of({id: null, name: null});
    }

    return this.tenantService
      .getTenant(tenantId)
      .pipe(catchError(this.notificationsService.handleHttpError('getTenant')))
      .pipe(catchError(() => of({id: tenantId, name: CommonMessages.UNREACHABLE_TENANT})));
  }


}
