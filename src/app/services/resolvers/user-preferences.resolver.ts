/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { FilterService } from '../filter.service';
import { NotificationsService } from '../notifications.service';
import { ResolverMessages } from './resolver-messages';
import { FolderViewBlock, CurrentUserService, UserPreferencesDto, ArchiveViewColumn, TaskViewColumn } from '@libriciel/iparapheur-standard';
import { FolderFilter } from '../../models/folder/folder-filter';
import { SelectedUserPreferencesService } from '../selected-user-preferences.service';

@Injectable({
  providedIn: 'root'
})
export class UserPreferencesResolver implements Resolve<UserPreferencesDto> {


  constructor(private currentUserService: CurrentUserService,
              private filterService: FilterService,
              private notificationService: NotificationsService,
              private selectedUserPreferencesService: SelectedUserPreferencesService) { }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserPreferencesDto> | Promise<UserPreferencesDto> | UserPreferencesDto {
    return this.currentUserService
      .getPreferences()
      .pipe(
        catchError(() => {
          this.notificationService.setGlobalError(ResolverMessages.ERROR_FETCHING_USER_PREF);
          return of({
            taskViewDefaultPageSize: 10,
            archiveViewDefaultPageSize: 10,
            showAdminIds: false,
            taskViewDefaultSortBy: TaskViewColumn.FolderName,
            taskViewDefaultAsc: true,
            currentFilter: null,
            currentFilterId: null,
            folderViewBlockList: [
              FolderViewBlock.Actions,
              FolderViewBlock.Draft,
              FolderViewBlock.Metadata,
              FolderViewBlock.Workflow,
              FolderViewBlock.MainDocuments,
              FolderViewBlock.AnnexeDocuments
            ],
            taskViewColumnList: [
              TaskViewColumn.FolderName,
              TaskViewColumn.State,
              TaskViewColumn.Type,
              TaskViewColumn.Subtype,
              TaskViewColumn.CurrentDesk,
              TaskViewColumn.LimitDate,
              TaskViewColumn.CreationDate,
              TaskViewColumn.TaskId,
              TaskViewColumn.FolderId,
              TaskViewColumn.OriginDesk,
            ],
            archiveViewColumnList: [
              ArchiveViewColumn.Id,
              ArchiveViewColumn.Name,
              ArchiveViewColumn.OriginDesk,
              ArchiveViewColumn.TypeSubtype,
              ArchiveViewColumn.CreationDate,
              ArchiveViewColumn.Actions
            ],
            favoriteDesks: [],
            favoriteDeskIds: [],
            notificationsRedirectionMail: null,
            notificationsCronFrequency: null,
            notifiedOnConfidentialFolders: false,
            notifiedOnFollowedFolders: false,
            notifiedOnLateFolders: false,
            signatureImageContentId: null,
          } as UserPreferencesDto);
        }),
        map(
          userPreferencesDto => {

            if (!!userPreferencesDto.currentFilter) {
              this.filterService.setDefaultFilter(Object.assign(new FolderFilter(), userPreferencesDto.currentFilter));
            }

            // Patching (on-the-fly) missing elements, to ease updates
            // Maybe we should do this on server-side too ?
            // TODO : The opposite, removing elements that doesn't exist anymore

            Object.keys(FolderViewBlock)
              .map(k => FolderViewBlock[k])
              .filter(o => userPreferencesDto.folderViewBlockList.indexOf(o) === -1)
              .forEach(o => userPreferencesDto.folderViewBlockList.push(o));

            // Sending back result

            this.selectedUserPreferencesService.update(userPreferencesDto);
            return userPreferencesDto;
          })
      );
  }


}
