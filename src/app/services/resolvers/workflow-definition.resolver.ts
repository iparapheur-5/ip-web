/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AdminWorkflowDefinitionService } from '@libriciel/iparapheur-standard';
import { map, catchError } from 'rxjs/operators';
import { DataConversionService } from '../data-conversion.service';
import { NotificationsService } from '../notifications.service';
import { WorkflowDefinition } from '../../models/workflow-definition';


@Injectable({
  providedIn: 'root'
})
export class WorkflowDefinitionResolver implements Resolve<WorkflowDefinition> {


  constructor(private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              private dataConversionService: DataConversionService,
              private notificationsService: NotificationsService) {}


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<WorkflowDefinition> {
    const workflowId = route.paramMap.get('workflowId');
    const tenantId = route.paramMap.get('tenantId');
    let tenant = {id: tenantId};

    if (workflowId) {
      return this.adminWorkflowDefinitionService
        .getWorkflowDefinitionById(tenant.id, workflowId)
        .pipe(
          map(dto => Object.assign(new WorkflowDefinition(), dto)),
          catchError(this.notificationsService.handleHttpError('getWorkflowDefinitionById'))
        )
    } else {
      return of(new WorkflowDefinition());
    }
  }


}
