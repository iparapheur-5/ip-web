/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../../models/auth/user';
import { CurrentUserService } from '@libriciel/iparapheur-standard';
import { NotificationsService } from '../notifications.service';
import { ResolverMessages } from './resolver-messages';
import { SelectedUserService } from '../selected-user.service';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserResolver implements Resolve<User> {


  constructor(private currentUserService: CurrentUserService,
              private notificationService: NotificationsService,
              private selectedUserService: SelectedUserService) { }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | User {
    return this.currentUserService
      .getCurrentUser()
      .pipe(
        catchError(() => {
          this.notificationService.setGlobalError(ResolverMessages.ERROR_FETCHING_USER);
          return of(new User());
        }),
        map(userDto => {
          let result: User = Object.assign(new User(), userDto);
          this.selectedUserService.update(result);
          return result;
        })
      );
  }


}
