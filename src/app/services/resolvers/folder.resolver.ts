/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { Folder } from '../../models/folder/folder';
import { FolderService } from '../ip-core/folder.service';
import { FolderUtils } from '../../utils/folder-utils';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { CommonMessages } from '../../shared/common-messages';


@Injectable({
  providedIn: 'root'
})
export class FolderResolver implements Resolve<Folder> {


  constructor(private folderService: FolderService,
              private notificationsService: NotificationsService) {}


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Folder> {

    const tenantId: string = route.paramMap.get('tenantId');
    const folderId = route.paramMap.get('folderId');
    let deskId: string = route.queryParamMap.get(FolderUtils.AS_DESK_QUERY_PARAM_NAME);

    if (!deskId) {
      deskId = route.paramMap.get('deskId');
    }

    if (folderId) {
      return this.folderService
        .getFolder(tenantId, folderId, deskId)
        .pipe(catchError((error: any): Observable<never> => {
          let errorDetails = error?.status === 403 ? CommonMessages.INSUFFICIENT_RIGHTS : '';
          this.notificationsService.showErrorMessage(CommonMessages.CANNOT_OPEN_FOLDER, errorDetails);
          return throwError(error.error);
        }));
    } else {
      return of(new Folder());
    }
  }


}
