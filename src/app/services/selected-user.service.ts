/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/auth/user';


/**
 * This service allows to share the current desk between components.
 *
 * Note that the user is available through the route data,
 * this is mainly for the components that are not in the router-outlet.
 */
@Injectable({
  providedIn: 'root'
})
export class SelectedUserService {


  private currentUserBehaviourSubject = new BehaviorSubject<User>(null);
  currentUser$: Observable<User> = this.currentUserBehaviourSubject.asObservable();


  update(user: User) {
    this.currentUserBehaviourSubject.next(user);
  }


}
