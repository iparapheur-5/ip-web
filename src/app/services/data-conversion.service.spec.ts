/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { TestBed } from '@angular/core/testing';

import { DataConversionService } from './data-conversion.service';


describe('DataConversionService', () => {


  let service: DataConversionService;


  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataConversionService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  describe('createBackendDataFromWorkflowModel', () => {


    it('should return a non-null WorkflowDefinition object', () => {
      const result = service.createBackendDataFromWorkflowModel('a name', null, null);
      expect(result).toBeTruthy();
    });


    it('should convert steps correctly', () => {
/*      const validSource: IpWorkflowModel = {
        steps: [
          Object.assign(new StepModel(), {
            validators: [
              {
                id: 'c8e57a04-1f1a-4d55-aaa1-690803fe7494',
                name: 'Secrétariat à l\'adjoint à la propreté de Raincourt',
              }
            ],
            type: 'VISA',
            name: 'VISA'
          }),
          Object.assign(new StepModel(), {
            validators: [
              {
                id: '487d87ee-0750-4fa7-994d-e3585d9a021e',
                name: 'Secrétariat du service comptabilité de Morlhon-le-Haut',
                description: null
              }
            ],
            type: 'SIGNATURE',
            name: 'SIGNATURE'
          })
        ],
        name: 'Circuit 3 signature - DISCARDED NAME',
        finalDesk: {
          id: 'c8e57a04-1f1a-4d55-aaa1-690803fe7494',
          name: 'Secrétariat à l\'adjoint à la propreté de Raincourt',
          description: null,
          level: 0
        }
      } as unknown as IpWorkflowModel;

      const expectedResult = {
        steps: [
          {
            validators: [
              'c8e57a04-1f1a-4d55-aaa1-690803fe7494'
            ],
            name: 'VISA',
            type: 'VISA'
          },
          {
            validators: [
              '487d87ee-0750-4fa7-994d-e3585d9a021e'
            ],
            name: 'SIGNATURE',
            type: 'SIGNATURE'
          }
        ],
        name: 'Circuit 3 signature',
        finalDesk: {
          id: 'c8e57a04-1f1a-4d55-aaa1-690803fe7494',
          name: 'Secrétariat à l\'adjoint à la propreté de Raincourt',
          description: null,
          level: 0
        },
        id: 'circuit_3_signature',
        key: 'circuit_3_signature',
        deploymentId: 'circuit_3_signature'
      }*/

      // FIXME : this model has changed, TU should be fixed
      // const result = service.createBackendDataFromWorkflowModel('Circuit 3 signature', validSource, new IpStepModel());
      // expect(result).toBeTruthy();
      // expect(result instanceof WorkflowDefinition).toBeTruthy();
      // expect(JSON.stringify(result)).toEqual(JSON.stringify(expectedResult));
    });


  });


  it('should sanitize workflow definition key', () => {
    expect(service.workflowNameToKey('Circuit 3 signature')).toEqual('circuit_3_signature');
    expect(service.workflowNameToKey('')).toEqual('');
    expect(service.workflowNameToKey('3 test')).toEqual('_3_test');
  });


});
