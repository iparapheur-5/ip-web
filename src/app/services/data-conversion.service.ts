/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Folder } from '../models/folder/folder';
import { IpWorkflowInstance } from '../models/workflows/ip-workflow-instance';
import { IpStepInstance } from '../models/workflows/ip-step-instance';
import { from } from 'rxjs';
import { map, reduce, filter } from 'rxjs/operators';
import { Task } from '../models/task';
import { IpWorkflowActor, GENERIC_DESK_MAP, GENERIC_DESK_IDS } from '../models/workflows/ip-workflow-actor';
import { StepType, IpStepModel, StepValidationMode } from '../models/workflows/ip-step-model';
import { IpWorkflowModel } from '../models/workflows/ip-workflow-model';
import { FolderUtils } from '../utils/folder-utils';
import { Action, StepDefinitionParallelType } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../shared/models/secondary-action.enum';
import { WorkflowDefinition } from '../models/workflow-definition';
import { StepDefinition } from '../models/workflows/step-definition';
import { ActionNamePipe } from '../shared/utils/action-name.pipe';
import { StepToActionPipe } from '../shared/utils/step-to-action.pipe';
import { WorkflowActor } from '@libriciel/ls-composants/workflows';

@Injectable({
  providedIn: 'root'
})
export class DataConversionService {


  public actionToStep(action: Action | SecondaryAction): StepType {
    switch (action) {
      case SecondaryAction.End: { return StepType.End; }
      case Action.ExternalSignature: { return StepType.ExternalSignature; }
      case Action.Ipng: { return StepType.Ipng; }
      case Action.Seal: { return StepType.Seal; }
      case Action.SecondOpinion: { return StepType.SecondOpinion; }
      case Action.SecureMail: { return StepType.SecureMail; }
      case Action.Signature: { return StepType.Signature; }
      case Action.Start: { return StepType.Start; }
      case Action.Visa: { return StepType.Visa; }
      default: { return null; }
    }
  }


  public createWorkflowInstanceFromFolder(folder: Folder): IpWorkflowInstance {
    console.log('createWorkflowInstanceFromFolder');

    let workflowInstance = null;
    const endActions: (Action | SecondaryAction)[] = [Action.Archive, Action.Delete, Action.Undo]; // TODO : Check why we have an undo here

    const instanceData: any = {};
    instanceData.id = folder.id;
    instanceData.name = folder.name;
    instanceData.finalDesk = folder.finalDesk;
    instanceData.isOver = folder.stepList
      .filter(t => !endActions.includes(t.action))
      .filter(t => !FolderUtils.isPassed(t))
      .length <= 0;

    if (!folder.stepList) {
      return instanceData;
    }

    const stepInstanceList: IpStepInstance[] = [];

    from(folder.stepList)
      .pipe(
        filter((task: Task) => Object.values(StepType).includes(task.action as unknown as StepType)),
        map((task: Task) => {
          const actor: IpWorkflowActor = new IpWorkflowActor(task.user);
          const stepInstance = new IpStepInstance({
            id: task.id,
            name: task.action,
            type: this.actionToStep(task.action),
            validators: task.desks as WorkflowActor[],
            delegatedByDesk: task.delegatedByDesk,
            actedUponBy: actor,
            state: task.state,
            workflowIndex: task.workflowIndex,
            stepIndex: task.stepIndex
          });

          stepInstance.completedDate = task.date;
          return stepInstance;
        }),
        reduce(
          (acc: IpStepInstance[], val: IpStepInstance) => {
            acc.push(val);
            return acc;
          },
          stepInstanceList
        )
      )
      .subscribe((stepList: IpStepInstance[]) => {
        instanceData.steps = stepList;
        workflowInstance = new IpWorkflowInstance(instanceData);
      });

    return workflowInstance;
  }


  public createStepModelFromBackendData(stepDefinition: StepDefinition): IpStepModel {

    const result = new IpStepModel();

    result.name = ActionNamePipe.compute(stepDefinition.type);
    result.type = this.actionToStep(stepDefinition.type);
    result.validationMode = (stepDefinition.parallelType == StepDefinitionParallelType.Or)
      ? StepValidationMode.Or
      : StepValidationMode.And;
    result.validators = !!stepDefinition.validatingDesks ? [...stepDefinition.validatingDesks] as WorkflowActor[] : [];
    result.notifiedDesks = stepDefinition.notifiedDesks;
    result.mandatoryValidationMetadata = stepDefinition.mandatoryValidationMetadata;
    result.mandatoryRejectionMetadata = stepDefinition.mandatoryRejectionMetadata;

    if (result.validators?.length == 1) {
      result.validationMode = StepValidationMode.Simple;
    }

    return result;
  }


  /**
   * Here, we only have to populate the xxxIds lists from the Dto.
   * These lists are the only values that will be used server-side
   * @param ipStepModel
   */
  public createBackendDataFromStepModel(ipStepModel?: IpStepModel): StepDefinition {

    const result = new StepDefinition();

    if (!ipStepModel) {
      return result;
    }

    result.type = StepToActionPipe.compute(ipStepModel.type) as Action;
    result.parallelType = (ipStepModel.validationMode == StepValidationMode.And)
      ? StepDefinitionParallelType.And
      : StepDefinitionParallelType.Or;
    result.validatingDeskIds = (!!ipStepModel.validators ? [...ipStepModel.validators.map(validator => validator.id)] : []) as string[];
    result.notifiedDeskIds = ipStepModel.notifiedDesks.map(desk => desk.id);
    result.mandatoryValidationMetadataIds = ipStepModel.mandatoryValidationMetadata?.map(metadata => metadata.id);
    result.mandatoryRejectionMetadataIds = ipStepModel.mandatoryRejectionMetadata?.map(metadata => metadata.id);
    return result;
  }


  public createWorkflowModelFromBackendData(workflowDefinition: WorkflowDefinition): IpWorkflowModel {
    console.debug('createWorkflowModelFromBackendData, input : ', workflowDefinition);

    let result = new IpWorkflowModel();
    result.name = workflowDefinition.name;

    if (!workflowDefinition.steps || workflowDefinition.steps?.length == 0) {
      return result;
    }

    result.steps = workflowDefinition.steps
      .map(definition => Object.assign(new StepDefinition(), definition))
      .map(step => this.createStepModelFromBackendData(step));

    result.steps.forEach(step => {
      const validatorList = step.validators ? [...step.validators] : [];
      for (const [idx, validator] of validatorList.entries()) {
        const genericActor = GENERIC_DESK_MAP.get(validator.id as string);
        if (genericActor) {
          step.validators.splice(idx, 1, genericActor);
        }
      }
    });
    workflowDefinition.steps.map(step => Object.assign(new StepDefinition(), step));

    return result;
  }


  public createBackendDataFromWorkflowModel(name: string, ipWorkflowModel: IpWorkflowModel, lastStep: IpStepModel): WorkflowDefinition {

    const result = new WorkflowDefinition();
    result.name = name;

    if (!ipWorkflowModel) {
      return result;
    }

    result.steps = ipWorkflowModel.steps.map(step => this.createBackendDataFromStepModel(step as IpStepModel));
    result.finalDeskId = lastStep.validators[0]?.id as string ?? GENERIC_DESK_IDS.EMITTER_ID;
    result.finalNotifiedDeskIds = lastStep.notifiedDesks.map(desk => desk.id);

    return result;
  }


  public workflowNameToKey(name: string): string {

    // Having a number as the first char is forbidden, we'll prefix it with a '_'
    if (/^\d/.test(name)) {
      name = '_' + name;
    }

    // Replace every character that is not alphanumeric, '_', '.' or '-'
    return name.replace(/[^a-z\d_.-]/gi, '_').toLowerCase();
  }


}
