/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { CommonMessages } from '../shared/common-messages';
import { HasName } from '../models/has-name';
import { CrudOperation } from './crud-operation';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  private gotGlobalErrorSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public globalErrorState$ = this.gotGlobalErrorSubject.asObservable();
  private globalErrorMsgSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public globalErrorMsg$ = this.globalErrorMsgSubject.asObservable();


  constructor(private toastr: ToastrService) {}


  public showCrudMessage(operation: CrudOperation, entity: HasName, details?: string, success = true) {
    let message: string;
    let showMethod: (message, details) => void;

    if (success) {
      message = CommonMessages.getCrudSuccessMessage(operation, entity);
      showMethod = (message, details) => this.showSuccessMessage(message, details);
    } else {
      message = CommonMessages.getCrudErrorMessage(operation, entity);
      showMethod = (message, details) => this.showErrorMessage(message, details);
    }

    showMethod(message, details);
  }


  public showErrorMessage(msg: string, details?: string) {
    this.toastr.error(
      msg + (details ? `&nbsp;:</br><i>${details}</i>` : ''),
      CommonMessages.ERROR_MESSAGE_TITLE,
      {enableHtml: true}
    );
  }


  public showSuccessMessage(msg: string, details?: string) {
    this.toastr.success(
      msg + (details ? `&nbsp;:</br><i>${details}</i>` : ''),
      null,
      {enableHtml: true}
    );
  }


  // noinspection JSUnusedGlobalSymbols
  public resetGlobalErrorState() {
    this.gotGlobalErrorSubject.next(false);
    this.globalErrorMsgSubject.next('');
  }


  public setGlobalError(msg: string) {
    this.globalErrorMsgSubject.next(msg);
    this.gotGlobalErrorSubject.next(true);
    this.showErrorMessage(CommonMessages.GLOBAL_ERROR);
  }


  public handleHttpError(operation = 'operation') {
    return (error: any): Observable<never> => {
      // TODO: send the error to remote logging infrastructure
      console.error(`%c${operation} failed%c: ${error.message}`, 'font-weight: bold', '');
      return throwError(error.error);
    };
  }

}
