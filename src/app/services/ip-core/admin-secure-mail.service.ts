/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaginatedResult } from '../../models/paginated-result';
import { SecureMailServer } from '../../models/securemail/secure-mail-server';
import { HttpParams, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { jsonHeader } from '../../utils/http-utils';
import { CONFIG } from '../../shared/config/config';
import { BatchResult } from '../../models/commons/batch-result';
import { IpService } from '../../shared/service/ip-service';
import { SecureMailEntity } from '../../models/securemail/secure-mail-entity';

@Injectable({
  providedIn: 'root'
})
export class AdminSecureMailService extends IpService {


  constructor(public http: HttpClient,
              public notificationsService: NotificationsService) {
    super();
  }


  public findSecureMailServersByPage = (tenantId: string,
                                        page: number,
                                        pageSize: number,
                                        searchTerm?: string): Observable<PaginatedResult<SecureMailServer>> => {

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('pageSize', pageSize.toString());

    if (searchTerm != null) {
      httpParams = httpParams.set('searchTerm', searchTerm);
    }

    return this.http
      .get<PaginatedResult<SecureMailServer>>(
        `${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/secureMail/server`,
        {
          headers: jsonHeader,
          params: httpParams
        }
      )
      .pipe(
        tap(value => value.data.forEach((s, i) => value.data[i] = Object.assign(new SecureMailServer(), s))),
        catchError(this.notificationsService.handleHttpError('findSecureMailServersByPage'))
      );
  };


  public testSecureMailServer(tenantId: string, secureMailServer: SecureMailServer): Observable<boolean> {
    AdminSecureMailService.trimSecureMailServerConfig(secureMailServer);
    return this.http
      .post<boolean>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/secureMail/server/test`, secureMailServer)
      .pipe(
        catchError(this.notificationsService.handleHttpError('createSecureMailServer'))
      );
  }


  public createSecureMailServer(tenantId: string, secureMailServer: SecureMailServer): Observable<SecureMailServer> {
    AdminSecureMailService.trimSecureMailServerConfig(secureMailServer);
    return this.http
      .post<SecureMailServer>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/secureMail/server`, secureMailServer)
      .pipe(
        map(sms => Object.assign(new SecureMailServer(), sms)),
        catchError(this.notificationsService.handleHttpError('createSecureMailServer'))
      );
  }


  public deleteSecureMailServer(tenantId: string, server: SecureMailServer): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/secureMail/server/${server.id}`)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteSecureMailServer')));
  }


  public updateSecureMailServer(tenantId: string, secureMailServer: SecureMailServer): Observable<void> {
    AdminSecureMailService.trimSecureMailServerConfig(secureMailServer);
    return this.http
      .post<void>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/secureMail/server/${secureMailServer.id}`, secureMailServer)
      .pipe(catchError(this.notificationsService.handleHttpError('updateSecureMailServer')));
  }


  public deleteSecureMailServers(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<SecureMailServer>(
      tenantId,
      batchSize,
      (tid, p, b) => this.findSecureMailServersByPage(tid, p, b),
      (tid, s) => this.deleteSecureMailServer(tid, s)
    );
  }


  public getSecureMailServersCount(tenantId: string): Observable<number> {
    return this
      .findSecureMailServersByPage(tenantId, 0, 1)
      .pipe(map(result => result.total));
  }


  public findSecureMailEntities(tenantId: string, server: SecureMailServer): Observable<SecureMailEntity[]> {
    return this.http
      .post<SecureMailEntity[]>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/secureMail/server/entities`, server);
  }


  private static trimSecureMailServerConfig(secureMailServer: SecureMailServer): void {
    secureMailServer.url = secureMailServer.url?.trim();
    secureMailServer.login = secureMailServer.login?.trim();
    secureMailServer.password = secureMailServer.password?.trim();
  }
}
