/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { NotificationsService } from '../notifications.service';
import { CONFIG } from '../../shared/config/config';
import { IpService } from '../../shared/service/ip-service';
import { SecureMailDocument } from '../../models/securemail/secure-mail-document';

@Injectable({
  providedIn: 'root'
})
export class SecureMailService extends IpService {


  constructor(public http: HttpClient,
              public notificationsService: NotificationsService) {
    super();
  }


  public findDocument(tenantId: string, serverId: number, folderId: string): Observable<SecureMailDocument> {
    return this.http
      .get<SecureMailDocument>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/secureMail/server/${serverId}/folder/${folderId}/document`)
  }
}
