/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { jsonHeader } from '../../utils/http-utils';
import { StringResult } from '../../models/commons/string-result';
import { Config } from '../../config';
import { CONFIG } from '../../shared/config/config';
import { IpService } from '../../shared/service/ip-service';

@Injectable({
  providedIn: 'root'
})
export class AdminTenantUserService extends IpService {


  constructor(public http: HttpClient,
              public notificationsService: NotificationsService) {
    super();
  }


  setSignatureImage(tenantId: string, userId: string, file: File): Observable<StringResult> {

    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http
      .post<StringResult>(`${CONFIG.BASE_API_URL}/admin/` + (tenantId ? `tenant/${tenantId}` : '') + `/user/${userId}/signatureImage`, formData)
      .pipe(
        map(sr => Object.assign(new StringResult(), sr)),
        catchError(this.notificationsService.handleHttpError('setUserSignatureImage'))
      );
  }


  updateSignatureImage(tenantId: string, userId: string, file: File): Observable<void> {

    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/admin/` + (tenantId ? `tenant/${tenantId}/` : '') + `/user/${userId}/signatureImage`, formData)
      .pipe(catchError(this.notificationsService.handleHttpError('updateUserSignatureImage')));
  }


  /**
   * @param tenantId  (self-explanatory)
   * @param userId    (self-explanatory)
   * @param salt      This parameter is not used server-side, but appending it will prevent browsers cache to show old images on updates.
   */
  signatureImageUrl = (tenantId: string, userId: string, salt: string) =>
    `/api/${Config.API_VERSION}/admin/` + (tenantId ? `tenant/${tenantId}/` : '') + `user/${userId}/signatureImage?${salt}`


  deleteSignatureImage(tenantId: string, userId: string): Observable<void> {
    return this.http
      .delete<void>(
        `${CONFIG.BASE_API_URL}/admin/` + (tenantId ? `tenant/${tenantId}/` : '') + `/user/${userId}/signatureImage`,
        {
          headers: jsonHeader,
          params: new HttpParams()
        }
      )
      .pipe(catchError(this.notificationsService.handleHttpError('deleteUserSignatureImage')));
  }


}
