/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaginatedResult } from '../../models/paginated-result';
import { Folder } from '../../models/folder/folder';
import { HttpParams, HttpClient } from '@angular/common/http';
import { isNotNullOrUndefined } from '../../utils/string-utils';
import { CONFIG } from '../../shared/config/config';
import { jsonHeader } from '../../utils/http-utils';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { FolderSortBy } from '@libriciel/iparapheur-standard';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient,
              private notificationsService: NotificationsService) {
  }

  searchFolders(tenantId: string,
                page: number,
                pageSize: number,
                searchTerm?: string): Observable<PaginatedResult<{ folder: Folder, tenantId: string }>> {

    let httpParams = new HttpParams()
      .set('tetId', tenantId)
      .set('page', String(page))
      .set('pageSize', String(pageSize))
      .set('sortBy', FolderSortBy.FolderName)
      .set('asc', String(true))

    if (isNotNullOrUndefined(searchTerm)) {
      httpParams = httpParams.set('searchTerm', searchTerm);
    }

    return this.http
      .get<PaginatedResult<{ folder: Folder, tenantId: string }>>(
        `${CONFIG.BASE_API_URL}/search/folders`,
        {
          headers: jsonHeader,
          params: httpParams
        }
      )
      .pipe(catchError(this.notificationsService.handleHttpError('searchFoldersByName')));
  }
}
