/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StatsGraphPeriod } from '../../models/stats-graph-period.enum';
import { StatsGraphType } from '../../models/stats-graph-type.enum';
import { jsonHeader } from '../../utils/http-utils';
import { CONFIG } from '../../shared/config/config';
import { Metadata } from '../../models/metadata';
import { FolderUtils } from '../../utils/folder-utils';
import { Page } from '../../models/page';
import { DeskRepresentation } from '@libriciel/iparapheur-standard';
import { Folder } from '../../models/folder/folder';

@Injectable({
  providedIn: 'root'
})
export class DeskService {

  readonly asDeskParam = FolderUtils.AS_DESK_QUERY_PARAM_NAME;

  constructor(public http: HttpClient) {
  }


  getUrlToViewFolderAsDesk(tenantId: string, deskId: string, folderId: string, asDeskId: string): string {
    return `/tenant/${tenantId}/desk/${deskId}/folder/${folderId}?${this.asDeskParam}=${asDeskId}`;
  }

  public getAsDeskQueryParam(targetDeskId: string): {[k: string]: string} {
    let queryParam = {};
    queryParam[this.asDeskParam] = targetDeskId;
    return queryParam;
  }

  getFolderViewUrl(tenantId: string, deskId: string, folderId: string): string {
    return `/tenant/${tenantId}/desk/${deskId}/folder/${folderId}`;
  }


  getDeskChartUrl(tenantId: string, desk: DeskRepresentation, width: number, height: number, startDateTime: string, endDateTime: string,
                  period: StatsGraphPeriod, graphType: StatsGraphType): URL {

    const url = new URL(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${desk.id}/stats`);

    url.searchParams.append('width', width.toString());
    url.searchParams.append('height', height.toString());
    url.searchParams.append('startDate', startDateTime);
    url.searchParams.append('endDate', endDateTime);
    url.searchParams.append('periodicity', period);
    url.searchParams.append('graphType', graphType);

    return url;
  }


}
