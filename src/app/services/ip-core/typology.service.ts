/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { jsonHeader } from '../../utils/http-utils';
import { NotificationsService } from '../notifications.service';
import { CONFIG } from '../../shared/config/config';
import { PaginatedResult } from '../../models/paginated-result';
import { TypologyEntity } from '../../models/typology-entity';
import { SubtypeDto, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { SubtypeRepresentation } from '@libriciel/iparapheur-provisioning';

@Injectable({
  providedIn: 'root'
})
export class TypologyService {


  constructor(public http: HttpClient,
              private notificationsService: NotificationsService) {
  }

  getCreationAllowedTypes(tenantId: string, deskId: string): Observable<PaginatedResult<TypeRepresentation>> {
    return this.http
      .get<PaginatedResult<TypeRepresentation>>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/types/creation-allowed`,
        {headers: jsonHeader})
      .pipe(
        catchError(this.notificationsService.handleHttpError('getCreationAllowedTypes'))
      );
  }

  getCreationAllowedSubtypes(tenantId: string, deskId: string, typeId: string): Observable<PaginatedResult<SubtypeRepresentation>> {
    return this.http
      .get<PaginatedResult<SubtypeRepresentation>>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/types/${typeId}/subtypes/creation-allowed`,
        {headers: jsonHeader})
      .pipe(
        catchError(this.notificationsService.handleHttpError('getCreationAllowedSubtypes'))
      );
  }

}
