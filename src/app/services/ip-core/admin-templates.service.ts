/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable, forkJoin, of, throwError } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NotificationsService } from '../notifications.service';
import { CONFIG } from '../../shared/config/config';
import { BatchResult } from '../../models/commons/batch-result';
import { TemplateType } from '@libriciel/iparapheur-standard';

@Injectable({
  providedIn: 'root'
})
export class AdminTemplatesService {


  constructor(public http: HttpClient,
              private notificationsService: NotificationsService) {}


  getTemplate(tenantId: string, templateType: TemplateType): Observable<string> {
    return this.http
      .get(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/templates/${templateType}`, {responseType: 'text'})
      // If template doesn't exist, server replies with a 404, and it's OK
      // @ts-ignore
      .pipe(catchError((err: HttpErrorResponse) => err.status === 404 ? throwError(err) : this.notificationsService.handleHttpError('getTemplate')));
  }


  deleteTemplate(tenantId: string, templateType: TemplateType): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/templates/${templateType}`)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteTemplate')));
  }


  deleteTemplates(tenantId: string): Observable<BatchResult> {
    return this.getCustomTemplates(tenantId)
      .pipe(switchMap(templateTypes => {
        const deleteCalls = templateTypes.map(templateType => this.deleteTemplate(tenantId, templateType));
        return deleteCalls.length === 0
          ? of({
            completed: 0,
            total: 0,
            done: true
          })
          : forkJoin(deleteCalls)
            .pipe(map(() => ({
                completed: 1,
                total: 1,
                done: true
              })
            ))
      }));
  }

  getCustomTemplates(tenantId: string): Observable<TemplateType[]> {
    const existenceCheckCalls = Object.values(TemplateType).map(templateType => {
        return this
          .getTemplate(tenantId, templateType)
          .pipe(
            map(() => templateType),
            // If template doesn't exist, server replies with a 404
            catchError((err: HttpErrorResponse) => err.status === 404 ? of(null) : throwError(err))
          );
      }
    );

    return forkJoin(existenceCheckCalls)
      .pipe(switchMap(results =>
        of(results.filter(result => result))));
  }

  getCustomTemplatesCount(tenantId: string): Observable<number> {
    return this.getCustomTemplates(tenantId).pipe(map(result => result.length));
  }


}
