/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../models/auth/user';
import { HttpParams, HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { jsonHeader } from '../../utils/http-utils';
import { StringResult } from '../../models/commons/string-result';
import { Config } from '../../config';
import { CONFIG } from '../../shared/config/config';
import { KeycloakService } from 'keycloak-angular';
import { FolderFilter } from '../../models/folder/folder-filter';
import { FolderFilterDto } from '@libriciel/iparapheur-standard';

@Injectable({
  providedIn: 'root'
})
export class LegacyUserService {


  constructor(public http: HttpClient,
              private notificationService: NotificationsService,
              private keycloak: KeycloakService) { }


  isCurrentUserSuperAdmin(): boolean {
    return this.keycloak
      .getUserRoles(true)
      .some(role => role === "admin");
  }


  isCurrentUserTenantAdminOfOneTenant(): boolean {
    return this.keycloak
      .getUserRoles(true)
      .some(role => role.startsWith("tenant_") && role.endsWith("_admin"));
  }


  isCurrentUserTenantAdminOfThisTenant(tenantId: string): boolean {
    return this.keycloak.isUserInRole("tenant_" + tenantId + "_admin");
  }


  isCurrentUserDeskOwner(tenantId: string, deskId: string): boolean {
    return this.keycloak
      .getUserRoles(true)
      .some(role => role === "tenant_" + tenantId + "_desk_" + deskId);
  }


  /**
   * return true if the user is superAdmin, tenant admin or functional admin
   */
  isCurrentUserAdmin(): boolean {
    return this.keycloak
      .getUserRoles(true)
      // this includes the "functionalAdmin" role
      .filter(role => (role === "admin") || (role.startsWith("tenant_") && role.endsWith("_admin")))
      .length > 0;
  }


  isCurrentUserOnlyFunctionalAdminOfThisTenant(tenantId: string): boolean {
    const isFunctionalAdmin = this.keycloak.isUserInRole("tenant_" + tenantId + "_functional_admin");
    const isHigherAdmin = this.isCurrentUserSuperAdmin() || this.keycloak.isUserInRole("tenant_" + tenantId + "_admin");

    return isFunctionalAdmin && !isHigherAdmin;
  }


  /**
   * If the current user is admin or functional admin of one or several tenants, randomly retrieve the id of one those tenant
   */
  getFirstAdministeredTenantId(): string {
    const prefixLen = "tenant_".length;
    const uuidLen = 36;
    return this.keycloak
      .getUserRoles(true)
      // this includes the "functionalAdmin" role
      .filter(role => role.startsWith("tenant_") && role.endsWith("_admin"))
      .map(role => role.substr(prefixLen, uuidLen))
      .pop()

  }


  getCurrentUser(): Observable<User> {
    return this.http
      .get<User>(
        `${CONFIG.BASE_API_URL}/currentUser`,
        {
          headers: jsonHeader,
          params: new HttpParams()
        })
      .pipe(
        map(u => Object.assign(new User(), u)),
        catchError(this.notificationService.handleHttpError('getUser'))
      );
  }


  setSignatureImage(file: File): Observable<string> {

    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http
      .post<StringResult>(
        `${CONFIG.BASE_API_URL}/currentUser/signatureImage`,
        formData
      )
      .pipe(
        map(sr => Object.assign(new StringResult(), sr).value),
        catchError(this.notificationService.handleHttpError('setSignatureImage'))
      );
  }


  updateSignatureImage(file: File): Observable<void> {

    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/currentUser/signatureImage`, formData)
      .pipe(catchError(this.notificationService.handleHttpError('updateSignatureImage')));
  }


  createOrUpdateFilter(folderFilter: FolderFilter): Observable<FolderFilter> {
    const call = folderFilter.id
      ? this.http.put<FolderFilter>(`${CONFIG.BASE_API_URL}/currentUser/userFilters/${folderFilter.id}`, folderFilter)
      : this.http.post<FolderFilter>(`${CONFIG.BASE_API_URL}/currentUser/userFilters`, folderFilter)
    return call.pipe(catchError(this.notificationService.handleHttpError('createOrUpdateFilter')));
  }


  deleteFilter(folderFilter: FolderFilterDto): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/currentUser/userFilters/${folderFilter.id}`)
      .pipe(catchError(this.notificationService.handleHttpError('deleteFilter')));
  }


  getCurrentUserFolderFilters(): Observable<FolderFilterDto[]> {
    return this.http
      .get<FolderFilterDto[]>(`${CONFIG.BASE_API_URL}/currentUser/userFilters`)
      .pipe(
        catchError(this.notificationService.handleHttpError('getCurrentUserFolderFilters')));
  }


  deleteSignatureImage(): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/currentUser/signatureImage`)
      .pipe(catchError(this.notificationService.handleHttpError('deleteSignatureImage')));
  }


  /**
   * @param salt      This parameter is not used server-side, but appending it will prevent browsers cache to show old images on updates.
   */
  signatureImageUrl = (salt: string) => `/api/${Config.API_VERSION}/currentUser/signatureImage?${salt}`;


  resetUserPassword(newPassword: string): Observable<void> {
    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/currentUser/password`, {password: newPassword})
      .pipe(catchError(this.notificationService.handleHttpError('resetUserPassword')));
  }


}
