/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { jsonHeader } from '../../utils/http-utils';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { JobDataResponse } from '../../models/monitoring/job-data-response';
import { Observable } from 'rxjs';
import { CONFIG } from '../../shared/config/config';

@Injectable({
  providedIn: 'root'
})
export class AdminMonitoringService {


  constructor(public http: HttpClient,
              public notificationsService: NotificationsService) {
  }

  public getLocalSpaceUsage(): Observable<number> {
    return this.http
      .get<number>(
        `${CONFIG.BASE_API_URL}/admin/monitoring/local_space_usage`,
        {headers: jsonHeader}
      )
      .pipe(
        catchError(this.notificationsService.handleHttpError('getLocalSpaceUsage'))
      );
  }


  public getJobsData(): Observable<JobDataResponse[]> {
    return this.http
      .get<JobDataResponse[]>(
        `${CONFIG.BASE_API_URL}/admin/monitoring/jobs`,
        {headers: jsonHeader}
      )
      .pipe(catchError(this.notificationsService.handleHttpError('GetAllJobsData')));
  }


}
