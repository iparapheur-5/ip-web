/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaginatedResult } from '../../models/paginated-result';
import { SealCertificate } from '../../models/seal-certificate';
import { map, catchError } from 'rxjs/operators';
import { jsonHeader } from '../../utils/http-utils';
import { HttpParams, HttpClient } from '@angular/common/http';
import { NotificationsService } from '../notifications.service';
import { StringResult } from '../../models/commons/string-result';
import { Config } from '../../config';
import { CONFIG } from '../../shared/config/config';
import { IpService } from '../../shared/service/ip-service';
import { AdminSealCertificateService, SubtypeDto } from '@libriciel/iparapheur-standard';

@Injectable({
  providedIn: 'root'
})
export class LegacyAdminSealCertificateService extends IpService {


  constructor(public http: HttpClient,
              private adminSealCertificateService: AdminSealCertificateService,
              private notificationsService: NotificationsService) {
    super();
  }


  getSealCertificateUsage(tenantId: string, sealCertificate: SealCertificate,
                          page: number, pageSize: number): Observable<PaginatedResult<SubtypeDto>> {

    let httpParams = new HttpParams()
      .set('page', page.toString())
      .set('pageSize', pageSize.toString());

    return this.http
      .get<PaginatedResult<SubtypeDto>>(
        `${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/sealCertificate/${sealCertificate.id}/subtypeUsage`,
        {
          headers: jsonHeader,
          params: httpParams
        }
      )
      .pipe(
        catchError(this.notificationsService.handleHttpError('getSealCertificate'))
      );
  }


  // <editor-fold desc="Signature image CRUD">


  setSignatureImage(tenantId: string, sealCertificate: SealCertificate, file: File): Observable<string> {

    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http
      .post<StringResult>(
        `${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/sealCertificate/${sealCertificate.id}/signatureImage`,
        formData
      )
      .pipe(
        map(sr => Object.assign(new StringResult(), sr).value),
        catchError(this.notificationsService.handleHttpError('setSignatureImage'))
      );
  }


  /**
   * @param tenantId        (self-explanatory)
   * @param sealCertificate (self-explanatory)
   * @param salt            This parameter is not used server-side,
   *                        but appending it will prevent browsers cache to show old images on updates.
   */
  signatureImageUrl = (tenantId: string, sealCertificate: SealCertificate, salt: string) =>
    `/api/${Config.API_VERSION}/admin/tenant/${tenantId}/sealCertificate/${sealCertificate.id}/signatureImage?${salt}`


  updateSignatureImage(tenantId: string, sealCertificate: SealCertificate, file: File): Observable<void> {

    const formData: FormData = new FormData();
    formData.append('file', file);

    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/sealCertificate/${sealCertificate.id}/signatureImage`, formData)
      .pipe(catchError(this.notificationsService.handleHttpError('updateSignatureImage')));
  }


  deleteSignatureImage(tenantId: string, sealCertificate: SealCertificate): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/sealCertificate/${sealCertificate.id}/signatureImage`)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteSignatureImage')));
  }


  // </editor-fold desc="Signature image CRUD">


}
