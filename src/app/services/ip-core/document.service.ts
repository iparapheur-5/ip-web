/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotificationsService } from '../notifications.service';
import { Observable } from 'rxjs';
import { Folder } from '../../models/folder/folder';
import { Document } from '../../models/document';
import { catchError } from 'rxjs/operators';
import { Config } from '../../config';
import { CONFIG } from '../../shared/config/config';
import { StickyNote } from '../../models/annotation/sticky-note';
import { SignaturePlacement } from '../../models/annotation/signature-placement';
import { DeskRepresentation } from '@libriciel/iparapheur-standard';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {


  constructor(public http: HttpClient,
              private notificationService: NotificationsService) { }


  documentUrl = (tenantId: string, folderId: string, documentId: string) =>
    `/api/${Config.API_VERSION}/tenant/${tenantId}/folder/${folderId}/document/${documentId}`;


  detachedSignatureUrl = (tenantId: string, folderId: string, documentId: string, detachedSignatureId: string) =>
    `/api/${Config.API_VERSION}/tenant/${tenantId}/folder/${folderId}/document/${documentId}/detachedSignature/${detachedSignatureId}`;


  pdfVisualUrl = (tenantId: string, folderId: string, document: Document) =>
    `/api/${Config.API_VERSION}/tenant/${tenantId}/folder/${folderId}/document/${document.id}/pdfVisual`;


  loadPageFromUrl(url: string): Observable<HTMLDocument> {
    const headers = new HttpHeaders({'Content-Type': 'text/html'});

    return this.http.get<HTMLDocument>(url, {responseType: 'document' as 'json', headers})
      .pipe(catchError(this.notificationService.handleHttpError('loadPageFromUrl')));
  }


  addDocument(tenantId: string, folder: Folder, mainDocument: boolean, name: string, file: File): Observable<void> {

    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('name', name);
    formData.append('isMainDocument', '' + mainDocument);

    return this.http
      .post<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/document/`, formData)
      .pipe(catchError(this.notificationService.handleHttpError('addDocument')));
  }


  updateDocument(tenantId: string, folder: Folder, document: Document, name: string, file: File): Observable<void> {

    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('name', name);

    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/document/${document.id}`, formData)
      .pipe(catchError(this.notificationService.handleHttpError('replaceDocument')));
  }


  deleteDocument(tenantId: string, folder: Folder, document: Document): Observable<void> {
    // /folder/{folderId}/document/{documentId}
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/document/${document.id}`)
      .pipe(catchError(this.notificationService.handleHttpError('deleteDocument')));
  }


  addDetachedSignature(tenantId: string, folderId: string, documentId: string, name: string, file: File, desk: DeskRepresentation): Observable<void> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('deskId', desk.id);

    return this.http
      .post<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folderId}/document/${documentId}/detachedSignature`, formData)
      .pipe(catchError(this.notificationService.handleHttpError('addDetachedSignature')));
  }

  getDocument(tenantId: string, folderId: string, documentId: string): Observable<Blob> {
    return this.http
      .get<Blob>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folderId}/document/${documentId}`,
        {
          responseType: 'blob' as 'json',
        }
      )
      .pipe(catchError(this.notificationService.handleHttpError('getDocument')));
  }


  deleteDetachedSignature(tenantId: string, folderId: string, documentId: string, detachedSignatureId: string): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folderId}/document/${documentId}/detachedSignature/${detachedSignatureId}`)
      .pipe(catchError(this.notificationService.handleHttpError('deleteDetachedSignature')));
  }


  createAnnotation(tenantId: string, folder: Folder, document: Document, annotation: StickyNote): Observable<void> {
    return this.http
      .post<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/document/${document.id}/annotations`,
        {
          id: annotation.id,
          content: annotation.content,
          page: annotation.page,
          pageRotation: annotation.pageRotation,
          rectangleOrigin: 'BOTTOM_LEFT',
          width: annotation.width,
          height: annotation.height,
          x: annotation.x,
          y: annotation.y,
        }
      )
      .pipe(catchError(this.notificationService.handleHttpError('createAnnotation')));
  }


  getSignaturePlacementAnnotations(tenantId: string, folderId: string, documentId: string): Observable<SignaturePlacement[]> {
    return this.http
      .get<SignaturePlacement[]>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folderId}/document/${documentId}/signaturePlacement`)
      .pipe(catchError(this.notificationService.handleHttpError('createAnnotation')));
  }


  createSignaturePlacement(tenantId: string, folder: Folder, document: Document, annotation: SignaturePlacement): Observable<void> {
    return this.http
      .post<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/document/${document.id}/signaturePlacement`,
        {
          id: annotation.id,
          signatureNumber: annotation.signatureNumber,
          page: annotation.page,
          pageRotation: annotation.pageRotation,
          rectangleOrigin: 'BOTTOM_LEFT',
          width: annotation.width,
          height: annotation.height,
          x: annotation.x,
          y: annotation.y,
        }
      )
      .pipe(catchError(this.notificationService.handleHttpError('createAnnotation')));
  }


  deleteAnnotation(tenantId: string, folderId: string, documentId: string, annotationId: string): Observable<void> {
    return this.http
      .delete<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folderId}/document/${documentId}/annotations/${annotationId}`)
      .pipe(catchError(this.notificationService.handleHttpError('deleteAnnotation')));
  }


}
