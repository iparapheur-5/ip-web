/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Folder } from '../../models/folder/folder';
import { HttpParams, HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { DataToSignHolder } from '../../models/signature/data-to-sign-holder';
import { MailParams } from '../../models/securemail/mail-params';
import { jsonHeader } from '../../utils/http-utils';
import { NotificationsService } from '../notifications.service';
import { CONFIG } from '../../shared/config/config';
import { Task } from '../../models/task';
import { User } from '../../models/auth/user';
import { isNotNullOrUndefined } from '../../utils/string-utils';
import { FolderDataToSign } from '../../models/signature/folder-data-to-sign';
import { IpngFolderProofs } from '../../models/ipng/ipng-folder-proofs';


@Injectable({
  providedIn: 'root'
})
export class FolderService {

  readonly ipngProofPathComponent: string = 'ipngProofs';


  constructor(public http: HttpClient,
              public notificationsService: NotificationsService) {
  }


  getFolder(tenantId: string, id: string, asOtherDeskId: string = null, doNotMarkAsRead: boolean = null): Observable<Folder> {

    let httpParams = new HttpParams();
    if (!!asOtherDeskId) {
      httpParams = httpParams.set('asDeskId', asOtherDeskId);
    }

    if (!!doNotMarkAsRead) {
      httpParams = httpParams.set('doNotMarkAsRead', doNotMarkAsRead);
    }

    return this.http
      .get<Folder>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${id}`,
        {
          headers: jsonHeader,
          params: httpParams
        })
      .pipe(
        map(f => {
          // Deep parse, to get proper typing
          let folder = Object.assign(new Folder(), f);
          folder.stepList.forEach((t, i) => {
            folder.stepList[i] = Object.assign(new Task(), t);
            folder.stepList[i].user = Object.assign(new User(), t.user);
            folder.stepList[i].beginDate = isNotNullOrUndefined(t.beginDate) ? new Date(t.beginDate) : null;
            folder.stepList[i].date = isNotNullOrUndefined(t.date) ? new Date(t.date) : null;
          });
          return folder;
        }),
        catchError(this.notificationsService.handleHttpError('getFolder'))
      );
  }


  editFolder(tenantId: string, folder: Folder): Observable<void> {

    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}`,
        {
          name: folder.name ?? null,
          typeId: folder.type.id ?? null,
          subtypeId: folder.subtype.id ?? null,
          dueDate: folder.dueDate ?? null
        }
      )
      .pipe(catchError(this.notificationsService.handleHttpError('editFolder')));
  }


  deleteFolder(tenantId: string, id: string): Observable<void> {
    return this.http
      .delete<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${id}`,
        {
          headers: jsonHeader,
          params: null
        }
      )
      .pipe(catchError(this.notificationsService.handleHttpError('deleteFolder')));
  }


  getHistoryTasks(tenantId: string, deskId: string, folder: Folder): Observable<Array<Task>> {
    return this.http
      .get<Array<Task>>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/historyTasks`,
        {headers: jsonHeader}
      )
      .pipe(
        tap(value => value.forEach((t, i) => value[i] = Object.assign(new Task(), t))),
        catchError(this.notificationsService.handleHttpError('getHistoryTasks'))
      );
  }


  getPrintUrl(tenantId: string, deskId: string, folder: Folder, includeDocket: boolean, annexesIdList: string[]): string {

    let httpParams = new HttpParams()
      .set('includeDocket', String(includeDocket));

    annexesIdList.forEach(id => {
      httpParams = httpParams.append('annexesIds', id);
    });

    return `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/print?` + httpParams.toString();
  }


  sendFolderByMail(tenantId: string, folderId: string, mailParams: MailParams): Observable<void> {
    return this.http
      .post<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folderId}/mail`, mailParams)
      .pipe(catchError(this.notificationsService.handleHttpError('sendFolderByMail')));
  }


  getDataToSign(tenantId: string, deskId: string, folderId: string, publicKeyBase64: string): Observable<FolderDataToSign> {

    const httpParams = new HttpParams()
      .set('deskId', deskId)
      .set('certBase64', publicKeyBase64);


    return this.http
      .get<DataToSignHolder[]>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folderId}/dataToSign`,
        {
          headers: jsonHeader,
          params: httpParams
        }
      )
      .pipe(
        map(dtsList => new FolderDataToSign(dtsList)),
        catchError(this.notificationsService.handleHttpError('getDataToSign'))
      );
  }


  setMetadata(tenantId: string, folder: Folder, metadataId: string, value: string): Observable<void> {
    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${folder.id}/metadata/${metadataId}`, {value})
      .pipe(catchError(this.notificationsService.handleHttpError('setMetadata')));
  }


  getIpngProofForFolder(tenantId: string, id: string): Observable<IpngFolderProofs[]> {

    return this.http
      .get<IpngFolderProofs[]>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/folder/${id}/${this.ipngProofPathComponent}`,
        {
          headers: jsonHeader
        })
      .pipe(
        catchError(this.notificationsService.handleHttpError('getIpngProofForFolder'))
      );
  }


}
