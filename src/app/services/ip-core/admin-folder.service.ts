/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PaginatedResult } from '../../models/paginated-result';
import { NotificationsService } from '../notifications.service';
import { jsonHeader, jsonHeaderObject } from '../../utils/http-utils';
import { IpService } from '../../shared/service/ip-service';
import { CONFIG } from '../../shared/config/config';
import { Folder } from '../../models/folder/folder';
import { BatchResult } from '../../models/commons/batch-result';
import { isNotNullOrUndefined } from '../../utils/string-utils';
import { State, FolderSortBy, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { SubtypeRepresentation } from '@libriciel/iparapheur-provisioning';


@Injectable({
  providedIn: 'root'
})
export class AdminFolderService extends IpService {


  constructor(public http: HttpClient,
              public notificationsService: NotificationsService) {
    super();
  }


  getFolders(tenantId: string, page: number, pageSize: number, sortBy: FolderSortBy = FolderSortBy.FolderId,
             asc: boolean = true, deskId?: string, state?: State, type?: TypeRepresentation, subtype?: SubtypeRepresentation,
             searchTerm?: string, emitBefore?: Date, stillSince?: Date): Observable<PaginatedResult<Folder>> {

    let httpParams = new HttpParams()
      .set('page', String(page))
      .set('pageSize', String(pageSize))
      .set('sortBy', sortBy)
      .set('asc', String(asc));

    if (isNotNullOrUndefined(searchTerm)) { httpParams = httpParams.set('searchTerm', searchTerm); }
    if (isNotNullOrUndefined(deskId)) { httpParams = httpParams.set('deskId', deskId); }
    if (isNotNullOrUndefined(type?.id)) { httpParams = httpParams.set('typeId', type.id); }
    if (isNotNullOrUndefined(subtype?.id)) { httpParams = httpParams.set('subtypeId', subtype.id); }
    if (isNotNullOrUndefined(emitBefore)) { httpParams = httpParams.set('emitBeforeTime', String(emitBefore.getTime())); }
    if (isNotNullOrUndefined(stillSince)) { httpParams = httpParams.set('stillSinceTime', String(stillSince.getTime())); }
    if (isNotNullOrUndefined(state)) { httpParams = httpParams.set('state', state); }

    return this.http
      .get<PaginatedResult<Folder>>(
        `${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/folder`,
        {
          headers: jsonHeader,
          params: httpParams
        }
      )
      .pipe(
        tap(value => value.data.forEach((f, i) => value.data[i] = Object.assign(new Folder(), f))),
        catchError(this.notificationsService.handleHttpError('getFoldersAsAdmin'))
      );
  }


  deleteFolder(tenantId: string, folder: Folder): Observable<void> {
    return this.http
      .delete<void>(`${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/folder/${folder.id}`, jsonHeaderObject)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteFolderAsAdmin')));
  }


  getFoldersCount(tenantId: string): Observable<number> {
    return this
      .getFolders(tenantId, 0, 1)
      .pipe(map(result => result.total));
  }


  deleteFolders = (tenantId: string, batchSize: number): Observable<BatchResult> =>
    IpService.deleteAllElements<Folder>(
      tenantId,
      batchSize,
      (tid, p, b) => this.getFolders(tid, p, b),
      (tid, f) => this.deleteFolder(tid, f)
    );


}
