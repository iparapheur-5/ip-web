/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Task } from '../../models/task';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { PaginatedResult } from '../../models/paginated-result';
import { Folder } from '../../models/folder/folder';
import { WorkflowInstance } from '@libriciel/ls-composants/workflows';
import { DataConversionService } from '../data-conversion.service';
import { jsonHeader } from '../../utils/http-utils';
import { NotificationsService } from '../notifications.service';
import { MailParams } from '../../models/securemail/mail-params';
import { CONFIG } from '../../shared/config/config';
import { FolderFilter } from 'src/app/models/folder/folder-filter';
import { IpWorkflowModel } from '../../models/workflows/ip-workflow-model';
import { SimpleTaskParams } from '../../models/simple-task-params';
import { Action, State, FolderSortBy, FolderFilterDto } from '@libriciel/iparapheur-standard';
import { SecondaryAction } from '../../shared/models/secondary-action.enum';
import { WorkflowDefinition } from '../../models/workflow-definition';
import { CustomMap } from '../../shared/models/custom-types';
import { ExternalSignRequestParams } from '../../models/external-signature-connector/external-sign-request-params';

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {


  constructor(public http: HttpClient,
              public dataConversionService: DataConversionService,
              public notificationsService: NotificationsService) { }


  getWorkflowInstance(folder: Folder): Observable<WorkflowInstance> {
    return of(this.dataConversionService.createWorkflowInstanceFromFolder(folder));
  }


  requestExternalProcedure(params: ExternalSignRequestParams, tenantId: string, deskId: string, folderId: string, task: Task): Observable<void> {
    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folderId}/task/${task.id}/external_signature`, params)
      .pipe(catchError(this.notificationsService.handleHttpError('createExternalSignatureProcedure')));
  }


  finalizeExternalSignature(tenantId: string, deskId: string, folderId: string, taskId: string, externalSignatureConfigId: string): Observable<void> {
    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folderId}/task/${taskId}/config/${externalSignatureConfigId}/external_signature/force`,
        {
          headers: jsonHeader
        }
      )
      .pipe(catchError(this.notificationsService.handleHttpError('getWorkflowDefinitionByKey')));
  }


  getWorkflowDefinitionByKey(tenantId: string,
                             deskId: string,
                             workflowDefinitionKey: string): Observable<WorkflowDefinition> {
    return this.http
      .get<WorkflowDefinition>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/workflowDefinition/${workflowDefinitionKey}`,
        {headers: jsonHeader}
      )
      .pipe(
        map(wm => Object.assign(new IpWorkflowModel(), wm)),
        catchError(this.notificationsService.handleHttpError('getWorkflowDefinitionByKey'))
      );
  }


  evaluateWorkflowSelectionScript(tenantId: string,
                                  deskId: string,
                                  typeId: string,
                                  subtypeId: string,
                                  metadata: CustomMap): Observable<WorkflowDefinition> {

    // FIXME : Use the lib
    const draftFolderParams = {
      'metadata': metadata,
      'typeId': typeId,
      'subtypeId': subtypeId,
      'name': ""
    }

    return this.http
      .post<WorkflowDefinition>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/evaluate-workflow-selection-script`, draftFolderParams)
      .pipe(catchError(this.notificationsService.handleHttpError('evaluateWorkflowSelectionScript')));
  }


  getFolders(tenantId: string,
             deskId: string,
             folderFilter: FolderFilterDto,
             sortBy: FolderSortBy,
             asc: boolean,
             page: number,
             pageSize: number): Observable<PaginatedResult<Folder>> {

    const targetState = folderFilter?.state?.toLowerCase() || State.Pending.toLowerCase();

    let httpParams = new HttpParams()
      .set('sortBy', sortBy)
      .set('asc', String(asc))
      .set('page', page.toString())
      .set('pageSize', pageSize.toString());
    return this.http
      .post<PaginatedResult<Folder>>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/search/${targetState}`,
        JSON.stringify(folderFilter || new FolderFilter()),
        {
          headers: jsonHeader,
          params: httpParams
        }
      )
      .pipe(
        tap(value => value.data.forEach((f, i) => value.data[i] = Object.assign(new Folder(), f))),
        catchError(this.notificationsService.handleHttpError('getFolders'))
      );
  }


  performTask(tenantId: string,
              folder: Folder,
              task: Task,
              deskId: string,
              performedAction: Action | SecondaryAction,
              simpleTaskParams: SimpleTaskParams): Observable<void> {
    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folder.id}/task/${task.id}/${performedAction.toLowerCase()}`,
        simpleTaskParams
      )
      .pipe(catchError(this.notificationsService.handleHttpError('performTask')));
  }


  performTransfer(tenantId: string,
                  folder: Folder,
                  task: Task,
                  deskId: string,
                  simpleTaskParams: SimpleTaskParams): Observable<void> {
    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folder.id}/task/${task.id}/${Action.Transfer.toLowerCase()}`,
        simpleTaskParams
      )
      .pipe(catchError(this.notificationsService.handleHttpError('performTransfer')));
  }


  performAskSecondOpinion(tenantId: string,
                          folder: Folder,
                          task: Task,
                          deskId: string,
                          simpleTaskParams: SimpleTaskParams): Observable<void> {
    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folder.id}/task/${task.id}/${Action.AskSecondOpinion.toLowerCase()}`,
        simpleTaskParams
      )
      .pipe(catchError(this.notificationsService.handleHttpError('performSecondOpinion')));
  }


  performChain(tenantId: string,
               folder: Folder,
               task: Task,
               deskId: string,
               typeId: string,
               subtypeId: string,
               variableDesksIds: CustomMap,
               metadata: Map<string, string>): Observable<void> {

    let name: string = folder.name;
    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folder.id}/task/${task.id}/${Action.Chain.toLowerCase()}`,
        {typeId, subtypeId, metadata, name, variableDesksIds}
      )
      .pipe(catchError(this.notificationsService.handleHttpError('performChain')));
  }


  performSecureMail(tenantId: string,
                    folder: Folder,
                    task: Task,
                    deskId: string,
                    params: MailParams): Observable<void> {
    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folder.id}/task/${task.id}/secure_mail`,
        params
      )
      .pipe(catchError(this.notificationsService.handleHttpError('performSecureMail')));
  }


  startFolder(tenantId: string, deskId: string, folderId: string, simpleTaskParams: SimpleTaskParams): Observable<{ id: string }> {
    return this.http
      .put<{ id: string }>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/draft/${folderId}`,
        simpleTaskParams,
        {
          headers: jsonHeader,
        }
      )
      .pipe(catchError(this.notificationsService.handleHttpError('startFolder')));
  }


  sendToTrashBin(tenantId: string, deskId: string, folderId: string): Observable<void> {
    return this.http
      .put<void>(
        `${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/finished/${folderId}`,
        {
          headers: jsonHeader,
          params: null
        }
      )
      .pipe(catchError(this.notificationsService.handleHttpError('sendToTrashBin')));
  }

}
