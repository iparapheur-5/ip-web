/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { NotificationsService } from './notifications.service';

@Injectable({
  providedIn: 'root'
})
export class FileService {


  constructor(private http: HttpClient,
              private notificationsService: NotificationsService) { }


  downloadFileWithObservable(observableRequest: Observable<any>, targetFilename: string): Observable<{}> {
    return observableRequest
      .pipe(catchError(this.notificationsService.handleHttpError('downloadFile')))
      .pipe(map((response: Blob) => {

          const objectUrl = URL.createObjectURL(response);

          let downloadLink = document.createElement('a');
          downloadLink.href = objectUrl;
          downloadLink.setAttribute('download', targetFilename);
          downloadLink.click();

          URL.revokeObjectURL(objectUrl);

          return of();
        })
      );
  }


  downloadFileWithUrl(url: string, targetFilename: string): Observable<{}> {
    return this.downloadFileWithObservable(
      this.http.get<Blob>(url, {responseType: 'blob' as 'json'}),
      targetFilename
    );
  }


}
