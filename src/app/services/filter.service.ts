/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { FolderFilter } from '../models/folder/folder-filter';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  // FIXME in order to be clean, we should also use userId in the key, to store a 'currentFilter' per user
  getDefaultFilter(): FolderFilter {
    const storedFilter: string = localStorage.getItem('currentFilter');
    let filter = new FolderFilter();
    if (storedFilter && storedFilter !== 'undefined') {
      filter = Object.assign(filter, JSON.parse(storedFilter));
    }
    return filter;
  }


  setDefaultFilter(folderFilter: FolderFilter): void {
    localStorage.setItem('currentFilter', JSON.stringify(folderFilter))
  }


}
