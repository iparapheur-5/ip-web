/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { ReleaseNotes, Version } from "@libriciel/ls-composants";
import { map, catchError } from "rxjs/operators";
import { CHANGELOG } from "../shared/config/changelog";
import { LICENSE } from "../shared/config/license";
import { CommonMessages } from '../shared/common-messages';

@Injectable({
  providedIn: 'root'
})
export class AboutService {

  constructor(public http: HttpClient) {
  }

  getChangelog(): Observable<ReleaseNotes[]> {
    return this.http
      .get<ReleaseNotes[]>('assets/changelog.json')
      .pipe(
        map(data => {
          CHANGELOG.splice(0, CHANGELOG.length);
          data = data.map(releaseNotes => {
              releaseNotes.version = Object.assign(new Version(), releaseNotes.version)
              return releaseNotes;
            }
          )
          Array.prototype.push.apply(CHANGELOG, data)
          return CHANGELOG;
        }),
        catchError(error => {
          console.error('unable to load changelog.json file');
          console.error(error);
          return of(CHANGELOG)
        })
      );
  }

  getLicence(): Observable<{ license: string }> {
    return this.http
      .get<{ license: string }>('assets/license.json')
      .pipe(
        map(data => {
          LICENSE.license = data.license;
          return LICENSE
        }),
        catchError(error => {
          console.error('unable to load license.json file');
          console.error(error);
          return of(LICENSE)
        })
      );
  }

  currentVersion(): Observable<Version> {
    return this.getChangelog().pipe(
      map(changelogs => {
        if (changelogs && changelogs.length) {
          const version = changelogs[0].version
          version.name = CommonMessages.APP_NAME;
          return version;
        }
        return null;
      }));
  }
}
