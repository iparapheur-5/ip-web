/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { CommonMessages } from '../shared/common-messages';
import { FolderViewBlock } from '@libriciel/iparapheur-standard';


@Pipe({
  name: 'folderViewBlockName'
})
export class FolderViewBlockNamePipe implements PipeTransform {


  public static compute(block: FolderViewBlock) {
    switch (block) {
      case FolderViewBlock.Actions: { return CommonMessages.ACTIONS; }
      case FolderViewBlock.Draft: { return 'Détails du dossier'; }
      case FolderViewBlock.Workflow: { return CommonMessages.WORKFLOW_NAME; }
      case FolderViewBlock.Metadata: { return CommonMessages.METADATA_NAME_PLURAL; }
      case FolderViewBlock.MainDocuments: { return 'Documents principaux'; }
      case FolderViewBlock.AnnexeDocuments: { return 'Documents annexes'; }
      case FolderViewBlock.PublicAnnotation: { return 'Annotations publiques'; }
      case FolderViewBlock.PrivateAnnotation: { return 'Annotations privées'; }
      default: { return null; }
    }
  }


  transform(value: any, args?: any): any {
    return FolderViewBlockNamePipe.compute(value);
  }


}
