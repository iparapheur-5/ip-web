/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { MetadataNamePipe } from './metadata-name.pipe';
import { InternalMetadata } from '@libriciel/iparapheur-standard'
import { Metadata } from '../models/metadata';
import { InternalMetadataKeyPipe } from './internal-metadata-key.pipe';


describe('MetadataNamePipe', () => {


  it('create an instance', () => {
    const pipe = new MetadataNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(InternalMetadata)
      .map(key => InternalMetadata[key])
      .map(internalMetadata => InternalMetadataKeyPipe.compute(internalMetadata))
      .map(key => {
        let metadata = new Metadata();
        metadata.key = key;
        return metadata;
      })
      .forEach(e => expect(MetadataNamePipe.compute(e)).toBeTruthy());
  });


  it('return null on invalid input', () => {
    expect(MetadataNamePipe.compute(undefined)).toBeNull();
    expect(MetadataNamePipe.compute(null)).toBeNull();
  });


});
