/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { StampType } from '@libriciel/iparapheur-standard';

@Pipe({
  name: 'stampTypeName'
})
export class StampTypeNamePipe implements PipeTransform {


  public static compute(type: StampType): string {
    switch (type) {
      case StampType.Signature: { return 'Signature'; }
      case StampType.Image: { return 'Image'; }
      case StampType.Text: { return 'Texte'; }
      case StampType.Metadata: { return 'Métadonnée'; }
      default: { return '' + type; }
    }
  }


  transform(value: StampType, ...args: unknown[]): string {
    return StampTypeNamePipe.compute(value);
  }


}
