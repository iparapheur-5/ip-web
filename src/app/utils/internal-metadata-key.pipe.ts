/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { InternalMetadata } from '@libriciel/iparapheur-standard';
import { iparapheurInternalComputedPrefix } from './string-utils';

@Pipe({
  name: 'internalMetadataKey'
})
export class InternalMetadataKeyPipe implements PipeTransform {


  public static compute(value: InternalMetadata): string {
    switch (value) {
      case InternalMetadata.FolderName: { return iparapheurInternalComputedPrefix + 'folder_name'; }
      case InternalMetadata.FolderType: { return iparapheurInternalComputedPrefix + 'folder_type'; }
      case InternalMetadata.FolderSubtype: { return iparapheurInternalComputedPrefix + 'folder_subtype'; }
      case InternalMetadata.LastVisaDesk: { return iparapheurInternalComputedPrefix + 'last_visa_desk'; }
      case InternalMetadata.LastVisaUser: { return iparapheurInternalComputedPrefix + 'last_visa_user'; }
      case InternalMetadata.LastVisaDate: { return iparapheurInternalComputedPrefix + 'last_visa_date'; }
      case InternalMetadata.LastSignatureDesk: { return iparapheurInternalComputedPrefix + 'last_signature_desk'; }
      case InternalMetadata.LastSignatureUser: { return iparapheurInternalComputedPrefix + 'last_signature_user'; }
      case InternalMetadata.LastSignatureDate: { return iparapheurInternalComputedPrefix + 'last_signature_date'; }
      case InternalMetadata.LastSignatureDelegatedBy: { return iparapheurInternalComputedPrefix + 'last_signature_delegated_by'; }
      case InternalMetadata.LastSignatureHash: { return iparapheurInternalComputedPrefix + 'last_signature_hash'; }
      default: { return null; }
    }
  }


  transform(value: InternalMetadata): string {
    return InternalMetadataKeyPipe.compute(value);
  }


}
