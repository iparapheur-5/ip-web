/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { InternalMetadata } from '@libriciel/iparapheur-standard';
import { Metadata } from '../models/metadata';
import { InternalMetadataKeyPipe } from './internal-metadata-key.pipe';


@Pipe({
  name: 'metadataName'
})
export class MetadataNamePipe implements PipeTransform {


  public static compute(value: Metadata): string {
    switch (value?.key) {
      case InternalMetadataKeyPipe.compute(InternalMetadata.FolderName): { return 'Nom du dossier'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.FolderType): { return 'Type du dossier'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.FolderSubtype): { return 'Sous-type du dossier'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastVisaDesk): { return 'Bureau du dernier validateur'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastVisaUser): { return 'Nom du dernier validateur'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastVisaDate): { return 'Date de la dernière validation'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastSignatureDesk): { return 'Bureau du dernier signataire'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastSignatureUser): { return 'Nom du dernier signataire'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastSignatureDate): { return 'Date de la dernière signature'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastSignatureDelegatedBy): { return 'Bureau absent en cas de suppléance'; }
      case InternalMetadataKeyPipe.compute(InternalMetadata.LastSignatureHash): { return 'Empreinte SHA256 de la dernière signature'; }
      default: { return value?.name ?? null; }
    }
  }


  transform(value: Metadata): string {
    return MetadataNamePipe.compute(value);
  }


}
