/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { StampTypeIconPipe } from './stamp-type-icon.pipe';
import { faFont } from '@fortawesome/free-solid-svg-icons';
import { StampType } from '@libriciel/iparapheur-standard';

describe('StampTypeIconPipe', () => {


  it('create an instance', () => {
    const pipe = new StampTypeIconPipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(StampType)
      .forEach(e => expect(StampTypeIconPipe.compute(e)).toBeTruthy());
  });


  it('return default value on invalid input', () => {
    expect(StampTypeIconPipe.compute('not-a-valid-intpur')).toBe(faFont);
    expect(StampTypeIconPipe.compute(undefined)).toBe(faFont);
    expect(StampTypeIconPipe.compute(null)).toBe(faFont);
  });


});
