/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { HasId } from '../models/has-id';
import { User } from '../models/auth/user';


// https://www.w3.org/TR/html5/forms.html#valid-e-mail-address
export const emailRfc5322Regex = /^[a-zA-Z\d.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?(?:\.[a-zA-Z\d](?:[a-zA-Z\d-]{0,61}[a-zA-Z\d])?)*$/;
export const integerRegex = /^[-+]?\d+$/;
export const floatRegex = /^[-+]?\d+([.,]\d+)?$/;
export const urlRfc3986Regex = /^[a-z]*:\/\/(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\\d{1,2}|2[0-4]\d|25[0-5])){2}\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4])|(?:[a-z\u00a1-\uffff\d]+-?)*[a-z\u00a1-\uffff\d]+(?:\.(?:[a-z\u00a1-\uffff\d]+-?)*[a-z\u00a1-\uffff\d]+)*\.[a-z\u00a1-\uffff]{2,})(?::\d{2,5})?(?:\/\S*)?$/;

export const iparapheurInternalPrefix = 'i_Parapheur_internal_';
export const iparapheurInternalComputedPrefix = iparapheurInternalPrefix + 'computed_';


export const MIME_TYPES_DOCS = 'application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, ' +
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-powerpoint, ' +
  'application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.oasis.opendocument.text, ' +
  'application/vnd.oasis.opendocument.presentation, application/vnd.oasis.opendocument.spreadsheet, application/rtf'
export const MIME_TYPES_IMAGES = 'image/jpeg, application/jpg, application/png, image/png'
export const MIME_TYPES_PRINCIPAL_ACTES = 'application/pdf, text/pdf, ' + MIME_TYPES_DOCS;
export const MIME_TYPES_PRINCIPAL_HELIOS = 'application/xml, text/xml';
export const MIME_TYPES_PRINCIPAL_DEFAULT = MIME_TYPES_PRINCIPAL_ACTES + ', ' + MIME_TYPES_PRINCIPAL_HELIOS + ', ' + MIME_TYPES_IMAGES + ', ' + MIME_TYPES_DOCS;
export const MIME_TYPES_ANNEXES_ACTES = MIME_TYPES_PRINCIPAL_ACTES + ', ' + MIME_TYPES_IMAGES;
export const MIME_TYPES_ANNEXES_DEFAULT = MIME_TYPES_ANNEXES_ACTES + ', ' + MIME_TYPES_DOCS + ', application/zip';
export const MIME_TYPES_DETACHED_SIGNATURES = 'application/pkcs7-signature, application/xml, text/xml';

export function stringifyNamedElement(element: { name: string }): string {
  return element.name;
}


export function stringifyUser(user: User): string {
  return user.firstName + ' ' + user.lastName + ' (' + user.userName + ')';
}


export function isRfc5322ValidMail(test: string) {

  if (!test) {
    return false;
  }

  let regexp = new RegExp(emailRfc5322Regex);
  return test.match(regexp);
}


export function compareById(o1: any, o2: any) {
  return (!!o1 === !!o2) && !!o1.id && !!o2.id && (o1.id === o2.id);
}


/**
 * Either the first validated error, returned by SpringBoot validator,
 * or the default message.
 *
 * TODO: There is probably a SpringBoot-related lib doing this, in a standardized way.
 * @param error
 */
export function getFirstErrorMessage(error: any): string {

  if (error.errors?.length > 0) {
    const {defaultMessage} = error.errors[0];
    if (!!defaultMessage) {
      return defaultMessage;
    }
  }

  return error.message;
}


export function trackByIndex(index: number, _: any): number {
  return index;
}


export function isNullOrUndefined(o: any): boolean {
  return (o === null) || (o === undefined);
}


export function isNotNullOrUndefined(o: any): boolean {
  return (o !== null) && (o !== undefined);
}


export function isNotEmpty(s: string): boolean {
  return isNotNullOrUndefined(s) && (s.length > 0);
}

export function isEmpty(s: string): boolean {
  return isNullOrUndefined(s) || (s.length == 0);
}


export function getOrDefault(map: Map<string, any>, key: string, defaultValue: any): any {
  const mapValue = map[key];
  return isNotNullOrUndefined(mapValue) ? mapValue : defaultValue;
}
