/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { SignatureFormat } from '@libriciel/iparapheur-standard';

@Pipe({
  name: 'signingFormatName'
})
export class SigningFormatNamePipe implements PipeTransform {


  public static compute(value: SignatureFormat): string {
    switch (value) {
      case SignatureFormat.Auto: { return 'Automatique'; }
      case SignatureFormat.Pkcs7: { return 'CAdES'; }
      case SignatureFormat.Pades: { return 'PAdES'; }
      case SignatureFormat.XadesDetached: { return 'XAdES détaché'; }
      case SignatureFormat.PesV2: { return 'XAdES enveloppé (pour PESv2)'; }
      default: { return value; }
    }
  }


  transform(value: SignatureFormat): string {
    return SigningFormatNamePipe.compute(value);
  }


}
