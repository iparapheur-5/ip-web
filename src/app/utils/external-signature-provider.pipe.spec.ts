/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ExternalSignatureProviderPipe } from './external-signature-provider.pipe';
import { ExternalSignatureProvider } from '@libriciel/iparapheur-standard';

describe('ExternalSignatureProviderPipe', () => {
  it('create an instance', () => {
    const pipe = new ExternalSignatureProviderPipe();
    expect(pipe).toBeTruthy();
  });

  it('return not null on valid input', () => {
    Object.keys(ExternalSignatureProvider)
      .map(k => ExternalSignatureProvider[k])
      .forEach(p => expect(ExternalSignatureProviderPipe.compute(p)).toBeTruthy());
  });


  it('return null on invalid input', () => {
    expect(ExternalSignatureProviderPipe.compute(undefined)).toBeNull();
    expect(ExternalSignatureProviderPipe.compute(null)).toBeNull();
  });
});
