/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ExternalStatePipe } from './external-signature-state.pipe';
import { ExternalState } from '../models/external-state.enum';

describe('ExternalSignatureStatePipe', () => {


  it('create an instance', () => {
    const pipe = new ExternalStatePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(ExternalState)
      .forEach(e => expect(ExternalStatePipe.compute(e)).toBeTruthy());
  });


  it('return some default value on invalid input', () => {
    expect(ExternalStatePipe.compute('not_a_valid_input')).toBeTruthy();
    expect(ExternalStatePipe.compute(undefined)).toBeTruthy();
    expect(ExternalStatePipe.compute(null)).toBeTruthy();
  });


});
