/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { DocumentIconPipe } from './document-icon.pipe';
import { faFileAlt, faFileCode } from '@fortawesome/free-regular-svg-icons';

describe('DocumentIconPipe', () => {


  it('create an instance', () => {
    const pipe = new DocumentIconPipe();
    expect(pipe).toBeTruthy();
  });


  it('return something on valid input', () => {
    expect(DocumentIconPipe.compute('application/xml')).toBe(faFileCode);
  });


  it('return the default value on invalid input', () => {
    expect(DocumentIconPipe.compute('no-a-valid-file')).toBe(faFileAlt);
    expect(DocumentIconPipe.compute(undefined)).toBe(faFileAlt);
    expect(DocumentIconPipe.compute(null)).toBe(faFileAlt);
  });


});
