/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ColumnNamePipe } from './column-name.pipe';
import { TaskViewColumn, ArchiveViewColumn } from '@libriciel/iparapheur-standard';

describe('ColumnNamePipe', () => {


  it('create an instance', () => {
    const pipe = new ColumnNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(TaskViewColumn)
      .map(k => TaskViewColumn[k])
      .forEach(e => expect(ColumnNamePipe.compute(e)).toBeTruthy());

    Object
      .keys(ArchiveViewColumn)
      .map(k => ArchiveViewColumn[k])
      .forEach(e => expect(ColumnNamePipe.compute(e)).toBeTruthy());
  });


  it('return null on invalid input', () => {
    expect(ColumnNamePipe.compute('not_a_valid_input')).toBeNull();
    expect(ColumnNamePipe.compute(undefined)).toBeNull();
    expect(ColumnNamePipe.compute(null)).toBeNull();
  });


});
