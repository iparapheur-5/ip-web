/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { PrivilegeIconPipe } from './privilege-icon.pipe';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { UserPrivilege } from '@libriciel/iparapheur-standard';


describe('PrivilegeIconPipe', () => {


  it('create an instance', () => {
    const pipe = new PrivilegeIconPipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(UserPrivilege)
      .map(k => UserPrivilege[k])
      .forEach(p => expect(PrivilegeIconPipe.compute(p)).toBeTruthy());
  });


  it('return default value on invalid input', () => {
    expect(PrivilegeIconPipe.compute(undefined)).toBe(faUserAlt);
    expect(PrivilegeIconPipe.compute(null)).toBe(faUserAlt);
  });


});
