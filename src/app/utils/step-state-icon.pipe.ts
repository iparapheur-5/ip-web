/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { faTimes, faShare, IconDefinition, faArrowRight, faCheck, faRetweet, faForward } from '@fortawesome/free-solid-svg-icons';
import { State } from '@libriciel/iparapheur-standard';
import { StepState } from '@libriciel/ls-composants/workflows';

@Pipe({
  name: 'stepStateIcon'
})
export class StepStateIconPipe implements PipeTransform {


  public static compute(state: any): IconDefinition {
    switch (state) {
      case State.Bypassed: { return faForward; } // TODO : validate this icon
      case State.Pending:
      case StepState.CURRENT:
      case State.Current: { return faArrowRight; }
      case State.Seconded: { return faRetweet; }
      case State.Transferred: { return faShare; }
      case StepState.REJECTED:
      case State.Rejected: { return faTimes; }
      case StepState.VALIDATED:
      case State.Validated: { return faCheck; }
      default: { return null; }
    }
  }


  transform(value: any): IconDefinition {
    return StepStateIconPipe.compute(value);
  }


}
