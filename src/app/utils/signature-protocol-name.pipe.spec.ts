/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { SignatureProtocolNamePipe } from './signature-protocol-name.pipe';
import { SignatureProtocol } from '@libriciel/iparapheur-standard';


describe('SignatureProtocolNamePipe', () => {


  it('create an instance', () => {
    const pipe = new SignatureProtocolNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(SignatureProtocol)
      .map(k => SignatureProtocol[k])
      .filter(p => p != SignatureProtocol.None)
      .forEach(e => {
        expect(SignatureProtocolNamePipe.compute(e)).toBeTruthy();
        expect(SignatureProtocolNamePipe.compute(e)).not.toBe(SignatureProtocolNamePipe.DEFAULT_VALUE);
      });
  });


  it('return given value empty string on invalid input', () => {
    expect(SignatureProtocolNamePipe.compute(SignatureProtocol.None)).toBe(SignatureProtocolNamePipe.DEFAULT_VALUE);
    expect(SignatureProtocolNamePipe.compute(undefined)).toBeUndefined();
    expect(SignatureProtocolNamePipe.compute(null)).toBeNull();
  });


});
