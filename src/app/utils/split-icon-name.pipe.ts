/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { IconLookup, IconName, IconPrefix } from '@fortawesome/fontawesome-svg-core';


// export type FaIcon = {prefix: string, name: string};

@Pipe({
  name: 'splitIconName'
})
export class SplitIconNamePipe implements PipeTransform {


  public static compute(value: string): IconLookup {
    let prefix: string;
    let name: string;
    let prefixLength = 3;

    if (value.startsWith('fa ')) {
      prefixLength = 2;
      prefix = 'fas';
    } else {
      prefix = value.substr(0, prefixLength);
    }

    name = value.substr(prefixLength + ' fa-'.length);

    return {prefix: prefix as IconPrefix, iconName: name as IconName};
  }


  transform(value: string, ...args: unknown[]): IconLookup {
    return SplitIconNamePipe.compute(value);
  }


}
