/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { FolderSortBy } from '@libriciel/iparapheur-standard';

@Pipe({
  name: 'folderSortByPipe'
})
export class FolderSortByPipe implements PipeTransform {


  public static compute(sortBy: FolderSortBy) {
    switch (sortBy) {
      case FolderSortBy.ActionType: { return `Type d'action`; }
      case FolderSortBy.CreationDate: { return 'Date de création'; }
      case FolderSortBy.EndDate: { return 'Date de fin'; }
      case FolderSortBy.FolderId: { return 'Identifiant de dossier'; }
      case FolderSortBy.FolderName: { return 'Nom de dossier'; }
      case FolderSortBy.LateDate: { return 'Retard'; }
      case FolderSortBy.StillSinceDate: { return `Temps d'inactivité`; }
      case FolderSortBy.TaskId: { return 'Identifiant de tâche'; }
      case FolderSortBy.TypeId: { return 'Identifiant de type'; }
      case FolderSortBy.TypeName: { return 'Nom de type'; }
      case FolderSortBy.SubtypeId: { return 'Identifiant de sous-type'; }
      case FolderSortBy.SubtypeName: { return 'Nom de sous-type'; }
      case FolderSortBy.ValidationStartDate: { return 'Date de début'; }
      default: { return null; }
    }
  }


  transform(value: any, args?: any): any {
    return FolderSortByPipe.compute(value);
  }


}
