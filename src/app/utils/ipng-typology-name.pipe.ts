/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { CommonMessages } from '../shared/common-messages';
import { IpngType } from '../models/ipng/ipng-type';

@Pipe({
  name: 'ipngTypologyName'
})
export class IpngTypologyNamePipe implements PipeTransform {

  static getPreferredNameFoTypology(value: IpngType): string {
    if (!value) {
      return CommonMessages.NO_ELEMENT;
    }

    if (value.name) {

      // TODO have a real language selection logic

      if (value.name['fr']) {
        return value.name['fr'];
      }

      if (value.name['fr_FR']) {
        return value.name['fr_FR'];
      }
    }

    return value.key;
  }

  transform(value: IpngType, ...args: unknown[]): unknown {
    return IpngTypologyNamePipe.getPreferredNameFoTypology(value);
  }

}
