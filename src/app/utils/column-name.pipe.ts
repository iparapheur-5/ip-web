/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { CommonMessages } from '../shared/common-messages';
import { TaskViewColumn, ArchiveViewColumn } from '@libriciel/iparapheur-standard';

@Pipe({
  name: 'columnNamePipe',
})
export class ColumnNamePipe implements PipeTransform {


  public static compute(column: any) {
    switch (column) {
      case TaskViewColumn.CurrentDesk: { return 'Bureau courant'; }
      case TaskViewColumn.OriginDesk: { return 'Émetteur'; }
      case TaskViewColumn.LimitDate: { return 'Date limite'; }
      case TaskViewColumn.State: { return CommonMessages.STATE; }
      case TaskViewColumn.Subtype: { return CommonMessages.SUBTYPE; }
      case TaskViewColumn.TaskId: { return 'ID de la tâche'; }
      case TaskViewColumn.Type: { return CommonMessages.TYPE; }

      case ArchiveViewColumn.OriginDesk: { return 'Demandée par'; }
      case ArchiveViewColumn.TypeSubtype: { return 'Type / Sous-type'; }
      case ArchiveViewColumn.Actions: { return CommonMessages.ACTIONS; }

      case TaskViewColumn.FolderName:
      case ArchiveViewColumn.Name: { return CommonMessages.NAME; }
      case TaskViewColumn.FolderId:
      case ArchiveViewColumn.Id: { return 'ID du dossier'; }
      case TaskViewColumn.CreationDate:
      case ArchiveViewColumn.CreationDate: { return 'Création'; }
      default: { return null; }
    }
  }


  transform(value: any, args?: any): any {
    return ColumnNamePipe.compute(value);
  }


}
