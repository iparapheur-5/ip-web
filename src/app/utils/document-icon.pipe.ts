/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { faFilePdf, faFileCode, faFileArchive, faFileImage, faFileAlt, faFileWord, faFileExcel } from '@fortawesome/free-regular-svg-icons';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Pipe({
  name: 'documentIcon'
})
export class DocumentIconPipe implements PipeTransform {


  public static compute(mediaType: string): IconDefinition {
    switch (mediaType) {
      case 'text/xml':
      case 'application/xml': { return faFileCode; }
      case 'text/pdf':
      case 'application/pdf': { return faFilePdf; }
      case 'application/zip': { return faFileArchive; }
      case 'image/jpeg':
      case 'image/png': { return faFileImage; }
      case 'application/vnd.oasis.opendocument.text':
      case 'application/msword':
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': { return faFileWord; }
      case 'application/vnd.oasis.opendocument.spreadsheet':
      case 'application/vnd.ms-excel':
      case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { return faFileExcel; }
      default: { return faFileAlt; }
    }
  }


  transform(value: string, ...args: unknown[]): IconDefinition {
    return DocumentIconPipe.compute(value);
  }


}
