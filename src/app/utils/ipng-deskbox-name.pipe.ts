/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { Deskbox } from '../models/ipng/deskbox';
import { CommonMessages } from '../shared/common-messages';

@Pipe({
  name: 'ipngDeskboxName'
})
export class IpngDeskboxNamePipe implements PipeTransform {

  static getPreferredNameForDeskbox(value: Deskbox): string {
    if (!value) {
      return CommonMessages.NO_ELEMENT;
    }

    if (value.title) {

      // TODO have a real language selection logic

      if (value.title['fr']) {
        return value.title['fr'];
      }

      if (value.title['fr_FR']) {
        return value.title['fr_FR'];
      }
    }

    return value.id;
  }

  transform(value: Deskbox, ...args: unknown[]): unknown {
    return IpngDeskboxNamePipe.getPreferredNameForDeskbox(value);
  }

}
