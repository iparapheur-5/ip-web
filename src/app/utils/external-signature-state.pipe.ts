/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ExternalState'
})
export class ExternalStatePipe implements PipeTransform {


  public static compute(state: string) {
    switch (state) {
      case 'FORM': { return 'Formulaire'; }
      case 'ACTIVE': { return 'En attente'; }
      case 'SIGNED': { return 'Signé'; }
      case 'REFUSED': { return 'Refusé'; }
      case 'EXPIRED': { return 'Expiré'; }
      case 'CREATED': { return 'Créé'; }
      case 'IN_REDACTION': { return 'En cours de rédaction'; }
      case 'DELETED': { return 'Supprimé'; }
      case 'SENT': { return 'Envoyé'; }
      case 'SENT_AGAIN': { return 'Renvoyé'; }
      case 'RECEIVED_PARTIALLY': { return 'Reçu partiellement'; }
      case 'RECEIVED': { return 'Reçu'; }
      case 'NOT_RECEIVED': { return 'Non reçu'; }
      case 'ERROR': { return 'En erreur'; }
      default: { return 'État inconnu'; }
    }
  }


  transform(value: any, args?: any): any {
    return ExternalStatePipe.compute(value);
  }


}
