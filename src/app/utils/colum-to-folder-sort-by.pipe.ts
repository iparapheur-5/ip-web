/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { FolderSortBy, TaskViewColumn } from '@libriciel/iparapheur-standard';

@Pipe({
  name: 'columToFolderSortByPipe'
})
export class ColumToFolderSortByPipe implements PipeTransform {


  public static compute(column: TaskViewColumn): FolderSortBy {
    switch (column) {
      case TaskViewColumn.LimitDate:
        return FolderSortBy.LateDate;
      case TaskViewColumn.Subtype:
        return FolderSortBy.SubtypeName;
      case TaskViewColumn.TaskId:
        return FolderSortBy.TaskId;
      case TaskViewColumn.Type:
        return FolderSortBy.TypeName;
      case TaskViewColumn.FolderName:
        return FolderSortBy.FolderName;
      case TaskViewColumn.FolderId:
        return FolderSortBy.FolderId;
      case TaskViewColumn.CreationDate:
        return FolderSortBy.CreationDate;
      default:
        return FolderSortBy.FolderName
    }
  }


  transform(value: TaskViewColumn, ...args: unknown[]): unknown {
    return ColumToFolderSortByPipe.compute(value);
  }


}
