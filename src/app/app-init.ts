/*
 * iparapheur Web
 * Copyright (C) 2019-2023 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { KeycloakService } from 'keycloak-angular';
import { CONFIG } from './shared/config/config';
import { HttpClient } from '@angular/common/http';
import { KeycloakConfig } from 'keycloak-js';
import { Config } from './config';
import { GlobalSettings, GLOBAL_SETTINGS, GlobalSettingsAdmin, GlobalSettingsUser, GlobalSettingsLiberSign } from './shared/models/global-settings';
import { isNullOrUndefined } from './utils/string-utils';
import { FolderVisibility } from '@libriciel/iparapheur-standard';


export function parseVisibility(visibility: string): FolderVisibility {
  if (isNullOrUndefined(visibility) || visibility === '') {
    return FolderVisibility.Confidential;
  }
  if (visibility === "confidentiel") {
    visibility = "confidential";
  }

  return visibility.toUpperCase() as FolderVisibility;
}


/**
 * Caution, this has to remain an exportable function.
 *
 * If the parameter is inlined in a lambda,
 * the `ng build` method will replace it with static values in the generated dist.
 *
 * @see https://stackoverflow.com/questions/42051200/angular2-build-issue-usefactory-with-function
 */
export function getApplicationUrl(): string {
  return CONFIG.APPLICATION_URL;
}


/**
 * Angular's global initialisation.
 */
export function initializer(keycloak: KeycloakService, http: HttpClient): () => Promise<any> {

  return (): Promise<any> =>
    new Promise(async (resolve) => {

      // Global settings initialization.

      http
        .get<GlobalSettings>('assets/settings.json')
        .subscribe(
          data => {
            // Manually set params, or default ones
            GLOBAL_SETTINGS.admin = data.admin ?? new GlobalSettingsAdmin();
            GLOBAL_SETTINGS.admin.connectedUsersThreshold = data?.admin?.connectedUsersThreshold ?? 10;
            GLOBAL_SETTINGS.admin.showTemplates = data?.admin?.showTemplates ?? true;
            GLOBAL_SETTINGS.admin.enableFortify = data?.admin?.enableFortify ?? false;
            GLOBAL_SETTINGS.admin.showCustomSignatureField = data?.admin?.showCustomSignatureField ?? false;

            GLOBAL_SETTINGS.user = data.user ?? new GlobalSettingsUser();
            GLOBAL_SETTINGS.user.folderDefaultVisibility = parseVisibility(data?.user?.folderDefaultVisibility);
            GLOBAL_SETTINGS.user.folderVisibility = data?.user?.folderVisibility ?? [];
            GLOBAL_SETTINGS.user.showPassword = data?.user?.showPassword ?? true;
            GLOBAL_SETTINGS.user.showSignature = data?.user?.showPassword ?? true;

            GLOBAL_SETTINGS.libersign = data.libersign ?? new GlobalSettingsLiberSign();
            GLOBAL_SETTINGS.libersign.firefoxExtensionUrl = data?.libersign?.firefoxExtensionUrl ?? 'https://libersign.libriciel.fr/libersign-2.0.5-an+fx.xpi';
            GLOBAL_SETTINGS.libersign.chromeExtensionUrl = data?.libersign?.chromeExtensionUrl ?? 'https://chrome.google.com/webstore/detail/jligpldajocilccnnokfnghlamfhnppc';
            GLOBAL_SETTINGS.libersign.nativeApplicationUrl = data?.libersign?.nativeApplicationUrl ?? 'https://libersign.libriciel.fr/libersign.exe';
          },
          error => console.error('unable to load settings.json file', error.message)
        );

      // Angular's initialisation integration.
      // Taken from https://www.npmjs.com/package/keycloak-angular

      http
        .get<any>('assets/config.json')
        .subscribe(
          config => {
            CONFIG.APPLICATION_URL = config.APPLICATION_URL;
            CONFIG.LIBERSIGN_UPDATE_URL = config.APPLICATION_URL + '/libersign/';
            CONFIG.VERSION = config.VERSION;
            CONFIG.KEYCLOAK_URL = config.KEYCLOAK_URL;
            CONFIG.KEYCLOAK_CLIENT_ID = config.KEYCLOAK_CLIENT_ID;
            CONFIG.KEYCLOAK_REALM = config.KEYCLOAK_REALM;
            CONFIG.BASE_API_URL = `${config.APPLICATION_URL}/api/${Config.API_VERSION}`;

            const keycloakConfig: KeycloakConfig = {
              url: CONFIG.KEYCLOAK_URL,
              clientId: CONFIG.KEYCLOAK_CLIENT_ID,
              realm: CONFIG.KEYCLOAK_REALM
            };

            keycloak
              .init({
                config: keycloakConfig,
                initOptions: {onLoad: 'login-required'},
                bearerExcludedUrls: ['/assets']  // path not protected by keycloak
              })
              .then(result => {
                resolve(result);
              });
          }
        );
    });

}

