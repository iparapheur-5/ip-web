#!/bin/sh

#
# iparapheur Web
# Copyright (C) 2019-2023 Libriciel SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

BASEDIR=$(dirname "$0")
TEMPLATE_FILE=setenv-template.json
DESTINATION_FILE=config.json
DESTINATION_PATH=$1
if [ ! "$DESTINATION_PATH" ]; then
    DESTINATION_PATH=$BASEDIR/../assets
fi

cp "$BASEDIR/$TEMPLATE_FILE" "$BASEDIR/$DESTINATION_FILE"

inject_variable() {
  name="$1"
  default_value="$2"
  value=$(eval echo "\$$name")
  if [ ! "$value" ]; then
    value=$default_value
  fi
  sed -i s,'\${'"${name}"'}',"${value}",g "$BASEDIR/$DESTINATION_FILE"
}

inject_variable APPLICATION_URL 'http://iparapheur.dom.local'
inject_variable VERSION '0.3.0-dev'
inject_variable KEYCLOAK_URL 'http://iparapheur.dom.local/auth'
inject_variable KEYCLOAK_REALM 'api'
inject_variable KEYCLOAK_CLIENT_ID 'ipcore-web'
inject_variable LIBERSIGN_FIREFOX_EXTENSION_URL 'https://libersign.libriciel.fr/libersign-2.0.5-an+fx.xpi'
inject_variable LIBERSIGN_CHROME_EXTENSION_URL 'https://chrome.google.com/webstore/detail/jligpldajocilccnnokfnghlamfhnppc'
inject_variable LIBERSIGN_NATIVE_APPLICATION_URL 'https://libersign.libriciel.fr/libersign.exe'

mv "$BASEDIR/$DESTINATION_FILE" "$DESTINATION_PATH"
